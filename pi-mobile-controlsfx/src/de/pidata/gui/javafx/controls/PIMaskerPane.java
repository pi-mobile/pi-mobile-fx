package de.pidata.gui.javafx.controls;

import javafx.scene.control.Skin;
import org.controlsfx.control.MaskerPane;

public class PIMaskerPane extends MaskerPane {

  @Override protected Skin<?> createDefaultSkin() { return new PIMaskerPaneSkin(this); }

}
