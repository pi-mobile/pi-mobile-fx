package de.pidata.gui.javafx.controls;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.MaskerPane;

public class PIMaskerPaneSkin extends SkinBase<MaskerPane> {

  public PIMaskerPaneSkin( MaskerPane maskerPane) {
    super(maskerPane);
    getChildren().add(createMasker(maskerPane));
  }

  private StackPane createMasker( MaskerPane maskerPane) {
    VBox vBox = new VBox();
    vBox.setAlignment( Pos.CENTER);
    vBox.setSpacing(10.0);
    vBox.getStyleClass().add("masker-center"); //$NON-NLS-1$

    HBox hBox_label = new HBox();
    hBox_label.setAlignment(Pos.CENTER);
    hBox_label.getChildren().add(createLabel());

    HBox hBox_progress = new HBox();
    hBox_progress.setAlignment(Pos.CENTER);
    hBox_progress.getChildren().add(createProgressIndicator());

    vBox.getChildren().add(hBox_label);
    vBox.getChildren().add(hBox_progress);
    vBox.setFillWidth( true );
    HBox.setHgrow( vBox, Priority.ALWAYS );

    HBox hBox = new HBox();
    hBox.setPadding(new Insets(0, 20, 0, 20));
    hBox.setAlignment(Pos.CENTER);
    hBox.getChildren().addAll(vBox);

    StackPane glass = new StackPane();
    glass.setAlignment(Pos.CENTER);
    glass.getStyleClass().add("masker-glass"); //$NON-NLS-1$
    glass.getChildren().add(hBox);

    return glass;
  }

  private Label createLabel() {
    Label text = new Label();
    text.textProperty().bind(getSkinnable().textProperty());
    text.getStyleClass().add("masker-text"); //$NON-NLS-1$
    return text;
  }

  private Label createProgressIndicator() {
    Label graphic = new Label();
    graphic.setGraphic(getSkinnable().getProgressNode());
    graphic.visibleProperty().bind(getSkinnable().progressVisibleProperty());
    graphic.getStyleClass().add("masker-graphic"); //$NON-NLS-1$
    return graphic;
  }
}