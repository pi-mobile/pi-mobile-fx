package de.pidata.gui.javafx.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;
import org.controlsfx.control.MaskerPane;
import org.controlsfx.control.NotificationPane;

public class ProgressPane extends StackPane {

  @FXML
  private PIMaskerPane progressMaskerPane;
  @FXML
  private NotificationPane notificationPane;

  public ProgressPane() {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader( getClass().getResource( "/layout/progressPane.fxml" ) );
      fxmlLoader.setRoot( this );
      fxmlLoader.setController( this );
      fxmlLoader.load();
    }
    catch ( Exception exception) {
      throw new RuntimeException( exception );
    }
  }

  public MaskerPane getMaskerPane(){
    return progressMaskerPane;
  }

  public NotificationPane getNotificationPane() { return notificationPane; }

  public void show( boolean busy ) {
    if (busy) {
      progressMaskerPane.setProgress( -1 );
      progressMaskerPane.setText( "Please wait..." );
      progressMaskerPane.setStyle( "-fx-accent: deepskyblue" );
      this.setMouseTransparent( false );
    }
    else {
      if (!notificationPane.isShowing()) {
        this.setMouseTransparent( true );
      }
    }
    progressMaskerPane.setVisible( busy );
  }
}
