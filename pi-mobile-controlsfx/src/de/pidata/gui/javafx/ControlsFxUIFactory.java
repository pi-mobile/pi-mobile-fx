package de.pidata.gui.javafx;


import de.pidata.gui.javafx.controls.ProgressInfo;
import de.pidata.gui.javafx.controls.ProgressPane;
import de.pidata.gui.javafx.ui.*;
import de.pidata.gui.ui.base.*;
import de.pidata.gui.view.base.*;
import de.pidata.models.tree.Model;
import javafx.scene.Node;
import javafx.scene.control.*;
import org.controlsfx.control.BreadCrumbBar;
import org.controlsfx.control.SearchableComboBox;
import org.controlsfx.control.SegmentedButton;
import org.controlsfx.control.ToggleSwitch;
import org.controlsfx.control.tableview2.FilteredTableView;

public class ControlsFxUIFactory extends FXUIFactory {

  @Override
  public UIProgressBarAdapter createProgressAdapter( UIContainer uiContainer, ProgressBarViewPI progressBarViewPI ) {
    Node fxNode = findUIComp( uiContainer, progressBarViewPI.getComponentID(), progressBarViewPI.getModuleID() );

    if (fxNode == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+progressBarViewPI.getComponentID() );
    }

    if (fxNode instanceof ProgressInfo) {
      return new FXProgressBarAdapter( (ProgressInfo) fxNode, progressBarViewPI, uiContainer );
    }

    if (fxNode instanceof ProgressPane) {
      return new FXProgressPaneAdapter( (ProgressPane) fxNode, progressBarViewPI, uiContainer );
    }

    if (fxNode instanceof Spinner) {
      return new FXSpinnerAdapter( (Spinner) fxNode, progressBarViewPI, uiContainer );
    }
    if (fxNode instanceof Slider) {
      return new FXSliderAdapter( (Slider) fxNode, progressBarViewPI, uiContainer );
    }

    throw new IllegalArgumentException( "Element is not type of UIProgressBarAdapter, ID="+progressBarViewPI.getComponentID() );
  }

  @Override
  public UIListAdapter createListAdapter( UIContainer uiContainer, ListViewPI listViewPI ) {
    Node fxListView = findUIComp( uiContainer, listViewPI.getComponentID(), listViewPI.getModuleID() );
    if (fxListView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+listViewPI.getComponentID() );
    }
    if (fxListView instanceof ListView) {
      return new FXListAdapter( (ListView) fxListView, listViewPI, uiContainer );
    }
    else if (fxListView instanceof SegmentedButton){
      return new FXSegmentedButtonAdapter( (SegmentedButton) fxListView, listViewPI, uiContainer );
    }
    else if (fxListView instanceof SearchableComboBox){
      return new FXSearchableComboboxAdapter( (SearchableComboBox) fxListView, listViewPI, uiContainer );
    }
    else if (fxListView instanceof ChoiceBox) {
      return new FXChoiceAdapter( (ChoiceBox) fxListView, listViewPI, uiContainer );
    }

    throw new IllegalArgumentException( "Element is not type of UIListAdapter, ID="+listViewPI.getComponentID() );
  }

  @Override
  public UITableAdapter createTableAdapter( UIContainer uiContainer, TableViewPI tableViewPI ) {
    TableView fxTableView = (TableView) findUIComp( uiContainer, tableViewPI.getComponentID(), tableViewPI.getModuleID() );

    if (fxTableView instanceof FilteredTableView) {
      return new FilteredFXTableAdapter( (FilteredTableView) fxTableView, tableViewPI, this, uiContainer );
    }
    else if (fxTableView instanceof TableView) {
      return new ControlsFXTableAdapter( fxTableView, tableViewPI, this, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Could not find UI component, ID="+tableViewPI.getComponentID() );
    }

  }


  @Override
  public UITreeAdapter createTreeAdapter( UIContainer uiContainer, TreeViewPI treeViewPI ) {
    Node fxTreeView = findUIComp( uiContainer, treeViewPI.getComponentID(), treeViewPI.getModuleID() );
    if (fxTreeView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+treeViewPI.getComponentID() );
    }
    if (fxTreeView instanceof BreadCrumbBar) {
      return new FXBreadCrumbBarAdapter( (BreadCrumbBar) fxTreeView, treeViewPI, uiContainer );
    }
    else {
      return super.createTreeAdapter( uiContainer, treeViewPI );
    }
  }

  @Override
  public UIFlagAdapter createFlagAdapter( UIContainer uiContainer, FlagViewPI flagViewPI ) {
    Node fxFlagView = findUIComp( uiContainer, flagViewPI.getComponentID(), flagViewPI.getModuleID() );
    if (fxFlagView instanceof ToggleSwitch) {
      return new ControlsFXFlagAdapter( (ToggleSwitch)fxFlagView, flagViewPI, uiContainer );
    }
    else {
      return super.createFlagAdapter( uiContainer, flagViewPI );
    }
  }
}
