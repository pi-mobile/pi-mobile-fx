package de.pidata.gui.javafx;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.controls.ProgressPane;
import de.pidata.qnames.QName;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.MaskerPane;
import org.controlsfx.control.Notifications;


public class ControlsFxDialog extends FXDialog {

  public ControlsFxDialog( QName dialogID, Stage stage ) {
    super( dialogID, stage );
  }

  /**
   * Show/hide busy animation if naksker pane exists (getScene().lookup("#maskerPane")).
   * Otherwise switch between busy cursor (true) and normal cursor (false).
   *
   * @param busy if true show busy, otherwise hide
   */
  @Override
  public void showBusy( boolean busy ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {

        MaskerPane maskerPane = (MaskerPane) getScene().lookup( "#maskerPane" );
        ProgressPane progessPane = (ProgressPane) getScene().lookup( "#progressPane" );

        if (maskerPane != null) {
          maskerPane.setVisible( busy );
        }
        else if (progessPane != null) {
          progessPane.show(busy);
        }
        else {
          getScene().setCursor( busy ? Cursor.WAIT : Cursor.DEFAULT );
        }
      }
    });
  }

  /**
   * returns new FXDialog Object of given id on given stage and given handle
   *
   * @param dialogID   of the new Dialog
   * @param childStage of the new Dialog
   * @return
   */
  @Override
  protected FXDialog createChildDialog( QName dialogID, Stage childStage ) {
    return new ControlsFxDialog( dialogID, childStage );
  }

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  @Override
  public void showToast( String message ) {
    Platform.getInstance().runOnUiThread( () -> {

      ProgressPane progessPane = (ProgressPane) getScene().lookup( "#progressPane" );
      if (progessPane != null) {
        progessPane.getNotificationPane().show(message);
      }
      else {
        Notifications notifications = Notifications.create();
        notifications.hideAfter( Duration.seconds( 10 ) );
        notifications.position( Pos.CENTER );
        notifications.text( message );
        notifications.showInformation();
      }
    } );
  }
}
