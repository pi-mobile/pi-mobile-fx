package de.pidata.gui.javafx.ui;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.FlagViewPI;
import org.controlsfx.control.ToggleSwitch;

public class ControlsFXFlagAdapter extends FXFlagAdapter {

  private ToggleSwitch fxToggleSwitch;

  public ControlsFXFlagAdapter( ToggleSwitch fxToggleSwitch, FlagViewPI flagViewPI, UIContainer uiContainer ) {
    super( fxToggleSwitch, flagViewPI, uiContainer );
    this.fxToggleSwitch = fxToggleSwitch;
    this.fxToggleSwitch.selectedProperty().addListener( this );
  }

  @Override
  public Object getValue() {
    if (fxToggleSwitch != null) {
      return Boolean.valueOf( fxToggleSwitch.isSelected() );
    }
    else {
      return Boolean.FALSE;
    }
  }

  @Override
  protected void fxSetValue( Object value ) {
    if (fxToggleSwitch != null) {
      if (value == null) {
        fxToggleSwitch.setSelected( false );
      }
      else {
        fxToggleSwitch.setSelected( ((Boolean) value).booleanValue() );
      }
    }
  }

  @Override
  public Object getLabelValue() {
    if (fxToggleSwitch != null) {
      return fxToggleSwitch.getText();
    }
    else {
      return null;
    }
  }

  @Override
  protected void fxSetLabelValue( Object value ) {
    if (value instanceof String) {
      if (fxToggleSwitch != null) {
        fxToggleSwitch.setText( (String) value );
      }
    }
  }
}
