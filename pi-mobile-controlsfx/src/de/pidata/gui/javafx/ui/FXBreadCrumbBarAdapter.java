package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;
import de.pidata.models.tree.Model;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.util.Callback;
import org.controlsfx.control.BreadCrumbBar;

public class FXBreadCrumbBarAdapter extends FXTreeAdapter {

  private final TreeViewPI treeViewPI;
  private final BreadCrumbBar breadcrumbBar;

  public FXBreadCrumbBarAdapter( BreadCrumbBar breadCrumbBar, TreeViewPI treeViewPI, UIContainer uiContainer ) {
    super(breadCrumbBar, treeViewPI, uiContainer);

    this.treeViewPI = treeViewPI;
    this.breadcrumbBar = breadCrumbBar;

    // Autonavigation removes all visible nodes below the selected crumb by itself.
    this.breadcrumbBar.setAutoNavigationEnabled( true );
    this.breadcrumbBar.setOnCrumbAction( (EventHandler<BreadCrumbBar.BreadCrumbActionEvent<TreeNodePI>>) event -> {
      TreeItem<TreeNodePI> selectedTreeItem =  event.getSelectedCrumb();
      TreeNodePI selectedNode = selectedTreeItem.getValue();
      treeViewPI.onSelectionChanged( selectedNode );
    } );

    this.breadcrumbBar.setCrumbFactory( (Callback<TreeItem<TreeNodePI>, Button>) treeItem -> {
      TreeNodePI treeNodePI = treeItem.getValue();
      Model nodeModel = treeNodePI.getNodeModel();
      Object displayObject = nodeModel.get( treeNodePI.getDisplayValueID() );
      return new BreadCrumbBar.BreadCrumbButton(displayObject != null ? displayObject.toString() : "");
    } );
  }

  @Override
  public void setRootNode( TreeNodePI rootNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      breadcrumbBar.setSelectedCrumb(getOrCreateLatestLeaf( rootNode ));
    } );
  }

  private TreeItem<TreeNodePI> getOrCreateLatestLeaf( TreeNodePI rootNode ) {
    TreeNodePI latestLeaf = rootNode;

    TreeItem<TreeNodePI> latestNode = getTreeItem( latestLeaf );
    if (latestNode == null) {
      latestNode = new FXTreeItem( latestLeaf, this );
    }

    while (latestLeaf.getFirstChild() != null)
    {
      latestLeaf = latestLeaf.getFirstChild();

      TreeItem<TreeNodePI> currentChild = getTreeItem( latestLeaf );
      if (currentChild == null) {
        currentChild = new FXTreeItem( latestLeaf, this );
      }

      latestNode.getChildren().add(currentChild);
      latestNode = currentChild;
    }

    return latestNode;

  }


  @Override
  public TreeNodePI getSelectedNode() {
    FXTreeItem selectedItem = (FXTreeItem) breadcrumbBar.getSelectedCrumb();
    if (selectedItem == null) {
      return null;
    }
    else {
      return selectedItem.getValue();
    }
  }

  @Override
  public void setSelectedNode( TreeNodePI selectedNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      // force node to be visible
      breadcrumbBar.setSelectedCrumb( getTreeItem( selectedNode ) );
      treeViewPI.onSelectionChanged( selectedNode );
    } );

  }

  @Override
  public void editNode( TreeNodePI treeNode ) {
    // Nothing to edit in the crumb.
  }


  @Override
  protected FXTreeItem getRoot() {
    TreeItem<TreeNodePI> currentItem = breadcrumbBar.getSelectedCrumb();
    TreeItem<TreeNodePI> parentItem = currentItem.getParent();

    if (parentItem == null) {
      return (FXTreeItem) currentItem;
    }

    while (parentItem != null) {
      parentItem = parentItem.getParent();
    }

    return (FXTreeItem) parentItem;
  }
}
