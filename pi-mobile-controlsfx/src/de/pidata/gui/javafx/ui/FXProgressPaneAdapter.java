package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.controls.ProgressPane;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.string.Helper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.MaskerPane;
import org.controlsfx.control.NotificationPane;


public class FXProgressPaneAdapter extends FXValueAdapter implements UIProgressBarAdapter {

  private MaskerPane fxMaskerPane;
  private ProgressPane fxProgressPane;
  private NotificationPane fxNotificationPane;

  private final ImageView warningImage;
  private final ImageView infoImage;
  private final ImageView errorImage;
  private boolean hasError = false;

  public FXProgressPaneAdapter( ProgressPane progressPane, ProgressBarViewPI progressBarViewPI, UIContainer uiContainer ) {
    super(progressPane.getMaskerPane(), progressBarViewPI, uiContainer );

    fxProgressPane = progressPane;
    fxMaskerPane = fxProgressPane.getMaskerPane();
    fxNotificationPane = fxProgressPane.getNotificationPane();

    fxMaskerPane.setProgress( -1 );
    fxMaskerPane.setText( "Please wait..." );
    fxMaskerPane.setStyle( "-fx-accent: deepskyblue" );

    String warningImagePath = getClass().getResource( "/images/dialog-warning.png" ).toExternalForm();
    warningImage = new ImageView(warningImagePath);

    String infoImagePath = getClass().getResource( "/images/dialog-information.png" ).toExternalForm();
    infoImage = new ImageView(infoImagePath);

    String errorImagePath = getClass().getResource( "/images/dialog-error.png" ).toExternalForm();
    errorImage = new ImageView(errorImagePath);


    fxProgressPane.setMouseTransparent( !fxMaskerPane.isVisible() );

    fxMaskerPane.visibleProperty().addListener( ( observable, oldValue, newValue ) -> {
      fxProgressPane.setMouseTransparent( !newValue.booleanValue() );
    } );

    fxNotificationPane.setOnShown( (event) -> {
      fxProgressPane.setMouseTransparent( false );
    } );

    fxNotificationPane.setOnHiding( (event) -> {
      if (hasError) {
        fxMaskerPane.setVisible( false );
        hasError = false;
      }
    } );

    fxNotificationPane.setOnHidden( (event) -> {
      if (!fxMaskerPane.isVisible()) {
        fxProgressPane.setMouseTransparent( true );
      }
    } );
  }

  @Override
  public void setMaxValue( int maxValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getMaxValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinValue( int minValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getMinValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setProgressMessage( Object progressMessage ) {
    Platform.getInstance().runOnUiThread( () -> {
      String message = "";
      if ( progressMessage != null ) {
        message = progressMessage.toString();
      }
      fxMaskerPane.setText( message );
      if (!fxMaskerPane.isVisible()
          && fxMaskerPane.getProgress() == -1
          && !Helper.isNullOrEmpty(message)) {
        fxMaskerPane.setVisible( true );
      }
    } );
  }

  @Override
  public void setProgress( double progress ) {
    double fxProgress;

    if (progress == 0.0) {
      fxProgress = -1;
    }
    else {
      fxProgress = progress/100.0;
    }

    Platform.getInstance().runOnUiThread( () -> {
      fxMaskerPane.setProgress( fxProgress );
      if (!fxMaskerPane.isVisible() && fxProgress == 0.0) {
        fxMaskerPane.setVisible( true );

      }
    } );
  }

  @Override
  public Object getValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  public void showError( String errorMessage ) {
    hasError = true;
    Platform.getInstance().runOnUiThread( () -> {
        fxNotificationPane.show( errorMessage, errorImage );
      }
    );
  }

  @Override
  public void showInfo( String infoMessage ) {
    Platform.getInstance().runOnUiThread( () -> {
        fxNotificationPane.show( infoMessage, infoImage );
      }
    );
  }

  @Override
  public void showWarning( String warningMessage){
    Platform.getInstance().runOnUiThread( () -> {
        fxNotificationPane.show( warningMessage, warningImage );
      }
    );
  }

  @Override
  public void resetColor(){
    Platform.getInstance().runOnUiThread( ()->{
      try{
        fxMaskerPane.setStyle( "-fx-accent: deepskyblue" );
      }
      catch (Exception e){
        Logger.error("FXProgressBarAdapter resetColor()", e);
      }
    } );
  }

  public void hideError() {
    Platform.getInstance().runOnUiThread( () -> {
        fxNotificationPane.hide();
      }
    );
  }
}
