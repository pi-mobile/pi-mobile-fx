package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.renderer.Renderer;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.SingleSelection;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.SingleSelectionModel;
import org.controlsfx.control.SearchableComboBox;

/**
 * Created by pru on 17.01.2017.
 */
public class FXSearchableComboboxAdapter extends FXAdapter implements UIListAdapter, ChangeListener<Number> {

  private static boolean DEBUG = false;

  private static ObservableList<String> EMPTY_OBSERVABLE_LIST = FXCollections.observableArrayList();

  private SearchableComboBox fxSearchableCombobox;
  private ListViewPI listViewPI;
  private ObservableList<String> fxComboboxItems;
  // fxListItems is used for synchronization, so has to be final
  private final ObservableList<Model> fxListItems = FXCollections.observableArrayList();

  public FXSearchableComboboxAdapter( SearchableComboBox fxSearchableCombobox, ListViewPI listViewPI, UIContainer uiContainer ) {
    super( fxSearchableCombobox, listViewPI, uiContainer );
    this.fxSearchableCombobox = fxSearchableCombobox;
    this.listViewPI = listViewPI;

    fxComboboxItems = FXCollections.observableArrayList();
    fxSearchableCombobox.setItems( fxComboboxItems );

    // Only add listener, but do not set initial selection here: fxListItems is not yet initialized
    fxSearchableCombobox.getSelectionModel().selectedIndexProperty().addListener( FXSearchableComboboxAdapter.this );
  }

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    return fxListItems.size();
  }

  /**
   * Returns the data displayed in row at position
   *
   * @param position
   * @return
   */
  @Override
  public Model getDisplayRow( int position ) {
    if (position < fxListItems.size()) {
      return fxListItems.get( position );
    }
    else {
      return null;
    }
  }

  private String getDisplayString( Model model ) {
    if (listViewPI == null) {
      return null;
    }
    else {
      QName valueID = listViewPI.getDisplayValueID();
      Object value = listViewPI.getCellValue( model, valueID );
      return listViewPI.render( null, value );
    }
  }



  @Override
  public void removeAllDisplayRows() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          fxListItems.clear();

          // fxComboboxItems.clear() runs into an error which is not comprehensible why.
          // Workaround is to remove items (empty list) and set again
          fxSearchableCombobox.setItems( EMPTY_OBSERVABLE_LIST );
          fxComboboxItems.clear();
          fxSearchableCombobox.setItems( fxComboboxItems );
        }
      }
    });
  }

  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          //-- In some special cases of initialization JavaFX (at least with BellSoft Full JDK 17.0.7) does not realize
          //   update of items - Workaround is to remove items (empty list) and set again (see last code line)
          fxSearchableCombobox.setItems( EMPTY_OBSERVABLE_LIST );
          int index;
          if (beforeRow == null) {
            index = -1;
          }
          else {
            index = fxListItems.indexOf( beforeRow );
          }
          if (index < 0) {
            fxListItems.add( newRow );
            fxComboboxItems.add( getDisplayString( newRow ) );
            index = fxListItems.size() - 1;
          }
          else {
            fxListItems.add( index, newRow );
            fxComboboxItems.add( index, getDisplayString( newRow ) );
          }
          fxSearchableCombobox.setItems( fxComboboxItems );
        }
      }
    });
  }


  @Override
  public void removeRow( Model removedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          //-- In some special cases of initialization JavaFX (at least with BellSoft Full JDK 17.0.7) does not realize
          //   update of items - Workaround is to remove items (empty list) and set again (see last code line)
          fxSearchableCombobox.setItems( EMPTY_OBSERVABLE_LIST );

          int index = fxListItems.indexOf( removedRow );
          if (index >= 0) {
            fxListItems.remove( removedRow );
            fxComboboxItems.remove( index );
          }
          fxSearchableCombobox.setItems( fxComboboxItems );
        }
      }
    });
  }

  @Override
  public void updateRow( Model changedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          int index = fxListItems.indexOf( changedRow );
          if (index >= 0) {
            fxListItems.set( index, changedRow );
            int selIndex = fxSearchableCombobox.getSelectionModel().getSelectedIndex();
            String displayStr = fxComboboxItems.get( index );
            String newStr = getDisplayString(  changedRow );
            if (!newStr.equals(  displayStr )) {
              fxComboboxItems.set( index, getDisplayString( changedRow ) );
              if (selIndex > 0) {
                fxComboboxItems.set(index, getDisplayString( changedRow ));
              }
            }
          }
        }
      }
    });
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          try {
            int index = fxListItems.indexOf( displayRow );
            if (selected) {
              fxSearchableCombobox.getSelectionModel().select( index );
            }
            else {
              fxSearchableCombobox.getSelectionModel().clearSelection( index );
            }
          }
          catch (Exception e) {
            Logger.error( this.getClass().getSimpleName(), e );
          }
        }
      }
    });
  }

  @Override
  public void changed( ObservableValue<? extends Number> observable, Number oldValue, Number newValue ) {

    if (DEBUG) {
      Logger.debug( "---" + listViewPI.getListCtrl().getName()+"."+this.getClass().getSimpleName() + ".changed():  oldValue="+oldValue + ", newValue="+newValue );
    }


    // If one opens the SearchableCombobox the cursor jumps to the search field and the changed event with newValue -1
    // occurs. We do not want to have the old value selected twice.
    if (oldValue != null && newValue.intValue() == -1) {
      return;
    }

    if ((oldValue == null) || (oldValue.intValue() < 0)) {
      listViewPI.onSelectionChanged( null, false );
    }
    else {
      Model oldSelectedRow = getDisplayRow( oldValue.intValue() );
      listViewPI.onSelectionChanged( oldSelectedRow, false );
    }
    if ((newValue == null) || (newValue.intValue() < 0)){
      listViewPI.onSelectionChanged( null, true );
    }
    else {
      Model newSelectedRow = getDisplayRow( newValue.intValue() );
      listViewPI.onSelectionChanged( newSelectedRow, true );
    }
  }
}
