package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentFactory;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.log.Logger;
import de.pidata.log.Profiler;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.controlsfx.control.table.ColumnFilter;
import org.controlsfx.control.table.FilterValue;
import org.controlsfx.control.table.TableFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class ControlsFXTableAdapter extends FXTableAdapter {

  private TableFilter<Model> tableFilter;

  public ControlsFXTableAdapter( TableView<Model> fxTableView, TableViewPI tableViewPI, FXUIFactory fxUIFactory, UIContainer uiContainer ) {
    super( fxTableView, tableViewPI, fxUIFactory, uiContainer );
  }

  @Override
  protected void initializeTableFilter() {

    //--- Add filter; don't add if table requires minimum one row selected. See DefaultTableController.onFiltered()
    Platform.getInstance().runOnUiThread( () -> {
      if (tableViewPI.getListCtrl().getSelection().isEmptyAllowed()) {
        tableFilter = TableFilter.forTableView( fxTableView ).lazy( false ).apply();
        tableFilter.getFilteredList().addListener( new ListChangeListener<Model>() {
          @Override
          public void onChanged( Change<? extends Model> c ) {
            if (tableFilter.getFilteredList().size() != tableFilter.getBackingList().size()) {
              tableViewPI.onFiltered( true );
            }
            else {
              tableViewPI.onFiltered( false );
            }
            // Important: fxTableView.getSelectionModel().clearSelection() does not work
            // because UIs item list is inconsistent and will lead to selection events with
            // wrong index.
            // Solution: TableController must clear its selection; be sure to allow empty selection, that is no row in the table is selected!
          }
        } );
      }
    } );

  }

  @Override
  public void resetFilter( ColumnInfo columnInfo) {
    if( tableFilter != null ) {
      Optional<ColumnFilter<Model, ?>> columnFilter = tableFilter.getColumnFilter( fxTableView.getColumns().get( columnInfo.getIndex() ) );
      columnFilter.ifPresent( ColumnFilter::selectAllValues );
    }
  }

  @Override
  public void resetFilter() {
    if (tableFilter != null) {
      javafx.application.Platform.runLater( new Runnable() {
        @Override
        public void run() {
          tableFilter.resetAllFilters();
        }
      } );
    }
  }

  public HashMap<QName, Object> getFilteredValues() {
    if (tableFilter == null) {
      Logger.info( "Called 'getFilteredValues' on table that has no tableFilter set" );
      return null;
    }
    HashMap<QName, Object> filteredValuesMap = new HashMap<>();
    ObservableList<ColumnFilter<Model, ?>> columnFilters = tableFilter.getColumnFilters();
    for (ColumnFilter<Model, ?> columnFilter : columnFilters) {
      if (columnFilter.isFiltered()) {
        String columnId = columnFilter.getTableColumn().getId();
        QName columnKey = ComponentFactory.NAMESPACE.getQName( columnId );
        List<Object> filteredValues = new ArrayList<>();
        for (FilterValue<Model, ?> filterValue : columnFilter.getFilterValues()) {
          if (!filterValue.selectedProperty().get()) {
            filteredValues.add( filterValue.getValue() );
          }
        }
        filteredValuesMap.put( columnKey, filteredValues );
      }
    }
    return filteredValuesMap;
  }

  @Override
  public void unselectValues( HashMap<QName, Object> unselectedValuesMap ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (tableFilter == null) {
        throw new RuntimeException( "Called 'unselectValues' on table that has no tableFilter set" );
      }
      for (ColumnFilter<Model, ?> columnFilter : tableFilter.getColumnFilters()) {
        if(!columnFilter.isInitialized()) {
          columnFilter.initialize();
        }
      }
      for (TableColumn<Model, ?> column : fxTableView.getColumns()) {
        String columnId = column.getId();
        QName columnKey = ComponentFactory.NAMESPACE.getQName( columnId );
        if (unselectedValuesMap.containsKey( columnKey )) {
          Object mapValue = unselectedValuesMap.get( columnKey );
          if (mapValue instanceof List) {
            List valueList = (List) mapValue;
            for (Object valueToUnselect : valueList) {
              tableFilter.unselectValue( column, valueToUnselect );
            }
          }
        }
      }
      tableFilter.executeFilter();
    } );
  }

  public List<Model> getVisibleRowModels() {
    if (tableFilter == null) {
      Logger.info( "Called 'getVisibleRowModels' on table that has no tableFilter set" );
      return null;
    }
    return tableFilter.getFilteredList();
  }

  @Override
  public void updateRow( Model changedRow ) {
    // We must not run this immediately even if on UI Thread, because at least if the change affects
    // selected row we get an uncatchable IndexOutOfBoundsException from fxTableItems.set()
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (tableFilter != null) {
          FilteredList<Model> filteredList = tableFilter.getFilteredList();
          synchronized (filteredList) {
            Profiler.count( "FXTableAdapter updateRow" );
            final int index = filteredList.indexOf( changedRow );
            if (index >= 0) {
              try {
                filteredList.set( index, changedRow );
              }
              catch (UnsupportedOperationException e) {
                ControlsFXTableAdapter.super.updateRow( changedRow );
              }
            }
          }
        }
        else {
          ControlsFXTableAdapter.super.updateRow( changedRow );
        }
      }
    } );
  }
}
