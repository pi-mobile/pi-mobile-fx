package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentFactory;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.DefaultTableController;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.log.Profiler;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.controlsfx.control.tableview2.FilteredTableColumn;
import org.controlsfx.control.tableview2.FilteredTableView;
import org.controlsfx.control.tableview2.filter.filtereditor.SouthFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupFilter;
import org.controlsfx.control.tableview2.filter.popupfilter.PopupStringFilter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilteredFXTableAdapter extends FXTableAdapter {

  FilteredTableView<Model> filteredTableView;

  public FilteredFXTableAdapter( FilteredTableView<Model> fxTableView, TableViewPI tableViewPI, FXUIFactory fxUIFactory, UIContainer uiContainer ) {
    super( fxTableView, tableViewPI, fxUIFactory, uiContainer );

    filteredTableView = fxTableView;
  }

  @Override
  protected void initItemsAndColumns( TableView<Model> fxTableView, TableViewPI tableViewPI, ObservableList<TableColumn<Model, ?>> tableColumnList ) {

    FilteredTableView.configureForFiltering( (FilteredTableView<Model>) fxTableView, fxTableItems );

    for (TableColumn tableColumn : tableColumnList) {
      tableColumn.setCellFactory( this );
      QName columnID = ComponentFactory.NAMESPACE.getQName( tableColumn.getId() );
      ColumnInfo columnInfo = tableViewPI.getColumnByCompID( columnID );
      if (columnInfo == null) {
        throw new IllegalArgumentException( "Column info not found for ColCompID="+columnID );
      }
      columnInfo.setView( tableViewPI );

      FXCellValueFactory cellValueFactory = new FXCellValueFactory( columnInfo );
      tableColumn.setCellValueFactory( cellValueFactory );

      PopupFilter<Model, String> popupFirstNameFilter = new PopupStringFilter<>((FilteredTableColumn)tableColumn);
      ((FilteredTableColumn)tableColumn).setOnFilterAction(e -> popupFirstNameFilter.showPopup());

      SouthFilter<Model, String> tableColumnFilter = new SouthFilter<>((FilteredTableColumn<Model, String>)tableColumn, String.class);
      ((FilteredTableColumn<Model, String>)tableColumn).setSouthNode(tableColumnFilter);

      tableColumn.setSortable( columnInfo.isSortable() );
    }

    DefaultTableController listCtrl = (DefaultTableController) tableViewPI.getListCtrl();
    for (int i = tableColumnList.size(); i < listCtrl.columnCount(); i++) {
      ColumnInfo columnInfo = listCtrl.getColumn( i );
      columnInfo.setView( tableViewPI );
      FilteredTableColumn tableColumn = new FilteredTableColumn( columnInfo.getColName().getName() );
      tableColumn.setId( columnInfo.getBodyCompID().getName() );
      tableColumn.setCellFactory( this );
      tableColumn.setCellValueFactory( new FXCellValueFactory( columnInfo ) );
      tableColumn.setSortable( columnInfo.isSortable() );

      SouthFilter<Model, String> tableColumnFilter = new SouthFilter<>( tableColumn, String.class);
      tableColumn.setSouthNode(tableColumnFilter);

      tableColumnList.add( i, tableColumn );
    }

  }

  @Override
  public void resetFilter() {
    filteredTableView.resetFilter();
  }

  @Override
  public HashMap<QName, Object> getFilteredValues() {
    // TODO:

    HashMap<QName, Object> filteredValuesMap = new HashMap<>();
    ObservableList<TableColumn<Model, ?>> columns = filteredTableView.getColumns();
    for (TableColumn<Model, ?> column : columns) {
      String columnId = column.getId();
      QName columnKey = ComponentFactory.NAMESPACE.getQName( columnId );
      List<Object> filteredValues = new ArrayList<>();
      SouthFilter<Model, String> southNode = (SouthFilter<Model, String>) ((FilteredTableColumn)column).getSouthNode();
      for (String filterValue : southNode.getFilterEditor().getItems()) {
        if (!filteredValues.contains( filterValue )) {
          filteredValues.add( filterValue );
        }
      }
      filteredValuesMap.put( columnKey, filteredValues );

    }
    return filteredValuesMap;
  }

  @Override
  public void unselectValues( HashMap<QName, Object> unselectedValuesMap ) {
    // There are no multi selectable values
    resetFilter();
  }

  @Override
  public List<Model> getVisibleRowModels() {
    return filteredTableView.getItems();
  }

  @Override
  public void updateRow( Model changedRow ) {
    // We must not run this immediately even if on UI Thread, because at least if the change affects
    // selected row we get an uncatchable IndexOutOfBoundsException from fxTableItems.set()
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        List<Model> filteredList = filteredTableView.getItems();
        synchronized (filteredList) {
          Profiler.count( "FXTableAdapter updateRow" );
          final int index = filteredList.indexOf( changedRow );
          if (index >= 0) {
            filteredList.set( index, changedRow );
          }
        }
      }
    } );
  }
}
