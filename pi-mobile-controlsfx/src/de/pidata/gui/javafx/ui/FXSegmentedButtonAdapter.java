package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.renderer.Renderer;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.SingleSelection;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import org.controlsfx.control.SegmentedButton;

/**
 * Created by pru on 20.06.16.
 */
public class FXSegmentedButtonAdapter extends FXAdapter implements UIListAdapter, ChangeListener<Toggle> {

  public static final boolean DEBUG = false;
  private static final ToggleButton DUMMY_ENTRY = new ToggleButton( "Dummy" );

  private SegmentedButton fxSegmentedButton;
  private ListViewPI listViewPI;
  private ObservableList<ToggleButton> fxButtonItems;
  private ObservableList<Model> fxListItems;

  public FXSegmentedButtonAdapter( SegmentedButton segmentedButton, ListViewPI listViewPI, UIContainer uiContainer ) {
    super( segmentedButton, listViewPI, uiContainer );
    this.fxSegmentedButton = segmentedButton;
    this.listViewPI = listViewPI;

    //--- Add values
    fxListItems = FXCollections.observableArrayList();
    fxButtonItems = this.fxSegmentedButton.getButtons();
    fxButtonItems.removeAll();
    fxButtonItems.add( DUMMY_ENTRY );

    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        //--- Activate Multi-Selection if allowed
        if (listViewPI.isMultiSelect()) {
          throw new IllegalArgumentException( "FXSegmentedButton does only support singleSelection" );
        }


        //--- Set initial selection
        SingleSelection selection = (SingleSelection) listViewPI.getListCtrl().getSelection();
        if (selection.selectedValueCount() > 0) {
          int index = fxListItems.indexOf( selection.getSelectedValue( 0 ) );
//          fxSegmentedButton.getToggleGroup().selectToggle( fxButtonItems.get( index ) );
        }
        else {
          fxSegmentedButton.getToggleGroup().selectToggle( null );
        }

        //--- Attach listener for selection changes
        fxSegmentedButton.getToggleGroup().selectedToggleProperty().addListener( FXSegmentedButtonAdapter.this );
      }
    } );
  }

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    return fxListItems.size();
  }

  /**
   * Returns the data displayed in row at position
   *
   * @param position
   * @return
   */
  @Override
  public Model getDisplayRow( int position ) {
    if (position < fxListItems.size()) {
      return fxListItems.get( position );
    }
    else {
      return null;
    }
  }

  private String getDisplayString( Model model ) {
    if (listViewPI == null) {
      return null;
    }
    else {
      QName valueID = listViewPI.getDisplayValueID();
      Object value = listViewPI.getCellValue( model, valueID );
      return listViewPI.render( null, value );
    }
  }

  private void startUpdate() {
    fxButtonItems.remove( DUMMY_ENTRY );
  }

  private void endUpdate() {
//    fxSegmentedButton.getButtons( ).removeAll(  );
//    fxSegmentedButton.getButtons().addAll( fxButtonItems );
  }

  @Override
  public void removeAllDisplayRows() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          fxListItems.clear();
          fxButtonItems.clear();
          endUpdate();
        }
      }
    } );
  }

  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();

          int index;
          if (beforeRow == null) {
            index = -1;
          }
          else {
            index = fxListItems.indexOf( beforeRow );
          }
          if (index < 0) {
            fxListItems.add( newRow );
            fxButtonItems.add( new ToggleButton( getDisplayString( newRow ) ) );
            index = fxListItems.size() - 1;
          }
          else {
            fxListItems.add( index, newRow );
            fxButtonItems.add( index, new ToggleButton( getDisplayString( newRow ) ) );
          }
          endUpdate();
        }
      }
    } );
  }


  @Override
  public void removeRow( Model removedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          int index = fxListItems.indexOf( removedRow );
          if (index >= 0) {
            fxListItems.remove( removedRow );
            fxButtonItems.remove( index );
          }
          endUpdate();
        }
      }
    } );
  }

  @Override
  public void updateRow( Model changedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          int index = fxListItems.indexOf( changedRow );
          fxListItems.set( index, changedRow );
          fxButtonItems.set( index, new ToggleButton( getDisplayString( changedRow ) ) );
          endUpdate();
        }
      }
    } );
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          if (fxButtonItems.size() > 0) {
            try {
              if (displayRow != null) {
                int index = fxListItems.indexOf( displayRow );
                fxSegmentedButton.getToggleGroup().selectToggle( fxButtonItems.get( index ) );
              }
              else {
                fxSegmentedButton.getToggleGroup().selectToggle( null );
              }
            }
            catch (Exception e) {
              Logger.error( this.getClass().getSimpleName(), e );
            }
          }
        }
      }
    } );
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue ) {
    if ((oldValue == null)) {
      listViewPI.onSelectionChanged( null, false );
      if (DEBUG) Logger.info( "Deselected nothing" );
    }
    else {
      if (fxButtonItems.contains( oldValue )) {
        Model oldSelectedRow = getDisplayRow( fxButtonItems.indexOf( oldValue ) );
        listViewPI.onSelectionChanged( oldSelectedRow, false );
        if (DEBUG) Logger.info( "Deselected row" + oldSelectedRow );
      }
    }
    if ((newValue == null)) {
      listViewPI.onSelectionChanged( null, true );
      if (DEBUG) Logger.info( "Selected nothing" );

    }
    else {
      if (fxButtonItems.contains( newValue )) {
        Model newSelectedRow = getDisplayRow( fxButtonItems.indexOf( newValue ) );
        listViewPI.onSelectionChanged( newSelectedRow, true );
        if (DEBUG) Logger.info( "Selected row " + newSelectedRow );
      }
    }
  }
}
