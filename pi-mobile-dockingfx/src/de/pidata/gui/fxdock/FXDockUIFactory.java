package de.pidata.gui.fxdock;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.docking.PIDockingDialog;
import de.pidata.gui.docking.PIDockingPane;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFragmentAdapter;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.string.Helper;

import java.util.Properties;

public class FXDockUIFactory extends FXUIFactory {

  @Override
  public UIFragmentAdapter createFragmentAdapter( UIContainer uiContainer, ModuleViewPI moduleViewPI ) {
    return super.createFragmentAdapter( uiContainer, moduleViewPI );
  }

  @Override
  public void updateLanguage( UIContainer uiContainer, Properties textProps, Properties glossaryProps ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
        @Override
        public void run() {
          if (uiContainer instanceof PIDockingPane) {
            String title = Helper.replaceParams( textProps.getProperty( "title" ), "{", "}", glossaryProps );
            if (Helper.isNotNullAndNotEmpty( title )) {
              ((PIDockingPane) uiContainer).setTitle( title );
            }
          }
          else if (uiContainer instanceof PIDockingDialog) {
            String title = Helper.replaceParams( textProps.getProperty( "title" ), "{", "}", glossaryProps );
            if (Helper.isNotNullAndNotEmpty( title )) {
              ((PIDockingDialog) uiContainer).setTitle( title );

              PIDockingPane dialogPane = ((PIDockingDialog) uiContainer).getDialogPane();
              if (dialogPane != null) {
                dialogPane.setTitle( title );
              }
            }
          }
          FXDockUIFactory.super.updateLanguage( uiContainer, textProps, glossaryProps );
        }
      }
    );
  }
}
