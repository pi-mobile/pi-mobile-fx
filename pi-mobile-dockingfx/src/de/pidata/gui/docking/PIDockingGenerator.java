package de.pidata.gui.docking;

import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import goryachev.common.util.GlobalSettings;
import goryachev.fxdock.FxDockFramework;
import goryachev.fxdock.FxDockPane;
import goryachev.fxdock.internal.FxDockSchema;

import static de.pidata.gui.docking.PIDockingDialog.DIALOG_ID;

public class PIDockingGenerator implements FxDockFramework.Generator {

  /**
   * creates custom window
   */
  public PIDockingDialog createWindow( String windowPrefix ) {
    String dialogName = GlobalSettings.getString( windowPrefix + FxDockSchema.SUFFIX_BINDINGS + "." + DIALOG_ID );
    if (Helper.isNullOrEmpty( dialogName )) {
      throw new IllegalArgumentException( "DialogId for not found for: " + windowPrefix );
    }

    return createWindowWithId( dialogName, AbstractParameterList.EMPTY );
  }

  public PIDockingDialog createWindowWithId( String dialogId, ParameterList parameterList ) {
    PIDockingPane piDockingPane = createPane(dialogId, parameterList);
    return createWindowForDockPane( piDockingPane );
  }


  public PIDockingDialog createWindowForDockPane( FxDockPane piDockingPane ) {
    PIDockingDialog piDockingDialog = ((PIDockingPane) piDockingPane).getPiDockingDialog();
    if (piDockingDialog != null) {
      return piDockingDialog;
    }
    return new PIDockingDialog( (PIDockingPane) piDockingPane );
  }

  /**
   * creates custom pane using the type id
   */
  public PIDockingPane createPane( String type, ParameterList parameterList ) {
    return new PIDockingPane( type, true, parameterList);
  }
}
