package de.pidata.gui.docking;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.javafx.FXApplication;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import goryachev.fxdock.FxDockPane;
import goryachev.fxdock.FxDockWindow;
import goryachev.fxdock.internal.DockTools;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;

public class PIDockingPane extends FxDockPane {

  private DialogController dialogController;
  private QName dialogID;
  private PIDockingDialog piDockingDialog;

  public PIDockingPane( String type, boolean createController, ParameterList parameterList ) {
    super( type );
    this.dialogID = Namespace.getInstance( "de.pidata.gui" ).getQName( type );

    if (createController) {
      try {
        DialogController parentDlgCtrl = FXApplication.getInstance().getRootDialogController();
        Context context = parentDlgCtrl.getContext();

        // We need a dummy wrapper for the DialogController and load the content to be there for the activate();
        piDockingDialog = new PIDockingDialog( this );
        URL mainDlgRes = getClass().getResource( "/layout/"+dialogID.getName()+".fxml" );
        Logger.info( "Load FXML: " + "/layout/"+dialogID.getName()+".fxml" );
        Node root = FXMLLoader.load( mainDlgRes );
        setContent( root );

        ((PIDockingDialog)parentDlgCtrl.getDialogComp()).addChildDialog( piDockingDialog );

        dialogController = Platform.getInstance().getControllerBuilder().createDialogController( dialogID, context, piDockingDialog, parentDlgCtrl );

        dialogController.setParameterList( parameterList );
        dialogController.activate( piDockingDialog );
        if (Helper.isNullOrEmpty(getTitle())) {
          setTitle( dialogController.getTitle() );
        }
      }
      catch (Exception ex) {
        throw new IllegalArgumentException( "Error initializing dialog ID="+ dialogID, ex );
      }
    }
    else {
      try {
        URL mainDlgRes = getClass().getResource( "/layout/"+type+".fxml" );
        System.out.println("Load FXML: " + "/layout/"+type+".fxml");
        setContent( FXMLLoader.load( mainDlgRes ));
      }
      catch (IOException e) {
        throw new IllegalArgumentException( "Not able to load file: layout/"+type+".fxml" );
      }
    }
  }

  public DialogController getDialogController() {
    return dialogController;
  }

  public QName getDialogID() {
    return dialogID;
  }

  @Override
  public void actionClose() {
    if (dialogController != null) {
      dialogController.close( true );
    }
    super.actionClose();
  }

  @Override
  public void actionMoveToPane( Pane target ) {
    if (dialogController == null) {

      try {
        FxDockWindow targetWindow = DockTools.getWindow( target );
        FxDockWindow parentWindow = DockTools.getWindow( this );

        DialogController parentDlgCtrl = ((PIDockingDialog) targetWindow).getController();
        Context context = parentDlgCtrl.getContext();

        ((PIDockingDialog)targetWindow).addChildDialog( (PIDockingDialog) parentWindow );

        dialogController = Platform.getInstance().getControllerBuilder().createDialogController( dialogID, context, (PIDockingDialog) parentWindow, parentDlgCtrl );
        dialogController.setParameterList( AbstractParameterList.EMPTY );
        dialogController.activate( (PIDockingDialog) parentWindow );
        if (Helper.isNullOrEmpty( getTitle() )) {
          setTitle( dialogController.getTitle() );
        }
      }
      catch (Exception e) {
        throw new IllegalArgumentException( "Error initializing dialog ID=" + dialogID, e );
      }

    }

  }

  public PIDockingDialog getPiDockingDialog() {
    return piDockingDialog;
  }
}
