package de.pidata.gui.docking;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.controller.file.FileChooserResult;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.javafx.*;
import de.pidata.gui.javafx.ui.NodeAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rights.RightsRequireListener;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import goryachev.fx.FxString;
import goryachev.fx.internal.LocalSettings;
import goryachev.fxdock.FxDockFramework;
import goryachev.fxdock.FxDockPane;
import goryachev.fxdock.FxDockWindow;
import goryachev.fxdock.internal.DeletedPane;
import goryachev.fxdock.internal.DockTools;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.robot.Robot;
import javafx.stage.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import static de.pidata.gui.javafx.ui.NodeAdapter.RootType.*;

public class PIDockingDialog extends FxDockWindow implements FXDialogInterface {

  public static final String DIALOG_ID = "DIALOG_ID";
  /** holds the loaded modules; necessary to find identical instances of JavaFX elements when searching for them */
  private Map<QName, Node> moduleMap = new HashMap<>();
  protected QName dialogID;
  protected FXDialogInterface parentDialog;
  protected List<Dialog> childDialogList = new ArrayList<>();
  protected DialogController dialogController;

  private PIDockingPane dialogPane;

  // Define a variable to store the property
  private SimpleStringProperty dialogIdProperty = new FxString();

  // Define a setter for the property's value
  public final void setDialogID(String value){dialogIdProperty.set(value);}

  // Define a getter for the property itself
  public SimpleStringProperty dialogIdProperty() {
    return dialogIdProperty;
  }

  public PIDockingDialog() {
    super();
  }


  @Override
  public void open() {
    super.open();

    DialogControllerDelegate delegate = dialogController.getDelegate();
    if (delegate != null) {
      delegate.dialogShowing( dialogController );
    }
  }

  public PIDockingDialog( PIDockingPane piDockingPane ) {
    super();

    dialogPane = piDockingPane;

    this.dialogID = piDockingPane.getDialogID();

    Node parent = DockTools.getParent( piDockingPane );
    if (parent != null) {
     doSetStyle( parent.getStyle() );
    }

    setDialogID( dialogID.getName() );
    LocalSettings.get(this).add( DIALOG_ID, dialogIdProperty() );
    setContent( piDockingPane );


  }

  protected void setMainContent( Node node) {
    setContent( node );
  }

  /**
   * Set UI Style
   *
   * @param style
   */
  @Override
  public void setStyle( String style ) {
    for (FxDockWindow w : FxDockFramework.getWindows()) {
      ((PIDockingDialog) w).doSetStyle( style );
    }
  }

  public void doSetStyle( String style ) {
    root.setStyle( style );

    Node parent = root.getParent();
    while (parent != null) {
     parent.setStyle( style );
     parent = parent.getParent();
    }
  }

  @Override
  public void onTitleChanged( String title ) {
    Platform.getInstance().runOnUiThread( () -> setTitle( title ) );
  }

  /**
   * Sets the controller for this dialog.
   *
   * @param dialogController this dialog's controller
   */
  @Override
  public void setController( DialogController dialogController ) {
    if (this.dialogController == null) {
      this.dialogController = dialogController;
      onTitleChanged( dialogController.getTitle() );
    }
  }

  /**
   * Returns this dialog's controller
   *
   * @return this dialog's controller
   */
  @Override
  public DialogController getController() {
    return dialogController;
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    return getController().getModel();
  }

  @Override
  public void updateLanguage() {
    Platform.getInstance().updateLanguage( this, getDialogID(), null );
    for (Controller childCtrl : getController().controllerIterator()) {
      if (childCtrl instanceof ModuleController) {
        ModuleGroup moduleGroup = ((ModuleController) childCtrl).getCurrentModule();
        if (moduleGroup != null) {
          UIContainer moduleUIContainer = moduleGroup.getUIContainer();
          if (moduleUIContainer != null) {
            Platform.getInstance().updateLanguage( moduleUIContainer, moduleGroup.getName(), null );
          }
        }
      }
    }
    for (Dialog childDlg : childDialogList) {
      childDlg.updateLanguage();
    }
  }
  
  /**
   * Returns the Node identified by uiCompID within this FXUIContainer
   * @param uiCompID component (Node) identifier
   * @param moduleID
   */
  public Node findUIComp( QName uiCompID, QName moduleID ) {
    if (moduleID != null) {
      Node moduleNode = moduleMap.get( moduleID );
      if (moduleNode == null) {
        String resourceName = "/layout/" + moduleID.getName() + ".fxml";
        URL editorModuleRes = getClass().getResource( resourceName );
        try {
          moduleNode = FXMLLoader.load( editorModuleRes );
          moduleMap.put( moduleID, moduleNode );
        }
        catch (IOException e) {
          Logger.error( "Error loading module resource name=" + resourceName, e );
        }
      }
      if (moduleNode == null) {
        throw new IllegalArgumentException( "Could not find UI module, moduleID="+moduleID );
      }
      return moduleNode.lookup( "#"+uiCompID.getName() );
    }
    else {

      // First search in the root, which might be always the case to find the Node.
      Node uiNode =  root.lookup( "#" + uiCompID.getName() );

      // We have a BorderPane so the content might be loaded to another region.
      // Look at the Center
      if (uiNode == null && root.getContent() != null) {
        uiNode = root.getContent().lookup( "#" + uiCompID.getName() );
      }

      // Look at Top
      if (uiNode == null && getTop() != null) {
        uiNode = getTop().lookup( "#" + uiCompID.getName() );
      }
      // Look at Bottom
      if (uiNode == null && getBottom() != null) {
        uiNode = getBottom().lookup( "#" + uiCompID.getName() );
      }
      // Look at Left
      if (uiNode == getLeft() && getLeft() != null) {
        uiNode = getLeft().lookup( "#" + uiCompID.getName() );
      }
      // Look at Right
      if (uiNode == null && getRight() != null) {
        uiNode = getRight().lookup( "#" + uiCompID.getName() );
      }

      return uiNode;
    }
  }

  @Override
  public MenuItem findMenuItem( QName componentID ) {
    // First search at the root.
    NodeAdapter nodeAdapter = findNodeAdapter( new NodeAdapter( root ), componentID );

    // The Menu might be loaded in another FXML and put to Top
    if (nodeAdapter == null && getTop() != null) {
      nodeAdapter = findNodeAdapter( new NodeAdapter( getTop() ), componentID );
    }

    if(nodeAdapter!=null && nodeAdapter.getRootType() == MENU_ITEM) {
      return nodeAdapter.getMenuItem();
    }

    return null;
  }

  @Override
  public Menu findMenu( QName componentID ) {
    // First search at the root.
    NodeAdapter nodeAdapter = findNodeAdapter( new NodeAdapter( root ), componentID );

    // The Menu might be loaded in another FXML and put to Top
    if (nodeAdapter == null && getTop() != null) {
      nodeAdapter = findNodeAdapter( new NodeAdapter( getTop() ), componentID );
    }

    if(nodeAdapter!=null && nodeAdapter.getRootType() == MENU) {
      return nodeAdapter.getMenu();
    }

    return null;
  }

  @Override
  public Node findToolBarItem( QName componentID ) {
    NodeAdapter nodeAdapter = findNodeAdapter( new NodeAdapter( root ), componentID );

    if(nodeAdapter!=null && nodeAdapter.getRootType() == TOOLBAR) {
      return nodeAdapter.getToolBar().lookup( "#"+componentID.getName() );
    }

    if(nodeAdapter!=null && nodeAdapter.getNode()!=null) {
      return nodeAdapter.getNode();
    }

    return null;
  }

  public synchronized List<NodeAdapter> getChildrenComplete( NodeAdapter nodeAdapter) {
    List<NodeAdapter> childNodes = new ArrayList<>( );

    for (NodeAdapter adapter : nodeAdapter.getChildrenUnmodifiable()) {
      childNodes.add( adapter );
      childNodes.addAll( getChildrenComplete(adapter) );
    }
    return childNodes;
  }

  public synchronized NodeAdapter findNodeAdapter( NodeAdapter nodeAdapter, QName componentID ) {
    NodeAdapter foundAdapter = null;
    for (NodeAdapter adapter : nodeAdapter.getChildrenUnmodifiable()) {
      if(componentID.getName().equals( adapter.getId() )) {
        foundAdapter =  adapter;
        break;
      }
      if(foundAdapter == null) {
        foundAdapter = findNodeAdapter( adapter, componentID );
      }
    }
    return foundAdapter;
  }

  @Override
  public void show( de.pidata.gui.event.Dialog parent, boolean fullScreen ) {
    this.parentDialog = (PIDockingDialog) parent;
    setFullScreen( fullScreen );;
    setOnCloseRequest( this );

    //--- Place window near mouse pointer
    Robot robot = new Robot();
    double x = robot.getMousePosition().getX() + 10;
    double y = robot.getMousePosition().getY() - 30;

    this.setX(x);
    this.setY(y);

    open();
    this.toFront();

    DialogControllerDelegate delegate = dialogController.getDelegate();
    if (delegate != null) {
      delegate.dialogShowing( dialogController );
    }
  }

  @Override
  public void toFront() {
    Platform.getInstance().runOnUiThread( () -> {
      requestFocus();
      if (!isAlwaysOnTop()) {
        // force top, revert immediately
        setAlwaysOnTop( true );
        setAlwaysOnTop( false );
      }
    });
  }

  @Override
  public void toBack() {
    Platform.getInstance().runOnUiThread( this::toBack );
  }

  /**
   * Show/hide busy animation if naksker pane exists (getScene().lookup("#maskerPane")).
   * Otherwise switch between busy cursor (true) and normal cursor (false).
   *
   * @param busy if true show busy, otherwise hide
   */
  @Override
  public void showBusy( boolean busy ) {
    if (Platform.getInstance().isOnUiThread()) {
      getScene().setCursor( busy ? Cursor.WAIT : Cursor.DEFAULT );
    }
    else {
      Platform.getInstance().runOnUiThread( new Runnable() {
        @Override
        public void run() {
          getScene().setCursor( busy ? Cursor.WAIT : Cursor.DEFAULT );
        }
      } );
    }
  }

  /**
   * Called by DialogController.close() before anything other is
   * done. By returning false this Dialog can interrupt closing if
   * the Platform supports to abort close requests (e.g. Android does
   * not support that). Java FX will try to close children and
   * abort closing if any child dialog need user feedback.
   *
   * @param ok true if closing is OK operation
   * @return false to abort closing
   */
  @Override
  public boolean closing( boolean ok ) {
    Dialog[] childDialogs = new Dialog[childDialogList.size()];
    childDialogList.toArray( childDialogs );
    for (Dialog childDlg : childDialogs) {
      DialogController childDlgController = childDlg.getController();
      if (childDlgController != null) {
        if (!childDlgController.close( false, this.dialogController )) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public void close( boolean ok, ParameterList resultList ) {
    Platform.getInstance().runOnUiThread( () -> {
      close();
    } );

    FXDialogManager.removeDialog( dialogID );

    if (parentDialog != null) {
      parentDialog.childClosed( this );
    }
    DialogController parentDlgCtrl = getController().getParentDialogController();
    if (parentDlgCtrl != null) {
      parentDlgCtrl.childDialogClosed( ok, resultList );
    }

    ((FXScreen) Platform.getInstance().getScreen()).setFocusDialog( parentDialog );
  }

  public void childClosed( FXDialogInterface childDialog ) {
    childDialogList.remove( childDialog );
  }

  @Override
  public QName getDialogID() {
    return dialogID;
  }

  @Override
  public String getSettingsID() {
    return dialogID.toString();
  }

  @Override
  public void speak( String text ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean getAlwaysOnTop() {
    return isAlwaysOnTop();
  }

  @Override
  public void setMinWidth( int width ) {
    ((FxDockWindow)this).setMinWidth( width );
  }

  @Override
  public void setMaxWidth( int width ) {
    ((FxDockWindow)this).setMaxWidth( width );
  }

  @Override
  public void setMinHeight( int height ) {
    ((FxDockWindow)this).setMinHeight( height );
  }

  @Override
  public void setMaxHeight( int height ) {
    ((FxDockWindow)this).setMaxHeight( height );
  }

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   */
  @Override
  public void openChildDialog( DialogType dialogDef, String title, ParameterList parameterList ) {
    QName dialogID = dialogDef.getID();
    String windowStyle = dialogDef.getWindowStyle();
    Boolean modal = dialogDef.getModal();
    if (PIDockingDialog.class.getName().equals( dialogDef.getDialogClass() )) {
      openChildDockingDialog( dialogID, parameterList );
    }
    else {
      openChildDialog( dialogID, title, parameterList, windowStyle, modal );
    }
  }

  /**
   * Opend child dialog with given dialogID and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void openChildDialog( DialogDef dialogDef, String title, ParameterList parameterList ) {
    QName dialogID = dialogDef.getID();
    String windowStyle = dialogDef.getWindowStyle();
    Boolean modal = dialogDef.getModal();
    if (PIDockingDialog.class.getName().equals( dialogDef.getDialogClass() )) {
      openChildDockingDialog( dialogID, parameterList );
    }
    else {
      openChildDialog( dialogID, title, parameterList, windowStyle, modal );
    }
  }

  private void openChildDockingDialog( QName dialogID, ParameterList parameterList ) {

    try {
      PIDockingDialog childDialog = (PIDockingDialog) FxDockFramework.createWindowForWithId( dialogID.getName(), parameterList );
      FxDockFramework.open( childDialog );
    }
    catch (Exception ex) {
      throw new IllegalArgumentException( "Error initializing dialog ID=" + dialogID, ex );
    }
  }


  private DialogController openChildDialog( QName dialogID, String title, ParameterList parameterList, String windowStyle, Boolean modal ) {
    Stage childStage = new Stage();
    Stage stage = (Stage) getScene().getWindow();
    if("DECORATED".equals( windowStyle )) {
      childStage.initStyle( StageStyle.DECORATED );

      if (stage.getIcons().stream().count() > 0) {
        childStage.getIcons().addAll( stage.getIcons() );
      }
    }
    else {
      childStage.initStyle( StageStyle.UTILITY );
      childStage.initOwner( stage );
    }

    if ((modal != null) && modal.booleanValue()) {
      childStage.initModality( Modality.WINDOW_MODAL );
    }
    else {
      childStage.initModality( Modality.NONE );
    }
    FXDialog childDialog = FXDialog.createFXDialog( dialogID, childStage );
    try {
      childDialog.load();
    }
    catch (IOException ex) {
      throw new IllegalArgumentException( "Could not load dialog ID="+ dialogID, ex );
    }

    Context context = getController().getContext();
    DialogController parentDlgCtrl = getController().getDialogController();
    DialogController childDlgCtrl = Platform.getInstance().getControllerBuilder().createDialogController( dialogID, context, childDialog, parentDlgCtrl );
    childDlgCtrl.setTitle( title );
    try {
      childDlgCtrl.setParameterList( parameterList );
      childDialog.setRootStyle( root.getStyle() );
      childDialog.show( this, false );
      childDialog.toFront();
      childDlgCtrl.activate( childDialog );
    }
    catch (Exception ex) {
      throw new IllegalArgumentException( "Error initializing dialog ID="+ dialogID, ex );
    }
    childDialogList.add( childDialog );
    return childDlgCtrl;
  }


  /**
   * Opens popup with given moduleDef and title
   *
   * @param popupCtrl
   * @param moduleGroup   module group to be shown in a Popup
   * @param title         title for the dialog, if null title from dialogDef is used
   */
  @Override
  public void showPopup( PopupController popupCtrl, ModuleGroup moduleGroup, String title ) {
    throw new UnsupportedOperationException("PIDockingDialog does not support Popup Windows yet.");
  }

  protected FXPopupDialog createChildPopup( QName dialogID, String title, int childHandle ) {
    throw new UnsupportedOperationException("PIDockingDialog does not support Popup Windows yet.");
  }

  /**
   * Hides current popup. If popup is not visible nothing happens.
   *
   * @param popupController
   */
  @Override
  public void closePopup( PopupController popupController ) {
    throw new UnsupportedOperationException("PIDockingDialog does not support Popup Windows yet.");
  }

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   */
  @Override
  public void showMessage( String title, String message ) {
    QuestionBoxParams params = new QuestionBoxParams( title, message );
    Platform.getInstance().runOnUiThread( () -> new FXMessageBox( params, this, getController() ));
  }

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showQuestion( String title, String message, String btnYesLabel, String btnNoLabel, String btnCancelLabel ) {
    QuestionBoxParams params = new QuestionBoxParams( title, message, "", btnYesLabel, btnNoLabel, btnCancelLabel );
    Platform.getInstance().runOnUiThread( () -> new FXMessageBox( params, getScene().getWindow(), getController() ));
  }

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a deafulat value for the inputfield
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showInput( String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel ) {
    QuestionBoxParams params = new QuestionBoxParams( title, message, defaultValue, btnOKLabel, null, btnCancelLabel );
    Platform.getInstance().runOnUiThread( () -> new FXInputDialog( params, getScene().getWindow(), getController() ));
  }

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  @Override
  public void showToast( String message ) {
    throw new UnsupportedOperationException("PIDockingDialog does not support Toasts yet.");
  }

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   */
  @Override
  public void showFileChooser( String title, FileChooserParameter fileChooserParams ) {
    File file;
    if ((fileChooserParams != null) && (FileChooserParameter.TYPE_OPEN_DIR.equals( fileChooserParams.getDialogType() ))) {
      DirectoryChooser directoryChooser = new DirectoryChooser();
      String dirPath = fileChooserParams.getDirPath();
      if (!Helper.isNullOrEmpty( dirPath )) {
        File initDir = new File( dirPath );
        if (initDir.exists()) {
          directoryChooser.setInitialDirectory( initDir );
        }
        else {
          Logger.warn( "FileChooser initDir does not exist: "+initDir.getAbsolutePath() );
        }
      }
      file = directoryChooser.showDialog( this.getScene().getWindow() );
    }
    else {
      FileChooser fileChooser = new FileChooser();
      fileChooser.setTitle( title );
      if (fileChooserParams != null) {
        String filePattern = fileChooserParams.getFilePattern();
        if (!Helper.isNullOrEmpty( filePattern )) {
          List<String> extensions = Arrays.asList( filePattern.split( ";" ) );
          FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter( filePattern, extensions );
          fileChooser.getExtensionFilters().setAll( filter );
        }
        String dirPath = fileChooserParams.getDirPath();
        if (!Helper.isNullOrEmpty( dirPath )) {
          File initDir = new File( dirPath );
          if (initDir.exists()) {
            fileChooser.setInitialDirectory( initDir );
          }
          else {
            Logger.warn( "FileChooser initDir does not exist: " + initDir.getAbsolutePath() );
          }
        }
      }
      file = fileChooser.showOpenDialog( this.getScene().getWindow() );
    }
    javafx.application.Platform.runLater( () -> {
      ParameterList resultList;
      if (file == null) {
        resultList = new FileChooserResult( null );
        getController().childDialogClosed( false, resultList );
      }
      else {
        String path;
        try {
          path = file.getCanonicalPath();
        }
        catch (IOException e) {
          Logger.warn( "Error getting canonical path, using path, ex=" + e );
          path = file.getPath();
        }
        Logger.info( "FileChooser result path=" + path );
        resultList = new FileChooserResult( path );
        getController().childDialogClosed( true, resultList );
      }
    } );
  }

  /**
   * Shows the platform specific image chooser dialog.
   *
   * @param imageChooserType type of the image chooser
   * @param fileName         name of file that has to be used, if image will be created (by using camera)
   */
  @Override
  public void showImageChooser( ImageChooserType imageChooserType, String fileName ) {
    throw new RuntimeException("TODO");
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param windowEvent the event which occurred
   */
  @Override
  public void handle( WindowEvent windowEvent ) {
    // Closing will be handled by the Docking Framework
  }

  @Override
  public void registerShortcut( QName command, ActionController controller ) {
    KeyCodeCombination keyCombination = buildKeyCodeCombination( command );
    if (keyCombination == null) return;

    Scene scene = getScene();
    scene.getAccelerators().put(
        keyCombination,
        new Runnable() {
          @Override public void run() {
            if (controller != null) {
              controller.doAction();
            }
          }
        }
    );
  }

  private KeyCodeCombination buildKeyCodeCombination( QName command ) {
    String cmdName = command.getName();
    String[] cmdParts = cmdName.split( "-" );

    KeyCode keyCode = null;
    List<KeyCombination.Modifier> modifierList = new ArrayList<>();
    for (String keyPart : cmdParts) {
      switch (keyPart) {
        case "Alt":
          modifierList.add( KeyCombination.ALT_DOWN );
          break;

        case "Shift":
          modifierList.add( KeyCombination.SHIFT_DOWN );
          break;

        case "Strg":
          modifierList.add( KeyCombination.SHORTCUT_DOWN );
          break;

        default:
          if (keyCode == null) {
            String keyName;
            if (keyPart.length() == 1) {
              // single character maps to upper case
              keyName = keyPart.toUpperCase();
            }
            else {
              keyName = keyPart;
            }
            keyCode = KeyCode.getKeyCode( keyName );
          }
          else {
            Logger.warn( "multiple key codes defined in shortcut [" + cmdName + "]" );
          }
      }
    }
    if (keyCode == null) {
      // no basic key defined - reject
      Logger.warn( "no key code defined in shortcut [" + cmdName + "]" );
      return null;
    }

    KeyCodeCombination keyCombination;
    if (modifierList.size() > 0) {
      KeyCombination.Modifier[] modifiers = new KeyCombination.Modifier[modifierList.size()];
      modifiers = modifierList.toArray( modifiers );
      keyCombination = new KeyCodeCombination( keyCode, modifiers );
    }
    else {
      keyCombination = new KeyCodeCombination( keyCode );
    }
    return keyCombination;
  }

  /**
   * Require rights at a specific platform.
   */
  @Override
  public void requireRights() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Add a RightsRequireListener for callback.
   *
   * @param rightsRequireListener
   */
  @Override
  public void addRightsRequireListener( RightsRequireListener rightsRequireListener ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void actionClose() {
    // Only do a close action if the pane the window contains is not the DeletedPane which will be added
    // on a Drag And Drop action to another window.
    if (!(getDockRootPane().getContent() instanceof DeletedPane)) {
      if (dialogController != null) {
        dialogController.close( true );
      }
    }
    if ( dialogController != null) {
     ((PIDockingDialog)dialogController.getDialogComp()).removeChildDialog( this );
    }

    super.actionClose();
  }

  public void addChildDialog( PIDockingDialog childDialog ) {
    if (!childDialogList.contains( childDialog )) {
      childDialogList.add( childDialog );
    }
  }

  public void removeChildDialog( PIDockingDialog childDialog ) {
    childDialogList.remove( childDialog );
  }

  public PIDockingPane getDialogPane() {
    return dialogPane;
  }
}

