package de.pidata.gui.docking;

import goryachev.common.util.GlobalSettings;
import goryachev.fx.OnWindowClosing;
import goryachev.fx.internal.LocalSettings;
import goryachev.fxdock.FxDockWindow;
import goryachev.fxdock.internal.FrameworkBase;
import goryachev.fxdock.internal.FxDockSchema;
import javafx.scene.Node;
import javafx.stage.WindowEvent;

import java.util.List;

public class PIDockingFrameworkBase extends FrameworkBase {

  @Override
  public int loadLayout() {

    int ct = FxDockSchema.getWindowCount();
    // restore in proper z order

    // For our applications we restore the 1 window (at pos 0) by ourselves and iterate only until i>=1
    for(int i=ct-1; i>=1; i--)
    {
      try
      {

        String prefix = FxDockSchema.windowID(i);
        // Create an empty window, the content will be loaded with FxDockSchema.loadLayout(prefix) bellow.
        FxDockWindow w = new PIDockingDialog();
        registerWindow( w );

        FxDockSchema.restoreWindow(prefix, w);

        Node n = FxDockSchema.loadLayout(prefix);
        w.setContent( n );

        LocalSettings settings = LocalSettings.find(w);
        if(settings != null)
        {
          String k = prefix + FxDockSchema.SUFFIX_BINDINGS;
          settings.loadValues(k);
        }

        FxDockSchema.loadContentSettings(prefix, w.getContent());

        w.show();
      }
      catch(Exception e)
      {
        log.error(e);
      }
    }
    return ct;
  }

  @Override
  public void registerWindow( FxDockWindow w ) {

    w.setOnCloseRequest( ( ev ) -> handleClose( w, ev ) );

    w.showingProperty().addListener( ( src, old, cur ) ->
    {
      if ((cur != null) && !(cur.booleanValue())) {
        unlinkWindow( w );
      }
    } );

    // We do not need to listen to focused events and just set the window to top of the list.
    if (!windowStack.contains( w )) {
      windowStack.add( w );
    }
  }

  @Override
  public void saveLayout() {
    List<FxDockWindow> ws = getWindows();
    int ct = ws.size();

    FxDockSchema.clearSettings();
    FxDockSchema.setWindowCount(ct);

    for(int i=0; i<ct; i++)
    {
      FxDockWindow w = ws.get(i);
      // For us save as registered.
      storeWindow(i, w);
    }

    GlobalSettings.save();
  }

  @Override
  protected void handleClose(FxDockWindow w, WindowEvent ev)
	{
		if(getWindowCount() == 1)
		{
			saveLayout();
		}

		OnWindowClosing ch = new OnWindowClosing(false);
		w.confirmClosing(ch);
		if(ch.isCancelled())
		{
			// don't close the window
			ev.consume();
		}
                else {
                  w.actionClose();
                }
	}
}
