package de.pidata.gui.javafx.component;

import de.pidata.gui.javafx.ui.FXCodeEditorAdapter;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author dsc
 */
public class CodeEditorTextChangedListener implements ChangeListener<String> {

  private final FXCodeEditorAdapter fxCodeEditorAdapter;
  private final CodeEditor fxCodeEditor;

  public CodeEditorTextChangedListener( FXCodeEditorAdapter fxCodeEditorAdapter ) {
    this.fxCodeEditorAdapter = fxCodeEditorAdapter;
    this.fxCodeEditor = fxCodeEditorAdapter.getFXCodeEditor();
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends String> observable, String oldValue, String newValue ) {
    fxCodeEditorAdapter.refreshTextChanges( observable, oldValue, newValue);
  }
}
