package de.pidata.gui.javafx.component;

import org.fxmisc.richtext.CodeArea;

/**
 * Wrapper for future extensibility.
 *
 * @author dsc
 */
public class CodeEditor extends CodeArea {
}
