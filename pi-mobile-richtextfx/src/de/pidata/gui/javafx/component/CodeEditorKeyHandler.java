package de.pidata.gui.javafx.component;

import javafx.event.EventHandler;
import javafx.scene.control.IndexRange;
import javafx.scene.input.KeyEvent;
import org.fxmisc.richtext.model.Paragraph;
import org.fxmisc.richtext.model.TwoDimensional;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static javafx.scene.input.KeyCode.ENTER;
import static javafx.scene.input.KeyCode.HOME;
import static javafx.scene.input.KeyCode.TAB;

/**
 * @author dsc
 */
public class CodeEditorKeyHandler implements EventHandler<KeyEvent> {

  private static final Pattern INDENT_PATTERN = Pattern.compile( "(^\\s*).*" );

  private int TABLENGTH = 4; // TODO: make configurable
  private String TABSTRING = String.format( "%" + TABLENGTH + "s", "" );

  private boolean autoindent = true;
  private boolean useTabSpaces = true;

  private final CodeEditor fxCodeEditor;

  public CodeEditorKeyHandler( CodeEditor fxCodeEditor ) {
    this.fxCodeEditor = fxCodeEditor;
  }

  public void setAutoindent( boolean autoindent ) {
    this.autoindent = autoindent;
  }

  public void setUseTabSpaces( boolean useTabSpaces ) {
    this.useTabSpaces = useTabSpaces;
  }

  public String getTabString() {
    return TABSTRING;
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param event the event which occurred
   */
  @Override
  public void handle( KeyEvent event ) {
    if (event.getCode().equals( TAB )) {
      if (!(event.isShortcutDown() || event.isAltDown() || event.isControlDown() || event.isMetaDown())) {
        // handle tab / shift+tab block indent
        handleBlockIndent( event );
      }
    }
    else if (event.getCode().equals( ENTER )) {
      handleAutoIndent( event );
    }
    else if (event.getCode().equals( HOME )) {
      handleHome( event );
    }
    // END: default handling is as expected
  }

  /**
   * Default handling just will set cursor to column 0. Desired behavior: toggle between line start and first non-whitespace character.
   *
   * @param event
   */
  private void handleHome( KeyEvent event ) {
    // handle home
    int caretPosition = fxCodeEditor.getCaretPosition();
    TwoDimensional.Position caretPos = fxCodeEditor.offsetToPosition( caretPosition, null );
    int caretLine = caretPos.getMajor();
    if (caretLine > 0) {
      Paragraph<Collection<String>, String, Collection<String>> lineParagraph = fxCodeEditor.getParagraph( caretLine );
      String lineText = lineParagraph.getText();
      Matcher indentMatcher = INDENT_PATTERN.matcher( lineText );
      int indentLength = 0;
      if (indentMatcher.find()) {
        String indent = indentMatcher.group( 1 );
        indentLength = indent.length();
      }
      int caretCol = caretPos.getMinor();
      int newCaretCol;
      if (caretCol == 0 || caretCol > indentLength) {
        newCaretCol = indentLength;
      }
      else {
        newCaretCol = 0;
      }
      if (event.isShiftDown()) {
        IndexRange selectedRange = fxCodeEditor.getSelection();
        int newSelectionEnd;
        if ( caretCol > selectedRange.getStart() ) {
          // caret at end of selection: mark up to selection start
          newSelectionEnd = selectedRange.getStart();
        }
        else {
          // caret already at the beginning of the line: mark up to old end
          newSelectionEnd = selectedRange.getEnd();
        }
        TwoDimensional.Position newAnchorPos = fxCodeEditor.offsetToPosition( newSelectionEnd, null );
        int newAnchorCol = newAnchorPos.getMinor();
        fxCodeEditor.selectRange( caretLine, newAnchorCol, caretLine, newCaretCol );
      }
      else {
        fxCodeEditor.moveTo( caretLine, newCaretCol );
      }

      // prevent bubbling
      event.consume();
    }
  }

  private void handleAutoIndent( KeyEvent event ) {
    if (autoindent) {
      // handle autoindent
      int caretPosition = fxCodeEditor.getCaretPosition();
      TwoDimensional.Position caretPos = fxCodeEditor.offsetToPosition( caretPosition, null );
      int caretLine = caretPos.getMajor();
      if (caretLine > 0) {
        Paragraph<Collection<String>, String, Collection<String>> prevParagraph = fxCodeEditor.getParagraph( caretLine );
        String prevText = prevParagraph.getText();
        Matcher indentMatcher = INDENT_PATTERN.matcher( prevText );
        if (indentMatcher.find()) {
          String prevIndent = indentMatcher.group( 1 );
          fxCodeEditor.insertText( caretPosition, "\n" + prevIndent );

          // prevent bubbling
          event.consume();
        }
      }
    }
  }

  private void handleBlockIndent( KeyEvent event ) {
    IndexRange selection = fxCodeEditor.getSelection();
    TwoDimensional.Position firstPos = fxCodeEditor.offsetToPosition( selection.getStart(), null );
    TwoDimensional.Position lastPos = fxCodeEditor.offsetToPosition( selection.getEnd(), null );
    int firstLine = firstPos.getMajor();
    int firstCol = firstPos.getMinor();
    int lastLine = lastPos.getMajor();
    int lastCol = lastPos.getMinor();

    int caretPosition = fxCodeEditor.getCaretPosition();
    TwoDimensional.Position caretPos = fxCodeEditor.offsetToPosition( caretPosition, null );
    int caretLine = caretPos.getMajor();
    int caretCol = caretPos.getMinor();
    if (event.isShiftDown()) {
      // remove indent from each line
      for (int curLine = firstLine; curLine <= lastLine; curLine++) {
        Paragraph<Collection<String>, String, Collection<String>> curParagraph = fxCodeEditor.getParagraph( curLine );
        String curText = curParagraph.getText();
        Matcher indentMatcher = INDENT_PATTERN.matcher( curText );
        if (indentMatcher.find()) {
          String curIndent = indentMatcher.group( 1 );
          if (curIndent.length() > 0) {
            int deleteLength = (curIndent.length() > TABLENGTH) ? TABLENGTH : curIndent.length();
            fxCodeEditor.deleteText( curLine, 0, curLine, deleteLength );
            if (curLine == lastLine) {
              lastCol = (deleteLength > 0 && lastCol > deleteLength) ? lastCol - deleteLength : 0;
            }
            if (curLine == firstLine) {
              firstCol = (deleteLength > 0 && firstCol > deleteLength) ? firstCol - deleteLength : 0;
            }
          }
        }
      }
    }
    else if (lastLine > firstLine) {
      // multiple lines selected: add indent to each line
      int toLine = (lastCol == 0) ? lastLine - 1 : lastLine;
      for (int curLine = firstLine; curLine <= toLine; curLine++) {
        insertTab( curLine, 0 );
        if (curLine == lastLine) {
          lastCol += TABLENGTH;
        }
        if (curLine == firstLine && firstCol > 0) {
          firstCol += TABLENGTH;
        }
      }
    }
    else {
      // single line (partially) selected
      if (lastCol != firstCol) {
        String selectedText = fxCodeEditor.getSelectedText();
        Pattern ctrlNoneWhiteSpaces = Pattern.compile( "\\S" );
        Matcher matchNoneWhiteSpaces = ctrlNoneWhiteSpaces.matcher( selectedText );
        if (matchNoneWhiteSpaces.find()) {
          insertTab( caretLine, 0 );
          firstCol += TABLENGTH;
          lastCol += TABLENGTH;
        }
        else {
          replaceWithTab( firstLine, firstCol, lastLine, lastCol );
        }
      }
      else {
        insertTab( caretLine, caretCol );
        firstCol += TABLENGTH;
        lastCol += TABLENGTH;
      }
    }

    // preserve selection - depends on original caret position
    if (caretLine < firstLine || caretLine > lastLine) {
      caretCol = 0;
    }
    else if (caretLine == firstLine) {
      caretCol = firstCol;
    }
    else if (caretLine == lastLine) {
      caretCol = lastCol;
    }
    if (caretLine <= firstLine) {
      fxCodeEditor.selectRange( lastLine, lastCol, caretLine, caretCol );
    }
    else {
      fxCodeEditor.selectRange( firstLine, firstCol, caretLine, caretCol );
    }

    // don't bubble tab
    event.consume();
  }

  /**
   * Insert a tab; respect if spaces should be used instead.
   *
   * @param firstLine
   * @param firstCol
   * @param lastLine
   * @param lastCol
   */
  private void replaceWithTab( int firstLine, int firstCol, int lastLine, int lastCol ) {
    if (useTabSpaces) {
      fxCodeEditor.replaceText( firstLine, firstCol, lastLine, lastCol, TABSTRING );
    }
    else {
      fxCodeEditor.replaceText( firstLine, firstCol, lastLine, lastCol, "\\t" );
    }
  }

  /**
   * Insert a tab; respect if spaces should be used instead.
   *
   * @param line
   * @param col
   */
  private void insertTab( int line, int col ) {
    if (useTabSpaces) {
      // replace tab with spaces
      fxCodeEditor.insertText( line, col, TABSTRING );
    }
    else {
      // add tab
      fxCodeEditor.insertText( line, col, "\\t" );
    }
  }

  /**
   * Replace tab with spaces
   *
   * @param strValue
   * @return
   */
  public String handleUseTabSpaces( String strValue ) {
    String newStrValue = strValue;
    if (useTabSpaces) {
      newStrValue.replace( "\t", getTabString() );
    }
    return newStrValue;
  }
}
