package de.pidata.gui.javafx.component;

import de.pidata.gui.component.base.ComponentColor;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import org.reactfx.collection.LiveList;
import org.reactfx.value.Val;

import java.util.List;
import java.util.function.IntFunction;

public class LineMarkerFactory implements IntFunction<Node> {

  private final LiveList<Integer> markLines;
  private final ComponentColor markerColor;

  public LineMarkerFactory( LiveList<Integer> markLines, ComponentColor markerColor ) {
    this.markLines = markLines;
    this.markerColor = markerColor;
  }

  /**
   * Applies this function to the given argument.
   *
   * @param lineNumber the function argument
   * @return the function result
   */
  @Override
  public Node apply( int lineNumber ) {
    Polygon triangle = new Polygon( 0.0, 0.0, 10.0, 5.0, 0.0, 10.0 );
    Color color = (markerColor == null) ? null : (Color) markerColor.getColor();
    triangle.setFill( color );

    ObservableValue<Boolean> visible = LiveList.collapse( markLines, ( List<Integer> s1 ) -> {
      return Boolean.valueOf( s1.contains( Integer.valueOf( lineNumber ) ) );
    } );

    triangle.visibleProperty().bind(
        Val.flatMap( triangle.sceneProperty(), scene -> {
          return scene != null ? visible : Val.constant( Boolean.FALSE );
        } ) );

    return triangle;
  }
}
