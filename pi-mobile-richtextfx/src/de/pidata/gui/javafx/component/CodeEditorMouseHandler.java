package de.pidata.gui.javafx.component;

import de.pidata.gui.javafx.ui.FXCodeEditorAdapter;
import de.pidata.string.Helper;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class CodeEditorMouseHandler implements EventHandler<MouseEvent> {

  private final FXCodeEditorAdapter fxCodeEditorAdapter;
  private final CodeEditor fxCodeEditor;

  public CodeEditorMouseHandler( FXCodeEditorAdapter fxCodeEditorAdapter ) {
    this.fxCodeEditorAdapter = fxCodeEditorAdapter;
    this.fxCodeEditor = fxCodeEditorAdapter.getFXCodeEditor();
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param event the event which occurred
   */
  @Override
  public void handle( MouseEvent event ) {
    if (event.getClickCount() == 2 && !event.isConsumed()) {
      // handle double click
      handleDoubleClick( event );
    }
  }

  private void handleDoubleClick( MouseEvent event ) {
    // word selection was moved to selection changed handler
  }

}
