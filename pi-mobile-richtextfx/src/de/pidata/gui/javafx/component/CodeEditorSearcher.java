package de.pidata.gui.javafx.component;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.controller.base.CodeEditorController;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.controller.base.TextController;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class CodeEditorSearcher extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID   the id of the event, see method description
   * @param source    original source of the event, may differ from eventGroup, e.g. when closing application
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller source, Model dataContext ) throws ServiceException {
    TextController searchTextField = (TextController) source.getDialogController().getController( GuiBuilder.NAMESPACE.getQName( "searchText" ) );
    TextController replaceTextField = (TextController) source.getDialogController().getController( GuiBuilder.NAMESPACE.getQName( "replaceText" ) );
    CodeEditorController editorController = (CodeEditorController) source.getDialogController().getController( GuiBuilder.NAMESPACE.getQName( "scriptText" ) );

    QName effectiveEventID = (eventID == null) ? this.eventID : eventID;
    String searchStr = searchTextField.getStringValue();
    String replaceStr = replaceTextField.getStringValue();
    if (effectiveEventID == CodeEditorController.SEARCH_NEXT || effectiveEventID == CodeEditorController.SEARCH_NEXT_SHORTCUT) {
      editorController.searchNext( searchStr );
    }
    else if (effectiveEventID == CodeEditorController.SEARCH_PREV || effectiveEventID == CodeEditorController.SEARCH_PREV_SHORTCUT) {
      editorController.searchPrev( searchStr );
    }
    else if (effectiveEventID == CodeEditorController.REPLACE_NEXT) {
      editorController.replaceNext( replaceStr );
    }
    else if (effectiveEventID == CodeEditorController.REPLACE_ALL) {
      editorController.replaceAll( replaceStr );
    }
    else if (effectiveEventID == CodeEditorController.ACTIVATE_SEARCH) {
      searchTextField.getView().setState( (short) 0, (short) 0, Boolean.TRUE, null, null, null );
    }
    else if (effectiveEventID == CodeEditorController.CLEAR_SEARCH) {
      searchTextField.setValue( "" );
    }
    else if (effectiveEventID == CodeEditorController.ACTIVATE_REPLACE) {
      replaceTextField.getView().setState( (short) 0, (short) 0, Boolean.TRUE, null, null, null );
    }
    else {
      Logger.warn( "unknown search event: " + eventID );
    }
  }
}
