package de.pidata.gui.javafx.component;

import de.pidata.gui.javafx.ui.FXCodeEditorAdapter;
import de.pidata.string.Helper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.IndexRange;

import java.util.regex.Pattern;

/**
 *
 * @author dsc
 */
public class CodeEditorSelectionChangedHandler implements ChangeListener<IndexRange> {

  private static final Pattern WORD_PATTERN = Pattern.compile( "\\W\\w+\\W" );

  private final FXCodeEditorAdapter fxCodeEditorAdapter;
  private final CodeEditor fxCodeEditor;

  public CodeEditorSelectionChangedHandler( FXCodeEditorAdapter fxCodeEditorAdapter ) {
    this.fxCodeEditorAdapter = fxCodeEditorAdapter;
    this.fxCodeEditor = fxCodeEditorAdapter.getFXCodeEditor();
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends IndexRange> observable, IndexRange oldValue, IndexRange newValue ) {
    synchronized (fxCodeEditorAdapter) {
      // highlight selected word
      boolean isWordSelected = false;
      String selectedText = fxCodeEditor.getSelectedText();
      IndexRange selectionRange = fxCodeEditor.getSelection();
      if (!Helper.isNullOrEmpty( selectedText )) {
        String addPreSpace = "";
        String addPostSpace = "";
        int startPos = selectionRange.getStart();
        if (startPos > 0) {
          startPos--;
        }
        else {
          addPreSpace = " ";
        }
        int endPos = selectionRange.getEnd();
        if (endPos < fxCodeEditor.getLength()) {
          endPos++;
        }
        else {
          addPostSpace = " ";
        }
        String matchText = fxCodeEditor.getText( startPos, endPos );
        isWordSelected = WORD_PATTERN.matcher( addPreSpace + matchText + addPostSpace ).matches();
        fxCodeEditorAdapter.refreshSelectionMarkers( isWordSelected ? selectedText : "" );
      }
    }
  }
}
