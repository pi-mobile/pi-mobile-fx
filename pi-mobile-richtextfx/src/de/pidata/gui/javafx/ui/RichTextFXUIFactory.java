package de.pidata.gui.javafx.ui;

import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.javafx.component.CodeEditor;
import de.pidata.gui.ui.base.UICodeEditorAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.CodeEditorViewPI;

public class RichTextFXUIFactory extends FXUIFactory {

  @Override
  public UICodeEditorAdapter createCodeEditorAdapter( UIContainer uiContainer, CodeEditorViewPI codeEditorViewPI ) {
    CodeEditor fxCodeEditor = (CodeEditor) findUIComp( uiContainer, codeEditorViewPI.getComponentID(), codeEditorViewPI.getModuleID() );
    if (fxCodeEditor == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID=" + codeEditorViewPI.getComponentID() );
    }
    return new FXCodeEditorAdapter( fxCodeEditor, codeEditorViewPI, uiContainer );
  }
}
