package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.SearchListener;
import de.pidata.gui.javafx.component.*;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.tree.Model;
import de.pidata.parser.ParseRange;
import de.pidata.parser.Parser;
import de.pidata.gui.ui.base.UICodeEditorAdapter;
import de.pidata.gui.view.base.CodeEditorViewPI;
import de.pidata.log.Logger;
import de.pidata.string.Helper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.IndexRange;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.reactfx.collection.LiveArrayList;
import org.reactfx.collection.LiveList;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.function.IntFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author dsc
 */
public class FXCodeEditorAdapter extends FXValueAdapter implements UICodeEditorAdapter {

  private CodeEditor fxCodeEditor;
  private CodeEditorViewPI codeViewPI;

  private ChangeListener<? super String> stringChangeListener = null;
  private List<SearchListener> searchListenerList = new ArrayList<>();

  private final CodeEditorKeyHandler editorKeyHandler;
  private final CodeEditorMouseHandler editorMouseHandler;
  private final CodeEditorSelectionChangedHandler editorSelectionChangedListener;
  private final CodeEditorTextChangedListener editorTextChangedListener;

  private IntFunction<Node> lineNumberFactory;
  private List<IntFunction<Node>> markerFactoryList = new ArrayList<>();
  private Map<String, List> markerListMap = new HashMap<>();
  private Map<String, LiveList> markerLiveListMap = new HashMap<>();

  private Map<String, String> patternMap = new LinkedHashMap<>();
  private Pattern codePattern;

  private String searchText;
  private final ArrayList<IndexRange> findRangeList = new ArrayList();

  private String selectedText;
  private final ArrayList<IndexRange> selectionRangeList = new ArrayList();
  private boolean searchRegex = true;

  private Parser parser;

  public FXCodeEditorAdapter( CodeEditor fxCodeEditor, CodeEditorViewPI codeEditorViewPI, UIContainer uiContainer ) {
    super( fxCodeEditor, codeEditorViewPI, uiContainer );
    this.fxCodeEditor = fxCodeEditor;
    this.codeViewPI = codeEditorViewPI;

    editorKeyHandler = new CodeEditorKeyHandler( fxCodeEditor );
    editorMouseHandler = new CodeEditorMouseHandler( this );
    editorSelectionChangedListener = new CodeEditorSelectionChangedHandler( this );
    editorTextChangedListener = new CodeEditorTextChangedListener( this );
  }

  public void initListeners() {
    // add text changed listener
    this.fxCodeEditor.textProperty().addListener(editorTextChangedListener );

    // add selection changed listener
    this.fxCodeEditor.selectionProperty().addListener( editorSelectionChangedListener );

    // add key handling
    fxCodeEditor.addEventFilter( KeyEvent.KEY_PRESSED, editorKeyHandler );

    // add mouse handling
    fxCodeEditor.addEventHandler( MouseEvent.MOUSE_CLICKED, editorMouseHandler );
  }

  @Override
  public void addSearchListener( SearchListener searchListener ) {
    if (!searchListenerList.contains( searchListener )) {
      searchListenerList.add( searchListener );
    }
  }

  public void refreshTextChanges( ObservableValue<? extends String> observable, String oldValue, String newValue ) {
    synchronized (this) {
      computeSearchMarkers( searchText );
      computeSelectionMarkers( selectedText );

      // TODO: refresh...
      clearLineMarkers();

      computeStyles( newValue );
    }
  }

  public void refreshSelectionMarkers( String selectedText ) {
    computeSelectionMarkers( selectedText );
    computeStyles();
  }

  private void computeStyles( String newText ) {
    // add keyword highlighting
    fxCodeEditor.setStyleSpans( 0, computeHighlighting( newText ) );

    // search/find
    for (IndexRange range : findRangeList) {
      updateStyleInRange( range, "marksearch" );
    }

    // selection
    for (IndexRange range : selectionRangeList) {
      updateStyleInRange( range, "markword" );
    }
  }

  public void updateStyleInRange( IndexRange findRange, String styleClass ) {
    StyleSpans<Collection<String>> styles = fxCodeEditor.getStyleSpans( findRange );
    StyleSpans<Collection<String>> newStyles = styles.mapStyles( ( Collection<String> style ) -> {
      List<String> newStylesList = new ArrayList<>(style);
      newStylesList.add( styleClass );
      return newStylesList;
    } );
    fxCodeEditor.setStyleSpans(findRange.getStart(), newStyles);
  }

  private void computeStyles() {
    Platform.getInstance().runOnUiThread( () -> {
      Object value = getValue();
      if (value != null) {
        computeStyles( value.toString() );
      }
    } );
  }

  @Override
  public Object getValue() {
    return fxCodeEditor.getText();
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (value == null) {
          fxCodeEditor.replaceText( "" );
        }
        else {
          String strValue = value.toString();
          if(editorKeyHandler != null) {
            strValue = editorKeyHandler.handleUseTabSpaces( strValue );
          }
          fxCodeEditor.replaceText( strValue );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    fxCodeEditor.position( posY, posX );
  }

  @Override
  public void select( int fromPos, int toPos ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void selectAll() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      if (stringChangeListener == null) {
        stringChangeListener = new ChangeListener<String>() {
          @Override
          public void changed( ObservableValue<? extends String> observable, String oldValue, String newValue ) {
            codeViewPI.textChanged( oldValue, newValue );
          }
        };
        fxCodeEditor.textProperty().addListener( stringChangeListener );
      }
    }
    else {
      if (stringChangeListener != null) {
        fxCodeEditor.textProperty().removeListener( stringChangeListener );
        stringChangeListener = null;
      }
    }
  }

  @Override
  public void showLinenumbers( boolean show ) {
    if (show) {
      //IntFunction<String> format = (digits -> " %" + digits + "d ");
      lineNumberFactory = LineNumberFactory.get( fxCodeEditor );
    }
    else {
      lineNumberFactory = null;
    }
    createAndSetLinemarkerFactory();
  }

  @Override
  public void addLineMarker( String marker, List<Integer> markLines, ComponentColor markerColor ) {
    synchronized (this) {
      LiveArrayList<Integer> markLinesLiveWrapper = new LiveArrayList<>( markLines );
      markerListMap.put( marker, markLines );
      markerLiveListMap.put( marker, markLinesLiveWrapper );
      markerFactoryList.add( new LineMarkerFactory( markLinesLiveWrapper, markerColor ) );
    }
  }

  @Override
  public void refreshLineMarkers() {
    synchronized (this) {
      for (String marker : markerListMap.keySet()) {
        List markerList = markerListMap.get( marker );
        LiveList markerLiveWrapper = markerLiveListMap.get( marker );
        markerLiveWrapper.clear();
        markerLiveWrapper.addAll( markerList );
      }
    }
  }

  // FIXME: decide which to clear (e.g. model access) and which to refresh (e.g. syntax errors)!
  private void clearLineMarkers() {
    for (String marker : markerListMap.keySet()) {
      List markerList = markerListMap.get( marker );
      markerList.clear();
      LiveList markerLiveWrapper = markerLiveListMap.get( marker );
      markerLiveWrapper.clear();
    }
  }

  private void createAndSetLinemarkerFactory() {
    Platform.getInstance().runOnUiThread( () -> {
      IntFunction<Node> graphicFactory = line -> {
        HBox hbox = new HBox();
        ObservableList<Node> hboxChildren = hbox.getChildren();
        if (lineNumberFactory != null) {
          hboxChildren.add( lineNumberFactory.apply( line ) );
        }
        for (IntFunction<Node> markerFactory : markerFactoryList) {
          hboxChildren.add( markerFactory.apply( line ) );
        }
        hbox.setAlignment( Pos.CENTER_LEFT );
        hbox.getStyleClass().add( "linemarker" );
        return hbox;
      };
      fxCodeEditor.setParagraphGraphicFactory( graphicFactory );
    } );
  }

  @Override
  public void setAutoindent( boolean autoindent ) {
    if (editorKeyHandler != null) {
      editorKeyHandler.setAutoindent( autoindent );
    }
  }

  @Override
  public void setSearchRegex( boolean searchRegex ) {
    synchronized (this) {
      this.searchRegex = searchRegex;
      computeSearchMarkers( searchText );
      computeStyles();
    }
  }

  /**
   * content highlighting
   */
  private StyleSpans<Collection<String>> computeHighlighting( String text ) {
    int lastKwEnd = 0;
    StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();

    if (parser != null) {
      try {
        InputStream stream = new ByteArrayInputStream( text.getBytes() );
        parser.initBuffer( stream, "UTF-8" );
        parser.setDumpOnError( false );
        parser.doParse();
      }
      catch (Exception e) {
        // do not write to log
      }
      int len = 0;
      for (ParseRange parseRange : parser.getParseRanges()) {
        if (parseRange.getStartPos() > lastKwEnd) {
          spansBuilder.add( Collections.singleton( "blank" ), parseRange.getStartPos() - lastKwEnd );
          len += (parseRange.getStartPos() - lastKwEnd);
        }
        String typeName = parseRange.getParsedObject().getClass().getSimpleName().toLowerCase();
        spansBuilder.add( Collections.singleton( typeName ), parseRange.getEndPos() - parseRange.getStartPos() );
        len += parseRange.getEndPos() - parseRange.getStartPos();
        lastKwEnd = parseRange.getEndPos();
      }
    }
    else if (patternMap.size() > 0) {
      Matcher matcher = codePattern.matcher( text );
      while (matcher.find()) {
        String styleClass = null;
        for (String styleGroup : patternMap.keySet()) {
          if (matcher.group( styleGroup.toUpperCase() ) != null) {
            styleClass = styleGroup;
            break;
          }
        }
        assert styleClass != null;
        spansBuilder.add( Collections.emptyList(), matcher.start() - lastKwEnd );
        spansBuilder.add( Collections.singleton( styleClass ), matcher.end() - matcher.start() );
        lastKwEnd = matcher.end();
      }
    }
    spansBuilder.add( Collections.singleton( "blank" ), text.length() - lastKwEnd );
    return spansBuilder.create();
  }

  /**
   * Sets the parser to use for syntax highlighting. The simple class name of the parse object
   * contained in parser's ParseRange is the CSS name to be used for highlighting.
   *
   * Note: If a Parser exists pattern map is ignored.
   *
   * @param parser the parser to use or null toi remove the parser
   */
  @Override
  public void setCodeParser( Parser parser ) {
    synchronized (this) {
      this.parser = parser;
    }
  }

  @Override
  public void setCodePatternMap( Map<String, String> newPatternMap ) {
    synchronized (this) {
      patternMap.clear();
      patternMap.putAll( newPatternMap );

      StringBuilder sb = new StringBuilder();
      boolean isFirst = true;
      for (String styleClass : patternMap.keySet()) {
        if (!isFirst) {
          sb.append( "|" );
        }
        sb.append( "(?<" + styleClass.toUpperCase() + ">" + patternMap.get( styleClass ) + ")" );
        isFirst = false;
      }
      codePattern = Pattern.compile( sb.toString() );
      // initial styles
      computeStyles();
    }
  }

  /**
   * Select first find.
   *
   * @param searchStr
   */
  @Override
  public void searchAll( String searchStr ) {
    synchronized (this) {
      computeSearchMarkers( searchStr );
      computeStyles();
    }
  }

  @Override
  public void searchNext( String searchStr ) {
    synchronized (this) {
      // set caret position
      if (!findRangeList.isEmpty()) {
        int startSearchPos = fxCodeEditor.getCaretPosition();
        IndexRange foundRange = null;
        if (startSearchPos >= findRangeList.get( findRangeList.size() - 1 ).getEnd()) {
          foundRange = findRangeList.get( 0 );
        }
        else {
          for (int i = 0; i < findRangeList.size(); i++) {
            foundRange = findRangeList.get( i );
            if (foundRange.getStart() >= startSearchPos) {
              break;
            }
          }
        }
        if (foundRange != null && foundRange.getEnd() != startSearchPos) {
          fxCodeEditor.selectRange( foundRange.getStart(), foundRange.getEnd() );
          fxCodeEditor.requestFollowCaret();
          Platform.getInstance().runOnUiThread( () -> {
            getViewPI().setState( (short) 0, (short) 0, Boolean.TRUE, null, null, null );
          } );
        }
      }
    }
  }

  @Override
  public void searchPrev( String searchStr ) {
    synchronized (this) {
      // set caret position
      if (!findRangeList.isEmpty()) {
        int startSearchPos = fxCodeEditor.getCaretPosition();
        IndexRange foundRange = null;
        if (startSearchPos <= findRangeList.get( 0 ).getEnd()) {
          foundRange = findRangeList.get( findRangeList.size() - 1 );
        }
        else {
          for (int i = findRangeList.size() - 1; i >= 0; i--) {
            foundRange = findRangeList.get( i );
            if (foundRange.getEnd() < startSearchPos) {
              break;
            }
          }
        }
        if (foundRange != null && foundRange.getEnd() != startSearchPos) {
          fxCodeEditor.selectRange( foundRange.getStart(), foundRange.getEnd() );
          fxCodeEditor.requestFollowCaret();
          Platform.getInstance().runOnUiThread( () -> {
            getViewPI().setState( (short) 0, (short) 0, Boolean.TRUE, null, null, null );
          } );
        }
      }
    }
  }

  @Override
  public void replaceNext( String replaceStr ) {
    synchronized (this) {
      Pattern searchPattern = Pattern.compile( searchText, (searchRegex ? 0 : Pattern.LITERAL) );
      Matcher matcher = searchPattern.matcher( fxCodeEditor.getSelectedText() );
      if (matcher.matches()) {
        // replace only if current selection matches search - user might have moved caret
        IndexRange selectedRange = fxCodeEditor.getSelection();
        fxCodeEditor.replaceText( selectedRange, replaceStr );
      }
      // find next match
      searchNext( searchText );
    }
  }

  @Override
  public void replaceAll( String replaceStr ) {
    synchronized (this) {
      String currentText = fxCodeEditor.getText();
      String newText = currentText.replaceAll( searchText, replaceStr );
      fxCodeEditor.replaceText( newText );
    }
  }

  private void computeSearchMarkers( String searchStr ) {
    if (Helper.isNullOrEmpty( searchStr )) {
      // reset search
      this.searchText = "";
      findRangeList.clear();
    }
    else {
      // find
      this.searchText = searchStr;
      findRangeList.clear();

      try {
        Pattern searchPattern = Pattern.compile( searchText, (searchRegex ? 0 : Pattern.LITERAL) );
        Matcher matcher = searchPattern.matcher( fxCodeEditor.getText() );
        while (matcher.find()) {
          findRangeList.add( new IndexRange( matcher.start(), matcher.end() ) );
        }
      }
      catch (PatternSyntaxException e) {
        // do nothing - user is responsible
        Logger.info( "invalid search pattern: " + searchText );
      }
    }

    // notify listeners
    for (SearchListener listener : searchListenerList) {
      listener.searchChanged( searchText, findRangeList.size() );
    }
  }

  protected void computeSelectionMarkers( String selectedText ) {
    this.selectedText = selectedText;
    ArrayList<IndexRange> newSelectionRangeList = new ArrayList<>();
    if (!Helper.isNullOrEmpty( selectedText ) && !selectedText.equals( searchText )) {
      Pattern searchPattern = Pattern.compile( "\\W(" + selectedText + ")\\W" );
      Matcher matcher = searchPattern.matcher( fxCodeEditor.getText() );
      while (matcher.find()) {
        newSelectionRangeList.add( new IndexRange( matcher.start( 1 ), matcher.end( 1 ) ) );
      }
    }
    selectionRangeList.clear();
    selectionRangeList.addAll( newSelectionRangeList );
  }

  public CodeEditor getFXCodeEditor() {
    return fxCodeEditor;
  }
}
