# PI-Mobile-FX

PI-Mobile JavaFX platform for desktop applications. Based on PI-Mobile-Core project.

See [PI-Mobile-Core](https://gitlab.com/pi-mobile/pi-mobile-core) for documentation.
