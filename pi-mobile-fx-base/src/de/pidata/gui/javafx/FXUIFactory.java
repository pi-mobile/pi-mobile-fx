/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.javafx.controls.ProgressInfo;
import de.pidata.gui.javafx.controls.TableCellTextArea;
import de.pidata.gui.javafx.controls.ZoomableImageView;
import de.pidata.gui.javafx.ui.*;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.*;
import de.pidata.gui.view.base.*;
import de.pidata.gui.view.figure.*;
import de.pidata.gui.view.figure.path.PathElementType;
import de.pidata.log.Logger;
import de.pidata.log.Profiler;
import de.pidata.messages.ErrorMessages;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.QName;
import de.pidata.rect.*;
import de.pidata.string.Helper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Properties;
import java.util.WeakHashMap;

;

/**
 * Created by pru on 31.05.16.
 */
public class FXUIFactory implements UIFactory {

  public static final String CONTROL_INDENT = " ";
  public static final String NO_INDENT = "";
  private static WeakHashMap<Binary,Image> imageCache = new WeakHashMap<>();

  private static FXMLLoader fxmlLoader;
  protected static FXUIFactory instance;

  public FXUIFactory() {
    instance = this;
  }

  public static final FXUIFactory getInstance() {
    return instance;
  }

  public static void updateImage( ImageView view, Object value, ObservableValue widthProperty, Class resourceContext ) {
    Image img;
    // Note: nearly same implementation in
    // Class ImageController -> setValue() and
    // Class UIFactoryAndroid -> updateImageView()
    if (value == null) {
      img = null;
    }
    else if (value instanceof QName) {
      ComponentBitmap bitmap = Platform.getInstance().getBitmap( ((QName) value) );
      if (bitmap == null) {
        img = null;
      }
      else {
        img = (Image) bitmap.getImage();
      }
    }
    else if (value instanceof ComponentBitmap) {
      ComponentBitmap componentBitmap = (ComponentBitmap) value;
      img = (Image) componentBitmap.getImage();
    }
    else if (value instanceof Binary) {
      Binary binary = (Binary) value;
      img = imageCache.get( binary );
      if (img == null) {
        img = new Image( new ByteArrayInputStream( ((Binary) value).getBytes() ) );
        imageCache.put( binary, img );
      }
    }
    else {
      QName imageID = GuiBuilder.NAMESPACE.getQName( value.toString() );
      ComponentBitmap bitmap = Platform.getInstance().getBitmap( imageID );
      if (bitmap == null) {
        img = null;
      }
      else {
        img = (Image) bitmap.getImage();
      }
    }

    view.setPreserveRatio( true );
    if (widthProperty != null) {
      view.fitWidthProperty().bind( widthProperty );
    }
    view.setImage( img );
  }

  public Node createCellRendererNode( Cell cell, ColumnInfo columnInfo, QName rowDefID ) {
    Profiler.count( "FXUIFactory createCellRendererNode" );
    Node rendererNode = null;
    if (rowDefID == null) {
      if (columnInfo.getIconValueID() == null) {
        Controller renderCtrl = columnInfo.getRenderCtrl();
        if (renderCtrl instanceof FlagController) {
          CheckBox checkBox = new CheckBox();
          checkBox.setDisable( columnInfo.isReadOnly() );
          rendererNode = checkBox;
        }
        else if (renderCtrl instanceof ImageController) {
          rendererNode = getImageView( cell );
        }
        else if (renderCtrl instanceof WebViewController) {
          rendererNode = getWebView( cell, columnInfo );
        }
         else if (renderCtrl instanceof TextEditorController) {
          rendererNode = getTextArea( cell );
        }
        else {
          rendererNode = getTextField( cell );
        }
      }
      else {
        rendererNode = getImageView( cell );
      }
    }
    else {
      String resourceName = "/layout/"+rowDefID.getName()+".fxml";
      URL renderCompRes = columnInfo.getContext().getClass().getResource( resourceName );
      if (renderCompRes == null) {
        Logger.warn( "Render resource file not found, name="+resourceName );
      }
      else {
        try {
          if (fxmlLoader == null) {
            fxmlLoader = new FXMLLoader();
          }
          Node renderCompContainer = fxmlLoader.load( renderCompRes );
          Controller renderCtrl = columnInfo.getRenderCtrl();
          QName renderCompID;
          if (renderCtrl == null) {
            renderCompID = columnInfo.getBodyCompID();
          }
          else {
            renderCompID = columnInfo.getRenderCtrl().getView().getComponentID();
          }
          rendererNode = renderCompContainer.lookup( "#" + renderCompID.getName() );
          if (rendererNode == null) {
            Logger.warn( "RendererNode ID=" + renderCompID.getName() + " not found in fxml=" + resourceName );
          }
        }
        catch (IOException e) {
          Logger.error( "Error loading row resource name=" + resourceName, e );
        }
      }
    }
    if (rendererNode == null) {
      rendererNode = getTextField( cell );
    }
    rendererNode.setFocusTraversable( false );
    return rendererNode;
  }

  private static WebView getWebView( Cell cell, ColumnInfo columnInfo ) {
    WebView webView = new WebView();
    // Catch scroll events and fire them to the parent node, to make the list scroll.
    webView.setOnScroll( event -> Event.fireEvent(webView.getParent(), event) );
    webView.setDisable( columnInfo.isReadOnly() );

    webView.setOnMousePressed( event -> {
      event.consume();
      try {
        if (cell instanceof TableCell) {
          TableCell tableCell = (TableCell) cell;
          tableCell.getTableView().getSelectionModel().select( tableCell.getTableRow().getIndex(), tableCell.getTableColumn() );
        }
        if (cell instanceof TreeTableCell) {
          TreeTableCell treeTableCell = (TreeTableCell) cell;
          treeTableCell.getTreeTableView().getSelectionModel().select( treeTableCell.getTableRow().getIndex(), treeTableCell.getTableColumn() );
        }
      }
      catch (Exception e) {
        // In some cases an IndexOutOfBoundsException will appear, as we think due to event processing after the selection.
        // With the catch everything works as expected.
      }

    } );
    return webView;
  }

  private Node getImageView( Cell cell ) {
    HBox hBoxContainer = new HBox();

    ImageView imageView = new ImageView();
    hBoxContainer.getChildren().add( imageView );

    TextField textField = getTextField( cell );
    textField.setAlignment( javafx.geometry.Pos.CENTER_LEFT );
    HBox.setHgrow( textField, Priority.ALWAYS );
    hBoxContainer.getChildren().add( textField );

    hBoxContainer.prefWidthProperty().bind( cell.widthProperty().subtract( 2 ) );
    hBoxContainer.maxWidthProperty().bind( cell.widthProperty().subtract( 2 ) );

    hBoxContainer.setAlignment( javafx.geometry.Pos.CENTER_LEFT );

    return hBoxContainer;
  }

  private TextField getTextField( Cell cell ) {
    TextField textField = new TextField();
    textField.setDisable( false );
    textField.setEditable( false );
    textField.setStyle( "-fx-background-color: transparent; -fx-background-radius: 0 ; -fx-text-fill: black" );

    textField.setOnMousePressed( event -> {
      event.consume();
      try {
        if (cell instanceof TableCell) {
          TableCell tableCell = (TableCell) cell;
          tableCell.getTableView().getSelectionModel().select( tableCell.getTableRow().getIndex(), tableCell.getTableColumn() );
        }
        if (cell instanceof TreeTableCell) {
          TreeTableCell treeTableCell = (TreeTableCell) cell;
          treeTableCell.getTreeTableView().getSelectionModel().select( treeTableCell.getTableRow().getIndex(), treeTableCell.getTableColumn() );
        }
      }
      catch (Exception e) {
        // In some cases an IndexOutOfBoundsException will appear, as we think due to event processing after the selection.
        // With the catch everything works as expected.
      }

    } );
    return textField;
  }

  private TextArea getTextArea( Cell cell ) {
    TableCellTextArea textArea = new TableCellTextArea();

    textArea.addEventFilter(
        MouseEvent.MOUSE_PRESSED,
        event -> {
          try {
            if (cell instanceof TableCell) {
              TableCell tableCell = (TableCell) cell;
              tableCell.getTableView().getSelectionModel().select( tableCell.getTableRow().getIndex(), tableCell.getTableColumn() );
            }
            if (cell instanceof TreeTableCell) {
              TreeTableCell treeTableCell = (TreeTableCell) cell;
              treeTableCell.getTreeTableView().getSelectionModel().select( treeTableCell.getTableRow().getIndex(), treeTableCell.getTableColumn() );
            }
          }
          catch (Exception e) {
            // In some cases an IndexOutOfBoundsException will appear, as we think due to event processing after the selection.
            // With the catch everything works as expected.
          }
        }
    );

    return textArea;
  }

  private String renderValue( ColumnInfo columnInfo, Object value, boolean defaultString ) {
    Controller renderer = columnInfo.getRenderCtrl();
    if (renderer == null) {
      if (defaultString) {
        return StringRenderer.getDefault().render( value );
      }
      else {
        return null;
      }
    }
    else {
      return renderer.render( value );
    }
  }

  public void updateCell( Cell cell, ColumnInfo columnInfo, double columnPrefWidth ) {
    Node fxNode = cell.getGraphic();
    Model rowModel = ((UIContainer) cell).getDataContext();
    Object value = columnInfo.getCellValue( rowModel );
    String labelValue = columnInfo.getLabelValue( rowModel );

    if (fxNode != null) {

      handleCellNode( cell, columnInfo, columnPrefWidth, fxNode, value, rowModel, labelValue );

      if (columnInfo.getIndex() == 0 && cell instanceof TreeTableCell) {
        // For the first column of a TreeTableCell set the alignment to CENTER_LEFT, otherwise it will be above all other contents
        // due to the disclosure arrow
        cell.setAlignment( javafx.geometry.Pos.CENTER_LEFT );

        // Due to a bug in FX we have to set a text with a 'blank' to not have text or controls overlay the arrow to open children in TreeTable.
        if (Helper.isNullOrEmpty( cell.getText() )) {
         cell.setText( CONTROL_INDENT );
        }
      }

    }
    else {
      cell.setText( ErrorMessages.ERROR );
    }

    //--- Set  optional background color - actually disables row selection color for this cell
    QName bgColID = columnInfo.getBackgroundValue( rowModel );
    if (bgColID != null) {
      FXColor compCol = (FXColor) Platform.getInstance().getColor( bgColID );
      if (compCol == null) {
        Logger.warn( "FXPlatform did not know color, ID=" + bgColID );
      }
      else {
        cell.setStyle( "-fx-background-color:" + compCol.toStyleFormat()+";" );
      }
    }

    //--- Set tool tip
    String tooltip = columnInfo.getHelpValue( rowModel );
    if (tooltip == null) {
      cell.setTooltip( null );
    }
    else {
      cell.setTooltip( new Tooltip( tooltip ) );
    }
  }

  private void handleCellNode( Cell cell, ColumnInfo columnInfo, double columnPrefWidth, Node fxNode, Object value, Model rowModel, String labelValue ) {

    if (fxNode instanceof ImageView) {
      ImageView imageView = (ImageView) fxNode;

      QName iconValueID = columnInfo.getIconValueID();
      if (iconValueID == null) {
        if (columnInfo.getValueID() != null) {
          if (value instanceof Binary) {
            updateImage( imageView, value, cell.widthProperty(), columnInfo.getContext().getClass() );
          }
          else {
            String strVal = renderValue( columnInfo, value, false );
            if (value instanceof QName) {
              updateImage( imageView, ((QName) value).getNamespace().getQName( strVal ), cell.widthProperty(), columnInfo.getContext().getClass() );
            }
            else {
              updateImage( imageView, strVal, cell.widthProperty(), columnInfo.getContext().getClass() );
            }
          }
        }
      }
      else {
        Object iconValue = columnInfo.getIconValue( rowModel );
        updateImage( imageView, iconValue, null, columnInfo.getContext().getClass() );
        if (columnInfo.getValueID() != null) {
          String renderedValue = renderValue( columnInfo, value, false );
          if (Helper.isNotNullAndNotEmpty( renderedValue )) {
            cell.setText( renderedValue );
          }
        }
      }
    }
    else if (fxNode instanceof CheckBox) {
      if ((value == null) || (value instanceof Boolean)) {
        updateCheckBox( (CheckBox) fxNode, (Boolean) value, labelValue );
      }
      else {
        // value is shown on label position checkBox is not bound to model (used as button)
        String renderedValue = renderValue( columnInfo, value, false );
        if (Helper.isNullOrEmpty(renderedValue)) {
          updateCheckBox( (CheckBox) fxNode, Boolean.FALSE, NO_INDENT );
        }
        else {
          updateCheckBox( (CheckBox) fxNode, Boolean.FALSE, renderedValue );
        }
      }
    }
    else if (fxNode instanceof Label) {
      String renderedValue = renderValue( columnInfo, value, true );
      updateLabel( (Label) fxNode, renderedValue );
    }
    else if (fxNode instanceof Text) {
      String renderedValue = renderValue( columnInfo, value, true );
      updateText( (Text) fxNode, renderedValue );
    }
    else if (fxNode instanceof TextInputControl) {
      String renderedValue = renderValue( columnInfo, value, true );
      if (fxNode instanceof TextArea) {
        updateTextArea( (TextArea) fxNode, renderedValue );
      }
      else {
        updateTextInput( (TextInputControl) fxNode, renderedValue );
      }
    }
    else if (fxNode instanceof Button) {
      QName valueID = columnInfo.getValueID();
      QName iconValueID = columnInfo.getIconValueID();
      QName labelValueID = columnInfo.getLabelValueID();
      if (valueID != null && (iconValueID != null || labelValueID != null)) {
        throw new IllegalArgumentException( "valueID must not be set on a table button if iconValueID or labelValueID is set" );
      }
      if (valueID != null) {
        String renderedValue = renderValue( columnInfo, value, true );
        Node graphicNode = ((Button) fxNode).getGraphic();
        if (graphicNode instanceof ImageView) {
          updateImage( (ImageView) graphicNode, renderedValue, cell.widthProperty(), columnInfo.getContext().getClass() );
        }
        else {
          ((Button) fxNode).setText( renderedValue );
        }
        fxNode.setVisible( value != null );
      }
      else if (iconValueID != null || labelValueID != null) {
        Object iconValue = columnInfo.getIconValue( rowModel );
        if (iconValueID != null) {
          String renderedValue = renderValue( columnInfo, iconValue, true );
          Node graphicNode = ((Button) fxNode).getGraphic();
          if (!(graphicNode instanceof ImageView)) {
            graphicNode = new ImageView();
            ((ImageView) graphicNode).setFitHeight( ((Button) fxNode).getPrefHeight() - FXButtonAdapter.IMAGE_HEIGHT_OFFSET );
            ((ImageView) graphicNode).setFitWidth( ((Button) fxNode).getPrefWidth() - FXButtonAdapter.IMAGE_WIDTH_OFFSET );
            ((ImageView) graphicNode).setPreserveRatio( true );
            ((Button) fxNode).setGraphic( graphicNode );
          }
          updateImage( (ImageView) graphicNode, renderedValue, cell.widthProperty(), columnInfo.getContext().getClass() );
        }
        if (labelValueID != null) {
          String renderedValue = renderValue( columnInfo, labelValue, true );
          ((Button) fxNode).setText( renderedValue );
        }
        fxNode.setVisible( iconValue != null || !Helper.isNullOrEmpty( labelValue ) );
      }
    }
    else if (fxNode instanceof WebView) {
      String renderedValue = renderValue( columnInfo, value, true );
      updateWebView( (WebView) fxNode, renderedValue, cell, columnPrefWidth );
    }
    else if (fxNode instanceof HTMLEditor) {
      String renderedValue = renderValue( columnInfo, value, true );
      updateHtmlEditor( (HTMLEditor) fxNode, renderedValue );
    }
    else if (fxNode instanceof Pane) {
      updatePane( (Pane) fxNode, rowModel, columnInfo, cell);
    }
    else if (fxNode instanceof Group) {
      updateGroup( (Group) fxNode, value, columnInfo );
    }
    else {
      throw new IllegalArgumentException( "Unsupported view class=" + fxNode.getClass() );
    }
  }

  private void updateGroup( Group fxGroup, Object value, ColumnInfo columnInfo ) {
    PaintController renderCtrl = (PaintController) columnInfo.getRenderCtrl();
    Figure figure = null;
    if (value instanceof ComponentBitmap) {
      CombinedFigure bitmapFigure = new CombinedFigure();
      ShapeStyle style = new ShapeStyle( ComponentColor.LIGHTGRAY, (QName) null );
      ComponentBitmap bitmap = (ComponentBitmap) value;
      SimpleRect bounds = new SimpleRect( 0, 0, bitmap.getWidth(), bitmap.getHeight() );
      BitmapPI bitmapPI = new BitmapPI( bitmapFigure, bounds, bitmap, style );
      bitmapFigure.addShape( bitmapPI );
      figure = bitmapFigure;
    }
    else if (value instanceof Figure) {
      figure = (Figure) value;
    }
    PaintViewPI paintViewPI = (PaintViewPI) renderCtrl.getView();
    ObservableList<Node> groupChilds = fxGroup.getChildren();
    groupChilds.clear();
    if (figure != null) {
      for (int k = 0; k < figure.shapeCount(); k++) {
        ShapePI shapePI = figure.getShape( k );
        if (shapePI != null) {
          FXShapeAdapter shapeAdapter = (FXShapeAdapter) shapePI.getUIAdapter();
          if (shapeAdapter == null) {
            shapeAdapter = (FXShapeAdapter) createShapeAdapter( (UIPaintAdapter) paintViewPI.getUIAdapter(), shapePI );
          }
          groupChilds.add( shapeAdapter.getFxShape() );
        }
      }
    }
  }

  private void updatePane( Pane fxPane, Model valueModel, ColumnInfo columnInfo, Cell cell ) {

    Controller renderCtrl = columnInfo.getRenderCtrl();

    if (renderCtrl instanceof ModuleController) {
      ModuleGroup renderModule = ((ModuleController) renderCtrl).getCurrentModule();
      renderModule.setModel( valueModel );
      for (Controller childCtrl : renderModule.controllerIterator()) {
        handlePaneCellNode( fxPane, valueModel, columnInfo, childCtrl, cell );
      }
    }
    else {
      handlePaneCellNode( fxPane, valueModel, columnInfo, renderCtrl, cell );
    }

  }

  private void handlePaneCellNode( Pane fxPane, Model valueModel, ColumnInfo columnInfo, Controller controller, Cell cell ) {

    String cellNodeID = controller.getView().getComponentID().getName();
    Node fxNode = fxPane.lookup( "#" + cellNodeID );
    if (fxNode == null) {
      if (fxPane instanceof HBox) {
        handleIconAndText( valueModel, columnInfo, cell, (HBox) fxPane );
      }
      else {
        Logger.warn( "Did not find cell child node ID=" + cellNodeID );
      }
    }
    else {
      Object value;
      if (controller instanceof ValueController) {
        Binding binding = ((ValueController) controller).getValueBinding();
        if (binding == null) {
          value = controller.getValue();
        }
        else {
          value = binding.fetchModelValue( valueModel );
        }
      }
      else {
        value = controller.getValue();
      }

      String renderedStringValue = controller.render( value );

      if (fxNode instanceof Text) {
        updateText( (Text) fxNode, renderedStringValue );
      }
      if (fxNode instanceof TextField) {
        TextField textField = (TextField) fxNode;
        textField.setText( renderedStringValue );
      }
      else if (fxNode instanceof Label) {
        updateLabel( (Label) fxNode, renderedStringValue );
      }
      else if (fxNode instanceof TextInputControl) {
        if (fxNode instanceof TextArea) {
          updateTextArea( (TextArea) fxNode, renderedStringValue );
        }
        else {
          updateTextInput( (TextInputControl) fxNode, renderedStringValue );
        }
      }
      else if (fxNode instanceof CheckBox) {
        if ((value == null) || (value instanceof Boolean)) {
          String label = ((CheckBox) fxNode).getText();
          Boolean visible = null;
          if (controller instanceof FlagController) {
            Binding labelBinding = ((FlagController) controller).getLabelBinding();
            if (labelBinding != null) {
              Object modelValue = labelBinding.fetchModelValue( valueModel );
              if (modelValue instanceof String) {
                label = (String) modelValue;
              }
            }
            Binding visibilityBinding = ((FlagController) controller).getVisibilityBinding();
            if (visibilityBinding != null) {
              Object modelValue = visibilityBinding.fetchModelValue( valueModel );
              if (modelValue instanceof Boolean) {
                visible = (Boolean) modelValue;
              }
              if (visible == null) {
                visible = Boolean.TRUE;
              }
            }
          }
          if (visible == null) {
            //visibility by value
            updateCheckBox( (CheckBox) fxNode, (Boolean) value, label );
          }
          else {
            //visibility by visibilityBinding value
            updateCheckBox( (CheckBox) fxNode, (Boolean) value, label, visible.booleanValue() );
          }
        }
      }
      else if (fxNode instanceof ImageView) {
        if (columnInfo.getValueID() != null) {
          if (value instanceof Binary) {
            updateImage( (ImageView) fxNode, value, fxPane.widthProperty(), columnInfo.getContext().getClass() );
          }
          else {
            updateImage( (ImageView) fxNode, renderedStringValue, fxPane.widthProperty(), columnInfo.getContext().getClass() );
          }
        }
      }
      else if (fxNode instanceof Button) {
        Binding visibilityBinding = ((ButtonController) controller).getVisibilityBinding();
        if (visibilityBinding != null) {
          Object modelValue = visibilityBinding.fetchModelValue( valueModel );
          Boolean visible = null;
          if (modelValue instanceof Boolean) {
            visible = (Boolean) modelValue;
          }
          if (visible == null) {
            visible = Boolean.TRUE;
          }
          fxNode.setVisible( visible.booleanValue() );
        }
      }
    }
  }

  /**
   * Special handling for elements placed in a HBox with an icon and text both bound to a common controller.
   *
   * @param valueModel
   * @param columnInfo
   * @param cell
   * @param fxNode
   */
  private void handleIconAndText( Model valueModel, ColumnInfo columnInfo, Cell cell, HBox fxNode ) {

    Object value = columnInfo.getCellValue( valueModel );

    for (Node childNode : fxNode.getChildren()) {

      if (childNode instanceof ImageView) {

        ImageView imageView = (ImageView) childNode;
        QName iconValueID = columnInfo.getIconValueID();
        if (iconValueID == null) {
          if (columnInfo.getValueID() != null) {
            if (value instanceof Binary) {
              updateImage( imageView, value, cell.widthProperty(), columnInfo.getContext().getClass() );
            }
            else {
              String strVal = renderValue( columnInfo, value, false );
              if (value instanceof QName) {
                updateImage( imageView, ((QName) value).getNamespace().getQName( strVal ), cell.widthProperty(), columnInfo.getContext().getClass() );
              }
              else {
                updateImage( imageView, strVal, cell.widthProperty(), columnInfo.getContext().getClass() );
              }
            }
          }
        }
        else {
          Object iconValue = columnInfo.getIconValue( valueModel );
          updateImage( imageView, iconValue, null, columnInfo.getContext().getClass() );
        }
      }

      if (childNode instanceof TextField) {
        if (columnInfo.getValueID() != null) {
          String renderedValue = renderValue( columnInfo, value, false );
          TextField textField = (TextField) childNode;
          textField.setText( renderedValue );
        }
      }

    }
  }

  public void updateCheckBox( CheckBox view, Boolean value, String label ) {
    boolean visible = true;
    if(value == null){
      if (!view.isAllowIndeterminate()){
        //value null is invisible state for two-state-checkboxes
        visible = false;
      }
    }
    updateCheckBox( view, value, label, visible );
  }

  public void updateCheckBox( CheckBox view, Boolean value, String label, boolean visible ) {
    if (value == null) {
      if (view.isAllowIndeterminate()) {
        //null is indeterminate state for tri-state-checkboxes
        view.setIndeterminate( true );
      }
    }
    else {
      if (view.isAllowIndeterminate()) {
        view.setIndeterminate( false );
      }
      boolean oldSelected = view.isSelected();
      if (oldSelected != value.booleanValue()) {
        view.setSelected( value.booleanValue() );
      }
    }
    boolean oldVisible = view.isVisible();
    if (oldVisible != visible) {
      view.setVisible( visible );
    }
    String oldLabel = view.getText();
    if (!Objects.equals(oldLabel, label)) {
      view.setText( label );
    }
  }

  public void updateLabel( Label view, Object value ) {
    if (value == null) {
      view.setText( NO_INDENT );
    }
    else {
      view.setText( value.toString() );
    }
  }

  public void updateText( Text view, Object value ) {
    if (value == null) {
      view.setText( NO_INDENT );
    }
    else {
      view.setText( value.toString() );
    }
  }

  public void updateTextInput( TextInputControl view, Object value ) {
    if (value == null) {
      view.setText( NO_INDENT );
    }
    else {
      view.setText( value.toString() );
    }
  }

  public void updateTextArea( TextArea view, Object value ) {
    view.setWrapText( true );
    if (value == null) {
      view.setPrefRowCount( 1 );
      view.setText( NO_INDENT );
    }
    else {
      String[] lines = value.toString().split( "\n" );
      view.setPrefRowCount( lines.length ); // FIXME: does not respect that wrapping increases line count
      view.setText( value.toString() );
    }
  }

  public void updateWebView( WebView view, Object value, Cell cell, double columnPrefWidth ) {
    cell.setPrefSize( columnPrefWidth, 200 );

    WebEngine engine = view.getEngine();
    engine.setUserStyleSheetLocation("data:,body { font: 14px Arial; }");
    if (value == null) {
      engine.loadContent( NO_INDENT );
    }
    else {
      engine.loadContent( value.toString() );
    }
  }

  public void updateHtmlEditor( HTMLEditor view, Object value ) {
    if (value == null) {
      view.setHtmlText( NO_INDENT );
    }
    else {
      view.setHtmlText( value.toString() );
    }
  }

  public Node findUIComp( UIContainer uiContainer, QName componentID, QName moduleID ) {
    return ((FXUIContainer) uiContainer).findUIComp( componentID, moduleID );
  }

  @Override
  public void updateLanguage( UIContainer uiContainer, Properties textProps, Properties glossaryProps ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        for (String textID : textProps.stringPropertyNames()) {
          Node fxNode = findUIComp( uiContainer, GuiBuilder.NAMESPACE.getQName( textID ), null );
          if (fxNode != null) {
            if (fxNode instanceof TableView) {
              String[] colText = textProps.getProperty( textID ).split( "\\|" );
              ObservableList<TableColumn> colList = ((TableView) fxNode).getColumns();
              for (int i = 0; i < colList.size(); i++) {
                if (i < colText.length) {
                  String text = Helper.replaceParams( colText[i], "{", "}", glossaryProps );
                  colList.get( i ).setText( text );
                }
              }
            }
            else if (fxNode instanceof TabPane) {
              String[] colText = textProps.getProperty( textID ).split( "\\|" );
              ObservableList<Tab> colList = ((TabPane) fxNode).getTabs();
              for (int i = 0; i < colList.size(); i++) {
                if (i < colText.length) {
                  String text = Helper.replaceParams( colText[i], "{", "}", glossaryProps );
                  colList.get( i ).setText( text );
                }
              }
            }
            else if (fxNode instanceof Labeled) {
              String text = Helper.replaceParams( textProps.getProperty( textID ), "{", "}", glossaryProps );
              try {
                // The MenuButton text can not be set directly, instead the MenuItem logic has to be used.
                if (fxNode instanceof MenuButton) {
                  handleMenuItems( (FXUIContainer) uiContainer, textProps, glossaryProps, textID );
                }
                else {
                  ((Labeled) fxNode).setText( text );
                }

              }
              catch (Exception e) {
                Logger.error( e.getMessage() );
              }
            }
            else {
              Logger.warn( "Unsupported element for updateLanguage: " + fxNode.getClass().getName() );
            }
          }

          else {
            // If nothing was found try to search for MenuItem
            handleMenuItems( (FXUIContainer) uiContainer, textProps, glossaryProps, textID );

          }
        }
      }
    } );
  }

  private static void handleMenuItems( FXUIContainer uiContainer, Properties textProps, Properties glossaryProps, String textID ) {
    MenuItem menuItem = uiContainer.findMenuItem( GuiBuilder.NAMESPACE.getQName( textID ));
    if (menuItem != null) {
      String text = Helper.replaceParams( textProps.getProperty( textID ), "{", "}", glossaryProps );
      menuItem.setText( text );
    }

    Menu menu = uiContainer.findMenu( GuiBuilder.NAMESPACE.getQName( textID ));
    if (menu != null) {
      String text = Helper.replaceParams( textProps.getProperty( textID ), "{", "}", glossaryProps );
      menu.setText( text );
    }
  }

  @Override
  public UIListAdapter createListAdapter( UIContainer uiContainer, ListViewPI listViewPI ) {
    Node fxListView = findUIComp( uiContainer, listViewPI.getComponentID(), listViewPI.getModuleID() );
    if (fxListView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+listViewPI.getComponentID() );
    }
    if (fxListView instanceof ListView) {
      return new FXListAdapter( (ListView) fxListView, listViewPI, uiContainer );
    }
    else {
      return new FXChoiceAdapter( (ChoiceBox) fxListView, listViewPI, uiContainer );
    }
  }

  @Override
  public UIButtonAdapter createButtonAdapter( UIContainer uiContainer, ButtonViewPI buttonViewPI ) {

    Button fxButton = (Button) findUIComp( uiContainer, buttonViewPI.getComponentID(), buttonViewPI.getModuleID() );
    if (fxButton != null) {
      return new FXButtonAdapter( fxButton, buttonViewPI, uiContainer );
    }

    MenuItem menuItem = ((FXUIContainer) uiContainer).findMenuItem( buttonViewPI.getComponentID() );
    if (menuItem != null) {
      return new FXMenuItemAdapter( menuItem, buttonViewPI, uiContainer );
    }

    Node toolBarNode = ((FXUIContainer) uiContainer).findToolBarItem( buttonViewPI.getComponentID() );
    if (toolBarNode instanceof Button) {
      return new FXButtonAdapter( (Button) toolBarNode, buttonViewPI, uiContainer );
    }

    throw new IllegalArgumentException( "Could not find UI component, ID=" + buttonViewPI.getComponentID() );
  }

  @Override
  public UITextAdapter createTextAdapter( UIContainer uiContainer, TextViewPI textViewPI ) {
    Node fxText = findUIComp( uiContainer, textViewPI.getComponentID(), textViewPI.getModuleID() );
    if (fxText == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+textViewPI.getComponentID() );
    }
    if (fxText instanceof TextField) {
      return new FXTextFieldAdapter( (TextField) fxText, textViewPI, uiContainer );
    }
    else if (fxText instanceof Label) {
      return new FXLabelAdapter( (javafx.scene.control.Label) fxText, textViewPI, uiContainer );
    }
    else if (fxText instanceof Text) {
      return new FXTextAdapter( (Text) fxText, textViewPI, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Illegal JavaFX control for TextView, class="+fxText.getClass()+", compID="+textViewPI.getComponentID()+", expected TextField or Label" );
    }
  }

  @Override
  public UIDateAdapter createDateAdapter( UIContainer uiContainer, DateViewPI dateViewPI ) {
    DatePicker fxDatePicker = (DatePicker) findUIComp( uiContainer, dateViewPI.getComponentID(), dateViewPI.getModuleID() );
    if (fxDatePicker == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+dateViewPI.getComponentID() );
    }
    return new FXDateAdapter( fxDatePicker, dateViewPI, uiContainer );
  }

  @Override
  public UITextEditorAdapter createTextEditorAdapter( UIContainer uiContainer, TextEditorViewPI textEditorViewPI ) {
    TextArea fxTextArea = (TextArea) findUIComp( uiContainer, textEditorViewPI.getComponentID(), textEditorViewPI.getModuleID() );
    if (fxTextArea == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+textEditorViewPI.getComponentID() );
    }
    return new FXTextAreaAdapter( fxTextArea, textEditorViewPI, uiContainer );
  }

  @Override
  public UICodeEditorAdapter createCodeEditorAdapter( UIContainer uiContainer, CodeEditorViewPI codeEditorViewPI ) {
    throw new IllegalArgumentException( "CodeEditor not supported by standard FX platform." );
  }

  @Override
  public UIWebViewAdapter createWebViewAdapter( UIContainer uiContainer, WebViewPI webViewPI ) {
    TextFlow fxWebView = (TextFlow) findUIComp( uiContainer, webViewPI.getComponentID(), webViewPI.getModuleID() );
    if (fxWebView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+webViewPI.getComponentID() );
    }
    return new FXTextFlowAdapter( fxWebView, webViewPI, uiContainer );
  }

  @Override
  public UIHTMLEditorAdapter createHTMLEditorAdapter( UIContainer uiContainer, HTMLEditorViewPI htmlEditorViewPI ) {
    HTMLEditor fxHtmlEditor = (HTMLEditor) findUIComp( uiContainer, htmlEditorViewPI.getComponentID(), htmlEditorViewPI.getModuleID() );
    return new FXHTMLEditorAdapter( fxHtmlEditor, htmlEditorViewPI, uiContainer );
  }

  @Override
  public UINodeAdapter createNodeAdapter( UIContainer uiContainer, NodeViewPI nodeViewPI ) {
    Node fxNode = findUIComp( uiContainer, nodeViewPI.getComponentID(), nodeViewPI.getModuleID() );
    if (fxNode == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+nodeViewPI.getComponentID() );
    }
    return new FXNodeAdapter( fxNode, nodeViewPI, uiContainer );
  }


  @Override
  public UITableAdapter createTableAdapter( UIContainer uiContainer, TableViewPI tableViewPI ) {
    TableView fxTableView = (TableView) findUIComp( uiContainer, tableViewPI.getComponentID(), tableViewPI.getModuleID() );
    if (fxTableView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+tableViewPI.getComponentID() );
    }
    return new FXTableAdapter( fxTableView, tableViewPI, this, uiContainer );
  }

  @Override
  public UIImageAdapter createImageAdapter( UIContainer uiContainer, ImageViewPI imageViewPI ) {
    Node uiComp = findUIComp( uiContainer, imageViewPI.getComponentID(), imageViewPI.getModuleID() );
    if (uiComp != null && uiComp instanceof ZoomableImageView) {
      ZoomableImageView fxImageView = (ZoomableImageView) uiComp;
      return new FXZoomableImageAdapter( fxImageView, imageViewPI, uiContainer );
    }
    else if (uiComp != null && uiComp instanceof ImageView) {
      ImageView fxImageView = (ImageView) uiComp;
      return new FXImageAdapter( fxImageView, imageViewPI, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Could not find UI component, ID=" + imageViewPI.getComponentID() );
    }
  }

  @Override
  public UIFlagAdapter createFlagAdapter( UIContainer uiContainer, FlagViewPI flagViewPI ) {
    Node fxFlag = findUIComp( uiContainer, flagViewPI.getComponentID(), flagViewPI.getModuleID() );
    if (fxFlag == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+flagViewPI.getComponentID() );
    }
    if (fxFlag instanceof CheckBox) {
      return new FXFlagAdapter( (CheckBox) fxFlag, flagViewPI, uiContainer );
    }
    else if (fxFlag instanceof RadioButton) {
      return new FXFlagAdapter( (RadioButton) fxFlag, flagViewPI, uiContainer );
    }
    else if (fxFlag instanceof ToggleButton){
      return new FXFlagAdapter( (ToggleButton) fxFlag, flagViewPI, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Illegal JavaFX control for FlagView, class="+fxFlag.getClass()+", compID="+flagViewPI.getComponentID()+", expected CheckBox, RadioButton, ToggleButton or ToggleSwitch" );
    }
  }

  @Override
  public UIPaintAdapter createPaintAdapter( UIContainer uiContainer, PaintViewPI paintViewPI ) {
    Group fxCanvas = (Group) findUIComp( uiContainer, paintViewPI.getComponentID(), paintViewPI.getModuleID() );
    if (fxCanvas == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+paintViewPI.getComponentID() );
    }
    return new FXPaintAdapter( fxCanvas, paintViewPI, uiContainer );
  }

  @Override
  public UIProgressBarAdapter createProgressAdapter( UIContainer uiContainer, ProgressBarViewPI progressBarViewPI ) {
    Node fxNode = findUIComp( uiContainer, progressBarViewPI.getComponentID(), progressBarViewPI.getModuleID() );

    if (fxNode == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+progressBarViewPI.getComponentID() );
    }

    if (fxNode instanceof ProgressInfo) {
      return new FXProgressBarAdapter( (ProgressInfo) fxNode, progressBarViewPI, uiContainer );
    }

    if (fxNode instanceof Spinner) {
      return new FXSpinnerAdapter( (Spinner) fxNode, progressBarViewPI, uiContainer );
    }
    if (fxNode instanceof Slider) {
      return new FXSliderAdapter( (Slider) fxNode, progressBarViewPI, uiContainer );
    }

    throw new IllegalArgumentException( "Element is not type of UIProgressBarAdapter, ID="+progressBarViewPI.getComponentID() );
  }

  @Override
  public UITabPaneAdapter createTabPaneAdapter( UIContainer uiContainer, TabPaneViewPI tabPaneViewPI ) {
    TabPane fxTabPane = (TabPane) findUIComp( uiContainer, tabPaneViewPI.getComponentID(), tabPaneViewPI.getModuleID() );
    if (fxTabPane == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+tabPaneViewPI.getComponentID() );
    }
    return new FXTabPaneAdapter( fxTabPane, tabPaneViewPI, uiContainer );
  }

  @Override
  public UITreeTableAdapter createTreeTableAdapter( UIContainer uiContainer, TreeTableViewPI treeTableViewPI ) {
    TreeTableView fxTreeTableView = (TreeTableView) findUIComp( uiContainer, treeTableViewPI.getComponentID(), treeTableViewPI.getModuleID() );
    if (fxTreeTableView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+treeTableViewPI.getComponentID() );
    }
    return new FXTreeTableAdapter( fxTreeTableView, treeTableViewPI, this, uiContainer );
  }

  @Override
  public UITabGroupAdapter createTabGroupAdapter( UIContainer uiContainer, TabGroupViewPI tabGroupViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UITreeAdapter createTreeAdapter( UIContainer uiContainer, TreeViewPI treeViewPI ) {
    TreeView fxTreeView = (TreeView) findUIComp( uiContainer, treeViewPI.getComponentID(), treeViewPI.getModuleID() );
    if (fxTreeView == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+treeViewPI.getComponentID() );
    }
    return new FXTreeViewAdapter( fxTreeView, treeViewPI, this, uiContainer );
  }

  public static void addRotation( ObservableList<Transform> transforms, Rotation rotation ) {
    Pos center = rotation.getCenter();
    if (center instanceof PosDir) {
      addRotation( transforms, ((PosDir) center).getRotation() );
    }
    transforms.add( new Rotate( rotation.getAngle(), center.getX(), center.getY() ) );
  }

  private Point2D rotatePoint( Rotation rotation, double x, double y ) {
    Pos center = rotation.getCenter();
    Rotate rotate = new Rotate( rotation.getAngle(), center.getX(), center.getY() );
    if (center instanceof PosDir) {
      Point2D pointRot = rotatePoint( ((PosDir) center).getRotation(), x, y );
      return rotate.transform( pointRot.getX(), pointRot.getY() );
    }
    else {
      return rotate.transform( x, y );
    }
  }

  @Override
  public UIShapeAdapter createShapeAdapter( UIPaintAdapter uiPaintAdapter, ShapePI shapePI ) {
    Shape shape;
    ShapeType shapeType = shapePI.getShapeType();
    ShapeStyle shapeStyle = shapePI.getShapeStyle();
    Rect bounds = shapePI.getBounds();
    switch (shapeType) {
      case rect: {
        Rectangle rectangle = new Rectangle( bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight() );
        if(shapePI instanceof RectanglePI) {
          rectangle.setArcHeight( ((RectanglePI) shapePI).getCornerRadius() );
          rectangle.setArcWidth( ((RectanglePI) shapePI).getCornerRadius() );
        }
        shape = rectangle;
        break;
      }
      case bitmap: {
        Rectangle rectangle = new Rectangle( bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight() );
        ComponentBitmap bitmap = ((BitmapPI) shapePI).getBitmap();
        if (bitmap != null) {
          rectangle.setFill( new ImagePattern( (Image) bitmap.getImage() ) );
        }
        shape = rectangle;
        break;
      }
      case line: {
        Pos startPos = shapePI.getPos( 0 );
        Pos endPos = shapePI.getPos( 1 );
        shape = new Line( startPos.getX(), startPos.getY(), endPos.getX(), endPos.getY() );
        break;
      }
      case ellipse: {
        Pos center = shapePI.getPos(0 );
        shape = new Ellipse( center.getX(), center.getY(), bounds.getWidth() / 2, bounds.getHeight() / 2 );
        break;
      }
      case path: {
        if (shapePI instanceof PolyLineShapePI) {
          Polyline polyline = new Polyline();
          ObservableList<Double> points = polyline.getPoints();
          for (int i = 0; i < shapePI.posCount(); i++) {
            Pos pos = shapePI.getPos( i );
            points.add( Double.valueOf( pos.getX() ) );
            points.add( Double.valueOf( pos.getY() ) );
          }
          shape = polyline;
        }
        else {
          Path path = new Path();
          ObservableList<javafx.scene.shape.PathElement> pathElements = path.getElements();
          de.pidata.gui.view.figure.path.PathElement[] pathElementDefinition = ((PathShapePI) shapePI).getPathElementDefinition();
          for (int i = 0; i < pathElementDefinition.length; i++) {
            de.pidata.gui.view.figure.path.PathElement pathElement = pathElementDefinition[i];
            Pos pos = pathElement.getPos();
            PathElementType pathType = pathElement.getPathType();
            switch (pathType) {
              case none: {
                MoveTo moveTo = new MoveTo( (float) pos.getX(), (float) pos.getY() );
                pathElements.add( moveTo );
                break;
              }
              case arc: {
                double startAngle = ((ArcShapePI) shapePI).getStartAngle();
                double sweepAngle = ((ArcShapePI) shapePI).getSweepAngle();
                boolean useCenter = ((ArcShapePI) shapePI).getUseCenter();
                ArcTo arcTo = new ArcTo( bounds.getWidth()/2, bounds.getHeight()/2, startAngle, pos.getX(), pos.getY(), true, true );
                pathElements.add( arcTo );
                break;
              }
              case line:
              default: {
                LineTo lineTo = new LineTo( (float) pos.getX(), (float) pos.getY() );
                pathElements.add( lineTo );
              }
            }
          }
          shape = path;
        }
        break;
      }
      case arc: {
        double startAngle = ((ArcShapePI) shapePI).getStartAngle();
        double sweepAngle = ((ArcShapePI) shapePI).getSweepAngle();
        boolean useCenter = ((ArcShapePI) shapePI).getUseCenter();
        Arc arc = new Arc( bounds.getX() + bounds.getWidth() / 2, bounds.getY() + bounds.getHeight() / 2, bounds.getWidth() / 2, bounds.getHeight() / 2, startAngle + 90, - sweepAngle );
        shape = arc;
        break;
      }
      case text: {
        Pos textPos = shapePI.getPos( 0 );
        double textSize = bounds.getHeight();
        String text = shapePI.getText();
        String fontName = shapeStyle.getFontName();
        Font font;
        if (fontName == null) {
          font = Font.getDefault();
        }
        else {
          font = Font.font( shapeStyle.getFontName(), textSize );
        }

        TextAlignment alignment;
        double textX;
        if (text == null) {
          text = NO_INDENT;
        }
        Text txt = new Text( text );
        txt.setFont( font );
        double textY = textPos.getY() + textSize - txt.getBaselineOffset();
        switch (shapeStyle.getTextAlign()) {
          case RIGHT: {
            alignment = TextAlignment.RIGHT;
            textX = textPos.getX() + bounds.getWidth() - txt.getBoundsInLocal().getWidth();
            break;
          }
          case CENTER: {
            alignment = TextAlignment.CENTER;
            textX = textPos.getX() + ((bounds.getWidth() - txt.getBoundsInLocal().getWidth()) * 0.5);
            break;
          }
          case LEFT:
          default: {
            alignment = TextAlignment.LEFT;
            textX = textPos.getX();
          }
        }
        Text textShape = new Text( textX, textY, text );
        textShape.setTextAlignment( alignment );
        textShape.setFont( font );
        shape = textShape;
        break;
      }
      default: {
        //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
        throw new RuntimeException( "TODO - unsupported ShapeType="+shapeType );
      }
    }
    if (bounds instanceof RectDir) {
      Rotation rotation = ((RectDir) bounds).getRotation();
      if (rotation != null) {
        addRotation( shape.getTransforms(), rotation );
      }
    }
    FXPaintAdapter.applyShapeStyle( shape, shapeStyle );
    return new FXShapeAdapter( shape, shapePI );
  }

  @Override
  public UIFragmentAdapter createFragmentAdapter( UIContainer uiContainer, ModuleViewPI moduleViewPI ) {

    Pane root = (Pane) findUIComp( uiContainer, moduleViewPI.getComponentID(), moduleViewPI.getModuleID() );

    if (root == null) {
      throw new IllegalArgumentException( "Could not find UI component, ID="+moduleViewPI.getComponentID() );
    }

    return new FXFragmentAdapter( root, moduleViewPI, uiContainer );
  }

  @Override
  public UIAdapter createCustomAdapter( UIContainer uiContainer, ViewPI viewPI ) {
    throw new IllegalArgumentException( "Unsupported custom view class="+viewPI.getClass() );
  }
}
