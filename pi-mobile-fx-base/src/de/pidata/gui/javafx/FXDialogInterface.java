package de.pidata.gui.javafx;

import de.pidata.gui.event.Dialog;
import de.pidata.qnames.QName;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

public interface FXDialogInterface extends Dialog, FXUIContainer, EventHandler<WindowEvent> {

  void childClosed( FXDialogInterface childDialog );

  QName getDialogID();

  String getSettingsID();
}
