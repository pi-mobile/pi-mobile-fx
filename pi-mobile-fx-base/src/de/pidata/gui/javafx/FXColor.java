/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.component.base.ComponentColor;
import javafx.scene.paint.Color;

import java.util.Objects;

/**
 * @author dsc
 */
public class FXColor implements ComponentColor {

  private Color fxColor;

  public FXColor( Color fxColor ) {
    this.fxColor = fxColor;
  }

  @Override
  public Object getColor() {
    return fxColor;
  }

  @Override
  public void setColor( Object color ) {
    if (color != null && color instanceof Color) {
      fxColor = (Color)color;
    }
    else {
      fxColor = null;
    }
  }

  public String toStyleFormat() {
    int r = (int) (255 * fxColor.getRed());
    int g = (int) (255 * fxColor.getGreen());
    int b = (int) (255 * fxColor.getBlue());
    double alpha = fxColor.getOpacity();
    return String.format("rgba("+r+","+g+","+b+","+alpha+")");
  }

  @Override
  public int hashCode() {
    return Objects.hashCode( fxColor );
  }

  @Override
  public boolean equals( Object obj ) {
    if (!(obj instanceof FXColor)) {
      return false;
    }
    else if (fxColor == null) {
      return (((FXColor) obj).fxColor == null);
    }
    else {
      return fxColor.equals( ((FXColor) obj).fxColor );
    }
  }
}
