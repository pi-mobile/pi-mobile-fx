/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cga on 02.03.2017.
 */
public final class FXDialogManager {

  private static List<QName> dialogList = new ArrayList<>();

  public static boolean contains( QName dialogID ) {
    return dialogList.contains( dialogID );
  }

  public static void addDialog(QName dialogID) {
    dialogList.add( dialogID );
  }

  public static void removeDialog(QName dialogID) {
    dialogList.remove( dialogID );
  }

  public static int getDialogCount( QName dialogID ) {
    int dialogCount = 0;
    for (QName dialog: dialogList) {
      if( dialog.getName().equals( dialogID.getName() ) ){
        dialogCount++;
      }
    }
    return dialogCount;
  }
}
