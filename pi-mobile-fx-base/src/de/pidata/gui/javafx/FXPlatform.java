/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.connect.base.ConnectionController;
import de.pidata.connect.bluetooth.SPPConnection;
import de.pidata.connect.stream.MessageSplitter;
import de.pidata.gui.component.base.*;
import de.pidata.gui.component.base.Dialog;
import de.pidata.gui.component.general.SimpleGuiBuilder;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.layout.Layouter;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.ViewFactory;
import de.pidata.log.Logger;
import de.pidata.models.config.DeviceTable;
import de.pidata.models.tree.Context;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.linux.LinuxConnectionController;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import net.sf.image4j.codec.ico.ICODecoder;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by pru on 31.05.16.
 */
public class FXPlatform extends Platform {

  private FXApplication fxApplication;
  private FXControllerBuilder controllerBuilder = new FXControllerBuilder();
  private GuiBuilder guiBuilder = new SimpleGuiBuilder();
  private ViewFactory viewFactory = new ViewFactory();
  private FXScreen screen = new FXScreen();

  private Map<QName, ComponentColor> colors = new HashMap<>();

  public FXPlatform( FXApplication fxApplication, String[] args ) throws Exception {
    this.fxApplication = fxApplication;
    init( SystemManager.getInstance().createContext(), args );

    colors.put( ComponentColor.BLACK, new FXColor( Color.BLACK ) );
    colors.put( ComponentColor.GRAY, new FXColor( Color.GRAY ) );
    colors.put( ComponentColor.WHITE, new FXColor( Color.WHITE ) );
    colors.put( ComponentColor.BLUE, new FXColor( Color.BLUE ) );
    colors.put( ComponentColor.LIGHTGRAY, new FXColor( Color.LIGHTGRAY ) );
    colors.put( ComponentColor.RED, new FXColor( Color.RED ) );
    colors.put( ComponentColor.ORANGE, new FXColor( Color.ORANGE ) );
    colors.put( ComponentColor.CYAN, new FXColor( Color.CYAN ) );
    colors.put( ComponentColor.MAGENTA, new FXColor( Color.MAGENTA ) );
    colors.put( ComponentColor.GREEN, new FXColor( Color.GREEN ) );
    colors.put( ComponentColor.PINK, new FXColor( Color.PINK ) );
    colors.put( ComponentColor.YELLOW, new FXColor( Color.YELLOW ) );
    colors.put( ComponentColor.DARKGRAY, new FXColor( Color.DARKGRAY ) );
    colors.put( ComponentColor.DARKGREEN, new FXColor( Color.DARKGREEN ) );
    colors.put( ComponentColor.LIGHTGREEN, new FXColor( Color.LIGHTGREEN ) );
    colors.put( ComponentColor.TRANSPARENT, new FXColor( Color.TRANSPARENT ) );
  }

  @Override
  protected void initComm( Context context ) throws Exception {
    SystemManager sysMan = SystemManager.getInstance();

    String connCtrl = sysMan.getProperty("comm.connCtrl", null );
    if ((connCtrl == null) || (connCtrl.length() == 0) || "DEFAULT".equals(connCtrl)) {
      String os = System.getProperty("os.name").toLowerCase();
      if ((os.indexOf( "linux" ) >= 0) || (os.indexOf( "mac os" ) >= 0)) {
        sysMan.setConnectionController( new LinuxConnectionController() );
      }
      else if (os.indexOf( "win" ) >= 0) {
        sysMan.setConnectionController( new LinuxConnectionController() );
      }
      else {
        throw new IllegalArgumentException( "Unknown operating system, os.name="+os );
      }
    }
    else {
      sysMan.setConnectionController( (ConnectionController) Class.forName(connCtrl).newInstance() );
    }
  }

  /**
   * Returns this platform's name
   *
   * @return this platform's name
   */
  @Override
  public String getPlatformName() {
    return "JavaFX";
  }

  /**
   * Returns the platform specific statup file name for programName.
   * For example a Windows desktop platfrom appends programName with ".bat"
   *
   * @param programName the program name
   * @return the platform specific statup file name for programName
   */
  @Override
  public String getStartupFile( String programName ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the sorage wher startfiles are placed on this platform
   *
   * @return the sorage wher startfiles are placed on this platform
   */
  @Override
  public Storage getStartupFileStorage() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Creates ComponentBitmap with platform dependant Graphics of size width x height
   *
   * @param width  the new ComponentBitmap's width
   * @param height the new ComponentBitmap's height
   * @return a platform depandant ComponentBitmap
   */
  @Override
  public ComponentBitmap createBitmap( int width, int height ) {
    Image img = new WritableImage( width, height );
    return new FXImage( img, "PNG" );
  }

  /**
   * Loads a bitmap from the given file system path.
   *
   * @param path the path of a image file (gif or jpeg)
   * @return the bitmap loaded
   */
  @Override
  public ComponentBitmap loadBitmapFile( String path ) {
    Image image;
    String imageFormat = "PNG";
    String pathUpper = path.toUpperCase();
    if (pathUpper.endsWith( "JPG" )) {
      imageFormat = "JPG";
    }
    int pos = path.indexOf( ':' );
    if (pos > 1) {
      //baseCase: path is a valid image URL and not a Windows drive letter
      try {
        image = new Image( path );
      }
      catch (Exception e) {
        Logger.error( "error loading image from url: " + path, e );
        return null;
      }
    }
    else{
      //case: Image can be found in Files
      File imageFile = new File(path);
      if (imageFile.exists()) {
        try {
          if (path.toUpperCase().endsWith( "ICO" )) {
            imageFormat = "ICO";
            image = SwingFXUtils.toFXImage( ICODecoder.read(new FileInputStream( imageFile )).get( 0 ), null);
          }
          else {
            image = new Image( new FileInputStream( imageFile ) );
          }
        }
        catch (Exception ex) {
          Logger.error( "error loading image from file: " + path, ex );
          return null;
        }
      }
      else {
        //case: Image can be found in Resources
        return loadBitmapAsset( GuiBuilder.NAMESPACE.getQName( path ) );
      }
    }
    return new FXImage( image, imageFormat );
  }

  @Override
  public ComponentBitmap loadBitmapResource( QName bitmapID ) {
    ComponentBitmap componentBitmap;
    String imagePath = bitmapID.getName();
    try {
      String dirName = imagePath.substring( imagePath.indexOf( "@" ) + 1, imagePath.indexOf( "/" ) );
      String fileName = imagePath.substring( imagePath.indexOf( "/" ) + 1 );

      // Old implementation was searching for a matching resource which does not work when starting from jar
      // see https://stackoverflow.com/questions/10144210/java-jar-file-use-resource-errors-uri-is-not-hierarchical
      Class resourceContext = bitmapID.getNamespace().getClass();
      URL url = resourceContext.getResource( "/" + dirName + "/" + fileName + ".png" );
      Image img = new Image( url.toString() );
      componentBitmap = new FXImage( img, "PNG" );
    }
    catch (Exception e) {
      Logger.error( "image resource not found: " + imagePath );
      return null;
    }
    return componentBitmap;
  }

  @Override
  public ComponentBitmap loadBitmapAsset( QName bitmapID ) {
    ComponentBitmap componentBitmap;
    String path = bitmapID.getName();
    if (!path.startsWith( "/" )) {
      path = "/" + path;
    }

    Class resourceContext = bitmapID.getNamespace().getClass();
    URL imgURL = resourceContext.getResource( path );
    if (imgURL != null) {
      try {
        Image img;
        String imageFormat = "PNG";
        if (path.toUpperCase().endsWith( "ICO" )) {
          imageFormat = "ICO";
          img = SwingFXUtils.toFXImage( ICODecoder.read(imgURL.openStream()).get( 0 ), null  );
        }
        else {
          img = new Image( imgURL.toExternalForm() );
        }
        if (img == null) {
          return null;
        }
        else {
          componentBitmap = new FXImage( img, imageFormat );
        }
      }
      catch (Exception ex) {
        Logger.error( "error loading image from resource: " + path, ex );
        return null;
      }
    }
    else {
      Logger.error( "image asset not found: " + path );
      return null;
    }
    return componentBitmap;
  }

  @Override
  public ComponentBitmap loadBitmap( InputStream imageStream ) {
    Image image = new Image( imageStream );
    if (image.isError()) {
      Logger.error( "Error creating Image from InputStream" );
      return null;
    }
    String imageFormat = "PNG"; // FIXME: get from?
    FXImage fxImage = new FXImage( image, imageFormat );
    return fxImage;
  }

  /**
   * Loads a thumb nail image, fitting into rectangle width, height
   *
   * @param imageStorage
   * @param imageFileName
   * @param width         desired with
   * @param height        desired height   @return thumb nail for image from imageSteam
   */
  @Override
  public ComponentBitmap loadBitmapThumbnail( Storage imageStorage, String imageFileName, int width, int height ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public ComponentColor setColor( QName name, int red, int green, int blue, double alpha ) {
    FXColor color = new FXColor( Color.rgb( red, green, blue, alpha ) );
    this.colors.put( name, color );
    return color;
  }

  @Override
  public ComponentColor getColor( QName colorID ) {
    ComponentColor color = this.colors.get( colorID );
    if (color == null) {
      throw new IllegalArgumentException( "Unknown color ID=" + colorID );
    }
    return color;
  }

  @Override
  public void setColor( QName colorID, String value ) {
    this.colors.put( colorID, getColor( value ) );
  }

  @Override
  public void setFont( QName fontID, String name, QName style, int size ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public ComponentFont createFont( String name, QName style, int size ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public ComponentFont getFont( QName fontID ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this platform's screen object
   *
   * @return this platform's screen object
   */
  @Override
  public Screen getScreen() {
    while (screen == null) {
      try {
        Thread.sleep(1000);
      }
      catch (InterruptedException e) {
        e.printStackTrace();
        break;
      }
    }
    return this.screen;
  }

  @Override
  public ComponentFactory getComponentFactory() {
    return null;
  }

  @Override
  public InputManager getInputManager() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set some platform characteristics, e.g. make skinDialog focusable in JDK 1.4,
   * toggle keyboard on screen.
   * This method is needed since j2sdk1.4.* requires
   * calling setFocusable on Windows to allow keyboard input.
   *
   * @param dialog the dialog to make focusable
   */
  @Override
  public void initWindow( Object dialog ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Shows or hides the on screen keyboard
   *
   * @param hasFocus if true the on screen keyboard is shown, otherwise hidden
   */
  @Override
  public void toggleKeyboard( boolean hasFocus ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean isSingleWindow() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean useDoubleBuffering() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * @param colorString
   * @return
   */
  @Override
  public ComponentColor getColor( String colorString ) {
    if (Helper.isNullOrEmpty( colorString )) {
      return null;
    }
    else if (colorString.equals( "none" )) {
      return null;
    }
    else {
      Color color = Color.valueOf( colorString );
      return new FXColor( color );
    }
  }

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @return
   */
  public ComponentColor getColor( int red, int green, int blue ) {
    return new FXColor( Color.rgb( red, green, blue ) );
  }

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @param alpha alpha value / opacity (0.0 .. 1.0)
   * @return
   */
  public ComponentColor getColor( int red, int green, int blue, double alpha ) {
    int alphaInt = (int) alpha * 255;
    return new FXColor( Color.rgb( red, green, blue, alphaInt ) );
  }

  /**
   * @param layouterX
   * @param layouterY
   * @param x
   * @param y
   * @param width
   * @param height
   */
  @Override
  public Dialog createDialog( Layouter layouterX, Layouter layouterY, short x, short y, short width, short height ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this platform's navigation capability: full keyboard, 4-way or 2-way
   *
   * @return one of the constants NAVKEYS_*
   */
  @Override
  public QName getNavigationKeys() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called to exit application, e.g. via System.exit(0) on J2SE desktop
   *
   * @param context
   */
  @Override
  public void exit( Context context ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns true if FontMetrics.charWidth() is working on this Platform. On some platforms
   * charWidth returns 0 on others (e.g. J0 on WinMobile 5) it takes more than 500ms per character.
   *
   * @return true if FontMetrics.charWidth() is working on this Platform
   */
  @Override
  public boolean isCharWidthUsable() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public ControllerBuilder getControllerBuilder() {
    return controllerBuilder;
  }

  @Override
  protected GuiBuilder createGuiBuilder() {
    return guiBuilder;
  }

  @Override
  protected void openInitialDialog( Context context, QName opID ) {
    fxApplication.setInitialDialog( context, opID );
  }

  @Override
  public UIFactory getUiFactory() {
    UIFactory uiFactory = FXUIFactory.getInstance();
    if (uiFactory == null) {
      return new FXUIFactory(); 
    }
    return uiFactory;
  }

  @Override
  public PlatformScheduler createScheduler() {
    return null;
  }

  public Application getApplication() {
    return fxApplication;
  }

  @Override
  public boolean hasTableFirstRowForEmptySelection() {
    return false;
  }

  @Override
  public void getPairedBluetoothDevices( DeviceTable deviceTable ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public SPPConnection createBluetoothSPPConnection( String serverAddress, String uuidString, MessageSplitter messageSplitter ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public MediaInterface getMediaInterface() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean isOnUiThread() {
    return javafx.application.Platform.isFxApplicationThread();
  }

  /**
   * Open url using System default web browser.
   * Uses two fallback strategies if java.awtDesktop does not support Action.BROWSE
   * @param url the url to be opened
   * @return false if an error/exception occurred calling browser
   */
  @Override
  public boolean openBrowser( String url ) {
    try {
      if (Desktop.isDesktopSupported()) {
        Desktop desktop = Desktop.getDesktop();

        try {
          // If it is not an URI we will get an URISyntaxException
          URI browserUri = new URI( url );
          if (desktop.isSupported( Desktop.Action.BROWSE )) {
            desktop.browse( browserUri );
            return true;
          }
        }
        catch (URISyntaxException uriEx) {
          Logger.debug( url + " is not an URI." );
        }

        // Try to open file.
        if (desktop.isSupported( Desktop.Action.OPEN )) {
          desktop.open( new File( url ) );
          return true;
        }
      }
    }
    catch (Exception e) {
      Logger.warn( "Error opening System Browser with AWT Desktop. Trying by native process. Url="+url );
    }

    Runtime runtime = Runtime.getRuntime();
    String os = System.getProperty( "os.name" ).toLowerCase();
    Logger.debug( "Try to open System Browser for OS: " + os );

    try {
      if (os.indexOf( "win" ) >= 0) {
        runtime.exec( "rundll32 url.dll,FileProtocolHandler " + url );
      }
      else if (os.indexOf( "mac" ) >= 0) {
        runtime.exec( "open " + url );
      }
      else {
        runtime.exec( "xdg-open " + url );
      }
      return true;
    }
    catch (Exception ex) {
      Logger.error( "Error opening System Browser native for OS: " + os, ex );
      return false;
    }
  }

  @Override
  public NfcTool getNfcTool() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void runOnUiThread( Runnable runnable ) {
    if(javafx.application.Platform.isFxApplicationThread()) {
      runnable.run();
    }
    else {
      javafx.application.Platform.runLater( runnable );
    }
  }
}
