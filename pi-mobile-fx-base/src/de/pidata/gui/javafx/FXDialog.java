/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.controller.file.FileChooserResult;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.javafx.ui.NodeAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rights.RightsRequireListener;
import de.pidata.service.base.ParameterList;
import de.pidata.string.Helper;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Region;
import javafx.stage.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.prefs.Preferences;

import static de.pidata.gui.javafx.ui.NodeAdapter.RootType.*;

/**
 * Created by pru on 31.05.16.
 */
public class FXDialog implements FXDialogInterface {

  /** holds the loaded modules; necessary to find identical instances of JavaFX elements when searching for them */
  private Map<QName, Node> moduleMap = new HashMap<>();

  protected QName dialogID;
  protected Parent root;
  protected Scene scene;
  protected Stage stage;
  protected FXDialogInterface parentDialog;
  protected List<Dialog> childDialogList = new ArrayList<>();
  private FXPopupDialog childPopUp;
  protected DialogController dialogController;

  private static final String WINDOW_POSITION_X = "Window_Position_X";
  private static final String WINDOW_POSITION_Y = "Window_Position_Y";
  private static final String WINDOW_WIDTH = "Window_Width";
  private static final String WINDOW_HEIGHT = "Window_Height";
  private static final String ALLWAYS_ON_TOP = "Window_OnTop";
  private static final String MAXIMIZED = "Maximized";

  public FXDialog( QName dialogID, Stage stage ) {
    this.dialogID = dialogID;
    this.stage = stage;
  }

  public void load() throws IOException {
    URL mainDlgRes = getClass().getResource( "/layout/"+dialogID.getName()+".fxml" );
    Logger.info( "Load FXML: " + "/layout/"+dialogID.getName()+".fxml" );
    root = FXMLLoader.load( mainDlgRes );
    loadDialogSettings();
  }

  public Scene getScene() {
    if (scene == null) {
      scene = new Scene( root );
    }
    return scene;
  }

  /**
   * Set UI Style
   *
   * @param style
   */
  @Override
  public void setStyle( String style ) {
    root.setStyle( style );
  }

  public Stage getStage() {
    return stage;
  }

  @Override
  public void onTitleChanged( String title ) {
    Platform.getInstance().runOnUiThread( () -> stage.setTitle( title ) );
  }

  @Override
  public String getTitle() {
    return stage.getTitle();
  }

  /**
   * Sets the controller for this dialog.
   *
   * @param dialogController this dialog's controller
   */
  @Override
  public void setController( DialogController dialogController ) {
    this.dialogController = dialogController;
    onTitleChanged( dialogController.getTitle() );
  }

  /**
   * Returns this dialog's controller
   *
   * @return this dialog's controller
   */
  @Override
  public DialogController getController() {
    return dialogController;
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    return getController().getModel();
  }

  /**
   * Returns the Node identified by uiCompID within this FXUIContainer
   * @param uiCompID component (Node) identifier
   * @param moduleID
   */
  public Node findUIComp( QName uiCompID, QName moduleID ) {
    if (moduleID != null) {
      Node moduleNode = moduleMap.get( moduleID );
      if (moduleNode == null) {
        String resourceName = "/layout/" + moduleID.getName() + ".fxml";
        URL editorModuleRes = getClass().getResource( resourceName );
        try {
          moduleNode = FXMLLoader.load( editorModuleRes );
          moduleMap.put( moduleID, moduleNode );
        }
        catch (IOException e) {
          Logger.error( "Error loading module resource name=" + resourceName, e );
        }
      }
      if (moduleNode == null) {
        throw new IllegalArgumentException( "Could not find UI module, moduleID="+moduleID );
      }
      return moduleNode.lookup( "#"+uiCompID.getName() );
    }
    else {
      return root.lookup( "#" + uiCompID.getName() );
    }
  }

  @Override
  public MenuItem findMenuItem( QName componentID ) {
    NodeAdapter nodeAdapter = findNodeAdapter( new NodeAdapter( root ), componentID );

    if(nodeAdapter!=null && nodeAdapter.getRootType() == MENU_ITEM) {
      return nodeAdapter.getMenuItem();
    }

    return null;
  }

  @Override
  public Menu findMenu( QName componentID ) {
    NodeAdapter nodeAdapter = findNodeAdapter( new NodeAdapter( root ), componentID );

    if(nodeAdapter!=null && nodeAdapter.getRootType() == MENU) {
      return nodeAdapter.getMenu();
    }

    return null;
  }

  @Override
  public Node findToolBarItem( QName componentID ) {
    NodeAdapter nodeAdapter = findNodeAdapter( new NodeAdapter( root ), componentID );

    if(nodeAdapter!=null && nodeAdapter.getRootType() == TOOLBAR) {
      return nodeAdapter.getToolBar().lookup( "#"+componentID.getName() );
    }

    if(nodeAdapter!=null && nodeAdapter.getNode()!=null) {
      return nodeAdapter.getNode();
    }

    return null;
  }

  public synchronized List<NodeAdapter> getChildrenComplete( NodeAdapter nodeAdapter) {
    List<NodeAdapter> childNodes = new ArrayList<>( );

    for (NodeAdapter adapter : nodeAdapter.getChildrenUnmodifiable()) {
      childNodes.add( adapter );
      childNodes.addAll( getChildrenComplete(adapter) );
    }
    return childNodes;
  }

  public synchronized NodeAdapter findNodeAdapter( NodeAdapter nodeAdapter, QName componentID ) {
    NodeAdapter foundAdapter = null;
    for (NodeAdapter adapter : nodeAdapter.getChildrenUnmodifiable()) {
      if(componentID.getName().equals( adapter.getId() )) {
        foundAdapter =  adapter;
      }
      if(foundAdapter == null) {
        foundAdapter = findNodeAdapter( adapter, componentID );
      }
    }
    return foundAdapter;
  }

  @Override
  public void show( de.pidata.gui.event.Dialog parent, boolean fullScreen ) {
    this.parentDialog = (FXDialogInterface) parent;
    stage.setTitle( dialogController.getTitle() );
    stage.setScene( getScene() );
    stage.setOnCloseRequest( this );
    stage.setFullScreen( fullScreen );
    stage.show();

    ((FXScreen) Platform.getInstance().getScreen()).setFocusDialog( this );

    DialogControllerDelegate delegate = dialogController.getDelegate();
    if (delegate != null) {
      delegate.dialogShowing( dialogController );
    }
  }

  @Override
  public void toFront() {
    Platform.getInstance().runOnUiThread( () -> {
      stage.requestFocus();
      stage.toFront();
      if (!stage.isAlwaysOnTop()) {
        // force top, revert immediately
        stage.setAlwaysOnTop( true );
        stage.setAlwaysOnTop( false );
      }
    });
  }

  @Override
  public void toBack() {
    Platform.getInstance().runOnUiThread( () -> stage.toBack() );
  }

  /**
   * Show/hide busy animation if naksker pane exists (getScene().lookup("#maskerPane")).
   * Otherwise switch between busy cursor (true) and normal cursor (false).
   *
   * @param busy if true show busy, otherwise hide
   */
  @Override
  public void showBusy( boolean busy ) {
    if (Platform.getInstance().isOnUiThread()) {
      scene.setCursor( busy ? Cursor.WAIT : Cursor.DEFAULT );
    }
    else {
      Platform.getInstance().runOnUiThread( new Runnable() {
        @Override
        public void run() {
          scene.setCursor( busy ? Cursor.WAIT : Cursor.DEFAULT );
        }
      } );
    }
  }

  public void updateLanguage() {
    Platform.getInstance().updateLanguage( this, getDialogID(), null );
    for (Dialog childDlg : childDialogList) {
      childDlg.updateLanguage();
    }
    ((AbstractDialogController)dialogController).updateChildrenLanguage();
  }


  /**
   * Called by DialogController.close() before anything other is
   * done. By returning false this Dialog can interrupt closing if
   * the Platform supports to abort close requests (e.g. Android does
   * not support that). Java FX will try to close children and
   * abort closing if any child dialog need user feedback.
   *
   * @param ok true if closing is OK operation
   * @return false to abort closing
   */
  @Override
  public boolean closing( boolean ok ) {
    FXDialog[] childDialogs = new FXDialog[childDialogList.size()];
    childDialogList.toArray( childDialogs );
    for (FXDialog childDlg : childDialogs) {
      if (!childDlg.getController().close( false, this.dialogController )) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void close( boolean ok, ParameterList resultList ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (scene.getWindow() != null) {
        ((Stage) scene.getWindow()).close();
      }
    } );
    saveDialogSettings();

    FXDialogManager.removeDialog( dialogID );

    if (parentDialog != null) {
      parentDialog.childClosed( this );
    }
    DialogController parentDlgCtrl = getController().getParentDialogController();
    if (parentDlgCtrl != null) {
      parentDlgCtrl.childDialogClosed( ok, resultList );
    }

    ((FXScreen) Platform.getInstance().getScreen()).setFocusDialog( parentDialog );
  }

  public void childClosed( FXDialogInterface childDialog ) {
    childDialogList.remove( childDialog );
  }

  @Override
  public void speak( String text ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setAlwaysOnTop( boolean onTop ) {
    stage.setAlwaysOnTop( onTop );
  }

  @Override
  public boolean getAlwaysOnTop() {
    return stage.isAlwaysOnTop();
  }

  @Override
  public void setMinWidth( int width ) {
    stage.setMinWidth( width );
  }

  @Override
  public void setMaxWidth( int width ) {
    stage.setMaxWidth( width );
  }

  @Override
  public void setMinHeight( int height ) {
    stage.setMinHeight( height );
  }

  @Override
  public void setMaxHeight( int height ) {
    stage.setMaxHeight( height );
  }

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   */
  @Override
  public void openChildDialog( DialogType dialogDef, String title, ParameterList parameterList ) {
    QName dialogID = dialogDef.getID();
    String windowStyle = dialogDef.getWindowStyle();
    Boolean modal = dialogDef.getModal();
    openChildDialog( dialogID, title, parameterList, windowStyle, modal );
  }

  /**
   * Opend child dialog with given dialogID and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void openChildDialog( DialogDef dialogDef, String title, ParameterList parameterList ) {
    QName dialogID = dialogDef.getID();
    String windowStyle = dialogDef.getWindowStyle();
    Boolean modal = dialogDef.getModal();
    openChildDialog( dialogID, title, parameterList, windowStyle, modal );
  }

   private DialogController openChildDialog( QName dialogID, String title, ParameterList parameterList, String windowStyle, Boolean modal ) {
    Stage childStage = new Stage();
    if("DECORATED".equals( windowStyle )) {
      childStage.initStyle( StageStyle.DECORATED );

      if(stage.getIcons().stream().count() > 0) {
        childStage.getIcons().addAll( stage.getIcons() );
      }
    }
    else {
      childStage.initStyle( StageStyle.UTILITY );
      childStage.initOwner( stage );
    }

    if ((modal != null) && modal.booleanValue()) {
      childStage.initModality( Modality.WINDOW_MODAL );
    }
    else {
      childStage.initModality( Modality.NONE );
    }
    FXDialog childDialog = createChildDialog( dialogID, childStage );
    try {
      childDialog.load();
    }
    catch (IOException ex) {
      throw new IllegalArgumentException( "Could not load dialog ID="+ dialogID, ex );
    }

    Context context = getController().getContext();
    DialogController parentDlgCtrl = getController().getDialogController();
    DialogController childDlgCtrl = Platform.getInstance().getControllerBuilder().createDialogController( dialogID, context, childDialog, parentDlgCtrl );
    childDlgCtrl.setTitle( title );
    try {
      childDlgCtrl.setParameterList( parameterList );
      childDialog.setRootStyle( root.getStyle() );
      childDialog.show( this, false );
      childDialog.toFront();
      childDlgCtrl.activate( childDialog );
    }
    catch (Exception ex) {
      throw new IllegalArgumentException( "Error initializing dialog ID="+ dialogID, ex );
    }
    childDialogList.add( childDialog );
    return childDlgCtrl;
  }

  public void setRootStyle( String style ) {
    this.root.setStyle( style );
  }

  /**
   * returns new FXDialog Object of given id on given stage and given handle
   *
   * @param dialogID   of the new Dialog
   * @param childStage of the new Dialog
   * @return
   */
  protected FXDialog createChildDialog( QName dialogID, Stage childStage ) {
    return createFXDialog( dialogID, childStage );
  }

  public static FXDialog createFXDialog( QName dialogID, Stage childStage ) {
    FXDialog newDialog = new FXDialog( dialogID, childStage );
    return newDialog;
  }

  /**
   * Opens popup with given moduleDef and title
   *
   * @param popupCtrl
   * @param moduleGroup   module group to be shown in a Popup
   * @param title         title for the dialog, if null title from dialogDef is used
   */
  @Override
  public void showPopup( PopupController popupCtrl, ModuleGroup moduleGroup, String title ) {
    if (this.childPopUp == null) {
      QName childDialogID = moduleGroup.getName();
      //create Dialog
      this.childPopUp = createChildPopup( childDialogID, title );
      //set parent Dialog to get same dataContext
      childPopUp.parentDialog = this;
      try {
        //load fxml into popup dialog
        childPopUp.load();
        childPopUp.getStage().setScene( childPopUp.getScene() );
      }
      catch (IOException ex) {
        throw new IllegalArgumentException( "Could not load dialog ID=" + dialogID, ex );
      }
      //activate module
      moduleGroup.activate( childPopUp );
      moduleGroup.activateModule( childPopUp );
    }
  }

  protected FXPopupDialog createChildPopup( QName dialogID, String title ) {
    return new FXPopupDialog( dialogID, title, getStage(), getScene() );
  }

  /**
   * Hides current popup. If popup is not visible nothing happens.
   *
   * @param popupController
   */
  @Override
  public void closePopup( PopupController popupController ) {
    //close dialog
    if (this.childPopUp != null) {
      childPopUp.getStage().close();
      this.childPopUp = null;
    }
    //deactivate module
    popupController.setModuleGroup( null, ViewAnimation.NONE );
    popupController.deactivate( true );
  }

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   */
  @Override
  public void showMessage( String title, String message ) {
    QuestionBoxParams params = new QuestionBoxParams( title, message );
    Platform.getInstance().runOnUiThread( () -> new FXMessageBox( params, stage, getController() ));
  }

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showQuestion( String title, String message, String btnYesLabel, String btnNoLabel, String btnCancelLabel ) {
    QuestionBoxParams params = new QuestionBoxParams( title, message, "", btnYesLabel, btnNoLabel, btnCancelLabel );
    Platform.getInstance().runOnUiThread( () -> new FXMessageBox( params, getScene().getWindow(), getController() ));
  }

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a deafulat value for the inputfield
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showInput( String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel ) {
    QuestionBoxParams params = new QuestionBoxParams( title, message, defaultValue, btnOKLabel, null, btnCancelLabel );
    Platform.getInstance().runOnUiThread( () -> new FXInputDialog( params, getScene().getWindow(), getController() ));
  }

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  @Override
  public void showToast( String message ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   */
  @Override
  public void showFileChooser( String title, FileChooserParameter fileChooserParams ) {
    File file;
    if ((fileChooserParams != null) && (FileChooserParameter.TYPE_OPEN_DIR.equals( fileChooserParams.getDialogType() ))) {
      DirectoryChooser directoryChooser = new DirectoryChooser();
      String dirPath = fileChooserParams.getDirPath();
      if (!Helper.isNullOrEmpty( dirPath )) {
        File initDir = new File( dirPath );
        if (initDir.exists()) {
          directoryChooser.setInitialDirectory( initDir );
        }
        else {
          Logger.warn( "FileChooser initDir does not exist: "+initDir.getAbsolutePath() );
        }
      }
      file = directoryChooser.showDialog( this.getScene().getWindow() );
    }
    else {
      FileChooser fileChooser = new FileChooser();
      fileChooser.setTitle( title );
      if (fileChooserParams != null) {
        String filePattern = fileChooserParams.getFilePattern();
        if (!Helper.isNullOrEmpty( filePattern )) {
          List<String> extensions = Arrays.asList( filePattern.split( ";" ) );
          FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter( filePattern, extensions );
          fileChooser.getExtensionFilters().setAll( filter );
        }
        String dirPath = fileChooserParams.getDirPath();
        if (!Helper.isNullOrEmpty( dirPath )) {
          File initDir = new File( dirPath );
          if (initDir.exists()) {
            fileChooser.setInitialDirectory( initDir );
          }
          else {
            Logger.warn( "FileChooser initDir does not exist: " + initDir.getAbsolutePath() );
          }
        }
      }
      file = fileChooser.showOpenDialog( this.getScene().getWindow() );
    }
    javafx.application.Platform.runLater( () -> {
      ParameterList resultList;
      if (file == null) {
        resultList = new FileChooserResult( null );
        getController().childDialogClosed( false, resultList );
      }
      else {
        String path;
        try {
          path = file.getCanonicalPath();
        }
        catch (IOException e) {
          Logger.warn( "Error getting canonical path, using path, ex=" + e );
          path = file.getPath();
        }
        Logger.info( "FileChooser result path=" + path );
        resultList = new FileChooserResult( path );
        getController().childDialogClosed( true, resultList );
      }
    } );
  }

  /**
   * Shows the platform specific image chooser dialog.
   *
   * @param imageChooserType type of the image chooser
   * @param fileName         name of file that has to be used, if image will be created (by using camera)
   */
  @Override
  public void showImageChooser( ImageChooserType imageChooserType, String fileName ) {
    throw new RuntimeException("TODO");
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param windowEvent the event which occurred
   */
  @Override
  public void handle( WindowEvent windowEvent ) {
    if (windowEvent.getEventType() == WindowEvent.WINDOW_CLOSE_REQUEST) {
      windowEvent.consume();
      saveDialogSettings();
      dialogController.close( false, dialogController );
    }
  }


  private void doClose() {
  }

  @Override
  public void registerShortcut( QName command, ActionController controller ) {
    KeyCodeCombination keyCombination = buildKeyCodeCombination( command );
    if (keyCombination == null) return;

    Scene scene = getScene();
    scene.getAccelerators().put(
        keyCombination,
        new Runnable() {
          @Override public void run() {
            if (controller != null) {
              controller.doAction();
            }
          }
        }
    );
  }

  private KeyCodeCombination buildKeyCodeCombination( QName command ) {
    String cmdName = command.getName();
    String[] cmdParts = cmdName.split( "-" );

    KeyCode keyCode = null;
    List<KeyCombination.Modifier> modifierList = new ArrayList<>();
    for (String keyPart : cmdParts) {
      switch (keyPart) {
        case "Alt":
          modifierList.add( KeyCombination.ALT_DOWN );
          break;

        case "Shift":
          modifierList.add( KeyCombination.SHIFT_DOWN );
          break;

        case "Strg":
          modifierList.add( KeyCombination.SHORTCUT_DOWN );
          break;

        default:
          if (keyCode == null) {
            String keyName;
            if (keyPart.length() == 1) {
              // single character maps to upper case
              keyName = keyPart.toUpperCase();
            }
            else {
              keyName = keyPart;
            }
            keyCode = KeyCode.getKeyCode( keyName );
          }
          else {
            Logger.warn( "multiple key codes defined in shortcut [" + cmdName + "]" );
          }
      }
    }
    if (keyCode == null) {
      // no basic key defined - reject
      Logger.warn( "no key code defined in shortcut [" + cmdName + "]" );
      return null;
    }

    KeyCodeCombination keyCombination;
    if (modifierList.size() > 0) {
      KeyCombination.Modifier[] modifiers = new KeyCombination.Modifier[modifierList.size()];
      modifiers = modifierList.toArray( modifiers );
      keyCombination = new KeyCodeCombination( keyCode, modifiers );
    }
    else {
      keyCombination = new KeyCodeCombination( keyCode );
    }
    return keyCombination;
  }

  private void loadDialogSettings() {
    try {
      Preferences pref = Preferences.userRoot().node( dialogID.getName() );
      double x = pref.getDouble(WINDOW_POSITION_X, stage.getX() );
      double y = pref.getDouble(WINDOW_POSITION_Y, stage.getY() );
      double width = pref.getDouble(WINDOW_WIDTH, stage.getWidth() );
      double height = pref.getDouble(WINDOW_HEIGHT, stage.getHeight() );
      boolean onTop = pref.getBoolean( ALLWAYS_ON_TOP, false );
      boolean maximized = pref.getBoolean( MAXIMIZED, false );

      //--- Check if x and y are on an available screen
      boolean foundScreen = false;
      ObservableList<Screen> screens = Screen.getScreensForRectangle( x, y, width, height );
      foundScreen = screens.size() == 1;
      if( foundScreen ) {
        stage.setX(x);
        stage.setY(y);
      }
      else {
        stage.setX(stage.getX());
        stage.setY(stage.getY());
      }

      //--- If the same dialog is opened already set an offset.
      int dialogCount = FXDialogManager.getDialogCount( dialogID );
      if( dialogCount > 0 ) {
        stage.setX( stage.getX() + (dialogCount*10) );
        stage.setY( stage.getY() + (dialogCount*10) );
      }
      FXDialogManager.addDialog( dialogID );

      //--- Make sure size is min. 250 x 150 and initial use prefWidth/prefHeight
      if (Double.isNaN(width) || (width < 250)) {
        if (root instanceof Region) {
          width = ((Region) root).getPrefWidth();
        }
        if (Double.isNaN(width) || (width < 250)) {
          width = 250;
        }
      }
      stage.setWidth(width);
      if (Double.isNaN(height) || (height < 150)) {
        if (root instanceof Region) {
          height = ((Region) root).getPrefHeight();
        }
        if (Double.isNaN(height) || (height < 150)) {
          height = 150;
        }
      }
      stage.setHeight(height);

      stage.setMaximized( maximized );
      stage.setAlwaysOnTop( onTop );
    }
    catch (Exception e) {
      Logger.error( "Error loading dialog settings.", e );
    }
  }

  private void saveDialogSettings() {
    try {
      Preferences pref = Preferences.userRoot().node( dialogID.getName() );
      pref.putDouble(WINDOW_POSITION_X, stage.getX());
      pref.putDouble(WINDOW_POSITION_Y, stage.getY());
      pref.putDouble(WINDOW_WIDTH, stage.getWidth());
      pref.putDouble(WINDOW_HEIGHT, stage.getHeight());
      pref.putBoolean( ALLWAYS_ON_TOP, stage.isAlwaysOnTop() );
      pref.putBoolean( MAXIMIZED, stage.isMaximized() );
    }
    catch (Exception e) {
      Logger.error( "Error saving dialog settings.", e );
    }
  }

  /**
   * Require rights at a specific platform.
   */
  @Override
  public void requireRights() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Add a RightsRequireListener for callback.
   *
   * @param rightsRequireListener
   */
  @Override
  public void addRightsRequireListener( RightsRequireListener rightsRequireListener ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  public QName getDialogID() {
    return dialogID;
  }

  @Override
  public String getSettingsID() {
    return dialogID.toString();
  }
}
