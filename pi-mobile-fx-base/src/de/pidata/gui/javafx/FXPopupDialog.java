/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.scene.Scene;
import javafx.scene.robot.Robot;
import javafx.stage.*;

/**
 * Created by bce on 09.03.21.
 */
public class FXPopupDialog extends FXDialog {

  public FXPopupDialog( QName dialogID, String title, Window owner, Scene parentScene ) {
    super( dialogID, new Stage() );
    stage.setTitle( title );
    stage.initOwner( owner );
    stage.initStyle( StageStyle.UTILITY );
    stage.initModality( Modality.WINDOW_MODAL );


    //--- Place window near owner
    Robot robot = new Robot();
    double x = robot.getMousePosition().getX() + 10;
    double y = robot.getMousePosition().getY() - 30;

    stage.setX( x );
    stage.setY( y );
    stage.setResizable( false );

    stage.show();
    stage.requestFocus();
    stage.toFront();
    // force top
    stage.setAlwaysOnTop( true );
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    return parentDialog.getDataContext();
  }
}
