/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.models.tree.Context;
import de.pidata.qnames.QName;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ParameterType;
import de.pidata.system.desktop.DesktopSystem;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.List;

/**
 * Created by pru on 31.05.16.
 */
public class FXApplication extends Application {

  private static FXApplication applicationInstance;

  protected QName initialDialogID;
  protected Context initialContext;
  protected FXPlatform platform;
  protected DialogController rootDlgCtrl;
  protected String basePath = ".";

  public static FXApplication getInstance() {
    return applicationInstance;
  }

  /**
   * The application initialization method. This method is called immediately
   * after the Application class is loaded and constructed. An application may
   * override this method to perform initialization prior to the actual starting
   * of the application.
   * <p>
   * <p>
   * The implementation of this method provided by the Application class does nothing.
   * </p>
   * <p>
   * <p>
   * NOTE: This method is not called on the JavaFX Application Thread. An
   * application must not construct a Scene or a Stage in this
   * method.
   * An application may construct other JavaFX objects in this method.
   * </p>
   */
  @Override
  public void init() throws Exception {

    applicationInstance = this;

    new DesktopSystem( this.basePath );
    String appName = getAppName();
    String[] args = new String[1];
    args[0] = appName;
    platform = new FXPlatform( this, args );
    platform.start();
  }

  protected String getAppName() {
    return getParameters().getUnnamed().get( 0 );
  }

  public void setInitialDialog( Context context, QName dialogID ) {
    this.initialDialogID = dialogID;
    this.initialContext = context;
  }

  protected ParameterList createInitialParamList() {
    List<String> args = getParameters().getUnnamed();
    String dataUri = AbstractParameterList.DATAURI_SCHEME_CLASS + "://" + AbstractParameterList.class.getName();
    String[] valueList = new String[args.size()];
    QName[] nameList = new QName[args.size()];
    ParameterType[] typeList = new ParameterType[args.size()];
    for (int i = 0; i < args.size(); i++) {
      nameList[i] = AbstractParameterList.DEFAULT_NS.getQName( "arg_" + i );
      typeList[i] = ParameterType.StringType;
      valueList[i] = args.get( i );
    }
    return new AbstractParameterList( dataUri, typeList, nameList, valueList );
  }

  /**
   * The main entry point for all JavaFX applications.
   * The start method is called after the init method has returned,
   * and after the system is ready for the application to begin running.
   * <p>
   * <p>
   * NOTE: This method is called on the JavaFX Application Thread.
   * </p>
   *
   * @param primaryStage the primary stage for this application, onto which
   *                     the application scene can be set. The primary stage will be embedded in
   *                     the browser if the application was launched as an applet.
   *                     Applications may create other stages, if needed, but they will not be
   *                     primary stages and will not be embedded in the browser.
   */
  @Override
  public void start( Stage primaryStage ) throws Exception {

    FXDialog fxDialog = new FXDialog( initialDialogID, primaryStage );
    fxDialog.load();
    rootDlgCtrl = platform.getControllerBuilder().createDialogController( initialDialogID, initialContext, fxDialog, null );
    rootDlgCtrl.setParameterList( createInitialParamList() );

    fxDialog.show( null, false );
    rootDlgCtrl.activate( fxDialog );
  }

   public DialogController getRootDialogController() {
    return rootDlgCtrl;
  }

  public static void main( String[] args ) {
    launch( args );
  }
}
