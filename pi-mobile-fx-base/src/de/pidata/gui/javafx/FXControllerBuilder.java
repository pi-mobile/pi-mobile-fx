/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.component.base.ProgressTask;
import de.pidata.gui.component.base.TaskHandler;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.ProgressController;
import de.pidata.gui.guidef.ControllerBuilder;

/**
 * Created by pru on 31.05.16.
 */
public class FXControllerBuilder extends ControllerBuilder {

  @Override
  public TaskHandler createTaskHandler( ProgressTask progressTask ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Shows a dialog containing a message and a progress bar. Content of message and progress bar
   * can be changed via ProgressController. The handle to the open dialog can also be obtained
   * from ProgressController
   *
   * @param title         the dialog title
   * @param message       the messsage, use CR (\n) for line breaks
   * @param minValue      the value representing 0%
   * @param maxValue      the value representing 100%
   * @param cancelable    true if progress dialog can be canceled by user
   * @param parentDlgCtrl the parent DialogController
   * @return the ProgressController
   */
  @Override
  public ProgressController createProgressDialog( String title, String message, int minValue, int maxValue, boolean cancelable, DialogController parentDlgCtrl ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

}
