/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITreeAdapter;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import javafx.scene.Node;
import javafx.scene.control.TreeItem;

import java.util.HashMap;
import java.util.Map;

public abstract class FXTreeAdapter extends FXAdapter implements UITreeAdapter {

  protected final Map<TreeNodePI, FXTreeItem> itemMap = new HashMap<>();

  protected FXTreeAdapter( Node fxUIComponent, ViewPI viewPI, UIContainer uiContainer ) {
    super( fxUIComponent, viewPI, uiContainer );
  }

  @Override
  public void updateNode( TreeNodePI treeNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      FXTreeItem treeItem = getTreeItem( treeNode );
      if (treeItem != null) {
        treeItem.update();
      }
    });
  }

  @Override
  public void updateChildList( TreeNodePI treeNodePI ) {
    Platform.getInstance().runOnUiThread( () -> {
      FXTreeItem treeItem = getTreeItem( treeNodePI );
      if (treeItem != null) {
        treeItem.updateChildList();
      }
    });
  }

  @Override
  public void setExpanded( TreeNodePI treeNodePI, boolean expand ) {
    Platform.getInstance().runOnUiThread( () -> getTreeItem( treeNodePI ).setExpanded( expand ) );
  }

  public void addTreeItem( FXTreeItem treeItem, TreeNodePI treeNode ) {
    itemMap.put( treeNode, treeItem );
  }

  public FXTreeItem getTreeItem( TreeNodePI nodeViewPI) {
    return itemMap.get( nodeViewPI );
  }

  public FXTreeItem removeTreeItem( TreeNodePI nodeViewPI) {
    return itemMap.remove( nodeViewPI );
  }

  public FXTreeItem findItem( Model nodeModel ) {
    if (nodeModel == null) {
      return null;
    }
    synchronized (itemMap) {
      for (TreeNodePI nodePI : itemMap.keySet()) {
        if (nodePI.getNodeModel() == nodeModel) {
          return itemMap.get( nodePI );
        }
      }
      return null;
    }
  }

  protected void expandParentPath( TreeNodePI nodePI ) {
    FXTreeItem root = getRoot();
    TreeItem nodeItem = getTreeItem( nodePI );
    if (nodeItem != null) {
      TreeItem parentItem = nodeItem.getParent();
      while (parentItem != null && !root.equals( parentItem )) {
        if (!parentItem.isExpanded()) {
          parentItem.setExpanded( true );
        }
        parentItem = parentItem.getParent();
      }
    }
  }

  protected abstract FXTreeItem getRoot();
}
