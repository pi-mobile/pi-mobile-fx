/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.gui.component.base.Platform;
import javafx.scene.control.Label;

/**
 * Created by pru on 28.09.16.
 */
public class FXLabelAdapter extends FXValueAdapter implements UITextAdapter {

  private Label fxLabel;
  private TextViewPI textViewPI;
  private Namespace valueNS;

  public FXLabelAdapter( Label fxLabel, TextViewPI textViewPI, UIContainer uiContainer ) {
    super( fxLabel, textViewPI, uiContainer );
    this.fxLabel = fxLabel;
    this.textViewPI = textViewPI;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxLabel = null;
    textViewPI = null;
    super.detach();
  }

  @Override
  public Object getValue() {
    if (valueNS == null) {
      return fxLabel.getText();
    }
    else {
      return valueNS.getQName( fxLabel.getText() );
    }
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (value == null) {
        setText( "" );
      }
      else if (value instanceof QName) {
        valueNS = ((QName) value).getNamespace();
        setText( ((QName) value).getName() );
      }
      else {
        setText( value.toString() );
        valueNS = null;
      }
    });
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    // do nothing
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      throw new IllegalArgumentException( "Listen text changes not supported for FXLabelAdapter" );
    }
  }

  private void setText( String text) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (this) {
          if (fxLabel != null) {
            fxLabel.setText( text );
          }
        }
      }
    } );
  }

  @Override
  public void select( int fromPos, int toPos ) {
  }

  public void selectAll() {
    // not supported -> ignore
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}

