/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Model;
import de.pidata.gui.component.base.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.util.Callback;

/**
 * Created by pru on 20.06.16.
 */
public class FXListAdapter extends FXAdapter implements UIListAdapter, ListChangeListener<Integer>, Callback<ListView, ListCell> {

  public static final boolean DEBUG = false;

  private ListView fxListView;
  private ListViewPI listViewPI;
  private ObservableList<Model> fxListItems;

  public FXListAdapter( ListView fxListView, ListViewPI listViewPI, UIContainer uiContainer ) {
    super( fxListView, listViewPI, uiContainer );
    this.fxListView = fxListView;
    this.listViewPI = listViewPI;

    fxListView.setCellFactory( FXListAdapter.this );

    //--- Add values
    fxListItems = FXCollections.observableArrayList();
    fxListView.setItems( fxListItems );

    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        //--- Activate Multi-Selection if allowed
        MultipleSelectionModel selectionModel = fxListView.getSelectionModel();
        if (listViewPI.isMultiSelect()) {
          selectionModel.setSelectionMode( SelectionMode.MULTIPLE );
        }

        //--- Set initial selection
        selectionModel.clearSelection();
        Selection selection = listViewPI.getListCtrl().getSelection();
        for (int i = 0; i < selection.selectedValueCount(); i++) {
          selectionModel.select( selection.getSelectedValue( i ) );
        }

        //--- Attach listener for selection changes
        fxListView.getSelectionModel().getSelectedIndices().addListener( FXListAdapter.this );
      }
    } );
  }

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    return fxListItems.size();
  }

  /**
   * Returns the data displayed in row at position
   *
   * @param position
   * @return
   */
  @Override
  public Model getDisplayRow( int position ) {
    return fxListItems.get( position );
  }

  private void startUpdate() {
  }

  private void endUpdate() {
  }

  @Override
  public void removeAllDisplayRows() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          fxListItems.clear();
          endUpdate();
        }
      }
    });
  }


  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          int index;
          if (beforeRow == null) {
            index = -1;
          }
          else {
            index = fxListItems.indexOf( beforeRow );
          }
          if (index < 0) {
            fxListItems.add( newRow );
            index = fxListItems.size() - 1;
          }
          else {
            fxListItems.add( index, newRow );
          }
          endUpdate();
        }
      }
    });
  }


  @Override
  public void removeRow( Model removedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          boolean found = fxListItems.remove( removedRow );
          endUpdate();
        }
      }
    });
  }

  @Override
  public void updateRow( Model changedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          int index = fxListItems.indexOf( changedRow );
          fxListItems.set( index, changedRow );
          endUpdate();
        }
      }
    });
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (selected) {
          fxListView.getSelectionModel().select( displayRow );
        }
        else {
          int index = fxListItems.indexOf( displayRow );
          fxListView.getSelectionModel().clearSelection( index );
        }
      }
    });
  }


  /**
   * Called after a change has been made to an ObservableList.
   *
   * @param selectionChange an object representing the change that was done
   * @see Change
   */
  @Override
  public void onChanged( Change<? extends Integer> selectionChange ) {
    // invoke on changes of selected index
    if (DEBUG) {
      Logger.info( "Selected indices: " );
      for (Integer integer : selectionChange.getList()) {
        Logger.info( integer + " " );
      }
    }

    while (selectionChange.next()) {
      if (selectionChange.wasRemoved()) {
        if (selectionChange.getRemovedSize() > 0) {
          for (Integer indexObj : selectionChange.getRemoved()) {
            if (indexObj != null) {
              int index = indexObj.intValue();
              if ((index >= 0) && (index < fxListItems.size())) {
                Model selectedRow = getDisplayRow( index );
                listViewPI.onSelectionChanged( selectedRow, false );
              }
            }
          }
        }
      }
      if (selectionChange.wasAdded()) {
        if (selectionChange.getAddedSize() > 0) {
          for (Integer indexObj : selectionChange.getList()) {
            if (indexObj != null) {
              int index = indexObj.intValue();
              //WORKAROUND: getAddedSublist() will sometimes return wrong row index for multiselection so we use getList()
              //it seems that deselecting rows are not affected by this bug.
              //if you want to reproduce this error:
              //first select row index 1 then index 5 and at last index 3 (or other between 1 and 5)
              //getAddedSublist will only return index 1 but not index 3 (tested on jdk 1.8_201)
              if ((index >= 0) && (index < fxListItems.size())) {
                Model selectedRow = getDisplayRow( index );
                listViewPI.onSelectionChanged( selectedRow, true );
              }
            }
          }
        }
      }
    }
  }

  /**
   * The <code>call</code> method is called when required, and is given a
   * single argument of type P, with a requirement that an object of type R
   * is returned.
   *
   * @param fxListView The single argument upon which the returned value should be
   *                   determined.
   * @return An object of type R that may be determined based on the provided
   * parameter value.
   */
  @Override
  public ListCell call( ListView fxListView ) {
    return new FXListCell( fxListView, listViewPI );
  }
}
