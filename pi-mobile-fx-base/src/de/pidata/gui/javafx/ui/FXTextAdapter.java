/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

/**
 * Created by pru on 28.09.16.
 */
public class FXTextAdapter extends FXValueAdapter implements UITextAdapter {

  private Text fxText;
  private TextViewPI textViewPI;
  private Namespace valueNS;

  public FXTextAdapter( Text fxText, TextViewPI textViewPI, UIContainer uiContainer ) {
    super( fxText, textViewPI, uiContainer );
    this.fxText = fxText;
    this.textViewPI = textViewPI;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxText = null;
    textViewPI = null;
    super.detach();
  }

  @Override
  public Object getValue() {
    if (valueNS == null) {
      return fxText.getText();
    }
    else {
      return valueNS.getQName( fxText.getText() );
    }
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (value == null) {
        setText( "" );
      }
      else if (value instanceof QName) {
        valueNS = ((QName) value).getNamespace();
        setText( ((QName) value).getName() );
      }
      else {
        setText( value.toString() );
        valueNS = null;
      }
    });
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    // do nothing
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      throw new IllegalArgumentException( "Listen text changes not supported for FXLabelAdapter" );
    }
  }

  private void setText( String text) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (this) {
          if (fxText != null) {
            fxText.setText( text );
          }
        }
      }
    } );
  }

  @Override
  public void select( int fromPos, int toPos ) {
  }

  public void selectAll() {
    // not supported -> ignore
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}

