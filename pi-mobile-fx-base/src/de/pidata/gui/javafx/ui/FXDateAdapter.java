/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIDateAdapter;
import de.pidata.gui.view.base.DateViewPI;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import javafx.scene.control.DatePicker;

import java.time.LocalDate;
import java.util.Calendar;

/**
 * Created by pru on 29.11.2016.
 */
public class FXDateAdapter extends FXValueAdapter implements UIDateAdapter {

  private DatePicker fxDatePicker;

  public FXDateAdapter( DatePicker fxDatePicker, DateViewPI dateViewPI, UIContainer uiContainer ) {
    super( fxDatePicker, dateViewPI, uiContainer );
    this.fxDatePicker = fxDatePicker;
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public Object getValue() {
    LocalDate localDate  = fxDatePicker.getValue();
    if (localDate == null) {
      return null;
    }
    else {
      // internal java inconsistency: LocalDate holds MONTH starting with January = 1,
      // DateObject wraps a Date which holds MONTH starting with January = 0
      return new DateObject( DateTimeType.TYPE_DATE, localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth() );
    }
  }

  @Override
  public void setValue( Object value ) {
    if (value == null) {
      fxDatePicker.setValue( null );
    }
    else {
      DateObject dateObject = (DateObject) value;
      Calendar cal = DateObject.getCalendar();
      cal.setTimeInMillis( dateObject.getTime() );
      LocalDate localDate = LocalDate.of( cal.get( Calendar.YEAR ), cal.get( Calendar.MONTH ), cal.get( Calendar.DAY_OF_MONTH ) );
      fxDatePicker.setValue( localDate );
    }
  }

  @Override
  public void setDateFormat( QName format ) {
    // TODO
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    // do nothing - date picker does not have a cursor
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      throw new IllegalArgumentException( "Listen text changes not supported for FXDateAdapter" );
    }
  }

  @Override
  public void select( int fromPos, int toPos ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void selectAll() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinDate( long minDate ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
