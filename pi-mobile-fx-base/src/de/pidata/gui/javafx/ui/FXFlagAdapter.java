/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.ui.FXValueAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFlagAdapter;
import de.pidata.gui.view.base.FlagViewPI;
import de.pidata.log.Profiler;
import de.pidata.models.tree.Model;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleButton;

public class FXFlagAdapter extends FXValueAdapter implements UIFlagAdapter, ChangeListener<Boolean> {

  private static final boolean DEBUG = false;

  private CheckBox fxCheckBox;
  private RadioButton fxRadioButton;
  private ToggleButton fxToggleButton;
  private FlagViewPI flagViewPI;
  private ObservableValue<Boolean> selectedProperty;
  // TODO: observer indeterminate!

  public FXFlagAdapter ( Node fxNode, FlagViewPI flagViewPI, UIContainer dataContext ){
    super( fxNode, flagViewPI, dataContext );
    this.flagViewPI = flagViewPI;
    //TODO: if fxNode is used, implement separately
  }

  public FXFlagAdapter( CheckBox fxCheckBox, FlagViewPI flagViewPI, UIContainer uiContainer ) {
    super( fxCheckBox, flagViewPI, uiContainer );
    this.flagViewPI = flagViewPI;
    this.fxCheckBox = fxCheckBox;
    this.fxCheckBox.setAllowIndeterminate( flagViewPI.isTristate() );
    this.selectedProperty = fxCheckBox.selectedProperty();
    this.selectedProperty.addListener( this );
  }

  public FXFlagAdapter( RadioButton fxRadioButton, FlagViewPI flagViewPI, UIContainer uiContainer ) {
    super( fxRadioButton, flagViewPI, uiContainer );
    if (flagViewPI.isTristate()) {
      throw new IllegalArgumentException( "Tristate not allowed for RadioButton");
    }
    this.flagViewPI = flagViewPI;
    this.fxRadioButton = fxRadioButton;
    this.selectedProperty = fxRadioButton.selectedProperty();
    this.selectedProperty.addListener( this );
  }

  public FXFlagAdapter( ToggleButton fxToggleButton, FlagViewPI flagViewPI, UIContainer uiContainer ) {
    super( fxToggleButton, flagViewPI, uiContainer );
    if (flagViewPI.isTristate()) {
      throw new IllegalArgumentException( "Tristate not allowed for ToggleButton");
    }
    this.flagViewPI = flagViewPI;
    this.fxToggleButton = fxToggleButton;
    this.selectedProperty = fxToggleButton.selectedProperty();
    this.selectedProperty.addListener( this );
  }

  @Override
  public Object getValue() {
    if (fxCheckBox != null) {
      if (fxCheckBox.isAllowIndeterminate()) {
        if (fxCheckBox.isIndeterminate()) {
          return null;
        }
        else {
          return Boolean.valueOf( fxCheckBox.isSelected() );
        }
      }
      else {
        return Boolean.valueOf( fxCheckBox.isSelected() );
      }
    }
    if (fxRadioButton != null) {
      return Boolean.valueOf( fxRadioButton.isSelected() );
    }
    if (fxToggleButton != null) {
      return Boolean.valueOf( fxToggleButton.isSelected() );
    }
    return Boolean.FALSE;
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> fxSetValue( value ) );
  }

  protected void fxSetValue( Object value ) {
    if (fxCheckBox != null) {
      if (value == null) {
        fxCheckBox.setIndeterminate( true );
      }
      else {
        fxCheckBox.setIndeterminate( false );
        fxCheckBox.setSelected( ((Boolean) value).booleanValue() );
      }
    }
    if (fxRadioButton != null) {
      if (value == null) {
        fxRadioButton.setSelected( false );
      }
      else {
        fxRadioButton.setSelected( ((Boolean) value).booleanValue() );
      }
    }
    if (fxToggleButton != null) {
      if (value == null) {
        fxToggleButton.setSelected( false );
      }
      else {
        fxToggleButton.setSelected( ((Boolean) value).booleanValue() );
      }
    }
  }

  @Override
  public void setLabelValue( Object value ) {
      Platform.getInstance().runOnUiThread( () -> fxSetLabelValue( value ) );
  }

  protected void fxSetLabelValue( Object value ) {
    if (value instanceof String) {
      if (fxCheckBox != null) {
        fxCheckBox.setText( (String) value );
      }
      if (fxRadioButton != null) {
        fxRadioButton.setText( (String) value );
      }
      if(fxToggleButton != null){
        fxToggleButton.setText( (String) value );
      }
    }
  }

  @Override
  public Object getLabelValue() {
    if (fxCheckBox != null) {
      return fxCheckBox.getText();
    }
    if (fxRadioButton != null) {
      return fxRadioButton.getText();
    }
    if(fxToggleButton != null){
      return fxToggleButton.getText();
    }
    return null;
  }

  /**
   * This JavaFX method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
    Profiler.count( "FXFlagAdapter changed" );
    if (observable == this.selectedProperty) {
      if (flagViewPI != null && oldValue != newValue) {
        flagViewPI.onCheckChanged( this, newValue, uiContainer.getDataContext() );
        if (DEBUG) {
          System.out.println( "Changed flag, new value: " + newValue );
        }
      }
    }
    else {
      // Process other property's events e.g. focusProperty
      super.changed( observable, oldValue, newValue );
    }
  }

}
