/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;


import de.pidata.connect.http.CookieManagerFactory;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.gui.ui.base.UIWebViewAdapter;
import de.pidata.gui.view.base.WebViewPI;
import de.pidata.log.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.w3c.dom.Document;

import java.net.*;
import java.util.List;

public class FXWebViewAdapter extends FXAdapter implements UIWebViewAdapter, UIValueAdapter, ChangeListener<Boolean> {

  private WebView webView;
  private WebViewPI webViewPI;
  private WebEngine webEngine;
  private ChangeListener<String> changeListener;
  private ChangeListener<Throwable> exceptionListener;
  private CookieManager cookieManager;

  public FXWebViewAdapter( WebView fxWebView, WebViewPI webViewPI, UIContainer uiContainer ) {
    super( fxWebView, webViewPI, uiContainer );

    this.webViewPI = webViewPI;
    webView = fxWebView;
    webEngine = webView.getEngine();
    webEngine.setJavaScriptEnabled( true );
    webView.setVisible( true );

    // Use the already created CookieManager if available. We might have problems with
    // missing cookies due to several instances of CookieManager.
    if (CookieHandler.getDefault() instanceof CookieManager) {
      cookieManager = (CookieManager) CookieHandler.getDefault();
    }
    // The default might be is com.sun.webkit.CookieManager. We do not want to use it, because there ist no available CookieStore
    else {
      cookieManager = CookieManagerFactory.getPersistentCookieManager();
      CookieHandler.setDefault( cookieManager );
    }

    changeListener = ( observable, oldLocation, newLocation ) -> {
      // might be detached
      if (FXWebViewAdapter.this.webViewPI != null) {
        FXWebViewAdapter.this.webViewPI.locationChanged( null, newLocation );
      }
    };
    webEngine.locationProperty().addListener( changeListener );

    exceptionListener = ( obs, oldExc, newExc ) -> {
      if (newExc != null) {
        Logger.error( "Error during login", newExc );
        webViewPI.closing();
      }
    };
    webEngine.getLoadWorker().exceptionProperty().addListener( exceptionListener );

    fxUIComponent.focusedProperty().addListener( this );

  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if (webView != null) {
      webView.setVisible( false );
    }
    fxUIComponent.focusedProperty().removeListener( this );
    revertProxy();

    super.detach();
  }

  @Override
  public Object getValue() {
    Document document = webEngine.getDocument();
    if (document == null) {
      return null;
    }
    return document.getNodeValue();
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (value != null) {
          loadURL( value.toString() );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    // nothing to set
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      throw new IllegalArgumentException( "Listen text changes not supported for FXBrowserViewAdpater" );
    }
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
    // might be detached
    if ((viewPI != null) && (newValue != null)) {
      viewPI.onFocusChanged( this, newValue.booleanValue(), uiContainer.getDataContext() );
    }
  }

  /**
   * If the given value represents an URI the represented Website will be loaded,
   * otherwise the String value will be shown in the content area.
   * @param value
   */
  @Override
  public void loadURL( String value ) {
    try {
      // Try to create a URL from the given text. If it is not an URL, show the text with loadContent.
      URL url = new URL( value );
      url.toURI();
    }
    catch (MalformedURLException | URISyntaxException e) {
      webEngine.setUserStyleSheetLocation("data:,body { font: 14px Arial; }");
      webEngine.loadContent( value );
      return;
    }

    webEngine.load( value );
  }

  /**
   * @see #loadURL(String)
   * @param url
   * @param proxyHost
   * @param proxyPort
   */
  @Override
  public void loadURL( String url, String proxyHost, String proxyPort ) {
    setProxy(proxyHost, proxyPort);
    loadURL( url );
  }

  @Override
  public void select( int fromPos, int toPos ) {
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void selectAll() {
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setTextColor( ComponentColor color ) {

  }

  public List<HttpCookie> getCookies() {
    return cookieManager.getCookieStore().getCookies();
  }

  public void setProxy( String proxyHost, String proxyPort)
  {
    System.setProperty("http.proxyHost", proxyHost);
    System.setProperty("http.proxyPort", proxyPort);
    System.setProperty( "https.proxyHost", proxyHost );
    System.setProperty( "https.proxyPort", proxyPort );

  }

  private void revertProxy()
  {
    System.clearProperty("http.proxyHost");
    System.clearProperty("http.proxyPort");
    System.clearProperty( "https.proxyHost");
    System.clearProperty( "https.proxyPort");
  }
}
