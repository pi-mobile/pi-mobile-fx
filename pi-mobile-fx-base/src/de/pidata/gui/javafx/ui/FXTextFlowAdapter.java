/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.FXPlatform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.gui.ui.base.UIWebViewAdapter;
import de.pidata.gui.view.base.WebViewPI;
import de.pidata.log.Logger;
import de.pidata.string.Helper;
import javafx.application.HostServices;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.text.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;

import java.util.List;

public class FXTextFlowAdapter extends FXAdapter implements UIWebViewAdapter, UIValueAdapter, ChangeListener<Boolean> {

  private TextFlow fxTextFlow;
  private WebViewPI webViewPI;

  public FXTextFlowAdapter( TextFlow fxTextFlow, WebViewPI webViewPI, UIContainer uiContainer ) {
    super( fxTextFlow, webViewPI, uiContainer );
    this.fxTextFlow = fxTextFlow;
    this.webViewPI = webViewPI;

    this.fxTextFlow.setPadding(new Insets(10));
    this.fxTextFlow.setTextAlignment( TextAlignment.LEFT);
    this.fxTextFlow.setStyle("-fx-background-color: #FFFFFF;");

    fxUIComponent.focusedProperty().addListener( this );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxTextFlow = null;
    webViewPI = null;

    fxUIComponent.focusedProperty().removeListener( this );

    super.detach();
  }

  @Override
  public Object getValue() {
    StringBuilder sb = new StringBuilder();
    for (Node node : fxTextFlow.getChildren()) {
      if (node instanceof Text) {
        sb.append(((Text) node).getText());
      }
      if (node instanceof Hyperlink) {
        sb.append(((Hyperlink) node).getText());
      }
    }
    return sb.toString();
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (Helper.isNullOrEmpty(value)) {
          fxTextFlow.getChildren().clear();
        }
        else {
          String stringValue = (String) value;

          fxTextFlow.getChildren().clear();
          Document document = Jsoup.parse( stringValue );
          addFields( document.childNodes() );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  private void addFields( List<org.jsoup.nodes.Node> children ) {
    for (org.jsoup.nodes.Node node : children) {
      Text textField;
      if (node instanceof Element) {
        Element element = (Element)node;
         if (node.nodeName().equals( "a" )) {
          String href = element.attr( "href" );
          String linkText = element.text();
          Hyperlink hLink = new Hyperlink( linkText );
          hLink.setFont( Font.font( "Verdana", FontWeight.NORMAL, 14 ) );
          hLink.setOnAction( event -> {
            HostServices hostServices = ((FXPlatform) de.pidata.gui.component.base.Platform.getInstance()).getApplication().getHostServices();
            hostServices.showDocument( href );
          } );
          fxTextFlow.getChildren().add( hLink );
          continue;
        }
      }
      else if (node instanceof TextNode) {
        TextNode childElement = (TextNode)node;
        if (node.parentNode().nodeName().equals( "b" )) {
          textField = new Text(childElement.getWholeText());
          textField.setFont( Font.font( "Verdana", FontWeight.BOLD, 14 ) );
          fxTextFlow.getChildren().add( textField );
        }
        else if (node.parentNode().nodeName().equals( "i" )) {
          textField = new Text(childElement.getWholeText());
          textField.setFont( Font.font( "Verdana", FontWeight.NORMAL, FontPosture.ITALIC, 14 ) );
          fxTextFlow.getChildren().add( textField );
        }
        else if (node.parentNode().nodeName().equals( "li" )) {
          textField = new Text( "   - " + childElement.text() );
          textField.setFont( Font.font( "Verdana", FontWeight.NORMAL, 14 ) );
          fxTextFlow.getChildren().add( textField );
          fxTextFlow.getChildren().add(new Text(System.lineSeparator()));
        }
        else {
          String text = childElement.text().trim();
          if (!Helper.isNullOrEmpty( text )) {
            textField = new Text(childElement.text());
            textField.setFont( Font.font( "Verdana", FontWeight.NORMAL, 14 ) );
            fxTextFlow.getChildren().add( textField );
          }
        }

        if (childElement.parentNode().nodeName().equals( "p" ) && childElement == childElement.parentNode().childNode( childElement.parentNode().childNodeSize()-1 )) {
          // Newline at the end of a paragraph.
          fxTextFlow.getChildren().add(new Text(System.lineSeparator()));
        }
      }

      addFields( node.childNodes() );
    }
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    // not directly supported on HTMLEditor, see https://stackoverflow.com/questions/16606054/find-htmleditor-cursor-pointer-for-inserting-image
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      throw new IllegalArgumentException( "Listen text changes not supported for FXWebViewAdapter" );
    }
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
    // might be detached
    if ((viewPI != null) && (newValue != null)) {
      viewPI.onFocusChanged( this, newValue.booleanValue(), uiContainer.getDataContext() );
    }
  }

  @Override
  public void loadURL(String url) {
    //
  }

  @Override
  public void loadURL( String url, String proxyHost, String proxyPort ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void select( int fromPos, int toPos ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void selectAll() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
