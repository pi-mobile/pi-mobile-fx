/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.javafx.FXColor;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeTableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.*;

import static de.pidata.gui.controller.base.GuiOperation.NAMESPACE;

/**
 * Created by pru on 12.07.2016.
 */
public class FXTreeTableCell extends TreeTableCell<TreeNodePI, TreeNodePI> implements ChangeListener, UIContainer {

  protected TreeTableViewPI treeTableViewPI;
  protected FXTreeTableAdapter fxTreeAdapter;
  protected TreeTableView<TreeNodePI> fxTreeTableView;
  protected FXTreeTableAdapter fxTreeTableAdapter;

  protected ColumnInfo columnInfo;
  protected ViewPI rendererViewPI;
  protected TreeTableColumn treeTableColumn;

  protected Node rendererNode;
  protected FXUIFactory fxUIFactory;

  public FXTreeTableCell( TreeTableViewPI treeTableViewPI, TreeTableView<TreeNodePI> fxTreeTableView, FXTreeTableAdapter fxTreeTableAdapter,
                          ColumnInfo columnInfo, ViewPI rendererViewPI, TreeTableColumn treeTableColumn, FXUIFactory fxUIFactory ) {
    super();
    this.treeTableViewPI = treeTableViewPI;
    this.fxTreeTableView = fxTreeTableView;
    this.fxTreeTableAdapter = fxTreeTableAdapter;
    this.fxTreeAdapter = (FXTreeTableAdapter) treeTableViewPI.getUIAdapter();
    this.columnInfo = columnInfo;
    this.rendererViewPI = rendererViewPI;
    this.treeTableColumn = treeTableColumn;
    this.fxUIFactory = fxUIFactory;

    fxTreeTableView.getSelectionModel().selectedItemProperty().addListener(this);
  }

  /**
   * The updateItem method should not be called by developers, but it is the
   * best method for developers to override to allow for them to customise the
   * visuals of the cell. To clarify, developers should never call this method
   * in their code (they should leave it up to the UI control, such as the
   * {@link ListView} control) to call this method. However,
   * the purpose of having the updateItem method is so that developers, when
   * specifying custom cell factories (again, like the ListView
   * {@link ListView#cellFactoryProperty() cell factory}),
   * the updateItem method can be overridden to allow for complete customisation
   * of the cell.
   * <p>
   * <p>It is <strong>very important</strong> that subclasses
   * of Cell override the updateItem method properly, as failure to do so will
   * lead to issues such as blank cells or cells with unexpected content
   * appearing within them. Here is an example of how to properly override the
   * updateItem method:
   * <p>
   * <pre>
   * protected void updateItem(T item, boolean empty) {
   *     super.updateItem(item, empty);
   *
   *     if (empty || item == null) {
   *         setText(null);
   *         setGraphic(null);
   *     } else {
   *         setText(item.toString());
   *     }
   * }
   * </pre>
   * <p>
   * <p>Note in this code sample two important points:
   * <ol>
   * <li>We call the super.updateItem(T, boolean) method. If this is not
   * done, the item and empty properties are not correctly set, and you are
   * likely to end up with graphical issues.</li>
   * <li>We test for the <code>empty</code> condition, and if true, we
   * set the text and graphic properties to null. If we do not do this,
   * it is almost guaranteed that end users will see graphical artifacts
   * in cells unexpectedly.</li>
   * </ol>
   *
   * @param item The new item for the cell.
   * @param empty whether or not this cell represents data from the list. If it
   *              is empty, then it does not represent any domain data, but is a cell
   *              being used to render an "empty" row.
   * @expert
   */
  @Override
  public void updateItem( TreeNodePI item, boolean empty ) {
    super.updateItem( item, empty );
    doUpdateItem( item, empty );
  }

  @Override
  public void updateSelected( boolean selected ) {
    setStyle( "-fx-background-color: null;" );
    super.updateSelected( selected );
  }

  protected void doUpdateItem( TreeNodePI treeItem, boolean empty ) {
    try {
      if (empty || treeItem == null) {
        setText( null );
        setGraphic( null );
        resetCellColor();
      }
      else {
        if (FXTreeViewAdapter.DEBUG) Logger.info( "upateNode [" + treeItem + "]" );
        if (rendererNode == null) {
          rendererNode = createCellRendererNode(  columnInfo, treeTableViewPI.getModuleID() );
        }

        QName rowColorID = treeTableViewPI.getRowColorID();
        if (rowColorID != null) {
          setCellColor( treeItem, rowColorID );
        }
        else {
          resetCellColor();
        }
        setGraphic( rendererNode );
        updateNode( rendererNode, treeItem );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error updating TreeTableCell item="+treeItem, ex );
    }
  }

  private void setCellColor( TreeNodePI treeItem, QName rowColorID ) {

    // Always reset the color before set the new one due to a fx problem about reusing row objects
    resetCellColor();

    QName colorColumnID = treeTableViewPI.getColorColumnID();
    if (colorColumnID == null || columnInfo.getColName().equals( colorColumnID )) {
      int rowIndex = getIndex();
      if (rowIndex >= 0) {
        Model nodeModel = treeItem.getNodeModel();
        Object rowColorObj = nodeModel.get( rowColorID );
        QName colorID = null;
        if (rowColorObj instanceof QName) {
          colorID = (QName) rowColorObj;
        }
        else if (!Helper.isNullOrEmpty( rowColorObj )) {
          colorID = NAMESPACE.getQName( rowColorObj.toString() );
        }
        if (colorID != null) {
          String colorString;
          FXColor fxColor;
          try {
            fxColor = (FXColor) Platform.getInstance().getColor( colorID );
          }
          catch (IllegalArgumentException ex) {
            //Logger.error( "ComponentColor '"+ colorID.toString() +"' not found, trying to get color by name '" + rowColorObj + "'" );
            fxColor = null;
          }
          if (fxColor != null) {
            colorString = fxColor.toStyleFormat();
          }
          else {
            colorString = rowColorObj.toString();
          }

          String cellCss;

          if (rowIndex % 2 == 0) { // gerade
            cellCss = " -fx-background-color: derive(" + colorString + ", 40%); " +
                " -fx-background-insets: 1 1 1 1;";
          }
          else { // ungerade
            cellCss = " -fx-background-color: " + colorString + "; " +
                " -fx-background-insets: 1 1 1 1;";
          }
          setStyle( cellCss );
        }
      }
    }
  }

  private void resetCellColor() {
    FXColor compCol = (FXColor) Platform.getInstance().getColor( ComponentColor.TRANSPARENT );
    if (compCol == null) {
      Logger.warn( "FXPlatform did not know color, ID=" + ComponentColor.TRANSPARENT );
    }
    else {
      setStyle( "-fx-background-color:" + compCol.toStyleFormat()+";" );
    }
  }

  protected Node createCellRendererNode( ColumnInfo columnInfo, QName rowDefID ) {
    return fxUIFactory.createCellRendererNode( this, columnInfo, rowDefID );
  }

  protected void updateNode( Node fxNode, TreeNodePI value ) {
    TreeTableColumn column = this.getTableColumn();
    fxUIFactory.updateCell( this, columnInfo, column.getPrefWidth() );
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return fxUIFactory;
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    TreeNodePI item = getItem();
    if (item != null) {
      return item.getNodeModel();
    }
    else {
      return null;
    }
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue observable, Object oldValue, Object newValue ) {

    if (getItem() == null) {
      return;
    }

    if (rendererNode == null) {
      rendererNode = createCellRendererNode(  columnInfo, treeTableViewPI.getModuleID() );
    }

    QName rowColorID = treeTableViewPI.getRowColorID();
    if (rowColorID != null) {
      Boolean multiSelectedRow = Boolean.FALSE;
      if(getTreeTableView().getSelectionModel().getSelectedIndices().size() > 1) {
        if(getTreeTableRow().isSelected()) {
          multiSelectedRow = Boolean.TRUE;
        }
      }
      TreeNodePI newNode = null;
      if (newValue != null) {
        newNode = ((FXTreeTableItem)newValue).getValue();
      }
      setCellColor( getItem(), rowColorID );
    }

    fxUIFactory.updateCell(this, columnInfo, treeTableColumn.getPrefWidth());
  }
}
