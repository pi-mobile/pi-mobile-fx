/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.models.tree.Model;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 * Created by pru on 01.06.16.
 */
public class FXMenuItemAdapter extends FXAdapter implements UIButtonAdapter, EventHandler<ActionEvent> {

  private MenuItem fxMenuItem;
  private ButtonViewPI buttonViewPI;

  public FXMenuItemAdapter( MenuItem fxMenuItem, ButtonViewPI buttonViewPI, UIContainer uiContainer ) {
    super( null, buttonViewPI, uiContainer );
    this.fxMenuItem = fxMenuItem;
    this.buttonViewPI = buttonViewPI;
    fxMenuItem.setOnAction( this );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxMenuItem.setOnAction( null );
    fxMenuItem = null;
    buttonViewPI = null;
    super.detach();
  }

  /**
   * Set this button's label
   *
   * @param label the label text
   */
  @Override
  public void setLabel( String label ) {
    Platform.getInstance().runOnUiThread( () -> fxMenuItem.setText( label ) );
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    return fxMenuItem.getText();
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param event the event which occurred
   */
  @Override
  public void handle( ActionEvent event ) {
    buttonViewPI.onClick( this, null );
  }

  @Override
  public void setSelected( boolean selected ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
