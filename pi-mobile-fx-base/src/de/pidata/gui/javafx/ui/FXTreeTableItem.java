/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.log.Logger;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

/**
 * Created by pru on 13.07.16.
 */
public class FXTreeTableItem extends TreeItem<TreeNodePI> {

  private final FXTreeTableAdapter fxTreeTableAdapter;
  private boolean childrenAdded = false;

  public boolean isChildrenAdded() {
    return childrenAdded;
  }

  /**
   * Creates a TreeItem with the value property set to the provided object.
   *
   * @param value The object to be stored as the value of this TreeItem.
   */
  public FXTreeTableItem( TreeNodePI value, FXTreeTableAdapter fxTreeTableAdapter ) {
    super( value );
    this.fxTreeTableAdapter = fxTreeTableAdapter;

    addEventHandler( TreeItem.branchExpandedEvent(), event -> {
      if (FXTreeTableAdapter.DEBUG) Logger.info( "Item expanded: " + event.getTreeItem().getValue().toString() );
      // initiate model loading on the underlying TreeNodePI on demand
      if (!childrenAdded) {
        childrenAdded = true;
        TreeNodePI treeNode = getValue();
        if (treeNode != null) {
          treeNode.loadChildren();
          updateChildList();
        }
      }
    } );
  }

  /**
   * A TreeItem is a leaf if it has no children. The isLeaf method may of
   * course be overridden by subclasses to support alternate means of defining
   * how a TreeItem may be a leaf, but the general premise is the same: a
   * leaf can not be expanded by the user, and as such will not show a
   * disclosure node or respond to expansion requests.
   */
  @Override
  public boolean isLeaf() {
    TreeNodePI value = getValue();
    return (value != null && value.isLeaf());
  }

  public void update() {
    TreeNodePI oldValue = getValue();
    this.setValue( null );
    this.setValue( oldValue );
  }

  /**
   * Refresh the {@link FXTreeTableItem} children with the content of the {@link TreeNodePI} children.
   * Don't initiate model loading on the {@link TreeNodePI}!
   */
  public void updateChildList() {
    childrenAdded = true;

    TreeNodePI nodePI = getValue();
    if (FXTreeTableAdapter.DEBUG) Logger.info( "...updateChildList for "+nodePI );

    ObservableList<TreeItem<TreeNodePI>> childItems = getChildren();
    int i = 0;
    for (TreeNodePI childNode : nodePI.childNodeIter()) {
      // IMPORTANT: We must not replace FXTreeTableItem's value (TreeNodePI) because that might change
      // number of children and will not be noticed by FXTreeTableItem !
      TreeItem<TreeNodePI> treeChild = findChildItem( childNode );
      if (treeChild == null) {
        if (i == childItems.size()) {
          childItems.add( new FXTreeTableItem( childNode, fxTreeTableAdapter ) );
        }
        else {
          childItems.set( i, new FXTreeTableItem( childNode, fxTreeTableAdapter ) );
        }
      }
      else {
        int oldIndex = childItems.indexOf( treeChild );
        if (oldIndex > i) {
          childItems.set( oldIndex, childItems.get( i ) );
          childItems.set( i, treeChild );
        }
        else if (oldIndex != i) {
          throw new IllegalArgumentException( "Internal error: duplicate node" );
        }
      }
      i++;
    }
    while (i < childItems.size()) {
      childItems.remove( i );
    }
  }

  private TreeItem<TreeNodePI> findChildItem( TreeNodePI childNode ) {
    for (TreeItem<TreeNodePI> childItem : getChildren()) {
      if (childItem.getValue() == childNode) {
        return childItem;
      }
    }
    return null;
  }
}
