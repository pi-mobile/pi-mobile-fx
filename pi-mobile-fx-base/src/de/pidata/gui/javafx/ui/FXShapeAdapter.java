/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.rect.Rotation;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.rect.Transformation;
import de.pidata.log.Logger;
import de.pidata.rect.Pos;
import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pru on 10.06.17.
 */
public class FXShapeAdapter implements UIShapeAdapter, EventHandler<MouseEvent> {

  private ShapePI shapePI;
  private Shape fxShape;
  private double mouseDownX;
  private double mouseDownY;
  private Map<Transformation,Transform> transformMap;

  public FXShapeAdapter( Shape fxShape, ShapePI shapePI ) {
    this.shapePI = shapePI;
    this.fxShape = fxShape;
    fxShape.setId( shapePI.getFigure().toString() );
    fxShape.setOnMousePressed( this );
    fxShape.setOnMouseReleased( this );
    fxShape.setOnMouseDragged( this );
    shapePI.attachUI( this );
  }

  @Override
  public ShapePI getShapePI() {
    return shapePI;
  }

  public Shape getFxShape() {
    return fxShape;
  }

  @Override
  public void addTransformation( Transformation transformation ) {
    Transform fxTransform;
    if (transformation instanceof Rotation) {
      Rotation rotation = (Rotation) transformation;
      Pos center = rotation.getCenter();
      fxTransform = new Rotate( rotation.getAngle(), center.getX(), center.getY() );
      fxShape.getTransforms().add( fxTransform );
    }
    else {
      throw new IllegalArgumentException( "Unsupported transformation: "+transformation.getClass() );
    }
    if (transformMap == null) {
      transformMap = new HashMap<Transformation,Transform>();
    }
    transformMap.put( transformation, fxTransform );
  }

  @Override
  public void transformationChanged( Transformation transformation ) {
    if (transformation instanceof Rotation) {
      Rotation rotation = (Rotation) transformation;
      Rotate fxRotate = (Rotate) transformMap.get( transformation );
      Pos center = rotation.getCenter();
      fxRotate.setAngle( rotation.getAngle() );
      fxRotate.setPivotX( center.getX() );
      fxRotate.setPivotY( center.getY() );
    }
    else {
      throw new IllegalArgumentException( "Unsupported transformation: "+transformation.getClass() );
    }
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param event the event which occurred
   */
  @Override
  public void handle( MouseEvent event ) {
    if (event.getButton() == MouseButton.PRIMARY) {
      if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
        mouseDownX = event.getX();
        mouseDownY = event.getY();
        shapePI.getFigure().onMousePressed( 1, event.getX(), event.getY(), shapePI );
      }
      else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
        shapePI.getFigure().onMouseDragging( 1, event.getX() - mouseDownX, event.getY() - mouseDownY, shapePI );
      }
      else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
        shapePI.getFigure().onMouseReleased( 1, event.getX(), event.getY(), shapePI );
      }
      else {
        Logger.warn( "FXShapeAdapter: Unknown mouse event="+event.getEventType().toString() );
      }
    }
    else if (event.getButton() == MouseButton.SECONDARY) {
      if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
        mouseDownX = event.getX();
        mouseDownY = event.getY();
        shapePI.getFigure().onMousePressed( 2, event.getX(), event.getY(), shapePI );
      }
    }
  }
}
