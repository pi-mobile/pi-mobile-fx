/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Slider;

public class FXSliderAdapter extends FXAdapter implements UIValueAdapter, UIProgressBarAdapter, ChangeListener<Number> {

  private Slider fxSlider;
  private ProgressBarViewPI progressBarViewPI;

  public FXSliderAdapter( Slider fxSlider, ProgressBarViewPI progressBarViewPI, UIContainer uiContainer ) {
    super( fxSlider, progressBarViewPI, uiContainer );
    this.progressBarViewPI = progressBarViewPI;
    this.fxSlider = fxSlider;
    this.fxSlider.valueProperty().addListener( this );

    this.fxSlider.setBlockIncrement(1);
    this.fxSlider.setMajorTickUnit(1);
    this.fxSlider.setMinorTickCount(0);
    this.fxSlider.setSnapToTicks(true);
  }

  @Override
  public void setMaxValue( int maxValue ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (fxSlider != null) {
        fxSlider.setMax( (double) maxValue );
      }
    });
  }

  @Override
  public int getMaxValue() {
    if(fxSlider != null){
      return (int) fxSlider.getMax();
    }
    return 0;
  }

  @Override
  public void setMinValue( int minValue ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (fxSlider != null) {
        fxSlider.setMin( (double) minValue );
      }
    });
  }

  @Override
  public int getMinValue() {
    if(fxSlider != null){
      return (int) fxSlider.getMin();
    }
    return 0;
  }

  @Override
  public void setProgressMessage( Object progressMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setProgress( double progress ) {
    setValue( Double.valueOf( progress ) );
  }

  @Override
  public void showError( String errorMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showInfo( String infoMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showWarning( String warningMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void hideError() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetColor() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public Object getValue() {
    return Double.valueOf( fxSlider.getValue() );
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (value instanceof DecimalObject) {
        fxSlider.setValue( ((DecimalObject) value).doubleValue() );
      }
      else if (value instanceof Double) {
        fxSlider.setValue( ((Double) value).intValue() );
      }
    });
  }


  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Number> observable, Number oldValue, Number newValue ) {
    if(progressBarViewPI != null && !oldValue.equals( newValue )){
      try {
        progressBarViewPI.onValueChanged( this, Integer.valueOf( newValue.intValue() ), uiContainer.getDataContext() );
      }
      catch (Exception ex) {
        Logger.error( "Error processing vue changed", ex );
      }
    }
  }
}
