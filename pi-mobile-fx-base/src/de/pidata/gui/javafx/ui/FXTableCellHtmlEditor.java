/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.HTMLEditor;

public class FXTableCellHtmlEditor extends FXTableCellEditable {

  public FXTableCellHtmlEditor( TableViewPI tableViewPI, TableView<Model> fxTableView, FXTableAdapter fxTableAdapter,
                                ColumnInfo columnInfo, ViewPI columnViewPI, ViewPI editorViewPI, TableColumn tableColumn,
                                FXUIFactory fxUIFactory ) {
    super( tableViewPI, fxTableView, fxTableAdapter, columnInfo, columnViewPI, editorViewPI, tableColumn, fxUIFactory );
  }

  @Override
  protected void initCellEditorNode() {
    HTMLEditor htmlEditor = getHtmlEditor();
    if (htmlEditor != null) {
      // add key handler
      EventHandler<KeyEvent> keyEventEventHandler = new EventHandler<KeyEvent>() {
        @Override
        public void handle( KeyEvent event ) {
          if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.SPACE) {
            // needs to skip event handling on TableCell as HTMLEditor doesn't get the ENTER key otherwise
            event.consume();
          }
          else {
            // add quick navigation
            if (event.isShortcutDown() && event.isShiftDown()) {
              switch (event.getCode()) {
                case DOWN:
                  cancelEdit();
                  tableViewPI.onNextSelected();
                  break;

                case UP:
                  cancelEdit();
                  tableViewPI.onPrevSelected();
                  break;
              }
            }
          }
        }
      };
      addEventHandler( KeyEvent.KEY_PRESSED, keyEventEventHandler );
    }
  }

  /**
   * Implements the editor specific {@link #prepareEditorNode()}.
   */
  @Override
  protected void prepareEditorNode() {
    super.prepareEditorNode();
    // do size calculation FIXME
  }

  @Override
  protected Object getEditorValue() {
    return ((HTMLEditor) editorNode).getHtmlText();
  }

  private HTMLEditor getHtmlEditor() {
    if (editorNode != null && editorNode instanceof HTMLEditor) {
      return (HTMLEditor)editorNode;
    }
    else {
      return null;
    }
  }
}
