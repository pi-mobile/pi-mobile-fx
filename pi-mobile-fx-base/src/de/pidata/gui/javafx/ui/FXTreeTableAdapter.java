/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentFactory;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.javafx.FXDialogInterface;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITreeTableAdapter;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeTableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.prefs.Preferences;

public class FXTreeTableAdapter extends FXAdapter implements UITreeTableAdapter, ChangeListener, Callback<TreeTableColumn, TreeTableCell>, SettingsHandler {

  public static final boolean DEBUG = false;
  private TreeItem root;
  public static final String COLUMN_WIDTH_PREFIX = "COLUMN_WIDTH_";

  private final EventHandler<MouseEvent> mousePressedEventHandler;
  private TreeTableView<TreeNodePI> fxTreeTableView;
  private TreeTableViewPI treeTableViewPI;
  private FXUIFactory fxUIFactory;

  public FXTreeTableAdapter( TreeTableView fxTreeTableView, TreeTableViewPI treeTableViewPI, FXUIFactory fxUIFactory, UIContainer uiContainer ) {
    super( fxTreeTableView, treeTableViewPI, uiContainer );

    this.fxTreeTableView = fxTreeTableView;
    this.treeTableViewPI = treeTableViewPI;
    this.fxUIFactory = fxUIFactory;

    fxTreeTableView.setRowFactory( getRowFactory() );

    fxTreeTableView.getSelectionModel().selectedItemProperty().addListener( this );

    ObservableList<TreeTableColumn<Model, ?>> tableColumnList = fxTreeTableView.getColumns();

    DefaultTreeTableController treeTableCtrl = (DefaultTreeTableController) treeTableViewPI.getTreeCtrl();
    fxTreeTableView.setShowRoot( treeTableCtrl.showRootNode() );

    if (tableColumnList.size() == 0) {
      int columnCount = treeTableCtrl.columnCount();
      for (short i = 0; i < columnCount; i++) {
        ColumnInfo currentInfo = treeTableCtrl.getColumn( i );
        currentInfo.setView( treeTableViewPI );
        TreeTableColumn column = new TreeTableColumn( currentInfo.getColName().getName() );
        column.setId( currentInfo.getBodyCompID().getName() );
        tableColumnList.add( i, column );
      }
    }
    for (TreeTableColumn tableColumn : tableColumnList) {
      QName columnID = ComponentFactory.NAMESPACE.getQName( tableColumn.getId() );
      ColumnInfo columnInfo = treeTableViewPI.getColumnByCompID( columnID );
      if (columnInfo == null) {
        throw new IllegalArgumentException( "Column info not found for ColCompID=" + columnID );
      }
      columnInfo.setView( treeTableViewPI );
      tableColumn.setCellFactory( this );
      tableColumn.setCellValueFactory( new FXTreeCellValueFactory() );

      tableColumn.setSortable( columnInfo.isSortable() );
    }

    /**
     * Add listeners to mouseclicks at the whole scene and detect whether it was inside the editcell.
     * JavaFXs MouseEvent.MOUSE_CLICKED will fire even if PRESS and RELEASE will have a move between.
     * In our case it will fire if we PRESS inside the the editcell and RELEASE outside as the event
     * listens at the whole scene.
     * handleMouseEventOutsideEditCell() will handle it the way we expect. PRESS and RELEASE ouside the editcell.
     */
    this.mousePressedEventHandler = createMousePressedEventHandler( fxTreeTableView );
    this.fxTreeTableView.addEventFilter( MouseEvent.MOUSE_PRESSED, mousePressedEventHandler );

    // Add click listeners
    initClickListeners();
  }

  private void initClickListeners() {
    fxTreeTableView.setOnMousePressed( e -> {

      //Handle single click
      if (e.isPrimaryButtonDown() && e.getClickCount() == 1) {
        EventTarget target = e.getTarget();
        if (target instanceof Node) {
          Node node = (Node) target;
          while (node != null) {
            if (node instanceof FXTreeTableCell) {
              FXTreeTableCell tableCell = (FXTreeTableCell) node;
              QName columnID = ComponentFactory.NAMESPACE.getQName( tableCell.getId() );
              ColumnInfo columnInfo = treeTableViewPI.getColumnByCompID( columnID );
              TreeNodePI treeNodePI = tableCell.getTreeTableRow().getItem();
              treeTableViewPI.onSelectedCell( treeNodePI, columnInfo );
              if (DEBUG) Logger.info("Left clicked node = " + treeNodePI );
              return;
            }
            node = node.getParent();
          }
        }
      }

      // Handle double click
      if (e.isPrimaryButtonDown() && e.getClickCount() == 2) {
        EventTarget target = e.getTarget();
        if (target instanceof Node) {
          Node node = (Node) target;
          while (node != null) {
            if (node instanceof FXTreeTableCell) {
              FXTreeTableCell tableCell = (FXTreeTableCell) node;
              QName columnID = ComponentFactory.NAMESPACE.getQName( tableCell.getId() );
              ColumnInfo columnInfo = treeTableViewPI.getColumnByCompID( columnID );
              TreeNodePI treeNodePI = tableCell.getTreeTableRow().getItem();
              treeTableViewPI.onDoubleClickCell( treeNodePI, columnInfo );
              if (DEBUG) Logger.info("Double clicked node = " + treeNodePI );
              return;
            }
            node = node.getParent();
          }
        }
      }

      // Handle right click
      if (e.isSecondaryButtonDown()) {
        EventTarget target = e.getTarget();
        if (target instanceof Node) {
          Node node = (Node) target;
          while (node != null) {
            if (node instanceof FXTreeTableCell) {
              FXTreeTableCell tableCell = (FXTreeTableCell) node;
              QName columnID = ComponentFactory.NAMESPACE.getQName( tableCell.getId() );
              ColumnInfo columnInfo = treeTableViewPI.getColumnByCompID( columnID );
              TreeNodePI treeNodePI = tableCell.getTreeTableRow().getItem();
              treeTableViewPI.onRightClickCell( treeNodePI, columnInfo );
              if (DEBUG) Logger.info("Right clicked node = " + treeNodePI );
              return;
            }
            node = node.getParent();
          }
        }
      }
    } );
  }

  @Override
  public TreeTableCell call( TreeTableColumn treeTableColumn ) {
    String columnID = treeTableColumn.getId();
    ColumnInfo columnInfo = treeTableViewPI.getColumnByCompID(  ComponentFactory.NAMESPACE.getQName( columnID ) );

    Controller colRenderer = columnInfo.getRenderCtrl();
    ViewPI rendererViewPI = colRenderer.getView();

    if (columnInfo.isReadOnly()) {
      return new FXTreeTableCell( treeTableViewPI, fxTreeTableView, this, columnInfo, rendererViewPI, treeTableColumn, fxUIFactory );
    }
    else {
      Controller columnEditor = columnInfo.getEditCtrl();
      if (columnEditor == null) {
        return new FXTreeTableCell( treeTableViewPI, fxTreeTableView, this, columnInfo, rendererViewPI, treeTableColumn, fxUIFactory );
      }
      else {
        ViewPI editorViewPI = columnEditor.getView();
        // TODO: Implement FXTTreeTableCellTextEditor if needed.
        //if (columnEditor instanceof TextEditorController) {
        //  return new FXTTreeTableCellTextEditor( tableViewPI, fxTableView, this, columnInfo, rendererViewPI, editorViewPI, tableColumn, fxUIFactory );
        //}
        if (columnEditor instanceof HTMLEditorController) {
          return new FXTreeTableCellHtmlEditor( treeTableViewPI, fxTreeTableView, this, columnInfo, rendererViewPI, editorViewPI, treeTableColumn, fxUIFactory );
        }
        else {
          return new FXTreeTableCellEditable( treeTableViewPI, fxTreeTableView, this, columnInfo, rendererViewPI, editorViewPI, treeTableColumn, fxUIFactory );
        }
      }
    }
  }

  private Callback<TreeTableView, TreeTableRow> getRowFactory() {
    return param -> new FXTreeTableRow( treeTableViewPI );
  }

  @Override
  public void setRootNode( final TreeNodePI rootNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      synchronized (this) {
        root = new FXTreeTableItem( rootNode, this );
        //set root first, other setters will send events for redrawing, which will end up in NPE when root is not set
        fxTreeTableView.setRoot( root );
        root.setExpanded( true );
        fxTreeTableView.setEditable( treeTableViewPI.isEditable() );
        fxTreeTableView.setDisable( false );
      }
    } );
  }

  @Override
  public void updateNode( TreeNodePI treeNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      synchronized (this) {
        FXTreeTableItem treeItem = getTreeItem( getRoot(), treeNode );
        if (treeItem != null) {
          treeItem.update();
        }
      }
    });
  }

  @Override
  public void updateChildList( TreeNodePI treeNodePI ) {
    Platform.getInstance().runOnUiThread( () -> {
      synchronized (this) {
        FXTreeTableItem treeItem = getTreeItem( getRoot(), treeNodePI );
        if (treeItem != null) {
          treeItem.updateChildList();
        }
      }
    });
  }

  @Override
  public TreeNodePI getSelectedNode() {
    TreeItem<TreeNodePI> selectedItem = fxTreeTableView.getSelectionModel().getSelectedItem();
    if (selectedItem == null) {
      return null;
    }
    else {
      return selectedItem.getValue();
    }
  }

  @Override
  public void setSelectedNode( TreeNodePI selectedNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      // force node to be visible
      synchronized (this) {
        expandParentPath( selectedNode );
        TreeItem nodeTreeItem = getTreeItem( getRoot(), selectedNode );
        fxTreeTableView.getSelectionModel().select( nodeTreeItem );
        int rowIndex = fxTreeTableView.getRow( nodeTreeItem );
        fxTreeTableView.scrollTo( rowIndex );
      }
    } );
  }

  private void expandParentPath( TreeNodePI nodePI ) {
    FXTreeTableItem root = getRoot();
    TreeItem nodeItem = getTreeItem( root, nodePI );
    if (nodeItem != null) {
      TreeItem parentItem = nodeItem.getParent();
      while (parentItem != null && !root.equals( parentItem )) {
        if (!parentItem.isExpanded()) {
          parentItem.setExpanded( true );
        }
        parentItem = parentItem.getParent();
      }
    }
  }

  @Override
  public void editNode( TreeNodePI treeNodePI ) {
    if (treeNodePI != null) {
      Platform.getInstance().runOnUiThread( () -> {
        synchronized (this) {
          FXTreeTableItem nodeItem = getTreeItem( getRoot(), treeNodePI );
          if (nodeItem != null) {
            // force edited node to be selected
            fxTreeTableView.getSelectionModel().select( nodeItem );
            int rowIndex = fxTreeTableView.getRow( nodeItem );
            fxTreeTableView.edit( rowIndex, fxTreeTableView.getTreeColumn() );
          }
        }
      } );
    }
  }

  @Override
  public void setExpanded( TreeNodePI treeNodePI, boolean expand ) {
    Platform.getInstance().runOnUiThread( () -> {
      synchronized (this) {
        TreeItem<TreeNodePI> treeItem = getTreeItem( getRoot(), treeNodePI );
        if (treeItem != null) {
          treeItem.setExpanded( expand );
        }
      }
    } );
  }

  private FXTreeTableItem getRoot() {
    TreeItem root = fxTreeTableView.getRoot();
    if (root != null) {
      return (FXTreeTableItem) root;
    }
    return null;
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed(ObservableValue observable, Object oldValue, Object newValue) {
    FXTreeTableItem selectedItem = (FXTreeTableItem) newValue;
    if (selectedItem != null) {
      treeTableViewPI.onSelectionChanged( selectedItem.getValue() );
    }
  }

  public FXTreeTableItem getTreeItem( FXTreeTableItem rootItem, TreeNodePI treeNodePI) {
    synchronized (this) {
      if (rootItem.getValue() == treeNodePI) {
        return rootItem;
      }
      else if (rootItem.isChildrenAdded()) {
        for (TreeItem<TreeNodePI> childNode : rootItem.getChildren()) {
          FXTreeTableItem result = getTreeItem( (FXTreeTableItem) childNode, treeNodePI );
          if (result != null) {
            return result;
          }
        }
      }
      return null;
    }
  }

  @Override
  public void columnChanged( ColumnInfo columnInfo ) {
    try {
      Platform.getInstance().runOnUiThread( new Runnable() {
        @Override
        public void run() {
          ObservableList<TreeTableColumn<TreeNodePI, ?>> tableColumnList = fxTreeTableView.getColumns();
          TreeTableColumn tableColumn = tableColumnList.get( columnInfo.getIndex() );
          tableColumn.setText( columnInfo.getColName().getName() );
        }
      } );
    }
    catch (Exception e) {
      Logger.error( "Error trying to set column changes.", e );
    }
  }

  private EventHandler<MouseEvent> createMousePressedEventHandler( TreeTableView<TreeNodePI> fxTreeTableView ) {
    return mouseEvent -> {
      EventTarget target = mouseEvent.getTarget();
      if(DEBUG) Logger.debug( "gotClickEvent: " + mouseEvent + " on target: " + target );
      //synchronized (fx) {
        if (target instanceof Node ) {
          if (isChildOfTable( (Node) target )) {
            Node node = (Node) target;
            while (node != null) {
              if (node instanceof TreeTableCell) {
                TreeTableCell tableCell = (TreeTableCell) node;
                if (tableCell.isEditing()) {
                  if (DEBUG) Logger.debug( "Clicked in editing cell" );
                }
                else {
                  if (DEBUG) Logger.debug( "Clicked in non-edit cell" );
                  int row = tableCell.getIndex();
                  // do not change selection here - that will damage multi selection!
                  if (tableCell.getTableColumn().isEditable()) {
                    fxTreeTableView.edit( row, tableCell.getTableColumn() );
                  }
                  // If the column is not editable explicitly switch off editing!
                  // Otherwise, the current changed cell content might get lost.
                  else {
                    fxTreeTableView.edit(-1, null);
                  }
                }
                return;
              }
              node = node.getParent();
            }
            if (DEBUG) Logger.debug( "Clicked outside of table cells" );
            fxTreeTableView.edit( -1, null );
          }
          else{
            if (DEBUG) Logger.debug( "Clicked outside of table cells" );
            fxTreeTableView.edit( -1, null );
          }
        }
        else {
          if (DEBUG) Logger.debug( "Click target is not a node" );
          fxTreeTableView.edit( -1, null );
        }
      //}
    };
  }

  private boolean isChildOfTable( Node node ) {
    while (node != null) {
      if (node instanceof TreeTableView && node == this.fxTreeTableView) {
        return true;
      }
      node = node.getParent();
    }
    return false;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if(this.mousePressedEventHandler != null) {
      Scene scene = this.fxTreeTableView.getScene();
      if (scene != null) {
        scene.removeEventFilter( MouseEvent.MOUSE_PRESSED, mousePressedEventHandler );
      }
    }
    super.detach();
  }

  @Override
  public void loadSettings() {

    try {
      String prefID = getPrefID();
      Preferences pref = Preferences.userRoot().node( prefID );

      List<TreeTableColumn> currentColumns = Collections.unmodifiableList( new ArrayList<>( fxTreeTableView.getColumns() ) );

      for (int i = 0; i < currentColumns.size(); i++) {
        TreeTableColumn column = currentColumns.get( i );
        String propertyLabel = COLUMN_WIDTH_PREFIX + column.getId().toUpperCase();
        Double storedWidth = Double.valueOf( pref.getDouble( propertyLabel, column.getWidth() ) );
        column.setPrefWidth( storedWidth.doubleValue() );
      }
    }
     catch (IllegalArgumentException e) {
      Logger.error( e.getMessage() );
    }
  }

  @Override
  public void saveSettings() {

    try {
      String prefID = getPrefID();
      Preferences pref = Preferences.userRoot().node( prefID );

      List<TreeTableColumn> currentColumns = Collections.unmodifiableList( new ArrayList<>( fxTreeTableView.getColumns() ) );

      for (int i = 0; i < currentColumns.size(); i++) {
        TreeTableColumn column = currentColumns.get( i );
        String propertyLabel = COLUMN_WIDTH_PREFIX + column.getId().toUpperCase();
        pref.putDouble( propertyLabel, column.getWidth() );
      }
    }
    catch (IllegalArgumentException e) {
      Logger.error( e.getMessage() );
    }
  }

   private String getPrefID() throws IllegalArgumentException {

    QName compID = treeTableViewPI.getComponentID();
    String settingsID;
    if (uiContainer instanceof FXDialogInterface) {
      settingsID = ((FXDialogInterface) uiContainer).getSettingsID();
    }
    else if (uiContainer instanceof FXFragmentAdapter) {
      settingsID = ((FXDialogInterface) ((FXFragmentAdapter) uiContainer).getViewPI().getController().getDialogController().getDialogComp()).getSettingsID();
    }
    else {
      throw new IllegalArgumentException( "LoadSettings not implemented for this type of container " + uiContainer );
    }

    return settingsID + "_" + compID.toString();
  }
}
