/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.javafx.FXColor;
import de.pidata.gui.javafx.FXUIContainer;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.*;
import de.pidata.log.Logger;
import de.pidata.log.Profiler;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 * Created by pru on 14.07.2016.
 */
public class FXTableCell extends TableCell<TableView, TableCellPI> implements ChangeListener, FXUIContainer {

  protected TableViewPI tableViewPI;
  protected FXTableAdapter fxTableAdapter;
  protected TableView<Model> fxTableView;

  protected ViewPI columnViewPI;
  protected TableColumn tableColumn;

  protected Node rendererNode;
  protected FXUIFactory fxUIFactory;

  public FXTableCell( TableViewPI tableViewPI, TableView<Model> fxTableView, FXTableAdapter fxTableAdapter,
                      ViewPI columnViewPI, TableColumn tableColumn, FXUIFactory fxUIFactory ) {
    super();
    this.tableViewPI = tableViewPI;
    this.fxTableAdapter = fxTableAdapter;
    this.fxTableView = fxTableView;
    this.columnViewPI = columnViewPI;
    this.tableColumn = tableColumn;
    this.fxUIFactory = fxUIFactory;

    tableColumn.setEditable( false );

    fxTableView.getSelectionModel().selectedItemProperty().addListener(this);
  }

  /**
   * The updateItem method should not be called by developers, but it is the
   * best method for developers to override to allow for them to customise the
   * visuals of the cell. To clarify, developers should never call this method
   * in their code (they should leave it up to the UI control, such as the
   * {@link ListView} control) to call this method. However,
   * the purpose of having the updateItem method is so that developers, when
   * specifying custom cell factories (again, like the ListView
   * {@link ListView#cellFactoryProperty() cell factory}),
   * the updateItem method can be overridden to allow for complete customisation
   * of the cell.
   * <p>
   * <p>It is <strong>very important</strong> that subclasses
   * of Cell override the updateItem method properly, as failure to do so will
   * lead to issues such as blank cells or cells with unexpected content
   * appearing within them. Here is an example of how to properly override the
   * updateItem method:
   * <p>
   * <pre>
   * protected void updateItem(T item, boolean empty) {
   *     super.updateItem(item, empty);
   *
   *     if (empty || item == null) {
   *         setText(null);
   *         setGraphic(null);
   *     } else {
   *         setText(item.toString());
   *     }
   * }
   * </pre>
   * <p>
   * <p>Note in this code sample two important points:
   * <ol>
   * <li>We call the super.updateItem(T, boolean) method. If this is not
   * done, the item and empty properties are not correctly set, and you are
   * likely to end up with graphical issues.</li>
   * <li>We test for the <code>empty</code> condition, and if true, we
   * set the text and graphic properties to null. If we do not do this,
   * it is almost guaranteed that end users will see graphical artifacts
   * in cells unexpectedly.</li>
   * </ol>
   *
   * @param item  The new item for the cell.
   * @param empty whether or not this cell represents data from the list. If it
   *              is empty, then it does not represent any domain data, but is a cell
   *              being used to render an "empty" row.
   * @expert
   */
  @Override
  protected final void updateItem( TableCellPI item, boolean empty ) {
    super.updateItem( item, empty );
    Platform.getInstance().runOnUiThread( () -> doUpdateItem(item, empty) );
  }

  protected void doUpdateItem( TableCellPI item, boolean empty ) {
    Profiler.count( "FXTableCell doUpdateItem" );
    try {
      if (empty || item == null) {
        setText( null );
        setGraphic( null );
        resetCellColor();
      }
      else {

        ColumnInfo columnInfo = item.getColumnInfo();
        if (rendererNode == null) {
          rendererNode = createCellRendererNode( columnInfo );
        }
        // dynamically set cell color
        boolean bgColChanged = false;
        QName rowColorID = tableViewPI.getRowColorID();
        if (rowColorID != null) {
          QName colorColumnID = tableViewPI.getColorColumnID();
          // if color column is defined, set color only for that column
          // else set color for whole row
          if (colorColumnID == null || columnInfo.getColName().equals( colorColumnID )) {
            int rowIndex = getIndex();
            if (rowIndex >= 0) {
              Model nodeModel = item.getModel();
              Object rowColor = nodeModel.get( rowColorID );
              if (!Helper.isNullOrEmpty( rowColor )) {
                String cellCss;
                if (getTableRow().isSelected()) {
                  cellCss = " -fx-background-color: transparent; ";
                } else {
                  if (rowIndex % 2 == 0) { // gerade
                    cellCss = " -fx-background-color: derive(" + rowColor.toString() + ", 40%); " +
                        " -fx-background-insets: 1 1 1 1;";
                  } else { // ungerade
                    cellCss = " -fx-background-color: " + rowColor.toString() + "; " +
                        " -fx-background-insets: 1 1 1 1;";
                  }
                }
                setStyle( cellCss );
                bgColChanged = true;
              }
            }
          }
        }
        if (!bgColChanged) {
          resetCellColor();
        }

        if (rendererNode instanceof Text) {
          rendererNode.setStyle("-fx-text-alignment:left;");
          ((Text)rendererNode).wrappingWidthProperty().bind(getTableColumn().widthProperty().subtract(35));
        }

        String columnAlign = columnInfo.getAlign();
        if (Helper.isNotNullAndNotEmpty( columnAlign )) {
          if (columnAlign.equals( "center" )) {
            this.setAlignment( Pos.CENTER );
          }
          if (columnAlign.equals( "right" )) {
            this.setAlignment( Pos.CENTER_RIGHT );
          }
        }

        setGraphic( rendererNode );
        fxUIFactory.updateCell( this, columnInfo, tableColumn.getPrefWidth() );
        setContentDisplay( ContentDisplay.GRAPHIC_ONLY );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error updating TableCell item="+item, ex );
    }
  }


  private void resetCellColor() {
    FXColor compCol = (FXColor) Platform.getInstance().getColor( ComponentColor.TRANSPARENT );
    if (compCol == null) {
      Logger.warn( "FXPlatform did not know color, ID=" + ComponentColor.TRANSPARENT );
    }
    else {
      setStyle( "-fx-background-color:" + compCol.toStyleFormat()+";" );
    }
  }


  protected Node createCellRendererNode( ColumnInfo columnInfo ) {
    if (columnViewPI != null) {
      QName moduleID = tableViewPI.getRowCompID();
      rendererNode = fxUIFactory.createCellRendererNode( this, columnInfo, moduleID );
      setGraphic( rendererNode );
      Controller renderCtrl = columnInfo.getRenderCtrl();
      rendererNode.setId( renderCtrl.getView().getComponentID().getName() );
      if (renderCtrl != null) {
        renderCtrl.getView().attachUIRenderComponent( this );
      }
    }
    if (rendererNode == null) {
      rendererNode = new Text();
      setGraphic( rendererNode );
    }
    return rendererNode;
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    return getItem().getModel();
  }

  @Override
  public void changed( ObservableValue observable, Object oldValue, Object newValue ) {

    if (getItem() == null) {
      return;
    }

    ColumnInfo columnInfo = getItem().getColumnInfo();
    if (rendererNode == null) {
      createCellRendererNode( columnInfo );
    }
    // dynamically set cell color
    QName rowColorID = tableViewPI.getRowColorID();
    if (rowColorID != null) {
      QName colorColumnID = tableViewPI.getColorColumnID();
      // if color column is defined, set color only for that column
      // else set color for whole row
      if (colorColumnID == null || columnInfo.getColName().equals(colorColumnID)) {
        int rowIndex = getIndex();
        if (rowIndex >= 0) {
          Model nodeModel = getItem().getModel();
          Object rowColor = nodeModel.get(rowColorID);
          if (!Helper.isNullOrEmpty(rowColor)) {

            String cellCss;

            boolean multiSelectedRow = false;
            if(getTableView().getSelectionModel().getSelectedIndices().size() > 1) {
              if(getTableRow().isSelected()) {
                multiSelectedRow = true;
              }
            }

            if (getTableRow().getItem().equals(newValue) || multiSelectedRow ) {
              cellCss = " -fx-background-color: transparent; ";
            }
            else {
              if (rowIndex % 2 == 0) { // gerade
                cellCss = " -fx-background-color: derive(" + rowColor.toString() + ", 40%); " +
                    " -fx-background-insets: 1 1 1 1;";
              } else { // ungerade
                cellCss = " -fx-background-color: " + rowColor.toString() + "; " +
                    " -fx-background-insets: 1 1 1 1;";
              }
            }

            setStyle(cellCss);
          }
        }
      }
    }

    fxUIFactory.updateCell(this, columnInfo, tableColumn.getPrefWidth());
  }

  /**
   * Returns the Node identified by uiCompID within this FXUIContainer
   *  @param uiCompID    component (Node) identifier
   * @param moduleID
   */
  @Override
  public Node findUIComp( QName uiCompID, QName moduleID ) {
    Node fxNode = this.rendererNode.lookup( "#" + uiCompID.getName() );
    return fxNode;
  }

  @Override
  public MenuItem findMenuItem( QName componentID ) {
    // There are no menuItems for cells.
    return null;
  }

  @Override
  public Menu findMenu( QName componentID ) {
    // There are no menus for cells.
    return null;
  }

  @Override
  public Node findToolBarItem( QName componentID ) {
    // There are no menus for cells.
    return null;
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return fxUIFactory;
  }
}
