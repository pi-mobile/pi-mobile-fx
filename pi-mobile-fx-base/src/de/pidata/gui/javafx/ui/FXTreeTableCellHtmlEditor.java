/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.FlagController;
import de.pidata.gui.controller.base.HTMLEditorController;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.javafx.controls.PIHtmlEditor;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeTableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.HTMLEditor;

import java.io.IOException;
import java.net.URL;

public class FXTreeTableCellHtmlEditor extends FXTreeTableCellEditable {

  public FXTreeTableCellHtmlEditor( TreeTableViewPI treeTableViewPI, TreeTableView<TreeNodePI> fxTreeTableView, FXTreeTableAdapter fxTreeTableAdapter,
                                    ColumnInfo columnInfo, ViewPI columnViewPI, ViewPI editorViewPI, TreeTableColumn treeTableColumn,
                                    FXUIFactory fxUIFactory ) {
    super( treeTableViewPI, fxTreeTableView, fxTreeTableAdapter, columnInfo, columnViewPI, editorViewPI, treeTableColumn, fxUIFactory );
  }

  @Override
  protected synchronized void createCellEditorNode() {
    if (editorViewPI != null) {
      editorNode = new PIHtmlEditor();
      editorNode.setId( editorViewPI.getController().getView().getComponentID().getName() );
      initCellEditorNode();

      editorNode.focusedProperty().addListener( new ChangeListener<Boolean>() {
        @Override
        public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
          if ((newValue != null) && !(newValue.booleanValue())) {
            cancelEdit();
          }
        }
      } );
    }
  }

  @Override
  protected void initCellEditorNode() {
    HTMLEditor htmlEditor = getHtmlEditor();
    if (htmlEditor != null) {
      // add key handler
      EventHandler<KeyEvent> keyEventEventHandler = new EventHandler<KeyEvent>() {
        @Override
        public void handle( KeyEvent event ) {
          if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.SPACE) {
            // needs to skip event handling on TableCell as HTMLEditor doesn't get the ENTER key otherwise
            event.consume();
          }
          else {
            // add quick navigation
            if (event.isShortcutDown() && event.isShiftDown()) {
              switch (event.getCode()) {
                case DOWN:
                  cancelEdit();
                  fxTreeTableView.getSelectionModel().selectNext();
                  break;

                case UP:
                  cancelEdit();
                  fxTreeTableView.getSelectionModel().selectPrevious();
                  break;
              }
            }
          }
        }
      };
      addEventHandler( KeyEvent.KEY_PRESSED, keyEventEventHandler );
    }
  }

  /**
   * Implements the editor specific {@link #prepareEditorNode()}.
   */
  @Override
  protected void prepareEditorNode() {
    super.prepareEditorNode();
    // do size calculation FIXME
  }

  @Override
  protected Object getEditorValue() {
    return ((HTMLEditor) editorNode).getHtmlText();
  }

  private HTMLEditor getHtmlEditor() {
    if (editorNode != null && editorNode instanceof HTMLEditor) {
      return (HTMLEditor)editorNode;
    }
    else {
      return null;
    }
  }
}
