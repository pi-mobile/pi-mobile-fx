/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.javafx.controls.ProgressInfo;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.string.Helper;
import de.pidata.gui.component.base.Platform;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

public class FXProgressBarAdapter extends FXValueAdapter implements UIProgressBarAdapter {

  private ProgressBar fxProgressBar;
  private ProgressBarViewPI progressBarViewPI;
  private Namespace valueNS;
  private Label progressText;
  private TextArea errorText;
  private HBox errorBox;
  private Button errorButton;

  public FXProgressBarAdapter( ProgressInfo fxProgressInfo, ProgressBarViewPI progressBarViewPI, UIContainer uiContainer ) {
    super( fxProgressInfo.getProgressBar(), progressBarViewPI, uiContainer );
    this.fxProgressBar = fxProgressInfo.getProgressBar();
    try {
      progressText = fxProgressInfo.getProgressText();
      this.fxProgressBar.setStyle( "-fx-accent: deepskyblue" );

      errorBox = fxProgressInfo.getErrorBox();
      errorBox.setManaged( false );
      errorBox.setVisible( false );

      errorText = fxProgressInfo.getErrorText();
      errorButton = fxProgressInfo.getErrorButton();
      errorButton.setOnAction( event -> {
        errorBox.setManaged( false );
        errorBox.setVisible( false );
        resetColor();
        }
      );
    }
    catch (Exception ex) {
      throw new RuntimeException( "Error while creating the ProgressInfo" );
    }
  }

  @Override
  public void setMaxValue( int maxValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getMaxValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinValue( int minValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getMinValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setProgressMessage( Object progressMessage ) {
    Platform.getInstance().runOnUiThread( () -> progressText.setText( progressMessage.toString() ) );
  }

  @Override
  public void setProgress( double progress ) {
    double fxProgress = progress/100.0;
    Platform.getInstance().runOnUiThread( () -> fxProgressBar.setProgress( fxProgress ) );
  }

  @Override
  public Object getValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  public void showError( String errorMessage ) {
    Platform.getInstance().runOnUiThread( () -> {
          try {
            if(!Helper.isNullOrEmpty( errorMessage )) {
              errorBox.setManaged( true );
              errorBox.setVisible( true );
              errorText.setStyle( "-fx-background-color: lightcoral" );
              errorText.setText( errorMessage );
            }
            fxProgressBar.setStyle( "-fx-accent: lightcoral" );
          }
          catch (Exception e) {
            Logger.error("FXProgressBarAdapter showError()", e);
          }
        }
    );
  }

  @Override
  public void showInfo( String infoMessage ) {
    Platform.getInstance().runOnUiThread( () -> {
          try {
            if(!Helper.isNullOrEmpty( infoMessage )) {
              errorBox.setManaged( true );
              errorBox.setVisible( true );
              errorText.setStyle( "-fx-background-color: lightgreen" );
              errorText.setText( infoMessage );
            }
            fxProgressBar.setStyle( "-fx-accent: lightgreen" );
          }
          catch (Exception e) {
            Logger.error("FXProgressBarAdapter showInfo()", e);
          }
        }
    );
  }

  @Override
  public void showWarning(String warningMessage){
    Platform.getInstance().runOnUiThread( () -> {
          try {
            if(!Helper.isNullOrEmpty( warningMessage )) {
              errorBox.setManaged( true );
              errorBox.setVisible( true );
              errorText.setStyle( "-fx-background-color: yellow" );
              errorText.setText( warningMessage );
            }
            fxProgressBar.setStyle( "-fx-accent: yellow" );
          }
          catch (Exception e) {
            Logger.error("FXProgressBarAdapter showWarning()", e);
          }
        }
    );
  }

  @Override
  public void resetColor(){
    Platform.getInstance().runOnUiThread( ()->{
      try{
        errorText.setStyle( "-fx-background-color: lightgreen" );
        fxProgressBar.setStyle( "-fx-accent: deepskyblue" );
      }
      catch (Exception e){
        Logger.error("FXProgressBarAdapter resetColor()", e);
      }
    } );
  }

  public void hideError() {
    Platform.getInstance().runOnUiThread( () -> {
          try {
            errorBox.setManaged( false );
            errorBox.setVisible( false );
            errorText.setText( "" );
          }
          catch (Exception e) {
            Logger.error("FXProgressBarAdapter hideError()", e);
          }
        }
    );
  }
}
