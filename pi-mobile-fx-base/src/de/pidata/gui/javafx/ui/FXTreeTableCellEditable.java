/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.*;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.javafx.controls.PIHtmlEditor;
import de.pidata.gui.view.base.FlagViewPI;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeTableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.messages.ErrorMessages;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.web.HTMLEditor;

import java.io.IOException;
import java.net.URL;

public class FXTreeTableCellEditable extends FXTreeTableCell {

  protected ViewPI editorViewPI;
  protected Node editorNode;

  public FXTreeTableCellEditable( TreeTableViewPI treeTableViewPI, TreeTableView<TreeNodePI> fxTreeTableView, FXTreeTableAdapter fxTreeTableAdapter,
                                  ColumnInfo columnInfo, ViewPI rendererViewPI, ViewPI editorViewPI, TreeTableColumn treeTableColumn, FXUIFactory fxUIFactory ) {
    super( treeTableViewPI, fxTreeTableView, fxTreeTableAdapter, columnInfo, rendererViewPI, treeTableColumn, fxUIFactory );
    this.editorViewPI = editorViewPI;
    setEditable( true );
    treeTableColumn.setEditable( true );
  }

  @Override
  protected Node createCellRendererNode( ColumnInfo columnInfo, QName rowDefID ) {
    Controller renderCtrl = columnInfo.getRenderCtrl();
    if (renderCtrl instanceof ModuleController) {
      ModuleGroup moduleGroup = ((ModuleController) renderCtrl).getModuleGroup();
      if (moduleGroup != null) {
        renderCtrl = moduleGroup.getController( 0 ); // TODO multiple child nodes
      }
    }
    if ((renderCtrl instanceof FlagController)) {
      FlagViewPI flagView = (FlagViewPI) renderCtrl.getView();
      CheckBox checkBox = new CheckBox();
      boolean triState = (flagView != null && flagView.isTristate());
      if (triState) {
        checkBox.setAllowIndeterminate( triState );
        checkBox.indeterminateProperty().addListener( new ChangeListener<Boolean>() {
          @Override
          public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
            if ((newValue != null) && newValue.booleanValue()) {
              //indeterminated changed to true -> new flagvalue is null
              newValue = null;
            }
            else {
              //indeterminated changed to false -> new flagvalue is selected value
              newValue = Boolean.valueOf( checkBox.isSelected() );
            }
            flagChanged( checkBox, newValue );
          }
        } );
      }
      if (columnInfo.isReadOnly()) {
        checkBox.setDisable( true );
      }
      else {
        checkBox.selectedProperty().addListener( new ChangeListener<Boolean>() {
          @Override
          public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
            flagChanged( checkBox, newValue );
          }
        } );
      }
      checkBox.setFocusTraversable( false );
      return checkBox;
    }
    else {
      return super.createCellRendererNode( columnInfo, rowDefID );
    }
  }

  private void flagChanged( CheckBox checkBox, Boolean newValue ) {
    if (!isEditing()) {
      editorNode = checkBox;
    }
    Model nodeModel = getItem().getNodeModel();
    Model row = columnInfo.getValueModel( nodeModel );
    Logger.info( "FlagChanged: "+row.get( Namespace.getInstance( "de.pidata.umltools" ).getQName( "name" ))+", value="+newValue);
    row.set( columnInfo.getValueID(), newValue );
  }

  protected synchronized void createCellEditorNode() {
    if (editorViewPI != null) {
      // load new UI instance for editing
      QName uiCompID = editorViewPI.getComponentID();
      QName moduleID = editorViewPI.getModuleID();
      if (moduleID != null) {
        String resourceName = "/layout/" + moduleID.getName() + ".fxml";
        URL editorModuleRes = getClass().getResource( resourceName );
        try {
          Node moduleNode = FXMLLoader.load( editorModuleRes );
          editorNode = moduleNode.lookup( "#" + uiCompID.getName() );
          if (editorNode == null) {
            throw new IllegalArgumentException( "Could not find UI node=" + uiCompID.getName() + " in moduleID=" + moduleID );
          }
          initCellEditorNode();
        }
        catch (IOException e) {
          Logger.error( "Error loading module resource name=" + resourceName, e );
          throw new IllegalArgumentException( "Could not find UI module, moduleID=" + moduleID );
        }
      }
    }
    if (editorNode == null) {
      Controller editorCtrl = editorViewPI.getController();
      if (editorCtrl instanceof FlagController) {
        editorNode = new CheckBox();
      }
      else {
        editorNode = new TextField();
      }
      initCellEditorNode();
    }
    editorNode.focusedProperty().addListener( new ChangeListener<Boolean>() {
      @Override
      public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
        if ((newValue != null) && !(newValue.booleanValue())) {
          cancelEdit();
        }
      }
    } );
  }

  protected void initCellEditorNode() {
    // default: nothing to do
  }

  /**
   * Implements the editor specific {@link #prepareEditorNode()}.
   * <p>
   * The default assumes having a {@link TextField} as editor.
   */
  protected void prepareEditorNode() {
    // TextField will adjust itself to the Cell's width
  }

  protected Object getEditorValue() {
    if (editorNode instanceof TextInputControl) {
      return ((TextInputControl) editorNode).getText();
    }
    else if (editorNode instanceof CheckBox) {
      return Boolean.valueOf(((CheckBox) editorNode).isSelected());
    }
    else {
      throw new IllegalArgumentException( ErrorMessages.ERROR + " Unsupported editorNode class="+editorNode.getClass().getName() );
    }
  }

  public void resetEditor() {
    if (FXTreeTableAdapter.DEBUG) Logger.debug( "resetEditor, row="+getIndex() );
    setGraphic( rendererNode );
    editorNode = null;
  }


  @Override
  protected void doUpdateItem( TreeNodePI treeItem, boolean empty ) {
    try {
      if (empty || treeItem == null) {
        setText( null );
        setGraphic( null );
      }
      else {
        if (isEditing()) {
          if (FXTreeTableAdapter.DEBUG) Logger.debug( "updateItem/isEditing, row=" + getIndex() );
          setGraphic( editorNode );
          fxUIFactory.updateCell( this, columnInfo, treeTableColumn.getPrefWidth() );
        }
        else {
          super.doUpdateItem( treeItem, empty );
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "Error updating TreeTableCell item="+treeItem, ex );
    }
  }

  /**
   * {@inheritDoc}
   * <br><br>
   * This is the default edit cell activated event's target.
   */
  @Override
  public void startEdit() {
    TreeNodePI treeItem = getItem();
    try {
      if (FXTreeTableAdapter.DEBUG) {
        Logger.debug( "startEdit, row=" + getIndex() + " row " + treeItem );
      }
      super.startEdit();

      if (FXTreeTableAdapter.DEBUG) {
        Logger.debug( "prepareEditor, row=" + getIndex() + " row " + treeItem );
      }
      if (editorViewPI != null) {
        if (editorNode == null) {
          createCellEditorNode();
        }
        prepareEditorNode();

        setGraphic( editorNode );
        fxUIFactory.updateCell( this, columnInfo, treeTableColumn.getPrefWidth() );

        treeTableViewPI.onStartEdit( treeItem, columnInfo );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error updating TreeTableCell item="+treeItem, ex );
    }
  }

  /**
   * {@inheritDoc}
   * <br><br>
   * This is the default edit cell deactivated event's target.
   */
  @Override
  public void cancelEdit() {
    if (FXTreeTableAdapter.DEBUG) Logger.debug( "cancelEdit, row="+getIndex() );
    int index = getIndex();
    if (index >= 0) {
      TreeNodePI selectedNode = getItem();
      Object newValue = getEditorValue();
      treeTableViewPI.onStopEdit( selectedNode, columnInfo.getValueID(), newValue );
    }
    super.cancelEdit();

    // editorNode durch rendererNode ersetzen
    resetEditor();
  }
}
