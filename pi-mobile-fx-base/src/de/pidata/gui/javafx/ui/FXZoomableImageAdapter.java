/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.javafx.controls.ZoomableImageView;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ImageViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;

/**
 *
 * implementation according to https://stackoverflow.com/questions/16680295/javafx-correct-scaling
 *
 * @author dsc
 */
public class FXZoomableImageAdapter extends FXImageAdapter {

  final double SCALE_DELTA = 1.1;

  private Group scrollContent;
  private StackPane zoomPane;
  private ZoomableImageView zoomableImageView;

  public FXZoomableImageAdapter( ZoomableImageView zoomableImageView, ViewPI viewPI, UIContainer uiContainer ) {
    super( zoomableImageView.getFxImageView(), viewPI, uiContainer );

    this.zoomableImageView = zoomableImageView;
    Node content = zoomableImageView.getContent();
    if (content instanceof Group) {
      scrollContent = (Group) content;
    }
    zoomPane = zoomableImageView.getZoomPane();

    EventHandler<ScrollEvent> zoomHandler = new EventHandler<ScrollEvent>() {
      @Override
      public void handle( ScrollEvent event ) {
        if (event.isControlDown()) {
          // handle zoom request
          event.consume();

          if (event.getDeltaY() == 0) {
            return;
          }

          double scaleFactor =
              (event.getDeltaY() > 0)
                  ? SCALE_DELTA
                  : 1 / SCALE_DELTA;

          // amount of scrolling in each direction in scrollContent coordinate
          // units
          Point2D scrollOffset = null;
          if (scrollContent != null) {
            scrollOffset = figureScrollOffset( scrollContent, zoomableImageView );
          }

          fxImageView.setScaleX( fxImageView.getScaleX() * scaleFactor );
          fxImageView.setScaleY( fxImageView.getScaleY() * scaleFactor );

          // move viewport so that old center remains in the center after the
          // scaling
          if (scrollOffset != null) {
            repositionScroller( scrollContent, zoomableImageView, scaleFactor, scrollOffset );
          }
          ((ImageViewPI)viewPI).onZoomFactorChanged( getZoomFactor(), uiContainer.getDataContext() );
        }
      }
    };

    if (scrollContent != null && zoomPane != null) {
      zoomableImageView.viewportBoundsProperty().addListener( new ChangeListener<Bounds>() {
        @Override
        public void changed( ObservableValue<? extends Bounds> observable,
                             Bounds oldValue, Bounds newValue ) {
          zoomPane.setMinSize( newValue.getWidth(), newValue.getHeight() );
          zoomPane.setPrefSize( newValue.getWidth(), newValue.getHeight() );
          zoomPane.setMaxSize( newValue.getWidth(), newValue.getHeight() );
        }
      } );

      zoomableImageView.setPrefViewportWidth( 256 );
      zoomableImageView.setPrefViewportHeight( 256 );

      zoomPane.setOnScroll( zoomHandler );
      zoomPane.layoutBoundsProperty().addListener(new ChangeListener<Bounds>() {
        @Override public void changed(ObservableValue<? extends Bounds> observable, Bounds oldBounds, Bounds bounds) {
          repositionScroller( scrollContent, zoomableImageView, getZoomFactor(), figureScrollOffset( scrollContent, zoomableImageView ));
        }
      });
    }
    else {
      fxImageView.setOnScroll( zoomHandler );
    }

    Scene scene = fxImageView.getScene();
    KeyCodeCombination keyCombination = new KeyCodeCombination( KeyCode.DIGIT0, KeyCombination.CONTROL_DOWN );
        scene.getAccelerators().put(
        keyCombination,
        new Runnable() {
          @Override public void run() {
            fxImageView.setScaleX( 1 );
            fxImageView.setScaleY( 1 );
          }
        }
    );
  }

  private Point2D figureScrollOffset( Node scrollContent, ScrollPane scroller ) {
    double extraWidth = scrollContent.getLayoutBounds().getWidth() - scroller.getViewportBounds().getWidth();
    double hScrollProportion = (scroller.getHvalue() - scroller.getHmin()) / (scroller.getHmax() - scroller.getHmin());
    double scrollXOffset = hScrollProportion * Math.max( 0, extraWidth );
    double extraHeight = scrollContent.getLayoutBounds().getHeight() - scroller.getViewportBounds().getHeight();
    double vScrollProportion = (scroller.getVvalue() - scroller.getVmin()) / (scroller.getVmax() - scroller.getVmin());
    double scrollYOffset = vScrollProportion * Math.max( 0, extraHeight );
    return new Point2D( scrollXOffset, scrollYOffset );
  }

  private void repositionScroller( Node scrollContent, ScrollPane scroller, double scaleFactor, Point2D scrollOffset ) {
    double scrollXOffset = scrollOffset.getX();
    double scrollYOffset = scrollOffset.getY();
    double extraWidth = scrollContent.getLayoutBounds().getWidth() - scroller.getViewportBounds().getWidth();
    if (extraWidth > 0) {
      double halfWidth = scroller.getViewportBounds().getWidth() / 2;
      double newScrollXOffset = (scaleFactor - 1) * halfWidth + scaleFactor * scrollXOffset;
      scroller.setHvalue( scroller.getHmin() + newScrollXOffset * (scroller.getHmax() - scroller.getHmin()) / extraWidth );
    }
    else {
      scroller.setHvalue( scroller.getHmin() );
    }
    double extraHeight = scrollContent.getLayoutBounds().getHeight() - scroller.getViewportBounds().getHeight();
    if (extraHeight > 0) {
      double halfHeight = scroller.getViewportBounds().getHeight() / 2;
      double newScrollYOffset = (scaleFactor - 1) * halfHeight + scaleFactor * scrollYOffset;
      scroller.setVvalue( scroller.getVmin() + newScrollYOffset * (scroller.getVmax() - scroller.getVmin()) / extraHeight );
    }
    else {
      scroller.setHvalue( scroller.getHmin() );
    }
  }

  @Override
  public boolean isZoomEnabled() {
    return true;
  }

  @Override
  public void setZoomFactor( double zoomFactor ) {
    fxImageView.setScaleX( zoomFactor );
    fxImageView.setScaleY( zoomFactor );
  }

  @Override
  public double getZoomFactor() {
    double scaleX = fxImageView.getScaleX();
    double scaleY = fxImageView.getScaleY();
    if(scaleX == scaleY){
      return scaleX;
    }
    else{
      throw new RuntimeException( "invalid ZoomFactor in compID=" + fxImageView.getId() + ": Scale Values are different (scaleX = "+scaleX+", scaleY = "+ scaleY +")" );
    }
  }

  public void centerImage() {
    Image img = fxImageView.getImage();
    if (img != null) {
      double w = 0;
      double h = 0;

      double ratioX = fxImageView.getFitWidth() / img.getWidth();
      double ratioY = fxImageView.getFitHeight() / img.getHeight();

      double reducCoeff = 0;
      if(ratioX >= ratioY) {
        reducCoeff = ratioY;
      } else {
        reducCoeff = ratioX;
      }

      w = img.getWidth() * reducCoeff;
      h = img.getHeight() * reducCoeff;

      fxImageView.setX((fxImageView.getFitWidth() - w) / 2);
      fxImageView.setY((fxImageView.getFitHeight() - h) / 2);

    }
  }
}
