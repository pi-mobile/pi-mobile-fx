/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;

public class FXTableCellTextEditor extends FXTableCellEditable {

  public FXTableCellTextEditor( TableViewPI tableViewPI, TableView<Model> fxTableView, FXTableAdapter fxTableAdapter,
                                ColumnInfo columnInfo, ViewPI columnViewPI, ViewPI editorViewPI, TableColumn tableColumn,
                                FXUIFactory fxUIFactory ) {
    super( tableViewPI, fxTableView, fxTableAdapter, columnInfo, columnViewPI, editorViewPI, tableColumn, fxUIFactory );
  }

  private TextArea getTextEditor() {
    if (editorNode != null && editorNode instanceof TextArea) {
      return (TextArea)editorNode;
    }
    else {
      return null;
    }
  }

}
