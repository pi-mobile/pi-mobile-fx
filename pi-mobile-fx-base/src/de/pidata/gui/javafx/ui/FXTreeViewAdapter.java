/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.util.Callback;

/**
 * Created by pru on 12.07.2016.
 */
public class FXTreeViewAdapter extends FXTreeAdapter implements Callback<TreeView, TreeCell>, ChangeListener {

  public static final boolean DEBUG = false;

  private TreeView fxTreeView;
  private TreeViewPI treeViewPI;
  private FXUIFactory fxUIFactory;



  public FXTreeViewAdapter( TreeView fxTreeView, TreeViewPI treeViewPI, FXUIFactory fxUIFactory, UIContainer uiContainer ) {
    super( fxTreeView, treeViewPI, uiContainer );

    this.fxTreeView = fxTreeView;
    this.treeViewPI = treeViewPI;
    this.fxUIFactory = fxUIFactory;

    fxTreeView.setCellFactory( this );
    fxTreeView.getSelectionModel().selectedItemProperty().addListener( this );
  }

  /**
   * The <code>call</code> method is called when required, and is given a
   * single argument of type P, with a requirement that an object of type R
   * is returned.
   *
   * @param param The single argument upon which the returned value should be
   *              determined.
   * @return An object of type R that may be determined based on the provided
   * parameter value.
   */
  @Override
  public TreeCell call( TreeView param ) {
    return new FXTreeCell( treeViewPI, fxUIFactory );
  }

  @Override
  public void setRootNode( final TreeNodePI rootNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      TreeItem<TreeNodePI> root = new FXTreeItem( rootNode, this );
      root.setExpanded( true );
      fxTreeView.setRoot( root );
      fxTreeView.setEditable( treeViewPI.isEditable() );
      fxTreeView.setDisable( false );
    } );
  }


  @Override
  public TreeNodePI getSelectedNode() {
    FXTreeItem selectedItem = (FXTreeItem) fxTreeView.getSelectionModel().getSelectedItem();
    if (selectedItem == null) {
      return null;
    }
    else {
      return selectedItem.getValue();
    }
  }

  @Override
  public void setSelectedNode( TreeNodePI selectedNode ) {
    Platform.getInstance().runOnUiThread( () -> {
      // force node to be visible
      expandParentPath( selectedNode );
      TreeItem<TreeNodePI> nodeTreeItem = getTreeItem( selectedNode );
      fxTreeView.getSelectionModel().select( nodeTreeItem );
    } );
  }


  @Override
  public void editNode( TreeNodePI treeNode ) {
    if (treeNode != null) {
      Platform.getInstance().runOnUiThread( () -> {
        TreeItem<TreeNodePI> nodeItem = findItem( treeNode.getNodeModel() );
        if (nodeItem != null) {
          // force edited node to be selected
          fxTreeView.getSelectionModel().select( nodeItem );
          fxTreeView.layout(); // force refresh, at least necessary to allow editing of a new added child
          fxTreeView.edit( nodeItem );
        }
      } );
    }
  }


  @Override
  protected FXTreeItem getRoot() {
    TreeItem root = fxTreeView.getRoot();
    if (root != null) {
      return (FXTreeItem) root;
    }
    return null;
  }


  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed(ObservableValue observable, Object oldValue, Object newValue) {
    FXTreeItem selectedItem = (FXTreeItem) newValue;
    if (selectedItem != null) {
      treeViewPI.onSelectionChanged( selectedItem.getValue() );
    }
  }

}
