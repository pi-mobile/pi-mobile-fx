/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.javafx.FXInputManager;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.log.Logger;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.gui.component.base.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pru on 01.06.16.
 */
public class FXTextFieldAdapter extends FXValueAdapter implements UITextAdapter {

  private TextField fxTextField;

  /*
  * Override default FXValueAdapter behavior.
  * The text should be selectable and in full color, but not editable.
  * */
  @Override
  public void setEnabled( boolean enabled ) {
    // FIXME: Wenn das Textfeld im GUI XML kein Model deklariert hat, wird die Funktion nur ein Mal aufgerufen und fxTextField ist null.
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (fxTextField != null) {
          fxTextField.setEditable( enabled );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  private TextViewPI textViewPI;
  private Namespace valueNS;

  private ChangeListener<? super String> stringChangeListener = null;

  public FXTextFieldAdapter( TextField fxTextField, TextViewPI textViewPI, UIContainer uiContainer ) {
    super( fxTextField, textViewPI, uiContainer );
    this.fxTextField = fxTextField;
    this.textViewPI = textViewPI;

    fxTextField.addEventHandler( KeyEvent.KEY_PRESSED, event -> {
      onKeyPressed( FXInputManager.getInputCommand( event.getCode() ) );
      // Fire the event on the parent, due to the fact the event seems to be consumed for any reason we don't know.
      if (fxTextField.getParent() != null) {
        // Do not fire if the LEFT or RIGHT key was clicked to move the caret.
        KeyCode keyCode = event.getCode();
        //TODO Sollte das nicht besser eine Whitelist statt einer Blacklist sein?
        if ((keyCode != KeyCode.LEFT) && (keyCode != KeyCode.RIGHT)
            && (keyCode != KeyCode.HOME) && (keyCode != KeyCode.END)
            && (keyCode != KeyCode.BACK_SPACE) && (keyCode != KeyCode.DELETE)) {
          Event.fireEvent( fxTextField.getParent(), event );
          // allow clipboard keys to bubble up
          if (!isClipboardEvent( event )) {
            event.consume();
          }
        }
      }
    } );
  }

  private static List<KeyCode> clipboardKeys = Arrays.asList( KeyCode.V, KeyCode.C, KeyCode.X );
  private boolean isClipboardEvent( KeyEvent event ) {
    if (clipboardKeys.contains( event.getCode() )) {
      return event.isControlDown();
    }
    return false;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxTextField = null;
    textViewPI = null;
    super.detach();
  }

  @Override
  public Object getValue() {
    if (valueNS == null) {
      return fxTextField.getText();
    }
    else {
      return valueNS.getQName( fxTextField.getText() );
    }
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (value == null) {
        setText( "" );
      }
      else if (value instanceof QName) {
        valueNS = ((QName) value).getNamespace();
        setText( ((QName) value).getName() );
      }
      else {
        setText( value.toString() );
        valueNS = null;
      }
    });
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    Platform.getInstance().runOnUiThread( () -> fxTextField.positionCaret( posX ) );
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      if (stringChangeListener == null) {
        stringChangeListener = new ChangeListener<String>() {
          @Override
          public void changed( ObservableValue<? extends String> observable, String oldValue, String newValue ) {
            textViewPI.textChanged( oldValue, newValue );
          }
        };
        fxTextField.textProperty().addListener( stringChangeListener );
      }
    }
    else {
      if (stringChangeListener != null) {
        fxTextField.textProperty().removeListener( stringChangeListener );
        stringChangeListener = null;
      }
    }
  }

  public void onKeyPressed( QName keyCode ) {
    textViewPI.keyPressed( keyCode );
  }

  private void setText( String text) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        fxTextField.setText( text );
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void select( int fromPos, int toPos ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        fxTextField.requestFocus(); // get focus first
        fxTextField.selectRange( fromPos, toPos );
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  public void selectAll() {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        fxTextField.requestFocus(); // get focus first
        fxTextField.selectAll();
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setBackground( ComponentColor color ) {
    de.pidata.gui.component.base.Platform.getInstance().runOnUiThread( () -> {
      try {
        if (fxUIComponent != null) {
          Color fxColor = (Color) color.getColor();
          int red = (int) ( 255 * fxColor.getRed());
          int green = (int) ( 255 * fxColor.getGreen());
          int blue = (int) ( 255 * fxColor.getGreen());
          String style = "-fx-control-inner-background: rgb(" + red + "," + green + ", " + blue + ");";
          fxUIComponent.setStyle( style );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }
}
