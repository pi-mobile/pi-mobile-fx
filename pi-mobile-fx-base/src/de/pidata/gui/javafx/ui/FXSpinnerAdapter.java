/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.gui.component.base.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;

public class FXSpinnerAdapter extends FXAdapter implements UIValueAdapter, UIProgressBarAdapter, ChangeListener<Integer> {

  private Spinner fxSpinner;
  private ProgressBarViewPI progressBarViewPI;

  public FXSpinnerAdapter( Spinner fxSpinner, ProgressBarViewPI progressBarViewPI, UIContainer uiContainer ) {
    super( fxSpinner, progressBarViewPI, uiContainer );
    this.progressBarViewPI = progressBarViewPI;
    this.fxSpinner = fxSpinner;
    this.fxSpinner.valueProperty().addListener( this );
    this.fxSpinner.focusedProperty().addListener( new ChangeListener<Boolean>() {
      @Override
      public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
        if ((newValue != null) && !(newValue.booleanValue())) {
          progressBarViewPI.onValueChanged( FXSpinnerAdapter.this, (Integer) getValue(), uiContainer.getDataContext() );
        }
      }
    } );
  }

  @Override
  public void setMaxValue( int maxValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getMaxValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinValue( int minValue ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getMinValue() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setProgressMessage( Object progressMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setProgress( double progress ) {
    setValue( Double.valueOf( progress ) );
  }

  @Override
  public void showError( String errorMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showInfo( String infoMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showWarning( String warningMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void hideError() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetColor() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public Object getValue() {
    // if value has been typed into editor and focus is lost, sometimes the
    // spinner value has not been updated, so use the editor's value
    String strVal = fxSpinner.getEditor().getText();
    try {
      return Integer.decode( strVal );
    }
    catch (Exception ex) {
      return fxSpinner.getValue();
    }
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () ->{
      int intValue;
      if (value == null) {
        intValue = 0;
      }
      else if (value instanceof DecimalObject) {
        intValue = ((DecimalObject)value).intValue();
      }
      else if (value instanceof Number) {
        intValue = ((Number)value).intValue();
      }
      else {
        intValue = 0;
      }
      fxSpinner.getValueFactory().setValue( Integer.valueOf( intValue ) );
    } );
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue ) {
    if (progressBarViewPI != null) {
      if ( ((oldValue == null) && (newValue != null))
           || ((oldValue != null) && (!oldValue.equals( newValue ))) ) {
        progressBarViewPI.onValueChanged( this, newValue, uiContainer.getDataContext() );
      }
    }
  }
}
