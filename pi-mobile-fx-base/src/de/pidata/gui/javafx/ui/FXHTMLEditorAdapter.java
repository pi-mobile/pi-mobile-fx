/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIHTMLEditorAdapter;
import de.pidata.gui.view.base.HTMLEditorViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import javafx.scene.web.HTMLEditor;

public class FXHTMLEditorAdapter extends FXValueAdapter implements UIHTMLEditorAdapter {

  private HTMLEditor fxHtmlEditor;
  private HTMLEditorViewPI htmlEditorViewPI;

  public FXHTMLEditorAdapter( HTMLEditor fxHtmlEditor, HTMLEditorViewPI htmlEditorViewPI, UIContainer uiContainer ) {
    super( fxHtmlEditor, htmlEditorViewPI, uiContainer );
    this.fxHtmlEditor = fxHtmlEditor;
    this.htmlEditorViewPI = htmlEditorViewPI;
  }

  private void customizeToolbar() {
    // TODO: hide/move toolbar buttons
    //https://gist.github.com/jewelsea/2514709
    //https://stackoverflow.com/questions/10075841/how-to-hide-the-controls-of-htmleditor
    //https://stackoverflow.com/questions/13462834/javafx-html-editor
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxHtmlEditor = null;
    htmlEditorViewPI = null;
    super.detach();
  }

  @Override
  public void setEnabled( boolean enabled ) {
    super.setEnabled( enabled );

    // TODO: <body contentEditable="true">
  }

  @Override
  public boolean isEnabled() {
    return super.isEnabled();
  }

  @Override
  public Object getValue() {
    return fxHtmlEditor.getHtmlText();
  }

  @Override
  public void setValue( Object value ) {
    if (value == null) {
      setHtmlText( "" );
    }
    else {
      setHtmlText( value.toString() );
    }
  }

  private void setHtmlText( String text ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        String htmlText = text;
        fxHtmlEditor.setHtmlText( htmlText );
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    // not directly supported on HTMLEditor, see https://stackoverflow.com/questions/16606054/find-htmleditor-cursor-pointer-for-inserting-image
  }

  @Override
  public void select( int fromPos, int toPos ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void selectAll() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      throw new IllegalArgumentException( "Listen text changes not supported for FXHtmlEditorAdapter" );
    }
  }
}
