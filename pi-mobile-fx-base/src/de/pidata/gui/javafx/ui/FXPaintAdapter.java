/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.ui.base.UIPaintAdapter;
import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.figure.*;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import de.pidata.rect.Pos;
import de.pidata.rect.Rect;
import de.pidata.rect.RectDir;
import de.pidata.rect.Rotation;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pru on 09.06.17.
 */
public class FXPaintAdapter extends FXAdapter implements UIPaintAdapter, EventHandler<MouseEvent>{

  private static final boolean DEBUG = false;

  private Group fxGroup;
  private PaintViewPI paintViewPI;
  private Map<Shape,FigureHandle> handleShapeMap = new HashMap<Shape,FigureHandle>();

  public FXPaintAdapter( Group fxGroup, PaintViewPI viewPI, UIContainer uiContainer ) {
    super( fxGroup, viewPI, uiContainer );
    this.fxGroup = fxGroup;
    this.paintViewPI = viewPI;
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        for (int i = 0; i < paintViewPI.figureCount(); i++) {
          Figure figure = paintViewPI.getFigure( i );
          uiAddShapes( figure );
        }
      }
    } );

    fxGroup.getParent().setOnMousePressed( this );
  }

  private void addShapes( Figure figure ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        uiAddShapes( figure );
      }
    } );
  }

  /**
   * Add shapes in UI Thread. Must not be called outside UI Thread.
   * @param figure
   */
  private void uiAddShapes( Figure figure ) {
    UIFactory uiFactory = Platform.getInstance().getUiFactory();
    for (int k = 0; k < figure.shapeCount(); k++) {
      ShapePI shapePI = figure.getShape( k );
      if (shapePI != null) {
        FXShapeAdapter shapeAdapter = (FXShapeAdapter) shapePI.getUIAdapter();
        if (shapeAdapter == null) {
          shapeAdapter = (FXShapeAdapter) uiFactory.createShapeAdapter( FXPaintAdapter.this, shapePI );
        }
        Shape fxShape = shapeAdapter.getFxShape();
        ObservableList<Node> groupChildren = fxGroup.getChildren();
        if (!groupChildren.contains( fxShape )) {
          groupChildren.add( fxShape );
        }
      }
    }
  }

  private void removeShapes( Figure figure ) {
    // Workaround for JavaFX-Bug: Removing affected Shapes does not update UI, but children.clear() !?
    // So we remove all shapes via children.clear() and then add remaining figures.
    // see https://stackoverflow.com/questions/26838456/javafx-does-not-repaint-when-removing-an-object-drawn-on-top-of-another-object
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        fxGroup.getChildren().clear();
        for (int i = 0; i < paintViewPI.figureCount(); i++) {
          Figure fig = paintViewPI.getFigure( i );
          uiAddShapes( fig );
        }
      }
    });
  }

  @Override
  public void setSelectionHandles( FigureHandle[] selectionHandles ) {
    for (Shape fxHandleShape : handleShapeMap.keySet()) {
      fxGroup.getChildren().remove( fxHandleShape );
    }
    handleShapeMap.clear();
    if (selectionHandles != null) {
      for (int i = 0; i < selectionHandles.length; i++) {
        ShapePI handleShapePI = selectionHandles[i];
        FXShapeAdapter fxShapeAdapter = (FXShapeAdapter) Platform.getInstance().getUiFactory().createShapeAdapter( this, handleShapePI );
        Shape fxHandleShape = fxShapeAdapter.getFxShape();
        fxGroup.getChildren().add( fxHandleShape );
        handleShapeMap.put( fxHandleShape, selectionHandles[i] );
      }
    }
  }

  @Override
  public void figureModified( Figure figure, ShapePI shapePI ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (shapePI == null) {
          for (int i = 0; i < figure.shapeCount(); i++) {
            ShapePI childShapePI = figure.getShape( i );
            if (childShapePI != null) {
              FXShapeAdapter uiShapeAdapter = (FXShapeAdapter) childShapePI.getUIAdapter();
              if (uiShapeAdapter == null) {
                UIFactory uiFactory = Platform.getInstance().getUiFactory();
                FXShapeAdapter shapeAdapter = (FXShapeAdapter) uiFactory.createShapeAdapter( FXPaintAdapter.this, childShapePI );
                fxGroup.getChildren().add( shapeAdapter.getFxShape() );
              }
              else {
                Shape shape = uiShapeAdapter.getFxShape();
                updateShape( shape, childShapePI );
              }
            }
          }
        }
        else {
          UIShapeAdapter uiAdapter = shapePI.getUIAdapter();
          if (uiAdapter != null) {
            Shape shape = ((FXShapeAdapter) uiAdapter).getFxShape();
            updateShape( shape, shapePI );
          }
        }
      }
    } );
  }


  @Override
  public void figureAdded( Figure figure ) {
    addShapes( figure );
  }

  @Override
  public void figureRemoved( Figure figure ) {
    removeShapes( figure );
  }

  @Override
  public void shapeRemoved( Figure figure, ShapePI shapePI ) {
    removeShapes( figure );
  }

  private void updateShape( Shape shape, ShapePI shapePI ) {
    ShapeType shapeType = shapePI.getShapeType();
    Rect bounds = shapePI.getBounds();
    ShapeStyle shapeStyle = shapePI.getShapeStyle();
    switch (shapeType) {
      case rect: {
        Rectangle rectangle = (Rectangle) shape;
        if (DEBUG) {
          Logger.info( "Update rectangle x=" + rectangle.getX() + ", y=" + rectangle.getY() + ", w=" + rectangle.getWidth() + ", h=" + rectangle.getHeight() );
        }
        rectangle.setX( bounds.getX() );
        rectangle.setY( bounds.getY() );
        rectangle.setWidth( bounds.getWidth() );
        rectangle.setHeight( bounds.getHeight() );
        if(shapePI instanceof RectanglePI) {
          rectangle.setArcWidth( ((RectanglePI) shapePI).getCornerRadius() );
          rectangle.setArcHeight( ((RectanglePI) shapePI).getCornerRadius() );
        }
        if (DEBUG) {
          Logger.info( "    to rectangle x=" + rectangle.getX() + ", y=" + rectangle.getY() + ", w=" + rectangle.getWidth() + ", h=" + rectangle.getHeight() );
        }
        break;
      }
      case bitmap: {
        Rectangle rectangle = (Rectangle) shape;
        if (DEBUG) {
          Logger.info( "Update bitmap x=" + rectangle.getX() + ", y=" + rectangle.getY() + ", w=" + rectangle.getWidth() + ", h=" + rectangle.getHeight() + "," );
        }
        rectangle.setX( bounds.getX() );
        rectangle.setY( bounds.getY() );
        rectangle.setWidth( bounds.getWidth() );
        rectangle.setHeight( bounds.getHeight() );
        ComponentBitmap bitmap = ((BitmapPI) shapePI).getBitmap();
        if (bitmap == null) {
          rectangle.setFill( null );
        }
        else {
          rectangle.setFill( new ImagePattern( (Image) bitmap.getImage() ) );
        }
        if (DEBUG) {
          Logger.info( "    to bitmap x=" + rectangle.getX() + ", y=" + rectangle.getY() + ", w=" + rectangle.getWidth() + ", h=" + rectangle.getHeight() );
        }
        break;
      }
      case line: {
        Line line = (Line) shape;
        if (DEBUG) {
          if (DEBUG) Logger.info( "Update line startX="+line.getStartX()+", startY="+line.getStartY()+", endX="+line.getEndX()+", endY="+line.getEndY() );
        }
        Pos startPos = shapePI.getPos( 0 );
        Pos endPos = shapePI.getPos( 1 );
        line.setStartX( startPos.getX() );
        line.setStartY( startPos.getY() );
        line.setEndX( endPos.getX() );
        line.setEndY( endPos.getY() );
        if (DEBUG) {
          Logger.info( "    to line startX=" + line.getStartX() + ", startY=" + line.getStartY() + ", endX=" + line.getEndX() + ", endY=" + line.getEndY() );
        }
        break;
      }
      case ellipse: {
        Ellipse ellipse = (Ellipse) shape;
        if (DEBUG) {
          if (DEBUG) Logger.info( "Update ellipse centerX="+ellipse.getCenterX()+", centerY="+ellipse.getCenterY()+", radiusX="+ellipse.getRadiusX()+", radiusY="+ellipse.getRadiusY() );
        }
        Pos center = shapePI.getPos(0 );
        ellipse.setCenterX( center.getX() );
        ellipse.setCenterY( center.getY() );
        ellipse.setRadiusX( bounds.getWidth() / 2 );
        ellipse.setRadiusY( bounds.getHeight() / 2 );
        if (DEBUG) {
          if (DEBUG) Logger.info( "    to ellipse centerX="+ellipse.getCenterX()+", centerY="+ellipse.getCenterY()+", radiusX="+ellipse.getRadiusX()+", radiusY="+ellipse.getRadiusY() );
        }
        break;
      }
      case arc:{
        Arc arc = (Arc) shape;
        if(DEBUG){
          Logger.info( "Update arc centerX=" +arc.getCenterX() + ", centerY)=" + arc.getCenterY() + "radiusX="+ arc.getRadiusX() + ", radiusY=" + arc.getRadiusY() );
        }
        double height = shapePI.getBounds().getHeight();
        double width = shapePI.getBounds().getWidth();
        double startX = shapePI.getBounds().getX();
        double startY = shapePI.getBounds().getY();
        arc.setCenterX( startX + width/2 );
        arc.setCenterY( startY + height/2 );
        arc.setRadiusX( width/2 );
        arc.setRadiusY( height/2 );
        break;
      }
      case path: {
        if (shapePI instanceof PolyLineShapePI) {
          Polyline polyline = (Polyline) shape;
          ObservableList<Double> points = polyline.getPoints();
          points.clear();
          for (int i = 0; i < shapePI.posCount(); i++) {
            Pos pos = shapePI.getPos( i );
            points.add( Double.valueOf( pos.getX() ) );
            points.add( Double.valueOf( pos.getY() ) );
          }
        }
        else {
          throw new RuntimeException( "TODO: not yet implemented" );
//          Path path = (Path) shape;
//          ObservableList<javafx.scene.shape.PathElement> pathElements = path.getElements();
//          pathElements.clear();
//          PathElement[] pathElementDefinition = ((PathShapePI) shapePI).getPathElementDefinition();
//          for (int i = 0; i < pathElementDefinition.length; i++) {
//            PathElement pathElement = pathElementDefinition[i];
//            Pos pos = pathElement.getPos();
//            PathElementType pathType = pathElement.getPathType();
//            switch (pathType) {
//              case none: {
//                MoveTo moveTo = new MoveTo( (float) pos.getX(), (float) pos.getY() );
//                pathElements.add( moveTo );
//                break;
//              }
//              case arc: {
//                double radius = pathElement.getRadius();
//                MoveTo moveTo = new MoveTo( arcStartX, arcStartY );
//                pathElements.add( moveTo );
//                ArcTo arcTo = new ArcTo( radius, radius, pathElement.getStartAngle(), arcEndX, arcEndY, true, true );
//                pathElements.add( arcTo );
//                break;
//              }
//              case line:
//              default: {
//                LineTo lineTo = new LineTo( (float) pos.getX(), (float) pos.getY() );
//                pathElements.add( lineTo );
//              }
//            }
//          }
        }
        break;
      }
      case text: {
        Text text = (Text) shape;
        double textSize = bounds.getHeight();
        String fontName = shapeStyle.getFontName();
        Font font;
        if (fontName == null) {
          font = Font.getDefault();
        }
        else {
          font = Font.font( shapeStyle.getFontName(), textSize );
        }
        Pos textPos = shapePI.getPos( 0 );
        double textX;
        String str = text.getText();
        if (str == null) {
          str = "";
        }
        Text txt = new Text( str );
        txt.setFont( font );
        double textY = textPos.getY() + textSize - txt.getBaselineOffset();
        switch (shapeStyle.getTextAlign()) {
          case RIGHT: {
            textX = textPos.getX() + bounds.getWidth() - txt.getBoundsInLocal().getWidth();
            break;
          }
          case CENTER: {
            textX = textPos.getX() + ((bounds.getWidth() - txt.getBoundsInLocal().getWidth()) * 0.5);
            break;
          }
          case LEFT:
          default: {
            textX = textPos.getX();
          }
        }
        text.setX( textX );
        text.setY( textY );
        text.setFont( font );
        break;
      }
      default: {
        //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
        throw new RuntimeException( "TODO - unsupported ShapeType="+shapeType );
      }
    }
    if (bounds instanceof RectDir) {
      Rotation rotation = ((RectDir) bounds).getRotation();
      if (rotation != null) {
        // TODO 
        ObservableList<Transform> transforms = shape.getTransforms();
        if (transforms.size() > 0) {
          transforms.clear();
        }
        FXUIFactory.addRotation( transforms, rotation );
      }
    }
    applyShapeStyle( shape, shapeStyle );
  }

  @Override
  public Object getValue() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public static CycleMethod getCycleMethod( GradientMode gradientMode ) {
    switch (gradientMode) {
      case CLAMP: return CycleMethod.NO_CYCLE;
      case MIRROR: return CycleMethod.REFLECT;
      case REPEAT: return CycleMethod.REPEAT;
    }
    return null;
  }

  public static void applyShapeStyle( Shape shape, ShapeStyle shapeStyle ) {
    if (shapeStyle instanceof LinearGradientStyle) {
      LinearGradientStyle gradientStyle = (LinearGradientStyle) shapeStyle;
      Pos start = gradientStyle.getStartPos();
      Pos end = gradientStyle.getEndPos();
      CycleMethod cycleMethod = getCycleMethod( gradientStyle.getGradientMode() );
      List<ComponentColor> colorList = gradientStyle.getColorList();
      List<Double> offsetList = gradientStyle.getOffsetList();
      List<Stop> stopList = new ArrayList<>();
      for (int i = 0; i < colorList.size(); i++) {
        stopList.add( new Stop( offsetList.get( i ).doubleValue(), (Color) colorList.get( i ).getColor() ) );
      }
      LinearGradient gradient = new LinearGradient( start.getX(), start.getY(), end.getX(), end.getY(), false, cycleMethod, stopList );
      shape.setFill( gradient );
    }
    else if (shapeStyle instanceof CircleGradientStyle) {
      CircleGradientStyle circleGradientStyle = (CircleGradientStyle) shapeStyle;
      Pos center = circleGradientStyle.getCenterPos();
      CycleMethod cycleMethod = getCycleMethod( circleGradientStyle.getGradientMode() );
      List<ComponentColor> colorList = circleGradientStyle.getColorList();
      List<Double> offsetList = circleGradientStyle.getOffsetList();
      List<Stop> stopList = new ArrayList<>();
      for (int i = 0; i < colorList.size(); i++) {
        stopList.add( new Stop( offsetList.get( i ).doubleValue(), (Color) colorList.get( i ).getColor() ) );
      }
      RadialGradient gradient = new RadialGradient( 0, 0, center.getX(), center.getY(), circleGradientStyle.getRadius(), false, cycleMethod, stopList );
      shape.setFill( gradient );
    }
    else {
      ComponentColor fillColor = shapeStyle.getFillColor();
      if (fillColor != null) {
        shape.setFill( (Color) fillColor.getColor() );
      }
      shape.setStrokeWidth( shapeStyle.getStrokeWidth() );
      ComponentColor borderColor = shapeStyle.getBorderColor();
      if (borderColor != null) {
        shape.setStroke( (Color) borderColor.getColor() );
      }
      if (shape instanceof Text) {
        Text text = (Text) shape;
        switch (shapeStyle.getTextAlign()) {
          case RIGHT: {
            text.setTextAlignment( TextAlignment.RIGHT );
            break;
          }
          case CENTER: {
            text.setTextAlignment( TextAlignment.CENTER );
            break;
          }
          case LEFT:
          default: {
            text.setTextAlignment( TextAlignment.LEFT );
          }
        }
        String fontName = shapeStyle.getFontName();
        Font font;
        if (fontName == null) {
          font = Font.getDefault();
        }
        else {
          font = Font.font( shapeStyle.getFontName(), text.getFont().getSize() );
        }
        text.setFont( font );
      }
    }
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param event the event which occurred
   */
  @Override
  public void handle( MouseEvent event ) {
    double mouseDownX;
    double mouseDownY;

    if (event.getButton() == MouseButton.PRIMARY) {
      if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
        // TODO if needed
      }
      else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
        // TODO if needed
      }
      else if (event.getEventType() == MouseEvent.MOUSE_RELEASED) {
        // TODO if needed
      }
      else {
        Logger.warn( "FXShapeAdapter: Unknown mouse event="+event.getEventType().toString() );
      }
    }
    else if (event.getButton() == MouseButton.SECONDARY) {
      if (event.getEventType() == MouseEvent.MOUSE_PRESSED) {
        mouseDownX = event.getX();
        mouseDownY = event.getY();
        paintViewPI.showContextMenu(mouseDownX, mouseDownY);
      }
    }
  }

}
