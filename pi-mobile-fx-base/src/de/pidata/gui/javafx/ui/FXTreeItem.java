/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.log.Logger;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

/**
 * Created by pru on 13.07.16.
 */
public class FXTreeItem extends TreeItem<TreeNodePI> {

  private final FXTreeAdapter fxTreeAdapter;
  private boolean childrenAdded = false;
  private ObservableList<TreeItem<TreeNodePI>> childItemList;

  /**
   * Creates a TreeItem with the value property set to the provided object.
   *
   * @param value The object to be stored as the value of this TreeItem.
   */
  public FXTreeItem( TreeNodePI value, FXTreeAdapter fxTreeAdapter ) {
    super( value );
    this.fxTreeAdapter = fxTreeAdapter;

    addEventHandler( TreeItem.branchExpandedEvent(), event -> {
      if (FXTreeViewAdapter.DEBUG) Logger.info( "Item expanded: " + event.getTreeItem().getValue().toString() );
      // initiate model loading on the underlying TreeNodePI on demand
      if (!childrenAdded) {
        childrenAdded = true;
        TreeNodePI treeNode = getValue();
        if (treeNode != null) {
          treeNode.loadChildren();
        }
      }
    } );

    fxTreeAdapter.addTreeItem( this, value );
  }

  /**
   * The children of this TreeItem. This method is called frequently, and
   * it is therefore recommended that the returned list be cached by
   * any TreeItem implementations.
   *
   * @return a list that contains the child TreeItems belonging to the TreeItem.
   */
  @Override
  public ObservableList<TreeItem<TreeNodePI>> getChildren() {
    if (childItemList == null) {
      // keep local reference on children list
      childItemList = super.getChildren();
    }
    return childItemList;
  }

  /**
   * A TreeItem is a leaf if it has no children. The isLeaf method may of
   * course be overridden by subclasses to support alternate means of defining
   * how a TreeItem may be a leaf, but the general premise is the same: a
   * leaf can not be expanded by the user, and as such will not show a
   * disclosure node or respond to expansion requests.
   */
  @Override
  public boolean isLeaf() {
    TreeNodePI value = getValue();
    return (value != null && value.isLeaf());
  }

  public void update() {
    TreeNodePI oldValue = getValue();
    this.setValue( null );
    this.setValue( oldValue );
  }

  /**
   * Refresh the {@link FXTreeItem} children with the content of the {@link TreeNodePI} children.
   * Don't initiate model loading on the {@link TreeNodePI}!
   */
  public void updateChildList() {
    childrenAdded = true;

    if (FXTreeViewAdapter.DEBUG) Logger.info( "...updateChildList" );

    ObservableList<TreeItem<TreeNodePI>> childItems = getChildren();
    TreeNodePI nodePI = getValue();
    int i = 0;
    int childItemCount = childItems.size();
    for (TreeNodePI childNode : nodePI.childNodeIter()) {
      if (i < childItemCount) {
        FXTreeItem childItem = (FXTreeItem) childItems.get( i );
        while ((i < childItemCount) && (childItem.getValue() != childNode)) {
          childItems.remove( i );
          childItemCount--;
          if (i < childItemCount) {
            childItem = (FXTreeItem) childItems.get( i );
          }
        }
        if (childItem.getValue() == childNode) {
          // got node at current position
          childItem.update();
        }
        else {
          childItems.add( new FXTreeItem( childNode, fxTreeAdapter ) );
        }
      }
      else {
        // reached end of items: add remaining
        childItems.add( new FXTreeItem( childNode, fxTreeAdapter ) );
      }
      i++;
    }
    if (i < childItemCount) {
      childItems.remove( i, childItemCount );
    }
  }
}
