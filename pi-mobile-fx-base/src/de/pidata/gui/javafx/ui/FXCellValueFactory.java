/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.view.base.TableCellPI;
import de.pidata.models.tree.Model;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.ObservableValueBase;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

/**
 * Created by pru on 14.07.16.
 */
public class FXCellValueFactory implements Callback<TableColumn.CellDataFeatures, ObservableValue> {

  private ColumnInfo columnInfo;

  public FXCellValueFactory( ColumnInfo columnInfo ) {
    this.columnInfo = columnInfo;
  }

  public ColumnInfo getColumnInfo() {
    return columnInfo;
  }

  /**
   * The <code>call</code> method is called when required, and is given a
   * single argument of type P, with a requirement that an object of type R
   * is returned.
   *
   * @param param The single argument upon which the returned value should be
   *              determined.
   * @return An object of type R that may be determined based on the provided
   * parameter value.
   */
  @Override
  public ObservableValue call( TableColumn.CellDataFeatures param ) {
    final TableCellPI value = new TableCellPI( (Model) param.getValue(), columnInfo );
    return new ObservableValueBase() {
      @Override
      public TableCellPI getValue() {
        return value;
      }
    };
  }
}
