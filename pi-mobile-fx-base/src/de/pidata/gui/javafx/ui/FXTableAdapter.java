/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentFactory;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.javafx.FXDialogInterface;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.log.Profiler;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.util.*;
import java.util.prefs.Preferences;

/**
 * Created by pru on 20.06.16.
 */
public class FXTableAdapter extends FXAdapter implements UITableAdapter, ListChangeListener<Integer>, Callback<TableColumn, TableCell>, SettingsHandler {

  public static final boolean DEBUG = false;
  public static final String COLUMN_WIDTH_PREFIX = "COLUMN_WIDTH_";

  private final EventHandler<MouseEvent> mousePressedEventHandler;
  private final EventHandler<WindowEvent> windowCloseEventHandler;
  private final EventHandler<ScrollEvent> scrollAnyEventHandler;

  protected TableView<Model> fxTableView;
  protected TableViewPI tableViewPI;
  // Use for synchronize so it must be final
  protected final ObservableList<Model> fxTableItems;
  private FXUIFactory fxUIFactory;
  private boolean tableFilterInitialized;

  public FXTableAdapter( TableView<Model> fxTableView, TableViewPI tableViewPI, FXUIFactory fxUIFactory, UIContainer uiContainer ) {
    super( fxTableView, tableViewPI, uiContainer );
    this.fxTableView = fxTableView;
    this.tableViewPI = tableViewPI;
    this.fxUIFactory = fxUIFactory;
    this.tableFilterInitialized = false;

    this.windowCloseEventHandler = window -> {
      if(DEBUG) Logger.debug( "gotWindowCloseEvent: " + window );
      fxTableView.edit( -1, null );
    };

    this.fxTableView.addEventHandler( WindowEvent.WINDOW_CLOSE_REQUEST, windowCloseEventHandler );

    fxTableItems = FXCollections.observableArrayList();
    ObservableList<TableColumn<Model, ?>> tableColumnList = fxTableView.getColumns();

    initItemsAndColumns( fxTableView, tableViewPI, tableColumnList );

    //--- Avoid problems with silly moving editor
    scrollAnyEventHandler = event -> fxTableView.edit( -1, null );
    this.fxTableView.addEventFilter( ScrollEvent.ANY, scrollAnyEventHandler );

    /**
     * Add listeners to mouseclicks at the whole scene and detect whether it was inside the editcell.
     * JavaFXs MouseEvent.MOUSE_CLICKED will fire even if PRESS and RELEASE will have a move between.
     * In our case it will fire if we PRESS inside the the editcell and RELEASE outside as the event
     * listens at the whole scene.
     * handleMouseEventOutsideEditCell() will handle it the way we expect. PRESS and RELEASE ouside the editcell.
     */
    this.mousePressedEventHandler = createMousePressedEventHandler( fxTableView );
    this.fxTableView.addEventFilter( MouseEvent.MOUSE_PRESSED, mousePressedEventHandler );

    if(tableViewPI.isSelectable()) {
      // Add click listeners
      initClickListeners();
    }
    else{
      // avoid Selection and Focus
      fxTableView.setSelectionModel( new NullTableViewSelectionModel( fxTableView ) );
      fxTableView.setFocusModel( new NullTableViewFocusModel(fxTableView) );
    }
  }

  protected void initItemsAndColumns( TableView<Model> fxTableView, TableViewPI tableViewPI, ObservableList<TableColumn<Model, ?>> tableColumnList ) {

    for (TableColumn tableColumn : tableColumnList) {
      tableColumn.setCellFactory( this );
      QName columnID = ComponentFactory.NAMESPACE.getQName( tableColumn.getId() );
      ColumnInfo columnInfo = tableViewPI.getColumnByCompID( columnID );
      if (columnInfo == null) {
        throw new IllegalArgumentException( "Column info not found for ColCompID="+columnID );
      }
      columnInfo.setView( tableViewPI );

      FXCellValueFactory cellValueFactory = new FXCellValueFactory( columnInfo );
      tableColumn.setCellValueFactory( cellValueFactory );
      tableColumn.setSortable( columnInfo.isSortable() );
    }

    DefaultTableController listCtrl = (DefaultTableController) tableViewPI.getListCtrl();
    for (int i = tableColumnList.size(); i < listCtrl.columnCount(); i++) {
      ColumnInfo columnInfo = listCtrl.getColumn( i );
      columnInfo.setView( tableViewPI );
      TableColumn tableColumn = new TableColumn( columnInfo.getColName().getName() );
      tableColumn.setId( columnInfo.getBodyCompID().getName() );
      tableColumn.setCellFactory( this );
      tableColumn.setCellValueFactory( new FXCellValueFactory( columnInfo ) );
      tableColumn.setSortable( columnInfo.isSortable() );
      tableColumnList.add( i, tableColumn );
    }

    synchronized (fxTableItems) {
      fxTableView.setItems( fxTableItems );
    }
  }

  private EventHandler<MouseEvent> createMousePressedEventHandler( TableView<Model> fxTableView ) {
    return mouseEvent -> {
      EventTarget target = mouseEvent.getTarget();
      if(DEBUG) Logger.debug( "gotClickEvent: " + mouseEvent + " on target: " + target );
      synchronized (fxTableItems) {
        if (target instanceof Node ) {
          if (isChildOfTable( (Node) target )) {
            Node node = (Node) target;
            while (node != null) {
              if (node instanceof TableCell) {
                TableCell tableCell = (TableCell) node;
                if (tableCell.isEditing()) {
                  if (DEBUG) Logger.debug( "Clicked in editing cell" );
                }
                else {
                  if (DEBUG) Logger.debug( "Clicked in non-edit cell" );
                  int row = tableCell.getIndex();
                  // do not change selection here - that will damage multi selection!
                  if (tableCell.getTableColumn().isEditable()) {
                    fxTableView.edit( row, tableCell.getTableColumn() );
                  }
                  // If the column is not editable explicitly switch off editing!
                  // Otherwise, the current changed cell content might get lost.
                  else {
                    fxTableView.edit(-1, null);
                  }
                }
                return;
              }
              node = node.getParent();
            }
            if (DEBUG) Logger.debug( "Clicked outside of table cells" );
            fxTableView.edit( -1, null );
          }
          else{
             if (DEBUG) Logger.debug( "Clicked outside of table cells" );
            fxTableView.edit( -1, null );
          }
        }
        else {
          if (DEBUG) Logger.debug( "Click target is not a node" );
          fxTableView.edit( -1, null );
        }
      }
    };
  }

  private boolean isChildOfTable( Node node ) {
    while (node != null) {
      if (node instanceof TableView && node == this.fxTableView) {
        return true;
      }
      node = node.getParent();
    }
    return false;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if(this.mousePressedEventHandler != null) {
      Scene scene = this.fxTableView.getScene();
      if (scene != null) {
        scene.removeEventFilter( MouseEvent.MOUSE_PRESSED, mousePressedEventHandler );
        scene.removeEventFilter( ScrollEvent.ANY, scrollAnyEventHandler );
        Window window = scene.getWindow();
        if (window != null) {
          window.removeEventHandler( WindowEvent.WINDOW_CLOSE_REQUEST, windowCloseEventHandler );
        }
      }
    }
    super.detach();
  }

  @Override
  public void finishedUpdateAllRows() {
    if (!tableFilterInitialized) {
      tableFilterInitialized = true;
      initializeTableFilter();
    }
  }

  protected void initializeTableFilter() {
    //--- Add filter; don't add if table requires minimum one row selected. See DefaultTableController.onFiltered()
  }

  private void initClickListeners() {
    fxTableView.setOnMousePressed( e -> {

      //Handle single click
      if (e.isPrimaryButtonDown() && e.getClickCount() == 1) {
        EventTarget target = e.getTarget();
        if (target instanceof Node) {
          Node node = (Node) target;
          while (node != null) {
            if (node instanceof TableCell) {
              TableCell tableCell = (TableCell) node;
              QName columnID = ComponentFactory.NAMESPACE.getQName( tableCell.getId() );
              ColumnInfo columnInfo = tableViewPI.getColumnByCompID( columnID );
              tableViewPI.onSelectedCell( fxTableView.getSelectionModel().getSelectedItem(), columnInfo );
              if (DEBUG) Logger.info("Left clicked row = " + fxTableView.getSelectionModel().getSelectedItem() );
              return;
            }
            node = node.getParent();
          }
        }
      }

      // Handle double click
      if (e.isPrimaryButtonDown() && e.getClickCount() == 2) {
        EventTarget target = e.getTarget();
        if (target instanceof Node) {
          Node node = (Node) target;
          while (node != null) {
            if (node instanceof TableCell) {
              TableCell tableCell = (TableCell) node;
              QName columnID = ComponentFactory.NAMESPACE.getQName( tableCell.getId() );
              ColumnInfo columnInfo = tableViewPI.getColumnByCompID( columnID );
              tableViewPI.onDoubleClickCell( fxTableView.getSelectionModel().getSelectedItem(), columnInfo );
              if (DEBUG) Logger.info("Double clicked row = " + fxTableView.getSelectionModel().getSelectedItem() );
              return;
            }
            node = node.getParent();
          }
        }
      }

      // Handle right click
      if (e.isSecondaryButtonDown()) {
        EventTarget target = e.getTarget();
        if (target instanceof Node) {
          Node node = (Node) target;
          while (node != null) {
            if (node instanceof TableCell) {
              TableCell tableCell = (TableCell) node;
              QName columnID = ComponentFactory.NAMESPACE.getQName( tableCell.getId() );
              ColumnInfo columnInfo = tableViewPI.getColumnByCompID( columnID );
              tableViewPI.onRightClickCell( fxTableView.getSelectionModel().getSelectedItem(), columnInfo );
              if (DEBUG) Logger.info("Right clicked row = " + fxTableView.getSelectionModel().getSelectedItem() );
              return;
            }
            node = node.getParent();
          }
        }
      }

    } );

    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        //--- Set selection type single or multiple
        TableView.TableViewSelectionModel selectionModel = fxTableView.getSelectionModel();
        if (tableViewPI.isMultiSelect()) {
          selectionModel.setSelectionMode( SelectionMode.MULTIPLE );
        }
        else {
          selectionModel.setSelectionMode( SelectionMode.SINGLE );
        }
        //--- Set initial selection
        selectionModel.clearSelection();
        Selection selection = tableViewPI.getListCtrl().getSelection();
        for (int i = 0; i < selection.selectedValueCount(); i++) {
          selectionModel.select( selection.getSelectedValue( i ) );
        }

        //--- Attach listener for selection changes
        selectionModel.getSelectedIndices().addListener( FXTableAdapter.this );

        //--- editing is managed by controller and must not be managed by JavaFX
        fxTableView.setEditable( tableViewPI.isEditable() );

        // FIXME: get from tableDef --> only usefull a table gets filled with lots of rows
        addAutoScroll( fxTableView );
      }
    } );
  }

  public static boolean isChildOfEditingCell( Node node ) {
    while (node != null) {
      if (node instanceof TableCell) {
        return ((TableCell) node).isEditing();
      }
      node = node.getParent();
    }
    return false;
  }

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    return fxTableView.getItems().size();
  }

  /**
   * Returns the data displayed in row at position
   *
   * @param position
   * @return
   */
  @Override
  public Model getDisplayRow( int position ) {
    return fxTableView.getItems().get( position );
  }

  private void startUpdate() {
  }

  private void endUpdate() {
    fxTableView.sort();
  }

  @Override
  public void removeAllDisplayRows() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxTableItems) {
          Profiler.count( "FXTableAdapter removeAllDisplayRows" );
          startUpdate();
          Thread.currentThread().setUncaughtExceptionHandler( new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException( Thread t, Throwable e ) {
              // There might show up some Exceptions, which do not cause the code from failing when using
              // controlsfx and java above 9 -> https://github.com/controlsfx/controlsfx/wiki/Using-ControlsFX-with-JDK-9-and-above
              // It is not necessary to show them in the log.
              if (!(e instanceof java.lang.reflect.InaccessibleObjectException)) {
                // Avoid large stack trace from JavaFX internal Bug
                Logger.error( "FXTableAdapter uncaughtException: " + e.getMessage(), e );
              }
            }
          } );
          fxTableItems.clear();
        }
      }
    } );
  }

  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxTableItems) {
          Profiler.count( "FXTableAdapter insertRow" );
          startUpdate();
          int index;
          if (beforeRow == null) {
            index = -1;
          }
          else {
            index = fxTableItems.indexOf( beforeRow );
          }
          if (index < 0) {
            fxTableItems.add( newRow );
          }
          else {
            fxTableItems.add( index, newRow );
          }
          endUpdate();
        }
      }
    } );
  }


  @Override
  public void removeRow( Model removedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxTableItems) {
          Profiler.count( "FXTableAdapter removeRow" );
          startUpdate();
          fxTableItems.remove( removedRow );
          endUpdate();
        }
      }
    } );
  }

  @Override
  public void updateRow( Model changedRow ) {
    // We must not run this immediately even if on UI Thread, because at least if the change affects
    // selected row we get an uncatchable IndexOutOfBoundsException from fxTableItems.set()
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxTableItems) {
          Profiler.count( "FXTableAdapter updateRow" );
          startUpdate();
          final int index = fxTableItems.indexOf( changedRow );
          if (index >= 0) {
            fxTableItems.set( index, changedRow );
          }
          endUpdate();
        }
      }
    } );
  }

  public <S> void addAutoScroll(final TableView<S> view) {
    if (view == null) {
      throw new NullPointerException();
    }
    view.getItems().addListener((ListChangeListener<S>) (new ListChangeListener<S>() {
      @Override
      public void onChanged( Change<? extends S> c ) {
        c.next();
        if (tableViewPI.isAutoScroll()) {
          final int size = view.getItems().size();
          if (size > 0) {

            Platform.getInstance().runOnUiThread( () -> {
              try {
                view.scrollTo( size - 1 );
              }
              catch (Exception e) {
                // Let the UI run smooth anyway.
              }
            } );

          }
        }
      }
    }));
  }

  /**
   * Called by ListViewPI in order to select row at index
   *  @param displayRow    the row to be selected
   * @param selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    if (tableViewPI.isSelectable() && displayRow != null) {
      Platform.getInstance().runOnUiThread( () -> {
        try {
          synchronized (this) {
            int index = fxTableView.getItems().indexOf( displayRow );
            if (selected) {
              fxTableView.getSelectionModel().select( displayRow );
              fxTableView.requestFocus();
              fxTableView.getFocusModel().focus(index);
              fxTableView.scrollTo( displayRow );
            }
            else {
              fxTableView.getSelectionModel().clearSelection( index );
            }
          }
        }
        catch (NullPointerException e) {
          // ignore
        }
      } );
    }
  }

  /**
   * Called by TableViewPI to start cell editing form application code.
   * This call should do the same as a mouse click on an editable cell.
   *
   * @param rowModel   row model to edit
   * @param columnInfo column to edit
   */
  @Override
  public void editCell( Model rowModel, ColumnInfo columnInfo ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called after a change has been made to an ObservableList.
   *
   * @param selectionChange an object representing the change that was done
   * @see Change
   */
  @Override
  public synchronized void onChanged( Change<? extends Integer> selectionChange ) {
    // invoke on changes of selected index
    List<Model> addedRows = new LinkedList<>();
    List<Model> removedRows = new LinkedList<>();
    while (selectionChange.next()) {
      if (selectionChange.wasRemoved()) {
        if (selectionChange.getRemovedSize() > 0) {
          for (Integer index : selectionChange.getRemoved()) {
            if (index != null) {
              int i = index.intValue();
              if ((i >= 0) && (i < fxTableView.getItems().size())) {
                Model selectedRow = getDisplayRow( i );
                removedRows.add( selectedRow );
              }
            }
          }
        }
      }
      if (selectionChange.wasAdded()) {
        if (selectionChange.getAddedSize() > 0) {
          for (Integer index : selectionChange.getAddedSubList()) {
            if (index != null) {
              int i = index.intValue();
              if ((i >= 0) && (i < fxTableView.getItems().size())) {
                Model selectedRow = getDisplayRow( i );
                addedRows.add( selectedRow );
              }
            }
          }
        }
      }
    }
    for (Model row : removedRows) {
      tableViewPI.onSelectionChanged( row, false );
    }
    for (Model row : addedRows) {
      tableViewPI.onSelectionChanged( row, true );
    }
  }

  /**
   * The <code>call</code> method is called when required, and is given a
   * single argument of type P, with a requirement that an object of type R
   * is returned.
   *
   * @param tableColumn The single argument upon which the returned value should be
   *              determined.
   * @return An object of type R that may be determined based on the provided
   * parameter value.
   */
  @Override
  public TableCell call( TableColumn tableColumn ) {
    String columnID = tableColumn.getId();
    ColumnInfo columnInfo = tableViewPI.getColumnByCompID( ComponentFactory.NAMESPACE.getQName( columnID ) );

    Controller colRenderer = columnInfo.getRenderCtrl();
    ViewPI rendererViewPI = colRenderer.getView();

    if (columnInfo.isReadOnly()) {
      return new FXTableCell( tableViewPI, fxTableView,this, rendererViewPI, tableColumn, fxUIFactory );
    }
    else {
      Controller columnEditor = columnInfo.getEditCtrl();
      if (columnEditor == null) {
        return new FXTableCell( tableViewPI, fxTableView,this, rendererViewPI, tableColumn, fxUIFactory );
      }
      else {
        ViewPI editorViewPI = columnEditor.getView();
        // TODO Mit einer UIFactory implementieren
        if (columnEditor instanceof TextEditorController) {
          return new FXTableCellTextEditor( tableViewPI, fxTableView, this, columnInfo, rendererViewPI, editorViewPI, tableColumn, fxUIFactory );
        }
        else if (columnEditor instanceof HTMLEditorController) {
          return new FXTableCellHtmlEditor( tableViewPI, fxTableView, this, columnInfo, rendererViewPI, editorViewPI, tableColumn, fxUIFactory );
        }
        else {
          return new FXTableCellEditable( tableViewPI, fxTableView, this, columnInfo, rendererViewPI, editorViewPI, tableColumn, fxUIFactory );
        }
      }
    }
  }

  public void selectAll() {
    if (tableViewPI.isSelectable()) {
      Platform.getInstance().runOnUiThread( () -> fxTableView.getSelectionModel().selectAll() );
    }
  }

  public void resetFilter( ColumnInfo columnInfo) {
    // There is no filter at the default FXTableView.
  }

  @Override
  public void resetFilter() {
    // There is no filter at the default FXTableView.
  }

  public TableView<Model> getFxTableView() {
    return fxTableView;
  }

  public HashMap<QName, Object> getFilteredValues(){
    Logger.info( "Called 'getFilteredValues' on table that has no tableFilter set" );
    return null;
  }

  @Override
  public void unselectValues( HashMap<QName, Object> unselectedValuesMap ) {
    throw new RuntimeException( "Called 'unselectValues' on table that has no tableFilter set" );
  }

  public List<Model> getVisibleRowModels() {
    Logger.info( "Called 'getVisibleRowModels' on table that has no tableFilter set" );
    return null;
  }

  @Override
  public void columnChanged( ColumnInfo columnInfo ) {
    try {
      Platform.getInstance().runOnUiThread( new Runnable() {
        @Override
        public void run() {
          ObservableList<TableColumn<Model, ?>> tableColumnList = fxTableView.getColumns();
          TableColumn tableColumn = tableColumnList.get( columnInfo.getIndex() );
          tableColumn.setText( columnInfo.getColName().getName() );
        }
      } );
    }
    catch (Exception e) {
      Logger.error( "Error trying to set column changes.", e );
    }
  }

  @Override
  public void loadSettings() {

    try {
      String prefID = getPrefID();
      Preferences pref = Preferences.userRoot().node( prefID );

      List<TableColumn> currentColumns = Collections.unmodifiableList( new ArrayList<>( fxTableView.getColumns() ) );

      for (int i = 0; i < currentColumns.size(); i++) {
        TableColumn column = currentColumns.get( i );
        String propertyLabel = COLUMN_WIDTH_PREFIX + column.getId().toUpperCase();
        Double storedWidth = Double.valueOf( pref.getDouble( propertyLabel, column.getWidth() ) );
        column.setPrefWidth( storedWidth.doubleValue() );
      }
    }
    catch (IllegalArgumentException e) {
      Logger.error( e.getMessage() );
    }
  }

  @Override
  public void saveSettings() {

    try {
      String prefID = getPrefID();
      Preferences pref = Preferences.userRoot().node( prefID );

      List<TableColumn> currentColumns = Collections.unmodifiableList( new ArrayList<>( fxTableView.getColumns() ) );

      for (int i = 0; i < currentColumns.size(); i++) {
        TableColumn column = currentColumns.get( i );
        String propertyLabel = COLUMN_WIDTH_PREFIX + column.getId().toUpperCase();
        pref.putDouble( propertyLabel, column.getWidth() );
      }
    }
    catch (IllegalArgumentException e) {
      Logger.error( e.getMessage() );
    }
  }

   private String getPrefID() throws IllegalArgumentException {
    QName compID = tableViewPI.getComponentID();
    String settingsID;
    if (uiContainer instanceof FXDialogInterface) {
      settingsID = ((FXDialogInterface) uiContainer).getSettingsID();
    }
    else if (uiContainer instanceof FXFragmentAdapter) {
      settingsID = ((FXDialogInterface) ((FXFragmentAdapter) uiContainer).getViewPI().getController().getDialogController().getDialogComp()).getSettingsID();
    }
    else {
      throw new IllegalArgumentException( "LoadSettings not implemented for this type of container " + uiContainer );
    }

    return settingsID + "_" + compID.toString();
  }


}
