/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITabPaneAdapter;
import de.pidata.gui.view.base.TabPaneViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pru on 20.06.16.
 */
public class FXTabPaneAdapter extends FXAdapter implements UITabPaneAdapter, Callback<TabPane, Tab> {


  private TabPane fxTabPane;
  private TabPaneViewPI tabPaneViewPI;
  private ObservableList<Tab> fxTabItems;
  private SingleSelectionModel<Tab> selectionModel;

  public FXTabPaneAdapter( TabPane fxTabPane, TabPaneViewPI tabPaneViewPI, UIContainer uiContainer ) {
    super( fxTabPane, tabPaneViewPI, uiContainer );
    this.fxTabPane = fxTabPane;
    this.tabPaneViewPI = tabPaneViewPI;

    //--- Get Tabs
    fxTabItems = fxTabPane.getTabs();
    selectionModel = fxTabPane.getSelectionModel();

    selectionModel.selectedItemProperty().addListener(
        new ChangeListener<Tab>() {
          @Override
          public void changed( ObservableValue<? extends Tab> ov, Tab oldTab, Tab newTab) {
            tabPaneViewPI.tabChanged(newTab.getId());
          }
        }
    );

  }

  /**
   * The <code>call</code> method is called when required, and is given a
   * single argument of type P, with a requirement that an object of type R
   * is returned.
   *
   * @param param The single argument upon which the returned value should be
   *              determined.
   * @return An object of type R that may be determined based on the provided
   * parameter value.
   */
  @Override
  public Tab call( TabPane param ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public String getSelectedTab() {
    return selectionModel.getSelectedItem().getText();
  }

  @Override
  public void setSelectedTab( String tabId ) {
    try {
      Tab tabToSelect = findTabById( tabId );
      selectionModel.select(tabToSelect);
    }
    catch (Exception e) {
      Logger.error("Tab '" + tabId + "' does not exist.");
    }
  }

  private Tab findTabById( String tabId ) {
    return fxTabItems.stream().filter( tab -> tab.getId().equals( tabId ) ).findAny().orElse(null);
  }

  @Override
  public void setSelectedTab( int index ) {
    selectionModel.select(index);
  }

  @Override
  public void setTabDisabled( String tabId, boolean disabled ) {
    Tab tabToDisable = findTabById( tabId );
    if (tabToDisable == null) {
      throw new IllegalArgumentException( "The tab with id'" + tabId + "' was not found." );
    }
    tabToDisable.setDisable( disabled );
    if(disabled){
      Logger.info( "Disabled Tab id='"+ tabId +"'" );
    }
    else{
      Logger.info( "Enabled Tab id='"+ tabId +"'" );
    }
  }

  @Override
  public void setTabText( String tabId, String text ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        Tab tabToFind = findTabById( tabId );
        if (tabToFind == null) {
          throw new IllegalArgumentException("The tab with id'" + tabId + "' was not found.");
        }
        tabToFind.setText( text );
      }
    } );
  }

  @Override
  public List<String> getTabs() {
    return fxTabItems.stream().map( Tab::getId ).collect(Collectors.toList());
  }
}
