/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;

public class NodeAdapter {

  public static enum RootType {
    NODE, MENU_ITEM, TAB, MENU, TOOLBAR
  }

  public static NodeAdapter adapt(Object fx) {
    if (fx instanceof Node) {
      return new NodeAdapter((Node)fx);
    }
    else if (fx instanceof Menu) {
      return new NodeAdapter((Menu)fx);
    }
    else if (fx instanceof MenuItem) {
      return new NodeAdapter((MenuItem)fx);
    }
    else if (fx instanceof Tab) {
      return new NodeAdapter((Tab)fx);
    }
    else if (fx instanceof ToolBar) {
      return new NodeAdapter((ToolBar)fx);
    }
    return null;
  }

  private Object fxObject;
  private RootType type;

  public NodeAdapter(Node node) {
    this.fxObject = node;
    this.type = RootType.NODE;
  }

  public NodeAdapter(MenuItem menuItem) {
    this.fxObject = menuItem;
    this.type = RootType.MENU_ITEM;
  }

  public NodeAdapter(Menu menu) {
    this.fxObject = menu;
    this.type = RootType.MENU;
  }

  public NodeAdapter(Tab tab) {
    this.fxObject = tab;
    this.type = RootType.TAB;
  }

  public NodeAdapter(ToolBar tab) {
    this.fxObject = tab;
    this.type = RootType.TOOLBAR;
  }

  public Object getFXObject() {
    return fxObject;
  }

  public RootType getRootType() {
    return type;
  }

  public Node getNode() {
    if (type != RootType.NODE)
      return null;
    return (Node)fxObject;
  }

  public MenuItem getMenuItem() {
    if (type != RootType.MENU_ITEM)
      return null;
    return (MenuItem)fxObject;
  }

  public Menu getMenu() {
    if (type != RootType.MENU)
      return null;
    return (Menu)fxObject;
  }

  public Tab getTab() {
    if (type != RootType.TAB)
      return null;
    return (Tab)fxObject;
  }

  public ToolBar getToolBar() {
    if (type != RootType.TOOLBAR)
      return null;
    return (ToolBar)fxObject;
  }


  public StringProperty idProperty() {
    switch (type) {
      case NODE:
        return getNode().idProperty();
      case MENU_ITEM:
        return getMenuItem().idProperty();
      case MENU:
        return getMenu().idProperty();
      case TAB:
        return getTab().idProperty();
      case TOOLBAR:
        return getToolBar().idProperty();
    }
    return null;
  }

  public String getId() {
    switch (type) {
      case NODE:
        return getNode().getId();
      case MENU_ITEM:
        return getMenuItem().getId();
      case MENU:
        return getMenu().getId();
      case TAB:
        return getTab().getId();
     case TOOLBAR:
        return getToolBar().getId();
    }
    return null;
  }

  public ObservableList<String> getStyleClass() {
    switch (type) {
      case NODE:
        return getNode().getStyleClass();
      case MENU_ITEM:
        return getMenuItem().getStyleClass();
      case MENU:
        return getMenu().getStyleClass();
      case TAB:
        return getTab().getStyleClass();
      case TOOLBAR:
        return getToolBar().getStyleClass();
    }
    return null;
  }

  public Object getUserData() {
    switch (type) {
      case NODE:
        return getNode().getUserData();
      case MENU_ITEM:
        return getMenuItem().getUserData();
      case MENU:
        return getMenu().getUserData();
      case TAB:
        return getTab().getUserData();
     case TOOLBAR:
        return getToolBar().getUserData();
    }
    return null;
  }

  public void setUserData(Object data) {
    switch (type) {
      case NODE:
        getNode().setUserData(data);
        break;
      case MENU_ITEM:
        getMenuItem().setUserData(data);
        break;
      case MENU:
        getMenu().setUserData(data);
      case TAB:
        getTab().setUserData(data);
        break;
      case TOOLBAR:
        getToolBar().setUserData(data);
        break;
    }
  }

  public List<NodeAdapter> getChildrenUnmodifiable() {
    List<NodeAdapter> result = new ArrayList<NodeAdapter>();

    // This returns the same thing as Parent.getChildrenUnmodifiable()
//		if (fxObject instanceof Node) {
//			((Node)fxObject).lookupAll("*").forEach(node -> {
//				if (!node.equals(fxObject)) {
//					result.add(new NodeAdapter(node));
//				}
//			});
//		}

    // primary parent type derived from the root type
    if (fxObject instanceof Parent) {
      ((Parent)fxObject).getChildrenUnmodifiable().forEach(node -> result.add(new NodeAdapter(node)));
    } else if (fxObject instanceof Menu) {
      ((Menu)fxObject).getItems().forEach(item -> result.add(new NodeAdapter(item)));
    } else if (fxObject instanceof Tab) {
      Node content = ((Tab)fxObject).getContent();
      if (content != null)
        result.add(new NodeAdapter(content));
    }

    // extended parent types
    if (fxObject instanceof MenuBar) {
      ((MenuBar)fxObject).getMenus().forEach(menu -> result.add(new NodeAdapter(menu)));
    }
    else if (fxObject instanceof TabPane) {
      ((TabPane)fxObject).getTabs().forEach(tab -> result.add(new NodeAdapter(tab)));
    } else if (fxObject instanceof TitledPane) {
      Node content = ((TitledPane)fxObject).getContent();
      if (content != null)
        result.add(new NodeAdapter(content));
    } else if (fxObject instanceof ScrollPane) {
      Node content = ((ScrollPane)fxObject).getContent();
      if (content != null)
        result.add(new NodeAdapter(content));
    } else if (fxObject instanceof Accordion) {
      ((Accordion)fxObject).getPanes().forEach(pane -> result.add(new NodeAdapter(pane)));
    } else if (fxObject instanceof SplitPane) {
      ((SplitPane)fxObject).getItems().forEach(node -> result.add(new NodeAdapter(node)));
    } else if (fxObject instanceof ToolBar) {
      ((ToolBar)fxObject).getItems().forEach(node -> result.add(new NodeAdapter(node)));
    } else if (fxObject instanceof ButtonBar) {
      ((ButtonBar)fxObject).getButtons().forEach(button -> result.add(new NodeAdapter(button)));
    }
    else if (fxObject instanceof ToolBar) {
      ((ToolBar)fxObject).getItems().forEach(item -> result.add(new NodeAdapter(item)));
    }

    // context menu
    if (fxObject instanceof Control) {
      ContextMenu contextMenu = ((Control)fxObject).getContextMenu();
      if (contextMenu != null) {
        contextMenu.getItems().forEach(item -> result.add(new NodeAdapter(item)));
      }
    }

    return Collections.unmodifiableList(result);
  }

  public NodeAdapter getAdapterForChild(Object node) {
    List<NodeAdapter> result = getChildrenUnmodifiable()
        .stream()
        .filter(child -> child.fxObject.equals(node))
        .collect(Collectors.toList());
    if (result.size() == 0)
      return null;
    return result.get(0);
  }

  public void accept(INodeVisitor visitor) {
    boolean result = visitor.visit(this);
    if (!result)
      return;
    getChildrenUnmodifiable().forEach(nodeAdapter -> nodeAdapter.accept(visitor));
  }

  @Override
  public String toString() {
    return "NodeAdapter [type=" + type + ", fxObject=" + fxObject + ", id= "+ idProperty() + "]";
  }

}
