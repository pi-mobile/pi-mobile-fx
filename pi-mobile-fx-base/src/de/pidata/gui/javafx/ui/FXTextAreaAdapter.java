/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextEditorAdapter;
import de.pidata.gui.view.base.TextEditorViewPI;
import de.pidata.log.Logger;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.gui.component.base.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;

/**
 * Created by pru on 29.09.2016.
 */
public class FXTextAreaAdapter extends FXValueAdapter implements UITextEditorAdapter {

  private TextArea fxTextArea;
  private TextEditorViewPI textViewPI;
  private Namespace valueNS;

  private ChangeListener<? super String> stringChangeListener = null;

  public FXTextAreaAdapter( TextArea fxTextArea, TextEditorViewPI textViewPI, UIContainer uiContainer ) {
    super( fxTextArea, textViewPI, uiContainer );
    this.fxTextArea = fxTextArea;
    this.textViewPI = textViewPI;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxTextArea = null;
    textViewPI = null;
    super.detach();
  }

  @Override
  public Object getValue() {
    if (valueNS == null) {
      return fxTextArea.getText();
    }
    else {
      return valueNS.getQName( fxTextArea.getText() );
    }
  }

  @Override
  public void setValue( Object value ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (value == null) {
        setText( "" );
      }
      else if (value instanceof QName) {
        valueNS = ((QName) value).getNamespace();
        setText( ((QName) value).getName() );
      }
      else {
        setText( value.toString() );
        valueNS = null;
      }
    });
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    Platform.getInstance().runOnUiThread( () -> {
      fxTextArea.positionCaret( posX );
    });
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - on screen keyboard not supported
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      if (stringChangeListener == null) {
        stringChangeListener = new ChangeListener<String>() {
          @Override
          public void changed( ObservableValue<? extends String> observable, String oldValue, String newValue ) {
            textViewPI.textChanged( oldValue, newValue );
          }
        };
        fxTextArea.textProperty().addListener( stringChangeListener );
      }
    }
    else {
      if (stringChangeListener != null) {
        fxTextArea.textProperty().removeListener( stringChangeListener );
        stringChangeListener = null;
      }
    }
  }

  private void setText(String text) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (fxTextArea != null) {
          fxTextArea.setText( text );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void select( int fromPos, int toPos ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        fxTextArea.requestFocus(); // get focus first
        fxTextArea.selectRange( fromPos, toPos );
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  public void selectAll() {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        fxTextArea.requestFocus(); // get focus first
        fxTextArea.selectAll();
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setEnabled( boolean enabled ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (fxTextArea != null) {
          fxTextArea.setEditable( enabled );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }
}

