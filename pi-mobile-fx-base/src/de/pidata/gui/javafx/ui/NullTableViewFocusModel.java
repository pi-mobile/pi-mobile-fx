/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumnBase;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;

public class NullTableViewFocusModel extends TableView.TableViewFocusModel {
  /**
   * Creates a default TableViewFocusModel instance that will be used to
   * manage focus of the provided TableView control.
   *
   * @param tableView The tableView upon which this focus model operates.
   * @throws NullPointerException The TableView argument can not be null.
   */
  public NullTableViewFocusModel( TableView tableView ) {
    super( tableView );
  }

  @Override
  public void focus( int row, TableColumn column ) {
    //do nothing because there is no focus
  }

  @Override
  public void focus( TablePosition pos ) {
    //do nothing because there is no focus
  }

  @Override
  public boolean isFocused( int row, TableColumn column ) {
    return false;
  }

  @Override
  public void focus( int index ) {
    //do nothing because there is no focus
  }

  @Override
  public void focusAboveCell() {
    //do nothing because there is no focus
  }

  @Override
  public void focusBelowCell() {
    //do nothing because there is no focus
  }

  @Override
  public void focusLeftCell() {
    //do nothing because there is no focus
  }

  @Override
  public void focusRightCell() {
    //do nothing because there is no focus
  }

  @Override
  public void focusPrevious() {
    //do nothing because there is no focus
  }

  @Override
  public void focusNext() {
    //do nothing because there is no focus
  }

  @Override
  public boolean isFocused( int index ) {
    return false;
  }

  @Override
  public void focus( int row, TableColumnBase column ) {
    //do nothing because there is no focus
  }

  @Override
  public boolean isFocused( int row, TableColumnBase column ) {
    return false;
  }
}
