/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ButtonController;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Created by pru on 01.06.16.
 */
public class FXButtonAdapter extends FXAdapter implements UIButtonAdapter, EventHandler<ActionEvent> {

  public static final double IMAGE_HEIGHT_OFFSET = 5;
  public static final double IMAGE_WIDTH_OFFSET = 5;

  private Button fxButton;
  private ButtonViewPI buttonViewPI;

  public FXButtonAdapter( Button fxButton, ButtonViewPI buttonViewPI, UIContainer uiContainer ) {
    super( fxButton, buttonViewPI, uiContainer );
    this.fxButton = fxButton;
    this.buttonViewPI = buttonViewPI;
    fxButton.setOnAction( this );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxButton.setOnAction( null );
    fxButton = null;
    buttonViewPI = null;
    super.detach();
  }

  /**
   * ---DEPRECATED---
   * setLabel will be called if binding exists using valueID,
   * use iconValueID or labelValueID instead!
   *
   * Set this button's label
   * @param label the label text
   */
  @Override
  public void setLabel( String label ) {
    final String newText;
    Namespace ns = viewPI.getController().getName().getNamespace();
    int pos = label.indexOf( '|' );
    if (pos > 0) {
      String iconName = label.substring( 0, pos );
      String text = label.substring( pos + 1 );
      if (iconName.length() > 0) {
        Node graphicNode = fxButton.getGraphic();
        if (graphicNode instanceof ImageView) {
          ComponentBitmap bitmap = Platform.getInstance().getBitmap( GuiBuilder.NAMESPACE.getQName( iconName ) );
          Image image;
          if (bitmap != null) {
            image = (Image) bitmap.getImage();
          }
          else {
            ComponentBitmap compBitmap = Platform.getInstance().getBitmap( ns.getQName( iconName ) );
            if (compBitmap == null) {
              image = null;
            }
            else {
              image = (Image) compBitmap.getImage();
            }
          }
          Platform.getInstance().runOnUiThread( () -> ((ImageView) graphicNode).setImage( image ) );
          newText = text;
        }
        else {
          newText = label;
        }
      }
      else {
        newText = label;
      }
    }
    else {
      newText = label;
    }
    Platform.getInstance().runOnUiThread( () -> fxButton.setText( newText ) );
  }

  /**
   * Set this button's text Property
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    if (text instanceof String) {
      Platform.getInstance().runOnUiThread( () -> fxButton.setText( (String) text ) );
    }
    else if (text instanceof QName){
      Platform.getInstance().runOnUiThread( () -> fxButton.setText( ((QName) text).getName() ) );
    }
  }

  /**
   * Set image on Button
   * @param value
   */
  @Override
  public void setGraphic( Object value ) {
    Image image = null;
    Node graphicNode = this.fxButton.getGraphic();
    //add ImageView to Button if missing
    if (!(graphicNode instanceof ImageView)) {
      graphicNode = new ImageView();
      ((ImageView) graphicNode).setFitHeight( fxButton.getPrefHeight() - IMAGE_HEIGHT_OFFSET );
      ((ImageView) graphicNode).setFitWidth( fxButton.getPrefWidth() - IMAGE_WIDTH_OFFSET );
      ((ImageView) graphicNode).setPreserveRatio( true );
      Node finalGraphicNode = graphicNode;
      Platform.getInstance().runOnUiThread(() -> fxButton.setGraphic( finalGraphicNode ));
    }

    if (value instanceof QName) {
      QName imageResourceID = (QName) value;
      ComponentBitmap bitmap = Platform.getInstance().getBitmap( imageResourceID );
      if (bitmap == null) {
        image = null;
      }
      else {
        image = (Image) bitmap.getImage();
      }
    }
    else if (value instanceof ComponentBitmap) {
      Object imageObject = ((ComponentBitmap) value).getImage();
      if(imageObject instanceof Image){
        image = (Image) imageObject;
      }
    }
    Node finalGraphicNode1 = graphicNode;
    Image finalImage = image;
    Platform.getInstance().runOnUiThread(() -> ((ImageView) finalGraphicNode1).setImage( finalImage ));
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    return fxButton.getText();
  }

  /**
   * Invoked when a specific event of the type for which this handler is
   * registered happens.
   *
   * @param event the event which occurred
   */
  @Override
  public void handle( ActionEvent event ) {
    buttonViewPI.onClick( this, uiContainer.getDataContext() );
  }

  @Override
  public void setSelected( boolean selected ) {
    //TODO
  }
}
