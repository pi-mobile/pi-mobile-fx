/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.view.base.TreeTableViewPI;
import javafx.scene.control.TreeTableColumn;

public class FXTreeTableColumn extends TreeTableColumn {

  private TreeTableViewPI treeTableViewPI;
  private FXTreeTableAdapter fxTreeAdapter;

  public FXTreeTableColumn( TreeTableViewPI treeTableViewPI ) {
    super();
    this.treeTableViewPI = treeTableViewPI;
    this.fxTreeAdapter = (FXTreeTableAdapter) treeTableViewPI.getUIAdapter();

  }


}
