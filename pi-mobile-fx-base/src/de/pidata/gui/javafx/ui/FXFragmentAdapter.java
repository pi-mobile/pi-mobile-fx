/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.javafx.FXUIContainer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.ui.base.UIFragmentAdapter;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class FXFragmentAdapter extends FXAdapter implements UIFragmentAdapter, FXUIContainer {

  /** holds the loaded modules; necessary to find identical instances of JavaFX elements when searching for them */
  private ModuleGroup activeModule = null;

  public FXFragmentAdapter( Pane fragmentContainer, ModuleViewPI moduleViewPI, UIContainer uiContainer ) {
    super( fragmentContainer, moduleViewPI, uiContainer );
    moduleViewPI.setFragmentAdapter( this );
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    if (activeModule == null) {
      return null;
    }
    else {
      return activeModule.getModel();
    }
  }

  /**
   * Returns the Node identified by uiCompID within this FXUIContainer
   *  @param uiCompID    component (Node) identifier
   * @param moduleID
   */
  @Override
  public Node findUIComp( QName uiCompID, QName moduleID ) {
    if (fxUIComponent != null) {
      return fxUIComponent.lookup( "#" + uiCompID.getName() );
    }
    return null;
  }

  @Override
  public MenuItem findMenuItem( QName componentID ) {
    // There will never be menu items in a fragment
    return null;
  }

  @Override
  public Menu findMenu( QName componentID ) {
    // There will never be menu items in a fragment
    return null;
  }

  @Override
  public Node findToolBarItem( QName componentID ) {
    // There will never be menu items in a fragment
    return null;
  }

  @Override
  public Scene getScene() {
    return null;
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  /**
   * Called by ModuleViewPI whenever the module has been replaced
   *
   * @param moduleGroup the new module or null to remove current module
   */
  @Override
  public void moduleChanged( ModuleGroup moduleGroup ) {
    final ModuleGroup oldModule = activeModule;
    if (moduleGroup == null) {
      activeModule = null;
    }
    else {
      activeModule = moduleGroup;
    }
    Logger.debug( "ModuleChanged: moduleID=" + moduleGroup );
    //case1: old is null, active is not null: load without removing
    //case2: old is not null, active is not null: remove and load
    //case3: old is not null, active is null: remove without loading
    if (oldModule != activeModule) {
      FXMLLoader fxmlLoader = null;
      if (activeModule != null) {
        fxmlLoader = new FXMLLoader( getClass().getResource( "/layout/" + activeModule.getName().getName() + ".fxml" ) );
        fxmlLoader.setRoot( fxUIComponent );

      }
//        fxmlLoader.setController( this );
      FXMLLoader finalFxmlLoader = fxmlLoader;
      Platform.getInstance().runOnUiThread( () -> {
        try {
          if (oldModule != null) {
            //remove old Module in UI
            ((Pane) fxUIComponent).getChildren().clear();
            ((ModuleViewPI) viewPI).onFragmentDestroyed( oldModule );
          }
          if (finalFxmlLoader != null) {
            //load new Module into UI
            finalFxmlLoader.load();
            ((ModuleViewPI) viewPI).onFragmentLoaded( activeModule );
          }
        }
        catch (IOException e) {
          throw new RuntimeException( e );
        }
      } );
    }
  }
}
