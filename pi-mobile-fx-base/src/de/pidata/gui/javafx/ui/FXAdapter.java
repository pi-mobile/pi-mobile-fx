/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.scene.Node;
import javafx.scene.paint.Color;

/**
 * Created by pru on 01.06.16.
 */
public abstract class FXAdapter implements UIAdapter {

  protected Node fxUIComponent;
  protected ViewPI viewPI;
  protected UIContainer uiContainer;

  protected FXAdapter( Node fxUIComponent, ViewPI viewPI, UIContainer uiContainer ) {
    this.fxUIComponent = fxUIComponent;
    this.viewPI = viewPI;
    this.uiContainer = uiContainer;
  }

  @Override
  public UIContainer getUIContainer() {
    return this.uiContainer;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxUIComponent = null;
    viewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return viewPI;
  }

  @Override
  public void setVisible( boolean visible ) {
    Platform.getInstance().runOnUiThread( () -> {
      if (fxUIComponent != null) {
        fxUIComponent.setVisible( visible );
        fxUIComponent.setManaged( visible );
      }
    } );
  }

  @Override
  public boolean isVisible() {
    return fxUIComponent.isVisible();
  }

  @Override
  public void setEnabled( boolean enabled ) {
    Platform.getInstance().runOnUiThread( () -> {
      try {
        if (fxUIComponent != null) {
          fxUIComponent.setDisable( !enabled );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  @Override
  public boolean isEnabled() {
    if (fxUIComponent == null) {
      return false;
    }
    else {
      return !(fxUIComponent.isDisabled());
    }
  }

  @Override
  public void setBackground( ComponentColor color ) {
    de.pidata.gui.component.base.Platform.getInstance().runOnUiThread( () -> {
      try {
        if (fxUIComponent != null) {
          Color fxColor = (Color) color.getColor();
          int red = (int) ( 255 * fxColor.getRed());
          int green = (int) ( 255 * fxColor.getGreen());
          int blue = (int) ( 255 * fxColor.getGreen());
          String style = "-fx-background-color: rgb(" + red + "," + green + ", " + blue + ");";
          fxUIComponent.setStyle( style );
        }
      }
      catch (Exception e) {
        Logger.error( getClass().getSimpleName(), e );
      }
    } );
  }

  /**
   * Resets this UIAdapters background color by its initial background color
   */
  @Override
  public void resetBackground() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void repaint() {
    fxUIComponent.getParent().requestLayout();
  }

  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * send onClick request from Controller -> ViewPI -> UIAdapter -> View
   */
  @Override
  public void performOnClick() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
//    if (fxUIComponent != null) {
//      fxUIComponent.fireEvent( new MouseEvent(  ) );
//    }
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   *
   * @param posX     the x position of the source event
   * @param posY     the y position of the source event
   * @param focus    set focus on/off
   * @param selected set selected if true, deselected if false, do not change if null
   * @param enabled  set enabled if true, disabled if false, do not change if null
   * @param visible  set visible if true, invisible if false, do not change if null
   */
  @Override
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    if (fxUIComponent != null) {
      Node currentFxUIComponent = fxUIComponent;
      if (focus != null) {
        if (focus.booleanValue() && !currentFxUIComponent.isFocused()) {
          Platform.getInstance().runOnUiThread( () -> currentFxUIComponent.requestFocus() );
        }
      }
      if(selected != null) {
        throw new RuntimeException( "TODO: not yet implemented" );
      }
      if (enabled != null) {
        Platform.getInstance().runOnUiThread( () -> currentFxUIComponent.setDisable( !(enabled.booleanValue()) ) );
      }
      if (visible != null) {
        Platform.getInstance().runOnUiThread( () -> currentFxUIComponent.setVisible( (visible.booleanValue()) ) );
      }
    }
  }
  @Override
  public boolean isFocused() {
    if (fxUIComponent != null) {
      return fxUIComponent.isFocused();
    }
    return false;
  }

}
