/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.javafx.FXPlatform;
import javafx.application.HostServices;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;
import org.w3c.dom.html.HTMLAnchorElement;

import java.net.URL;
import java.util.ResourceBundle;

public class FXWebViewController implements Initializable, ChangeListener<Worker.State>, EventListener {

  @FXML
  WebView webViewMessage;
  @FXML
  WebView webViewSolution;

  private static final String CLICK_EVENT = "click";
  private static final String ANCHOR_TAG = "a";

  /**
   * Called to initialize a controller after its root element has been
   * completely processed.
   *
   * @param location  The location used to resolve relative paths for the root object, or
   *                  <tt>null</tt> if the location is not known.
   * @param resources The resources used to localize the root object, or <tt>null</tt> if
   */
  @Override
  public void initialize( URL location, ResourceBundle resources ) {
    webViewSolution.getEngine().getLoadWorker().stateProperty().addListener(this);
    webViewSolution.getEngine().setUserStyleSheetLocation(getClass().getResource("/css/html.css").toString());
    webViewMessage.getEngine().setUserStyleSheetLocation(getClass().getResource("/css/html.css").toString());
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State newValue ) {
    if (Worker.State.SUCCEEDED.equals(newValue)) {
      Document document = webViewSolution.getEngine().getDocument();
      NodeList anchors = document.getElementsByTagName(ANCHOR_TAG);
      for (int i = 0; i < anchors.getLength(); i++) {
        Node node = anchors.item(i);
        EventTarget eventTarget = (EventTarget) node;
        eventTarget.addEventListener(CLICK_EVENT, this, false);
      }
    }
  }

  /**
   * This method is called whenever an event occurs of the type for which
   * the <code> EventListener</code> interface was registered.
   *
   * @param evt The <code>Event</code> contains contextual information
   *            about the event. It also contains the <code>stopPropagation</code>
   *            and <code>preventDefault</code> methods which are used in
   *            determining the event's flow and default action.
   */
  @Override
  public void handleEvent( Event evt ) {
    HTMLAnchorElement anchorElement = (HTMLAnchorElement)evt.getCurrentTarget();
    String href = anchorElement.getHref();
    // IMPORTANT: the following does not work with Amazon corretto JDK;
    // the HostServices implementation references com.sun.deploy via Reflection which comes from ORACLE.
    HostServices hostServices = ((FXPlatform) de.pidata.gui.component.base.Platform.getInstance()).getApplication().getHostServices();
    hostServices.showDocument(href);
    evt.preventDefault();
  }
}
