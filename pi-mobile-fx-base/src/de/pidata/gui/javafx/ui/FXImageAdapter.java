/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.javafx.FXPlatform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIImageAdapter;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FXImageAdapter extends FXAdapter implements UIImageAdapter {

  protected ImageView fxImageView;

  public FXImageAdapter( ImageView fxImageView, ViewPI viewPI, UIContainer uiContainer ) {
    super( fxImageView, viewPI, uiContainer );
    this.fxImageView = fxImageView;
  }

  @Override
  public void setImageResource( QName imageResourceID ) {
    Image image;
    ComponentBitmap bitmap = Platform.getInstance().getBitmap( imageResourceID );
    if (bitmap == null) {
      image = null;
    }
    else {
      image = (Image) bitmap.getImage();
    }
    fxImageView.setImage( image );
  }

  @Override
  public void setImage( ComponentBitmap bitmap ) {
    if (bitmap == null) {
      fxImageView.setImage( null );
    }
    else {
      Object image = bitmap.getImage();
      if (image == null) {
        fxImageView.setImage( null );
      }
      else {
        fxImageView.setImage( (Image)image );
      }
    }
  }

  @Override
  public boolean isZoomEnabled() {
    return false;
  }

  @Override
  public void setZoomFactor( double zoomFactor ) {
    throw new RuntimeException( "Zoom not allowed for CompID=" + fxImageView.getId() + "use ZoomableImageView for zooming" );
  }

  @Override
  public double getZoomFactor() {
    throw new RuntimeException( "Zoom not allowed for CompID=" + fxImageView.getId() + "use ZoomableImageView for zooming" );
  }
}
