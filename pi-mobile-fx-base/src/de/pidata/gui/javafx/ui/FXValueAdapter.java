/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ValueController;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;

/**
 * Created by pru on 01.06.16.
 */
public abstract class FXValueAdapter extends FXAdapter implements UIValueAdapter, ChangeListener<Boolean> {

  protected FXValueAdapter( Node fxControl, ViewPI viewPI, UIContainer uiContainer ) {
    super( fxControl, viewPI, uiContainer );
    fxUIComponent.focusedProperty().addListener( this );
    setEnabled( !(((ValueController) viewPI.getController()).isReadOnly()) );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    fxUIComponent.focusedProperty().removeListener( this );
    super.detach();
  }

  /**
   * This method needs to be provided by an implementation of
   * {@code ChangeListener}. It is called if the value of an
   * {@link ObservableValue} changes.
   * <p>
   * In general is is considered bad practice to modify the observed value in
   * this method.
   *
   * @param observable The {@code ObservableValue} which value changed
   * @param oldValue   The old value
   * @param newValue
   */
  @Override
  public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
    // There are a lot of boolean properties. Our childrer might have beeen attached to another,
    // e.g. FXFlagAdapter is attached to its selectedProperty, which also results in call to this method
    if (observable == fxUIComponent.focusedProperty()) {
      // might be detached
      if ((viewPI != null) && (newValue != null)) {
        viewPI.onFocusChanged( this, newValue.booleanValue(), uiContainer.getDataContext() );
      }
    }
  }
}
