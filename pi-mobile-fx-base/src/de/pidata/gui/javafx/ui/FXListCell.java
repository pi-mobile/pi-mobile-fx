/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;

/**
 * Created by pru on 20.06.16.
 */
public class FXListCell extends ListCell<Model> {

  private ListViewPI listViewPI;

  public FXListCell( ListView fxListView, ListViewPI listViewPI ) {
    this.listViewPI = listViewPI;
  }

  /**
   * The updateItem method should not be called by developers, but it is the
   * best method for developers to override to allow for them to customise the
   * visuals of the cell. To clarify, developers should never call this method
   * in their code (they should leave it up to the UI control, such as the
   * {@link ListView} control) to call this method. However,
   * the purpose of having the updateItem method is so that developers, when
   * specifying custom cell factories (again, like the ListView
   * {@link ListView#cellFactoryProperty() cell factory}),
   * the updateItem method can be overridden to allow for complete customisation
   * of the cell.
   * <p>
   * <p>It is <strong>very important</strong> that subclasses
   * of Cell override the updateItem method properly, as failure to do so will
   * lead to issues such as blank cells or cells with unexpected content
   * appearing within them. Here is an example of how to properly override the
   * updateItem method:
   * <p>
   * <pre>
   * protected void updateItem(T item, boolean empty) {
   *     super.updateItem(item, empty);
   *
   *     if (empty || item == null) {
   *         setText(null);
   *         setGraphic(null);
   *     } else {
   *         setText(item.toString());
   *     }
   * }
   * </pre>
   * <p>
   * <p>Note in this code sample two important points:
   * <ol>
   * <li>We call the super.updateItem(T, boolean) method. If this is not
   * done, the item and empty properties are not correctly set, and you are
   * likely to end up with graphical issues.</li>
   * <li>We test for the <code>empty</code> condition, and if true, we
   * set the text and graphic properties to null. If we do not do this,
   * it is almost guaranteed that end users will see graphical artifacts
   * in cells unexpectedly.</li>
   * </ol>
   *
   * @param item  The new item for the cell.
   * @param empty whether or not this cell represents data from the list. If it
   *              is empty, then it does not represent any domain data, but is a cell
   *              being used to render an "empty" row.
   * @expert
   */
  @Override
  protected void updateItem( Model item, boolean empty ) {
    super.updateItem(item, empty);

    if (empty || item == null) {
      setText(null);
      setGraphic(null);
    }
    else {
      Model rowModel = item;
      Node fxRowNode = createRowNode();
      QName valueID = listViewPI.getDisplayValueID();
      updateNode( fxRowNode, rowModel, valueID, null );
      setText(null);
      setGraphic(fxRowNode);
    }
  }

  public void updateNode( Node fxNode, Model model, QName valueID, ColumnInfo columnInfo ) {
    Object value = null;
    Controller renderer = null;
    if (listViewPI != null) {
      value = listViewPI.getCellValue( model, valueID );
    }
    if (columnInfo != null) {
      renderer = columnInfo.getRenderCtrl();
      value = renderer.render( value );
    }
    if (fxNode instanceof ImageView) {
      updateImageView( (ImageView) fxNode, value );
    }
    else if (fxNode instanceof CheckBox) {
      if (value instanceof Boolean) {
        updateCheckBox( (CheckBox) fxNode, (Boolean) value, "ok" );
      }
      else {
        updateCheckBox( (CheckBox) fxNode, Boolean.FALSE, value.toString() );
      }
    }
    else if (fxNode instanceof Label) {
      if (renderer == null) {
        value = StringRenderer.getDefault().render( value );
      }
      updateLabel( (Label) fxNode, value );
    }
    else if (fxNode instanceof Text) {
      if (renderer == null) {
        value = StringRenderer.getDefault().render( value );
      }
      updateText( (Text) fxNode, value );
    }
    else if (fxNode instanceof TextField) {
      if (renderer == null) {
        value = StringRenderer.getDefault().render( value );
      }
      updateTextField( (TextField) fxNode, value );
    }
    else {
      throw new IllegalArgumentException( "Unsupported view class="+fxNode.getClass() );
    }
  }

  private void updateCheckBox( CheckBox view, Boolean value, String label ) {
    if (value != null) {
      view.setSelected( value.booleanValue() );
    }
    else {
      view.setSelected( false );
    }
    view.setText( label );
  }

  private void updateImageView( ImageView view, Object value ) {
    //TODO value==null behandeln
    if (value != null) {
      //QName imageID = QName.getInstance( value.toString() );
      //TODO view.setImage( getRessourceID( IDCLASS_DRAWABLE, imageID ) );
      throw new RuntimeException( "TODO" );
    }
  }

  private void updateLabel( Label view, Object value ) {
    if (value == null) {
      view.setText( "" );
    }
    else {
      view.setText( value.toString() );
    }
  }

  private void updateText( Text view, Object value ) {
    if (value == null) {
      view.setText( "" );
    }
    else {
      view.setText( value.toString() );
    }
  }

  private void updateTextField( TextField view, Object value ) {
    if (value == null) {
      view.setText( "" );
    }
    else {
      view.setText( value.toString() );
    }
  }

  protected Node createRowNode() {
    Node view = null;
    QName rowDefID;
    if (listViewPI == null) {
      rowDefID = null;
    }
    else {
      rowDefID = listViewPI.getRowCompID();
    }
    if (rowDefID == null) {
      view = new Label();
    }
    else {
      String resourceName = "/layout/"+rowDefID.getName()+".fxml";
      URL mainDlgRes = getClass().getResource( resourceName );
      try {
        view = FXMLLoader.load( mainDlgRes );
      }
      catch (IOException e) {
        Logger.error( "Error loading row resource name="+ resourceName, e );
      }
    }
    return view;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void startEdit() {
    super.startEdit();
  }

  /**
   * {@inheritDoc}
   *
   * @param newValue
   */
  @Override
  public void commitEdit( Model newValue ) {
    super.commitEdit( newValue );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelEdit() {
    super.cancelEdit();
  }

  /**
   * Updates whether this cell is in a selected state or not.
   *
   * @param selected whether or not to select this cell.
   * @expert
   */
  @Override
  public void updateSelected( boolean selected ) {
    super.updateSelected( selected );
  }
}
