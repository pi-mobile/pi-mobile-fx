/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeTableViewPI;
import javafx.geometry.Pos;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableRow;
import javafx.scene.layout.StackPane;

public class FXTreeTableRow extends TreeTableRow<TreeNodePI> {

  private TreeTableViewPI treeTableViewPI;
  private FXTreeTableAdapter fxTreeAdapter;

  public FXTreeTableRow( TreeTableViewPI treeTableViewPI ) {
    super();
    this.treeTableViewPI = treeTableViewPI;
    this.fxTreeAdapter = (FXTreeTableAdapter) treeTableViewPI.getUIAdapter();
  }

  /**
   * The updateItem method should not be called by developers, but it is the
   * best method for developers to override to allow for them to customise the
   * visuals of the cell. To clarify, developers should never call this method
   * in their code (they should leave it up to the UI control, such as the
   * {@link ListView} control) to call this method. However,
   * the purpose of having the updateItem method is so that developers, when
   * specifying custom cell factories (again, like the ListView
   * {@link ListView#cellFactoryProperty() cell factory}),
   * the updateItem method can be overridden to allow for complete customisation
   * of the cell.
   *
   * <p>It is <strong>very important</strong> that subclasses
   * of Cell override the updateItem method properly, as failure to do so will
   * lead to issues such as blank cells or cells with unexpected content
   * appearing within them. Here is an example of how to properly override the
   * updateItem method:
   *
   * <pre>
   * protected void updateItem(T item, boolean empty) {
   *     super.updateItem(item, empty);
   *
   *     if (empty || item == null) {
   *         setText(null);
   *         setGraphic(null);
   *     } else {
   *         setText(item.toString());
   *     }
   * }
   * </pre>
   *
   * <p>Note in this code sample two important points:
   * <ol>
   * <li>We call the super.updateItem(T, boolean) method. If this is not
   * done, the item and empty properties are not correctly set, and you are
   * likely to end up with graphical issues.</li>
   * <li>We test for the <code>empty</code> condition, and if true, we
   * set the text and graphic properties to null. If we do not do this,
   * it is almost guaranteed that end users will see graphical artifacts
   * in cells unexpectedly.</li>
   * </ol>
   *
   * @param item  The new item for the cell.
   * @param empty whether or not this cell represents data from the list. If it
   *              is empty, then it does not represent any domain data, but is a cell
   *              being used to render an "empty" row.
   * @expert
   */
  @Override
  protected void updateItem( TreeNodePI item, boolean empty ) {

    try {
      StackPane pane = (StackPane) disclosureNodeProperty().get();
      pane.translateYProperty().bind( heightProperty().multiply( 0.4 ).subtract( pane.heightProperty().multiply( 0.5 ) ) );
    }
    catch (Exception e) {
      // Maybe in a custom control disclosureNodeProperty() might not be a StackPane
    }

    super.updateItem( item, empty );
  }
}
