/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.renderer.Renderer;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.SingleSelection;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.gui.component.base.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.SingleSelectionModel;

/**
 * Created by pru on 17.01.2017.
 */
public class FXChoiceAdapter extends FXAdapter implements UIListAdapter, ChangeListener<Number> {

  private static final String DUMMY_ENTRY = "Dummy";
  private ChoiceBox fxChoiceBox;
  private ListViewPI listViewPI;
  private ObservableList<String> fxChoiceItems;
  private ObservableList<Model> fxListItems;

  public FXChoiceAdapter( ChoiceBox fxChoiceBox, ListViewPI listViewPI, UIContainer uiContainer ) {
    super( fxChoiceBox, listViewPI, uiContainer );
    this.fxChoiceBox = fxChoiceBox;
    this.listViewPI = listViewPI;

    fxChoiceItems = FXCollections.observableArrayList();
    fxListItems = FXCollections.observableArrayList();
    fxChoiceItems.add( DUMMY_ENTRY );
    fxChoiceBox.setItems( fxChoiceItems );

    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        SingleSelectionModel selectionModel = fxChoiceBox.getSelectionModel();

        //--- Set initial selection
        SingleSelection selection = (SingleSelection) listViewPI.getListCtrl().getSelection();
        if (selection.selectedValueCount() > 0) {
          int index = fxListItems.indexOf( selection.getSelectedValue( 0 ) );
          selectionModel.select( index );
        }
        else {
          selectionModel.clearSelection();
        }

        //--- Attach listener for selection changes
        selectionModel.selectedIndexProperty().addListener( FXChoiceAdapter.this );
      }
    } );
  }

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    return fxListItems.size();
  }

  /**
   * Returns the data displayed in row at position
   *
   * @param position
   * @return
   */
  @Override
  public Model getDisplayRow( int position ) {
    if (position < fxListItems.size()) {
      return fxListItems.get( position );
    }
    else {
      return null;
    }
  }

  private String getDisplayString( Model model ) {
    if (listViewPI == null) {
      return null;
    }
    else {
      QName valueID = listViewPI.getDisplayValueID();
      Object value = listViewPI.getCellValue( model, valueID );
      return listViewPI.render( null, value );
    }
  }

  private void startUpdate() {
    fxChoiceItems.remove( DUMMY_ENTRY );
  }

  private void endUpdate() {
    fxChoiceBox.setItems( fxChoiceItems );
  }

  @Override
  public void removeAllDisplayRows() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          fxListItems.clear();
          fxChoiceItems.clear();
          endUpdate();
        }
      }
    });
  }

  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();

          int index;
          if (beforeRow == null) {
            index = -1;
          }
          else {
            index = fxListItems.indexOf( beforeRow );
          }
          if (index < 0) {
            fxListItems.add( newRow );
            fxChoiceItems.add( getDisplayString( newRow ) );
            index = fxListItems.size() - 1;
          }
          else {
            fxListItems.add( index, newRow );
            fxChoiceItems.add( index, getDisplayString( newRow ) );
          }

          endUpdate();
        }
      }
    });
  }


  @Override
  public void removeRow( Model removedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          int index = fxListItems.indexOf( removedRow );
          if (index >= 0) {
            fxListItems.remove( removedRow );
            fxChoiceItems.remove( index );
          }
          endUpdate();
        }
      }
    });
  }

  @Override
  public void updateRow( Model changedRow ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          startUpdate();
          int index = fxListItems.indexOf( changedRow );
          if (index >= 0) {
            fxListItems.set( index, changedRow );
            int selIndex = fxChoiceBox.getSelectionModel().getSelectedIndex();
            String displayStr = fxChoiceItems.get( index );
            String newStr = getDisplayString(  changedRow );
            if (!newStr.equals(  displayStr )) {
              fxChoiceItems.set( index, getDisplayString( changedRow ) );
              if (selIndex > 0) {
                // JavaFX Bug Workaround: fxChoiceItems.set() clears selection
                fxChoiceBox.getSelectionModel().select( index );
              }
            }
          }
          endUpdate();
        }
      }
    });
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        synchronized (fxListItems) {
          try {
            int index = fxListItems.indexOf( displayRow );
            if (selected) {
              fxChoiceBox.getSelectionModel().select( index );
            }
            else {
              fxChoiceBox.getSelectionModel().clearSelection( index );
            }
          }
          catch (Exception e) {
            Logger.error( this.getClass().getSimpleName(), e );
          }
        }
      }
    });
  }

  @Override
  public void changed( ObservableValue<? extends Number> observable, Number oldValue, Number newValue ) {
    if ((oldValue == null) || (oldValue.intValue() < 0)) {
      listViewPI.onSelectionChanged( null, false );
    }
    else {
      Model oldSelectedRow = getDisplayRow( oldValue.intValue() );
      listViewPI.onSelectionChanged( oldSelectedRow, false );
    }
    if ((newValue == null) || (newValue.intValue() < 0)){
      listViewPI.onSelectionChanged( null, true );
    }
    else {
      Model newSelectedRow = getDisplayRow( newValue.intValue() );
      listViewPI.onSelectionChanged( newSelectedRow, true );
    }
  }
}
