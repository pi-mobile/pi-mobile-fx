/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;

/**
 *SelectionModel for Tables that are not selectable
 */
public class NullTableViewSelectionModel extends TableView.TableViewSelectionModel {

  public NullTableViewSelectionModel (TableView tableView){
    super(tableView);
  }

  @Override
  public ObservableList<TablePosition> getSelectedCells() {
    return FXCollections.emptyObservableList();
  }

  @Override
  public void clearSelection( int row, TableColumn column ) {
    //do nothing because there is no selection
  }

  @Override
  public void clearAndSelect( int row, TableColumn column ) {
    //do nothing because there is no selection
  }

  @Override
  public void select( int row, TableColumn column ) {
    //do nothing because there is no selection
  }

  @Override
  public boolean isSelected( int row, TableColumn column ) {
    return false;
  }

  @Override
  public void selectLeftCell() {
    //do nothing because there is no selection
  }

  @Override
  public void selectRightCell() {
    //do nothing because there is no selection
  }

  @Override
  public void selectAboveCell() {
    //do nothing because there is no selection
  }

  @Override
  public void selectBelowCell() {
    //do nothing because there is no selection
  }
}
