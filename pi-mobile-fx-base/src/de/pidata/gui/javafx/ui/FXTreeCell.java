/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;
import de.pidata.log.Logger;
import de.pidata.messages.ErrorMessages;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;

/**
 * Created by pru on 12.07.2016.
 */
public class FXTreeCell extends TreeCell<TreeNodePI> {

  private TreeViewPI treeViewPI;
  private FXTreeViewAdapter fxTreeViewAdapter;

  private Node rendererNode;
  private Node editorNode;
  private FXUIFactory fxUIFactory;

  public FXTreeCell( TreeViewPI treeViewPI, FXUIFactory fxUIFactory ) {
    super();
    this.treeViewPI = treeViewPI;
    this.fxTreeViewAdapter = (FXTreeViewAdapter) treeViewPI.getUIAdapter();
    this.fxUIFactory = fxUIFactory;

    setEditable( treeViewPI.isEditable() );
  }

  /**
   * The updateItem method should not be called by developers, but it is the
   * best method for developers to override to allow for them to customise the
   * visuals of the cell. To clarify, developers should never call this method
   * in their code (they should leave it up to the UI control, such as the
   * {@link ListView} control) to call this method. However,
   * the purpose of having the updateItem method is so that developers, when
   * specifying custom cell factories (again, like the ListView
   * {@link ListView#cellFactoryProperty() cell factory}),
   * the updateItem method can be overridden to allow for complete customisation
   * of the cell.
   * <p>
   * <p>It is <strong>very important</strong> that subclasses
   * of Cell override the updateItem method properly, as failure to do so will
   * lead to issues such as blank cells or cells with unexpected content
   * appearing within them. Here is an example of how to properly override the
   * updateItem method:
   * <p>
   * <pre>
   * protected void updateItem(T item, boolean empty) {
   *     super.updateItem(item, empty);
   *
   *     if (empty || item == null) {
   *         setText(null);
   *         setGraphic(null);
   *     } else {
   *         setText(item.toString());
   *     }
   * }
   * </pre>
   * <p>
   * <p>Note in this code sample two important points:
   * <ol>
   * <li>We call the super.updateItem(T, boolean) method. If this is not
   * done, the item and empty properties are not correctly set, and you are
   * likely to end up with graphical issues.</li>
   * <li>We test for the <code>empty</code> condition, and if true, we
   * set the text and graphic properties to null. If we do not do this,
   * it is almost guaranteed that end users will see graphical artifacts
   * in cells unexpectedly.</li>
   * </ol>
   *
   * @param treeNodePI The new item for the cell.
   * @param empty whether or not this cell represents data from the list. If it
   *              is empty, then it does not represent any domain data, but is a cell
   *              being used to render an "empty" row.
   * @expert
   */
  @Override
  public void updateItem( TreeNodePI treeNodePI, boolean empty ) {
    super.updateItem( treeNodePI, empty );

    if (empty || treeNodePI == null) {
      setText(null);
      setGraphic(null);
    }
    else {
      if (isEditing()) {
        if (FXTreeViewAdapter.DEBUG) Logger.info( "isEditing - " + isEditable() );
        updateNode( rendererNode, treeViewPI, treeNodePI, this );
        setGraphic( editorNode );
        setContentDisplay( ContentDisplay.GRAPHIC_ONLY );
      }
      else {
        if(FXTreeViewAdapter.DEBUG) Logger.info( "upateNode [" + treeNodePI + "]" );
        if (rendererNode == null) {
          createCellRendererNode( treeNodePI );
        }
        updateNode( rendererNode, treeViewPI, treeNodePI, this );
        setGraphic( rendererNode );
        setContentDisplay( ContentDisplay.GRAPHIC_ONLY );
      }
    }
  }

  private synchronized void createCellRendererNode( TreeNodePI treeNodePI ) {
    QName rowDefID;
    if (treeNodePI == null) {
      rowDefID = null;
    }
    else {
      rowDefID = treeNodePI.getCellViewDefID();
    }
    if (rowDefID == null) {
      rendererNode = new Label();
    }
    else {
      String resourceName = "/layout/"+rowDefID.getName()+".fxml";
      URL renderCompRes = getClass().getResource( resourceName );
      try {
        rendererNode = FXMLLoader.load( renderCompRes );
      }
      catch (IOException e) {
        Logger.error( "Error loading row resource name="+ resourceName, e );
      }
    }
    if (rendererNode == null) {
      rendererNode = new Label();
    }
  }

  private synchronized void createCellEditorNode() {
    if (editorNode == null) {
      editorNode = new TextField();
    }
    editorNode.setOnKeyReleased( t -> {
      if (t.getCode() == KeyCode.ENTER) {
        if(FXTreeViewAdapter.DEBUG) Logger.info( "key ENTER" );
        cancelEdit();
      }
    }  );
    editorNode.setOnKeyPressed( t -> {
      if (t.getCode() == KeyCode.ESCAPE) {
        if(FXTreeViewAdapter.DEBUG) Logger.info( "key ESCAPE" );
        cancelEditAbort();
      }
    }  );
    editorNode.focusedProperty().addListener( new ChangeListener<Boolean>() {
      @Override
      public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
        if ((newValue != null) && !(newValue.booleanValue())) {
          cancelEdit();
        }
      }
    } );
  }

  private Object getEditorValue() {
    if (editorNode instanceof TextInputControl) {
      return ((TextInputControl) editorNode).getText();
    }
    else {
      throw new IllegalArgumentException( ErrorMessages.ERROR + " Unsupported editorNode class="+editorNode.getClass().getName() );
    }
  }

  public void resetEditor() {
    if (FXTableAdapter.DEBUG) Logger.info( "resetEditor, row="+getIndex() );
    if (rendererNode != null) {
      setGraphic( rendererNode );
    }
    editorNode = null;
  }

  private void updateNode( Node fxCellNode, TreeViewPI treeViewPI, TreeNodePI treeNodePI, Cell cell ) {
    Model nodeModel = treeNodePI.getNodeModel();
    QName valueID = treeNodePI.getDisplayValueID();
    QName editFlagID = treeNodePI.getEditFlagID();
    QName enableFlagID = treeNodePI.getEnabledFlagID();

    if (!cell.isEditing()) {
      // dynamically set cell editable
      Object editFlag = nodeModel.get( editFlagID );
      boolean effectiveEditable;
      if (editFlag == null) {
        effectiveEditable = treeViewPI.isEditable();
      }
      else {
        effectiveEditable = (treeViewPI.isEditable() && ((Boolean) editFlag).booleanValue());
      }
      cell.setEditable( effectiveEditable );

      // dynamically set cell enabled
      Object enabledFlag = nodeModel.get( enableFlagID );
      boolean effectiveEnabled;
      if (enabledFlag == null) {
        effectiveEnabled = treeViewPI.isEnabled();
      }
      else {
        effectiveEnabled = (treeViewPI.isEnabled() && ((Boolean) enabledFlag).booleanValue());
      }
      cell.setDisable( !effectiveEnabled );
    }

    Object value;
    if (valueID == null) {
      value = nodeModel.toString();
    }
    else {
      value = nodeModel.get( valueID );
    }
    if (fxCellNode instanceof Label) {
      String cellString = treeViewPI.render( value, valueID );
      fxUIFactory.updateLabel( (Label) fxCellNode, cellString );
    }
    else if (fxCellNode instanceof Text) {
      String cellString = treeViewPI.render( value, valueID );
      fxUIFactory.updateText( (Text) fxCellNode, cellString );
    }
    else if (fxCellNode instanceof ImageView) {
      Object icon = null;
      QName iconID = treeNodePI.getIconID();
      if (iconID != null) {
        icon = nodeModel.get( iconID );
      }
      FXUIFactory.updateImage( (ImageView) fxCellNode, icon, cell.widthProperty(), valueID.getNamespace().getClass() );
      if (value == null) {
        cell.setText( "" );
      }
      else {
        cell.setText( value.toString() );
      }
    }
    else if (fxCellNode instanceof TextInputControl) {
      String cellString = treeViewPI.render( value, valueID );
      fxUIFactory.updateTextInput( (TextInputControl) fxCellNode, cellString );
    }
    else {
      throw new IllegalArgumentException( "Unsupported view class="+fxCellNode.getClass() );
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void startEdit() {
    if (!isEditable()) {
      return;
    }

    super.startEdit();

    if (FXTreeViewAdapter.DEBUG) {
      TreeNodePI treeNodePI = getItem();
      Model nodeModel = treeNodePI.getNodeModel();
      QName valueID = treeNodePI.getDisplayValueID();
      Logger.info( "startEdit value=" + nodeModel.get( valueID ) );
    }

    if (editorNode == null) {
      createCellEditorNode();
    }
    updateNode( editorNode, treeViewPI, getItem(), this );
    setGraphic( editorNode );
    editorNode.requestFocus();
    setContentDisplay( ContentDisplay.GRAPHIC_ONLY );
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void cancelEdit() {
    if (isEditing()) {
      Object newValue = getEditorValue();
      if (FXTreeViewAdapter.DEBUG) Logger.info( "cancelEdit newValue=" + newValue );

      TreeNodePI currentNode = getItem();
      treeViewPI.onStopEdit( currentNode, currentNode.getDisplayValueID(), newValue );
      //fxTreeAdapter.resetFilter();

      super.cancelEdit();

      // replace editorNode by rendererNode
      resetEditor();
    }
  }

  protected void cancelEditAbort() {
    if (isEditing()) {
      super.cancelEdit();

      // replace editorNode by rendererNode
      resetEditor();
    }
  }
}
