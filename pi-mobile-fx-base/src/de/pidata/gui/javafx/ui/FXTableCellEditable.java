/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.ui;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.FlagController;
import de.pidata.gui.controller.base.HTMLEditorController;
import de.pidata.gui.javafx.FXUIFactory;
import de.pidata.gui.javafx.controls.PIHtmlEditor;
import de.pidata.gui.view.base.TableCellPI;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.log.Profiler;
import de.pidata.messages.ErrorMessages;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.web.HTMLEditor;

import java.io.IOException;
import java.net.URL;

public class FXTableCellEditable extends FXTableCell {

  private ViewPI editorViewPI;
  protected Node editorNode;
  protected ColumnInfo columnInfo;

  public FXTableCellEditable( TableViewPI tableViewPI, TableView<Model> fxTableView, FXTableAdapter fxTableAdapter,
                              ColumnInfo columnInfo, ViewPI columnViewPI, ViewPI editorViewPI, TableColumn tableColumn,
                              FXUIFactory fxUIFactory) {
    super( tableViewPI, fxTableView, fxTableAdapter, columnViewPI, tableColumn, fxUIFactory );
    this.columnInfo = columnInfo;
    this.editorViewPI = editorViewPI;
    setEditable( true );
    tableColumn.setEditable( true );
  }

  @Override
  protected Node createCellRendererNode( ColumnInfo columnInfo ) {
    if ((columnInfo.getRenderCtrl() instanceof FlagController)){
      CheckBox checkBox = new CheckBox();
      if (columnInfo.isReadOnly()) {
        checkBox.setDisable( true );
      }
      else {
        checkBox.selectedProperty().addListener( new ChangeListener<Boolean>() {
          @Override
          public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
            flagChanged( checkBox, newValue );
          }
        } );
      }
      return checkBox;
    }
    else {
      return super.createCellRendererNode( columnInfo );
    }
  }

  private void flagChanged( CheckBox checkBox, Boolean newValue ) {
    if (!isEditing()) {
      editorNode = checkBox;
    }
    Model cellRow = getItem().getModel();
    ColumnInfo columnInfo = getItem().getColumnInfo();
    Model row = columnInfo.getValueModel( cellRow );
    row.set( columnInfo.getValueID(), newValue );
    Parent parent = checkBox.getParent();
    if (parent != null) {
      parent.requestFocus();
    }
  }

  private synchronized void createCellEditorNode() {
    if (editorViewPI != null) {
      // load new UI instance for editing
      QName uiCompID = editorViewPI.getComponentID();
      // FIXME: GUI2 set missing moduleID from GUI file <editor>
      QName moduleID = editorViewPI.getModuleID();

      if (moduleID != null) {
        String resourceName = "/layout/" + moduleID.getName() + ".fxml";
        URL editorModuleRes = getClass().getResource( resourceName );
        try {
          Node moduleNode = FXMLLoader.load( editorModuleRes );
          editorNode = moduleNode.lookup( "#" + uiCompID.getName() );
          if (editorNode == null) {
            throw new IllegalArgumentException( "Could not find UI node=" + uiCompID.getName() + " in moduleID=" + moduleID );
          }
          initCellEditorNode();
        }
        catch (IOException e) {
          Logger.error( "Error loading module resource name=" + resourceName, e );
          throw new IllegalArgumentException( "Could not find UI module, moduleID=" + moduleID );
        }
      }
      else {
        if (editorViewPI.getController() instanceof HTMLEditorController) {
          // editorNode = new HTMLEditor();
          // FIXME: For the RequirementsEditor we use a custom editor with a reduced toolbar.
          // Can be removed with loading of the moduleID above if needed.
          editorNode = new PIHtmlEditor();
          editorNode.setId( editorViewPI.getController().getView().getComponentID().getName() );

          initCellEditorNode();
        }
      }
    }

    if (editorNode == null) {
      editorNode = new TextField();
      initCellEditorNode();
    }
    editorNode.focusedProperty().addListener( new ChangeListener<Boolean>() {
      @Override
      public void changed( ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue ) {
        if ((newValue != null) && !(newValue.booleanValue())) {
          cancelEdit();
        }
      }
    } );
  }

  protected void initCellEditorNode() {
    // default: nothing to do
  }

  /**
   * Implements the editor specific {@link #prepareEditorNode()}.
   * <p>
   * The default assumes having a {@link TextField} as editor.
   */
  protected void prepareEditorNode() {
    // TextField will adjust itself to the Cell's width
  }

  protected Object getEditorValue() {
    if (editorNode instanceof TextInputControl) {
      return ((TextInputControl) editorNode).getText();
    }
    else {
      throw new IllegalArgumentException( ErrorMessages.ERROR + " Unsupported editorNode class="+editorNode.getClass().getName() );
    }
  }

  public void resetEditor() {
    if (FXTableAdapter.DEBUG) Logger.debug( "resetEditor, row="+getIndex() );
    setGraphic( rendererNode );
    editorNode = null;
  }


  @Override
  protected void doUpdateItem( TableCellPI item, boolean empty ) {
    try {
      if (isEditing()) {
        Profiler.count( "FXTableCellEditable editing doUpdateItem" );
        if (FXTableAdapter.DEBUG) Logger.debug( "updateItem/isEditing, row=" + getIndex() );
        setGraphic( editorNode );
        ColumnInfo columnInfo = item.getColumnInfo();
        fxUIFactory.updateCell( this, columnInfo, tableColumn.getPrefWidth() );
      }
      else {
        super.doUpdateItem( item, empty );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error updating TableCellEditable item="+item, ex );
    }
  }

  /**
   * {@inheritDoc}
   * <br><br>
   * This is the default edit cell activated event's target.
   */
  @Override
  public void startEdit() {
    TableCellPI item = getItem();
    try {
      if (FXTableAdapter.DEBUG) {
        Model selectedRow = getItem().getModel();
        Logger.debug( "startEdit, row=" + getIndex() + " row " + selectedRow );
      }
      super.startEdit();

      Model selectedRow = getItem().getModel();
      //    tableViewPI.onStartEdit( selectedRow, columnInfo );

      if (FXTableAdapter.DEBUG) Logger.debug( "prepareEditor, row=" + getIndex() + " row " + selectedRow );
      if (editorViewPI != null) {
        if (editorNode == null) {
          createCellEditorNode();
        }
        prepareEditorNode();

        setGraphic( editorNode );
        ColumnInfo columnInfo = item.getColumnInfo();
        fxUIFactory.updateCell( this, columnInfo, tableColumn.getPrefWidth() );

        tableViewPI.onStartEdit( selectedRow, columnInfo );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error in startEdit item="+item, ex );
    }
  }

  /**
   * {@inheritDoc}
   * <br><br>
   * This is the default edit cell deactivated event's target.
   */
  @Override
  public void cancelEdit() {
    if (FXTableAdapter.DEBUG) Logger.debug( "cancelEdit, row="+getIndex() );
    int index = getIndex();
    if (index >= 0) {
      Model selectedRow = getItem().getModel();
      Object newValue = getEditorValue();
      tableViewPI.onStopEdit( selectedRow, columnInfo, newValue );
      fxTableAdapter.resetFilter(columnInfo);
    }
    super.cancelEdit();

    // editorNode durch rendererNode ersetzen
    resetEditor();
  }
}
