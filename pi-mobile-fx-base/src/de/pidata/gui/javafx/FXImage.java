/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentGraphics;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FXImage implements ComponentBitmap {

  private Image fxImage;
  private String imageFormat;

  public FXImage( Image image, String imageFormat ) {
    if (image == null) {
      throw new IllegalArgumentException( "FX Image must not be null" );
    }
    this.fxImage = image;
    this.imageFormat = imageFormat;
  }

  @Override
  public ComponentGraphics getGraphics() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int getWidth() {
    return (int) Math.floor( fxImage.getWidth());
  }

  @Override
  public int getHeight() {
    return (int) Math.floor( fxImage.getHeight());
  }

  @Override
  public Object getImage() {
    return fxImage;
  }

  /**
   * Saves this bitmap to the given OutputStream
   *
   * @param outStream the strem to which image will be written
   * @param imageType the image type, see constants (TIFF, JPEG, ...)
   */
  @Override
  public void save( OutputStream outStream, QName imageType ) throws IOException {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public int size() {
    return getBytes().length;
  }

  @Override
  public byte[] getBytes() {
    byte[] imageBytes = null;
    try {
      BufferedImage bufferedImage = SwingFXUtils.fromFXImage( fxImage, null );
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      ImageIO.write( bufferedImage, imageFormat, bos );
      imageBytes = bos.toByteArray();
    }
    catch (IOException e) {
      Logger.error( "Could not load image bytes", e );
    }
    return imageBytes;
  }

  @Override
  public void readBytes( InputStream byteStream ) throws IOException {
    fxImage = new Image( byteStream );
  }

  @Override
  public void writeBytes( OutputStream out ) throws IOException {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
