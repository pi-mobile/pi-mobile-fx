/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.qnames.QName;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;

public interface FXUIContainer extends UIContainer {

  /**
   * Returns the Node identified by uiCompID within this FXUIContainer
   * @param uiCompID component (Node) identifier
   * @param moduleID
   */
  Node findUIComp( QName uiCompID, QName moduleID );

  MenuItem findMenuItem( QName componentID );

  Menu findMenu( QName componentID );

  Node findToolBarItem( QName componentID );

  Scene getScene();
}
