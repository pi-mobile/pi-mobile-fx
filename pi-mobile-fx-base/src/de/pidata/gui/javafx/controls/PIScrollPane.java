package de.pidata.gui.javafx.controls;

import javafx.scene.Node;
import javafx.scene.control.ScrollPane;

public class PIScrollPane extends ScrollPane {

  @Override
  public Node lookup( String selector ) {
    Node n = super.lookup( selector );
    if (n == null) {
      for (int i = 0, max = getChildren().size(); i < max; i++) {
        final Node node = getChildren().get( i );
        n = node.lookup( selector );
        if (n != null) return n;
      }
    }
    if (n == null) {
     final Node node = getContent();
     if (node != null) {
       n = node.lookup( selector );
       if (n != null) return n;
     }
    }

    return n;
  }
}
