package de.pidata.gui.javafx.controls;

import javafx.scene.Node;
import javafx.scene.control.SplitPane;

public class PISplitPane extends SplitPane {

  @Override
  public Node lookup( String selector ) {
    Node n = super.lookup( selector );
    if (n == null) {
      for (int i = 0, max = getChildren().size(); i < max; i++) {
        final Node node = getChildren().get( i );
        n = node.lookup( selector );
        if (n != null) return n;
      }
    }
    if (n == null) {
      for (int i = 0, max = getItems().size(); i < max; i++) {
        final Node node = getItems().get( i );
        n = node.lookup( selector );
        if (n != null) return n;
      }
    }

    return n;
  }
}
