package de.pidata.gui.javafx.controls;

import javafx.event.ActionEvent;
import javafx.scene.control.CheckBox;

/**
 * - CheckBox will only cycle through the checked and unchecked states.
 * - Indeterminate state can only be reached programmatically but not by UI.
 * - Each time a matceTriStateCheckBox is indeterminate it is also disabled.
 * - It remains in this state when clicking on the checkbox, until the state is set programmatically to checked or unchecked.
 */
public class MatceTriStateCheckBox extends CheckBox {

  /**
   * Creates a check box with an empty string for its label.
   */
  public MatceTriStateCheckBox() {
    super();
    init();
  }

  /**
   * Creates a check box with the specified text as its label.
   *
   * @param text A text string for its label.
   */
  public MatceTriStateCheckBox( String text ) {
    super( text );
    init();
  }

  private void init() {
    this.setAllowIndeterminate( true );
    //changes on indeterminateProperty will cause changes on disabled property as well!
    //Each time a matceTriStateCheckBox is indeterminate it is also disabled.
    this.indeterminateProperty().addListener( ( observable, oldValue, newValue ) -> setDisable( newValue.booleanValue() ) );
  }

  /**
   * Toggles the state of the {@code CheckBox}.
   */
  @Override
  public void fire() {
    if (!isDisabled()) {

      if (isAllowIndeterminate()) {
        if (!isIndeterminate()) {
          setSelected( !isSelected() );
        }
        fireEvent( new ActionEvent() );
      }
    }
  }
}
