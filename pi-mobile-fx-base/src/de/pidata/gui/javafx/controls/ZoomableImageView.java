/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

/**
 * implementation according to https://stackoverflow.com/questions/16680295/javafx-correct-scaling
 * needs a specific structure around the ImageView to get zoom/scroll working
 *
 * @author dsc
 */
public class ZoomableImageView extends ScrollPane {

  @FXML private ImageView fxImageView;
  @FXML private StackPane zoomPane;

  public ZoomableImageView() {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader( getClass().getResource( "/zoomableImageView.fxml" ) );
      fxmlLoader.setRoot( this );
      fxmlLoader.setController( this );
      fxmlLoader.load();
    }
    catch ( Exception exception) {
      throw new RuntimeException( exception );
    }
  }

  public ImageView getFxImageView() {
    return fxImageView;
  }

  public void setFxImageView( ImageView fxImageView ) {
    this.fxImageView = fxImageView;
  }

  public StackPane getZoomPane() {
    return zoomPane;
  }

  public void setZoomPane( StackPane zoomPane ) {
    this.zoomPane = zoomPane;
  }
}
