/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class ProgressInfo extends VBox{

  @FXML private ProgressBar progressBar;
  @FXML private Label progressText;
  @FXML private HBox errorBox;
  @FXML private TextArea errorText;
  @FXML private Button errorButton;

  public ProgressInfo() {
    try {
      FXMLLoader fxmlLoader = new FXMLLoader( getClass().getResource( "/progressInfo.fxml" ) );
      fxmlLoader.setRoot( this );
      fxmlLoader.setController( this );
      fxmlLoader.load();
    }
    catch ( Exception exception) {
      throw new RuntimeException( exception );
    }
  }

  public ProgressBar getProgressBar(){
    return progressBar;
  }

  public void setProgressBar(ProgressBar newProgressBar){
    progressBar = newProgressBar;
  }

  public Label getProgressText(){
    return progressText;
  }

  public void setProgressText( String message){
    progressText.setText(message);
  }

  public HBox getErrorBox(){
    return errorBox;
  }

  public void setErrorBox(HBox newErrorBox){
    errorBox = newErrorBox;
  }

  public TextArea getErrorText(){
    return errorText;
  }

  public void setErrorText(String message){
    errorText.setText( message );
  }

  public Button getErrorButton(){
    return errorButton;
  }

  public void setErrorButton (Button newErrorButton){
    errorButton = newErrorButton;
  }
}
