package de.pidata.gui.javafx.controls;

import de.pidata.log.Logger;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.web.HTMLEditor;

public class PIHtmlEditor extends HTMLEditor {

  private ToolBar topToolBar;
  private ToolBar bottomToolbar;

  public PIHtmlEditor() {
    super();

    Node topToolbarNode = lookup( ".top-toolbar" );
    topToolBar = (ToolBar) topToolbarNode;

    topToolBar.getItems().addListener( (ListChangeListener<Node>) item -> {
      try {
        Node currentNode = item.getList().get( item.getList().size() - 1 );
        checkAndHideNode( currentNode );
      }
      catch (Exception ex) {
        Logger.error("HTMLEditor Top Toolbar", ex);
      }
    } );

    Node bottomToolbarNode = lookup( ".bottom-toolbar" );
    bottomToolbar = (ToolBar) bottomToolbarNode;
    ToolBar finalTopToolBar = topToolBar;
    bottomToolbar.getItems().addListener( (ListChangeListener<Node>) item -> {

      try {
        Node currentNode = item.getList().get( item.getList().size() - 1 );
        if (currentNode instanceof ToggleButton) {

          String styleClassName = currentNode.getStyleClass().get( 1 );
          if (finalTopToolBar != null) {

            if ("html-editor-bold".equals( styleClassName )) {
              finalTopToolBar.getItems().add( currentNode );
            }
            if ("html-editor-italic".equals( styleClassName )) {
              finalTopToolBar.getItems().add( currentNode );
            }
            if ("html-editor-underline".equals( styleClassName )) {
              finalTopToolBar.getItems().add( currentNode );
            }
          }
        }
      }
      catch (Exception ex) {
        Logger.error( "HTMLEditor Bottom Toolbar", ex );
      }
    } );

    if (bottomToolbar != null) {
      bottomToolbar.setVisible( false );
      bottomToolbar.setManaged( false );
    }

  }

  private void checkAndHideNode( Node currentNode ) {

    if (currentNode instanceof Separator) {
      currentNode.setVisible( false );
      currentNode.setManaged( false );
    }

    if (currentNode instanceof ToggleButton) {
      String styleClassName = currentNode.getStyleClass().get( 1 );
      if ("html-editor-align-left".equals( styleClassName ) ||
          "html-editor-align-right".equals( styleClassName ) ||
          "html-editor-align-center".equals( styleClassName ) ||
          "html-editor-align-justify".equals( styleClassName )) {
        currentNode.setVisible( false );
        currentNode.setManaged( false );
      }
    }

    if (currentNode instanceof Button) {
      String styleClassName = currentNode.getStyleClass().get( 1 );
      if ("html-editor-outdent".equals( styleClassName ) ||
          "html-editor-indent".equals( styleClassName )) {
        currentNode.setVisible( false );
        currentNode.setManaged( false );
      }
    }

    if (currentNode instanceof ColorPicker) {
      if ("html-editor-background".equals( currentNode.getStyleClass().get( 2 ) )) {
        currentNode.setVisible( false );
        currentNode.setManaged( false );
      }
    }
  }

}
