package de.pidata.gui.javafx.controls;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.control.TextArea;


public class TableCellTextArea extends TextArea {

  public TableCellTextArea() {
    super();
    setDisable( false );
    setEditable( false );
    setWrapText( true );
    getStylesheets().add( "/css/TableCellTextArea.css" );

    // Make the TextAreas height fit the text height. This will take care of the wrapped text.
    sceneProperty().addListener( ( observableNewScene, oldScene, newScene ) -> {
      if (newScene != null) {
        applyCss();
        Node text = lookup( ".text" );

        if (text != null) {
          prefHeightProperty().bind( Bindings.createDoubleBinding( () -> {
            return Double.valueOf( getFont().getSize() + text.getBoundsInLocal().getHeight() );
          }, text.boundsInLocalProperty() ) );

          text.boundsInLocalProperty().addListener( ( observableBoundsAfter, boundsBefore, boundsAfter ) -> {
            Platform.runLater( () -> requestLayout() );
          } );
        }

      }
    } );

  }

}
