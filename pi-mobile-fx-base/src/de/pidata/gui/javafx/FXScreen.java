package de.pidata.gui.javafx;

import de.pidata.gui.component.base.Rect;
import de.pidata.gui.component.base.Screen;
import de.pidata.gui.event.Dialog;

public class FXScreen implements Screen {

  private FXDialogInterface focusDialog;

  @Override
  public Rect getScreenBounds() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns true if this screen works with a single window. If true
   * PaintManager has to simulate Windows and Popups
   *
   * @return true if this screen works with a single window
   */
  @Override
  public boolean isSingleWindow() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setScreenSize( int width, int height ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public Dialog getFocusDialog() {
    return focusDialog;
  }

  public void setFocusDialog( FXDialogInterface fxDialog ) {
    this.focusDialog = fxDialog;
  }
}
