/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.QuestionBoxParams;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.gui.event.Dialog;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.robot.Robot;
import javafx.stage.*;
import javafx.stage.Window;


/**
 * Created by pru on 25.10.14.
 */
public class FXMessageBox {

  public FXMessageBox( QuestionBoxParams params, Window owner, final DialogController parentDlgCtrl ) {
    final Stage stage = new Stage();

    stage.setTitle( params.getTitle() );
    stage.initOwner( owner );
    stage.initStyle( StageStyle.UTILITY );
    stage.initModality( Modality.WINDOW_MODAL );

    stage.setOnCloseRequest( new EventHandler<WindowEvent>() {
      @Override
      public void handle( WindowEvent event ) {
        stage.close();
        QuestionBoxResult resultList = new QuestionBoxResult( QuestionBoxResult.CANCEL, null, params.getTitle() );
        parentDlgCtrl.childDialogClosed( false, resultList );
      }
    } );

    final Label messageLabel = new Label( params.getMessage() );
    messageLabel.setWrapText( true );
    messageLabel.setMinHeight( TextField.USE_PREF_SIZE );

    final HBox btnLayout = new HBox( 10 );
    btnLayout.setAlignment( Pos.CENTER_RIGHT );
    btnLayout.setStyle( "-fx-background-color: azure; -fx-padding: 10;" );

    final Button submitButton = new Button( params.getLabelYesOk() );
    submitButton.setDefaultButton( true );
    submitButton.setOnAction( t -> {
      stage.close();
      QuestionBoxResult resultList = new QuestionBoxResult( QuestionBoxResult.YES_OK, null, params.getTitle() );
      parentDlgCtrl.childDialogClosed( true, resultList );
    } );
    btnLayout.getChildren().add( submitButton );

    if (params.getLabelNo() != null) {
      final Button noButton = new Button( params.getLabelNo() );
      noButton.setOnAction( t -> {
        stage.close();
        QuestionBoxResult resultList = new QuestionBoxResult( QuestionBoxResult.NO, null, params.getTitle() );
        parentDlgCtrl.childDialogClosed( false, resultList );
      } );
      btnLayout.getChildren().add( noButton );
    }

    if (params.getLabelCancel() != null) {
      final Button cancelButton = new Button( params.getLabelCancel() );
      cancelButton.setCancelButton( true );
      cancelButton.setOnAction( t -> {
        stage.close();
        QuestionBoxResult resultList = new QuestionBoxResult( QuestionBoxResult.CANCEL, null, params.getTitle() );
        parentDlgCtrl.childDialogClosed( false, resultList );
      } );
      btnLayout.getChildren().add( cancelButton );
    }

    final VBox layout = new VBox( 10 );
    layout.setAlignment( Pos.CENTER_RIGHT );
    layout.setStyle( "-fx-background-color: azure; -fx-padding: 20;");
    layout.getChildren().setAll( messageLabel, btnLayout );

    stage.setScene( new Scene( new Group(layout) ) );
    stage.setResizable( false );


    double x;
    double y;

    if (parentDlgCtrl != null) {
      //--- Place window near owner
      Dialog fxParent = parentDlgCtrl.getDialogComp();
      Scene scene = ((FXUIContainer) fxParent).getScene();
      x = scene.getWindow().getX() + scene.getWidth() / 2;
      y = scene.getWindow().getY() + scene.getHeight() / 2;
    }
    else {
      Robot robot = new Robot();
      x = robot.getMousePosition().getX() + 10;
      y = robot.getMousePosition().getY() - 30;
    }
    if (x < 0) x = 0.0;
    if (y < 0) y = 0.0;

    stage.setX( x );
    stage.setY( y );

    stage.show();
    stage.requestFocus();
    stage.toFront();
    // force top
    stage.setAlwaysOnTop( true );
  }
}
