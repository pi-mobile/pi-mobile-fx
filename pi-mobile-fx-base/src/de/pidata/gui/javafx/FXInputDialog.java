/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.QuestionBoxParams;
import de.pidata.gui.controller.base.QuestionBoxResult;
import de.pidata.service.base.AbstractParameterList;
import javafx.geometry.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.robot.Robot;
import javafx.stage.*;
import javafx.stage.Window;

import java.awt.*;

/**
 * Created by pru on 01.06.16.
 */
public class FXInputDialog {

  public FXInputDialog( QuestionBoxParams params, Window owner, final DialogController parentDlgCtrl ) {
    final Stage dialog = new Stage();

    dialog.setTitle( params.getTitle() );
    dialog.initOwner( owner );
    dialog.initStyle( StageStyle.UTILITY );
    dialog.initModality( Modality.WINDOW_MODAL );

    //--- Place window near mouse pointer
    javafx.scene.robot.Robot robot = new Robot();
    double x = robot.getMousePosition().getX() + 10;
    double y = robot.getMousePosition().getY() - 30;

    dialog.setX( x );
    dialog.setY( y );

    dialog.setOnCloseRequest( event -> {
      dialog.close();
      parentDlgCtrl.childDialogClosed( false, AbstractParameterList.EMPTY );
    } );

    Label label = new Label( params.getMessage() );
    label.setWrapText( true );

    final TextField textField = new TextField();
    textField.setMinHeight( TextField.USE_PREF_SIZE );
    textField.setText( params.getDefaultValue() );

    final Button submitButton = new Button( params.getLabelYesOk() );
    submitButton.setDefaultButton( true );
    submitButton.setOnAction( t -> {
      dialog.close();
      QuestionBoxResult resultList = new QuestionBoxResult( QuestionBoxResult.YES_OK, textField.getText(), params.getTitle() );
      parentDlgCtrl.childDialogClosed( true, resultList );
    } );

    final Button cancelButton = new Button( params.getLabelCancel() );
    cancelButton.setCancelButton( true );
    cancelButton.setOnAction( t -> {
      dialog.close();
      QuestionBoxResult resultList = new QuestionBoxResult( QuestionBoxResult.CANCEL, null, params.getTitle() );
      parentDlgCtrl.childDialogClosed( false, resultList );
    } );

    final HBox btnLayout = new HBox( 10 );
    btnLayout.setAlignment( Pos.CENTER_RIGHT );
    btnLayout.setStyle( "-fx-background-color: azure; -fx-padding: 10;" );
    btnLayout.getChildren().setAll( submitButton, cancelButton );

    final VBox layout = new VBox( 10 );
    layout.setAlignment( Pos.CENTER_LEFT );
    layout.setStyle( "-fx-background-color: azure; -fx-padding: 20;" );
    layout.getChildren().setAll( label, textField, btnLayout );

    dialog.setScene( new Scene( new Group(layout) ) );
    dialog.show();
    dialog.requestFocus();
    dialog.toFront();
    // force top
    dialog.setAlwaysOnTop( true );
  }
}
