/*
 * This file is part of PI-Mobile FX platform (https://gitlab.com/pi-mobile/pi-mobile-fx).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.javafx;

import de.pidata.gui.event.InputManager;
import de.pidata.qnames.QName;
import javafx.scene.input.KeyCode;
import java.util.HashMap;
import java.util.Map;

public class FXInputManager {

  /**
   * Maps JavaFX KeyCodes to pimobile QNames
   */
  private static Map<KeyCode, QName> keyMap = new HashMap<>();
  static {
    keyMap.put(KeyCode.ENTER, InputManager.CMD_ENTER );
    keyMap.put(KeyCode.BACK_SPACE, InputManager.CMD_BACK_SPACE );
    keyMap.put(KeyCode.TAB, InputManager.CMD_TAB );
    keyMap.put(KeyCode.CANCEL, InputManager.CMD_CANCEL );
    keyMap.put(KeyCode.CLEAR, InputManager.CMD_CLEAR );
    keyMap.put(KeyCode.SHIFT, InputManager.CMD_SHIFT );
    keyMap.put(KeyCode.CONTROL, InputManager.CMD_CONTROL );
    keyMap.put(KeyCode.ALT, InputManager.CMD_ALT );
    keyMap.put(KeyCode.PAUSE, InputManager.CMD_PAUSE );
    keyMap.put(KeyCode.CAPS, InputManager.CMD_CAPS );
    keyMap.put(KeyCode.ESCAPE, InputManager.CMD_ESCAPE );
    keyMap.put(KeyCode.SPACE, InputManager.CMD_SPACE );
    keyMap.put(KeyCode.PAGE_UP, InputManager.CMD_PAGE_UP );
    keyMap.put(KeyCode.PAGE_DOWN, InputManager.CMD_PAGE_DOWN );
    keyMap.put(KeyCode.END, InputManager.CMD_END );
    keyMap.put(KeyCode.HOME, InputManager.CMD_HOME );
    keyMap.put(KeyCode.LEFT, InputManager.CMD_LEFT );
    keyMap.put(KeyCode.UP, InputManager.CMD_UP );
    keyMap.put(KeyCode.RIGHT, InputManager.CMD_RIGHT );
    keyMap.put(KeyCode.DOWN, InputManager.CMD_DOWN );
    keyMap.put(KeyCode.COMMA, InputManager.CMD_COMMA );
    keyMap.put(KeyCode.MINUS, InputManager.CMD_MINUS );
    keyMap.put(KeyCode.PERIOD, InputManager.CMD_PERIOD );
    keyMap.put(KeyCode.SLASH, InputManager.CMD_SLASH );
    keyMap.put(KeyCode.DIGIT0, InputManager.CMD_DIGIT0 );
    keyMap.put(KeyCode.DIGIT1, InputManager.CMD_DIGIT1 );
    keyMap.put(KeyCode.DIGIT2, InputManager.CMD_DIGIT2 );
    keyMap.put(KeyCode.DIGIT3, InputManager.CMD_DIGIT3 );
    keyMap.put(KeyCode.DIGIT4, InputManager.CMD_DIGIT4 );
    keyMap.put(KeyCode.DIGIT5, InputManager.CMD_DIGIT5 );
    keyMap.put(KeyCode.DIGIT6, InputManager.CMD_DIGIT6 );
    keyMap.put(KeyCode.DIGIT7, InputManager.CMD_DIGIT7 );
    keyMap.put(KeyCode.DIGIT8, InputManager.CMD_DIGIT8 );
    keyMap.put(KeyCode.DIGIT9, InputManager.CMD_DIGIT9 );
    keyMap.put(KeyCode.SEMICOLON, InputManager.CMD_SEMICOLON );
    keyMap.put(KeyCode.EQUALS, InputManager.CMD_EQUALS );
    keyMap.put(KeyCode.A, InputManager.CMD_A );
    keyMap.put(KeyCode.B, InputManager.CMD_B );
    keyMap.put(KeyCode.C, InputManager.CMD_C );
    keyMap.put(KeyCode.D, InputManager.CMD_D );
    keyMap.put(KeyCode.E, InputManager.CMD_E );
    keyMap.put(KeyCode.F, InputManager.CMD_F );
    keyMap.put(KeyCode.G, InputManager.CMD_G );
    keyMap.put(KeyCode.H, InputManager.CMD_H );
    keyMap.put(KeyCode.I, InputManager.CMD_I );
    keyMap.put(KeyCode.J, InputManager.CMD_J );
    keyMap.put(KeyCode.K, InputManager.CMD_K );
    keyMap.put(KeyCode.L, InputManager.CMD_L );
    keyMap.put(KeyCode.M, InputManager.CMD_M );
    keyMap.put(KeyCode.N, InputManager.CMD_N );
    keyMap.put(KeyCode.O, InputManager.CMD_O );
    keyMap.put(KeyCode.P, InputManager.CMD_P );
    keyMap.put(KeyCode.Q, InputManager.CMD_Q );
    keyMap.put(KeyCode.R, InputManager.CMD_R );
    keyMap.put(KeyCode.S, InputManager.CMD_S );
    keyMap.put(KeyCode.T, InputManager.CMD_T );
    keyMap.put(KeyCode.U, InputManager.CMD_U );
    keyMap.put(KeyCode.V, InputManager.CMD_V );
    keyMap.put(KeyCode.W, InputManager.CMD_W );
    keyMap.put(KeyCode.X, InputManager.CMD_X );
    keyMap.put(KeyCode.Y, InputManager.CMD_Y );
    keyMap.put(KeyCode.Z, InputManager.CMD_Z );
    keyMap.put(KeyCode.OPEN_BRACKET, InputManager.CMD_OPEN_BRACKET );
    keyMap.put(KeyCode.BACK_SLASH, InputManager.CMD_BACK_SLASH );
    keyMap.put(KeyCode.CLOSE_BRACKET, InputManager.CMD_CLOSE_BRACKET );
    keyMap.put(KeyCode.NUMPAD0, InputManager.CMD_NUMPAD0 );
    keyMap.put(KeyCode.NUMPAD1, InputManager.CMD_NUMPAD1 );
    keyMap.put(KeyCode.NUMPAD2, InputManager.CMD_NUMPAD2 );
    keyMap.put(KeyCode.NUMPAD3, InputManager.CMD_NUMPAD3 );
    keyMap.put(KeyCode.NUMPAD4, InputManager.CMD_NUMPAD4 );
    keyMap.put(KeyCode.NUMPAD5, InputManager.CMD_NUMPAD5 );
    keyMap.put(KeyCode.NUMPAD6, InputManager.CMD_NUMPAD6 );
    keyMap.put(KeyCode.NUMPAD7, InputManager.CMD_NUMPAD7 );
    keyMap.put(KeyCode.NUMPAD8, InputManager.CMD_NUMPAD8 );
    keyMap.put(KeyCode.NUMPAD9, InputManager.CMD_NUMPAD9 );
    keyMap.put(KeyCode.MULTIPLY, InputManager.CMD_MULTIPLY );
    keyMap.put(KeyCode.ADD, InputManager.CMD_ADD );
    keyMap.put(KeyCode.SEPARATOR, InputManager.CMD_SEPARATOR );
    keyMap.put(KeyCode.SUBTRACT, InputManager.CMD_SUBTRACT );
    keyMap.put(KeyCode.DECIMAL, InputManager.CMD_DECIMAL );
    keyMap.put(KeyCode.DIVIDE, InputManager.CMD_DIVIDE );
    keyMap.put(KeyCode.DELETE, InputManager.CMD_DELETE );
    keyMap.put(KeyCode.NUM_LOCK, InputManager.CMD_NUM_LOCK );
    keyMap.put(KeyCode.SCROLL_LOCK, InputManager.CMD_SCROLL_LOCK );
    keyMap.put(KeyCode.F1, InputManager.CMD_F1 );
    keyMap.put(KeyCode.F2, InputManager.CMD_F2 );
    keyMap.put(KeyCode.F3, InputManager.CMD_F3 );
    keyMap.put(KeyCode.F4, InputManager.CMD_F4 );
    keyMap.put(KeyCode.F5, InputManager.CMD_F5 );
    keyMap.put(KeyCode.F6, InputManager.CMD_F6 );
    keyMap.put(KeyCode.F7, InputManager.CMD_F7 );
    keyMap.put(KeyCode.F8, InputManager.CMD_F8 );
    keyMap.put(KeyCode.F9, InputManager.CMD_F9 );
    keyMap.put(KeyCode.F10, InputManager.CMD_F10 );
    keyMap.put(KeyCode.F11, InputManager.CMD_F11 );
    keyMap.put(KeyCode.F12, InputManager.CMD_F12 );
    keyMap.put(KeyCode.F13, InputManager.CMD_F13 );
    keyMap.put(KeyCode.F14, InputManager.CMD_F14 );
    keyMap.put(KeyCode.F15, InputManager.CMD_F15 );
    keyMap.put(KeyCode.F16, InputManager.CMD_F16 );
    keyMap.put(KeyCode.F17, InputManager.CMD_F17 );
    keyMap.put(KeyCode.F18, InputManager.CMD_F18 );
    keyMap.put(KeyCode.F19, InputManager.CMD_F19 );
    keyMap.put(KeyCode.F20, InputManager.CMD_F20 );
    keyMap.put(KeyCode.F21, InputManager.CMD_F21 );
    keyMap.put(KeyCode.F22, InputManager.CMD_F22 );
    keyMap.put(KeyCode.F23, InputManager.CMD_F23 );
    keyMap.put(KeyCode.F24, InputManager.CMD_F24 );
    keyMap.put(KeyCode.PRINTSCREEN, InputManager.CMD_PRINTSCREEN );
    keyMap.put(KeyCode.INSERT, InputManager.CMD_INSERT );
    keyMap.put(KeyCode.HELP, InputManager.CMD_HELP );
    keyMap.put(KeyCode.META, InputManager.CMD_META );
    keyMap.put(KeyCode.BACK_QUOTE, InputManager.CMD_BACK_QUOTE );
    keyMap.put(KeyCode.QUOTE, InputManager.CMD_QUOTE );
    keyMap.put(KeyCode.KP_UP, InputManager.CMD_KP_UP );
    keyMap.put(KeyCode.KP_DOWN, InputManager.CMD_KP_DOWN );
    keyMap.put(KeyCode.KP_LEFT, InputManager.CMD_KP_LEFT );
    keyMap.put(KeyCode.KP_RIGHT, InputManager.CMD_KP_RIGHT );
    keyMap.put(KeyCode.DEAD_GRAVE, InputManager.CMD_DEAD_GRAVE );
    keyMap.put(KeyCode.DEAD_ACUTE, InputManager.CMD_DEAD_ACUTE );
    keyMap.put(KeyCode.DEAD_CIRCUMFLEX, InputManager.CMD_DEAD_CIRCUMFLEX );
    keyMap.put(KeyCode.DEAD_TILDE, InputManager.CMD_DEAD_TILDE );
    keyMap.put(KeyCode.DEAD_MACRON, InputManager.CMD_DEAD_MACRON );
    keyMap.put(KeyCode.DEAD_BREVE, InputManager.CMD_DEAD_BREVE );
    keyMap.put(KeyCode.DEAD_ABOVEDOT, InputManager.CMD_DEAD_ABOVEDOT );
    keyMap.put(KeyCode.DEAD_DIAERESIS, InputManager.CMD_DEAD_DIAERESIS );
    keyMap.put(KeyCode.DEAD_ABOVERING, InputManager.CMD_DEAD_ABOVERING );
    keyMap.put(KeyCode.DEAD_DOUBLEACUTE, InputManager.CMD_DEAD_DOUBLEACUTE );
    keyMap.put(KeyCode.DEAD_CARON, InputManager.CMD_DEAD_CARON );
    keyMap.put(KeyCode.DEAD_CEDILLA, InputManager.CMD_DEAD_CEDILLA );
    keyMap.put(KeyCode.DEAD_OGONEK, InputManager.CMD_DEAD_OGONEK );
    keyMap.put(KeyCode.DEAD_IOTA, InputManager.CMD_DEAD_IOTA );
    keyMap.put(KeyCode.DEAD_VOICED_SOUND, InputManager.CMD_DEAD_VOICED_SOUND );
    keyMap.put(KeyCode.DEAD_SEMIVOICED_SOUND, InputManager.CMD_DEAD_SEMIVOICED_SOUND );
    keyMap.put(KeyCode.AMPERSAND, InputManager.CMD_AMPERSAND );
    keyMap.put(KeyCode.ASTERISK, InputManager.CMD_ASTERISK );
    keyMap.put(KeyCode.QUOTEDBL, InputManager.CMD_QUOTEDBL );
    keyMap.put(KeyCode.LESS, InputManager.CMD_LESS );
    keyMap.put(KeyCode.GREATER, InputManager.CMD_GREATER );
    keyMap.put(KeyCode.BRACELEFT, InputManager.CMD_BRACELEFT );
    keyMap.put(KeyCode.BRACERIGHT, InputManager.CMD_BRACERIGHT );
    keyMap.put(KeyCode.AT, InputManager.CMD_AT );
    keyMap.put(KeyCode.COLON, InputManager.CMD_COLON );
    keyMap.put(KeyCode.CIRCUMFLEX, InputManager.CMD_CIRCUMFLEX );
    keyMap.put(KeyCode.DOLLAR, InputManager.CMD_DOLLAR );
    keyMap.put(KeyCode.EURO_SIGN, InputManager.CMD_EURO_SIGN );
    keyMap.put(KeyCode.EXCLAMATION_MARK, InputManager.CMD_EXCLAMATION_MARK );
    keyMap.put(KeyCode.INVERTED_EXCLAMATION_MARK, InputManager.CMD_INVERTED_EXCLAMATION_MARK );
    keyMap.put(KeyCode.LEFT_PARENTHESIS, InputManager.CMD_LEFT_PARENTHESIS );
    keyMap.put(KeyCode.NUMBER_SIGN, InputManager.CMD_NUMBER_SIGN );
    keyMap.put(KeyCode.PLUS, InputManager.CMD_PLUS );
    keyMap.put(KeyCode.RIGHT_PARENTHESIS, InputManager.CMD_RIGHT_PARENTHESIS );
    keyMap.put(KeyCode.UNDERSCORE, InputManager.CMD_UNDERSCORE );
    keyMap.put(KeyCode.WINDOWS, InputManager.CMD_WINDOWS );
    keyMap.put(KeyCode.CONTEXT_MENU, InputManager.CMD_CONTEXT_MENU );
    keyMap.put(KeyCode.FINAL, InputManager.CMD_FINAL );
    keyMap.put(KeyCode.CONVERT, InputManager.CMD_CONVERT );
    keyMap.put(KeyCode.NONCONVERT, InputManager.CMD_NONCONVERT );
    keyMap.put(KeyCode.ACCEPT, InputManager.CMD_ACCEPT );
    keyMap.put(KeyCode.MODECHANGE, InputManager.CMD_MODECHANGE );
    keyMap.put(KeyCode.KANA, InputManager.CMD_KANA );
    keyMap.put(KeyCode.KANJI, InputManager.CMD_KANJI );
    keyMap.put(KeyCode.ALPHANUMERIC, InputManager.CMD_ALPHANUMERIC );
    keyMap.put(KeyCode.KATAKANA, InputManager.CMD_KATAKANA );
    keyMap.put(KeyCode.HIRAGANA, InputManager.CMD_HIRAGANA );
    keyMap.put(KeyCode.FULL_WIDTH, InputManager.CMD_FULL_WIDTH );
    keyMap.put(KeyCode.HALF_WIDTH, InputManager.CMD_HALF_WIDTH );
    keyMap.put(KeyCode.ROMAN_CHARACTERS, InputManager.CMD_ROMAN_CHARACTERS );
    keyMap.put(KeyCode.ALL_CANDIDATES, InputManager.CMD_ALL_CANDIDATES );
    keyMap.put(KeyCode.PREVIOUS_CANDIDATE, InputManager.CMD_PREVIOUS_CANDIDATE );
    keyMap.put(KeyCode.CODE_INPUT, InputManager.CMD_CODE_INPUT );
    keyMap.put(KeyCode.JAPANESE_KATAKANA, InputManager.CMD_JAPANESE_KATAKANA );
    keyMap.put(KeyCode.JAPANESE_HIRAGANA, InputManager.CMD_JAPANESE_HIRAGANA );
    keyMap.put(KeyCode.JAPANESE_ROMAN, InputManager.CMD_JAPANESE_ROMAN );
    keyMap.put(KeyCode.KANA_LOCK, InputManager.CMD_KANA_LOCK );
    keyMap.put(KeyCode.INPUT_METHOD_ON_OFF, InputManager.CMD_INPUT_METHOD_ON_OFF );
    keyMap.put(KeyCode.CUT, InputManager.CMD_CUT );
    keyMap.put(KeyCode.COPY, InputManager.CMD_COPY );
    keyMap.put(KeyCode.PASTE, InputManager.CMD_PASTE );
    keyMap.put(KeyCode.UNDO, InputManager.CMD_UNDO );
    keyMap.put(KeyCode.AGAIN, InputManager.CMD_AGAIN );
    keyMap.put(KeyCode.FIND, InputManager.CMD_FIND );
    keyMap.put(KeyCode.PROPS, InputManager.CMD_PROPS );
    keyMap.put(KeyCode.STOP, InputManager.CMD_STOP );
    keyMap.put(KeyCode.COMPOSE, InputManager.CMD_COMPOSE );
    keyMap.put(KeyCode.ALT_GRAPH, InputManager.CMD_ALT_GRAPH );
    keyMap.put(KeyCode.BEGIN, InputManager.CMD_BEGIN );
    keyMap.put(KeyCode.UNDEFINED, InputManager.CMD_UNDEFINED );
    keyMap.put(KeyCode.SOFTKEY_0, InputManager.CMD_SOFTKEY_0 );
    keyMap.put(KeyCode.SOFTKEY_1, InputManager.CMD_SOFTKEY_1 );
    keyMap.put(KeyCode.SOFTKEY_2, InputManager.CMD_SOFTKEY_2 );
    keyMap.put(KeyCode.SOFTKEY_3, InputManager.CMD_SOFTKEY_3 );
    keyMap.put(KeyCode.SOFTKEY_4, InputManager.CMD_SOFTKEY_4 );
    keyMap.put(KeyCode.SOFTKEY_5, InputManager.CMD_SOFTKEY_5 );
    keyMap.put(KeyCode.SOFTKEY_6, InputManager.CMD_SOFTKEY_6 );
    keyMap.put(KeyCode.SOFTKEY_7, InputManager.CMD_SOFTKEY_7 );
    keyMap.put(KeyCode.SOFTKEY_8, InputManager.CMD_SOFTKEY_8 );
    keyMap.put(KeyCode.SOFTKEY_9, InputManager.CMD_SOFTKEY_9 );
    keyMap.put(KeyCode.GAME_A, InputManager.CMD_GAME_A );
    keyMap.put(KeyCode.GAME_B, InputManager.CMD_GAME_B );
    keyMap.put(KeyCode.GAME_C, InputManager.CMD_GAME_C );
    keyMap.put(KeyCode.GAME_D, InputManager.CMD_GAME_D );
    keyMap.put(KeyCode.STAR, InputManager.CMD_STAR );
    keyMap.put(KeyCode.POUND, InputManager.CMD_POUND );
    keyMap.put(KeyCode.POWER, InputManager.CMD_POWER );
    keyMap.put(KeyCode.INFO, InputManager.CMD_INFO );
    keyMap.put(KeyCode.COLORED_KEY_0, InputManager.CMD_COLORED_KEY_0 );
    keyMap.put(KeyCode.COLORED_KEY_1, InputManager.CMD_COLORED_KEY_1 );
    keyMap.put(KeyCode.COLORED_KEY_2, InputManager.CMD_COLORED_KEY_2 );
    keyMap.put(KeyCode.COLORED_KEY_3, InputManager.CMD_COLORED_KEY_3 );
    keyMap.put(KeyCode.EJECT_TOGGLE, InputManager.CMD_EJECT_TOGGLE );
    keyMap.put(KeyCode.PLAY, InputManager.CMD_PLAY );
    keyMap.put(KeyCode.RECORD, InputManager.CMD_RECORD );
    keyMap.put(KeyCode.FAST_FWD, InputManager.CMD_FAST_FWD );
    keyMap.put(KeyCode.REWIND, InputManager.CMD_REWIND );
    keyMap.put(KeyCode.TRACK_PREV, InputManager.CMD_TRACK_PREV );
    keyMap.put(KeyCode.TRACK_NEXT, InputManager.CMD_TRACK_NEXT );
    keyMap.put(KeyCode.CHANNEL_UP, InputManager.CMD_CHANNEL_UP );
    keyMap.put(KeyCode.CHANNEL_DOWN, InputManager.CMD_CHANNEL_DOWN );
    keyMap.put(KeyCode.VOLUME_UP, InputManager.CMD_VOLUME_UP );
    keyMap.put(KeyCode.VOLUME_DOWN, InputManager.CMD_VOLUME_DOWN );
    keyMap.put(KeyCode.MUTE, InputManager.CMD_MUTE );
    keyMap.put(KeyCode.COMMAND, InputManager.CMD_COMMAND );
    keyMap.put(KeyCode.SHORTCUT, InputManager.CMD_SHORTCUT );
  }


  public static QName getInputCommand( KeyCode keyCode ) {
    if ( keyMap.containsKey( keyCode )) {
      return keyMap.get( keyCode );
    }
    else {
     throw new IllegalArgumentException( "The Keycode '" + keyCode + "' has no InputManager command equivalent."  );
    }
  }

}
